package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Handlers.BillingManager;
import com.elixeer.maltapp.Handlers.PointsManager;
import com.elixeer.maltapp.Models.Promo;
import com.elixeer.maltapp.Models.TotalPoint;
import com.elixeer.maltapp.Models.User;
import com.googlesamples.billing.Purchase;
import com.googlesamples.billing.SkuDetails;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class PromotionsDetailsFragment extends Fragment {
    MainActivity parent;

    ImageView promoImageView;
    TextView titleTextView;
    TextView DescriptionNewTextView;
    TextView costTextView;
    Button buyPromotionBuy;
    BillingManager billingManager;
    Promo promo;
    int user_id;
    int beer_coins;
    User user;
    TotalPoint totalPoint;
    String logTag;

    //ArrayList<SkuDetails> products;
    //SkuDetails skuPremium;

    /*
    static ArrayList<String> productIdentifiers = new ArrayList<String>() {
        {
            add("elixeer.maltapp.premium");
        }
    };
    */

    public PromotionsDetailsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.promotionsDetailttitle);
                actionBar.show();
            }
        }

        View rootView = inflater.inflate(R.layout.fragment_promotion_details, container, false);
        Bundle arguments = getArguments();

        promoImageView = rootView.findViewById(R.id.PromotionsDetailImageTextView);
        titleTextView = rootView.findViewById(R.id.PromotionsDetailNameTextView);
        DescriptionNewTextView = rootView.findViewById(R.id.PromotionsDetailDescriptionTextView);
        costTextView = rootView.findViewById(R.id.PromotionsDetailCostTextView);
        buyPromotionBuy = rootView.findViewById(R.id.promotionsDetailsBuyPromotionButton);

        //getProducts();

        if (arguments != null) {
            promo = (Promo) arguments.getSerializable("PROMOS");
            user_id = arguments.getInt("USER_ID");
            beer_coins = arguments.getInt("BEER_COINS");

            if (promo != null) {
                if (promo.type == 2) {
                    //NEW PROMOTION
                    buyPromotionBuy.setVisibility(View.VISIBLE);
                } else if (promo.type == 3) {
                    //MY PROMOTION
                    buyPromotionBuy.setVisibility(View.GONE);
                } else {
                    //NO DEBERIA LLEGAR AQUI
                    Log.e(logTag, "Promoción desconocida: " + promo.type);
                }
                titleTextView.setText(promo.name);
                costTextView.setText(String.valueOf(promo.points).concat(" BeerCoins"));
                DescriptionNewTextView.setText(promo.description);

                ImageCache.getImage(promo.imageUrlMedium, getContext(), new ImageCache.OnResponseListener() {
                    @Override
                    public void onResponse(Bitmap bitmap, String error) {
                        promoImageView.setImageBitmap(bitmap);
                    }
                });
                buyPromotionBuy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (beer_coins >= promo.points) {
                            getPoints();
                        } else {
                            notEnoughtPoints();
                            //showAlertMsj(getString(R.string.insufficientPoints));
                        }
                        // parent.getIabHelper().flagEndAsync();
                        //parent.getIabHelper().launchPurchaseFlow(parent, skuPremium.getSku(), 10001, mPurchaseFinishedListener, "");
                    }
                });

            }
        }
        if (user_id > 0) {
            getUser(user_id);
        }

        billingManager = new BillingManager(getActivity()) {
            @Override
            public void onConsume(Purchase purchase) {
                Log.i(logTag, "CONSUMED " + purchase.getSku());
            }

            @Override
            public void onInventory(Purchase purchase) {
                Log.i(logTag, "ON INVENTORY " + purchase.getSku());
                // this.consume(purchase);
                final Purchase test2 = purchase;
                Runnable test = new Runnable() {
                    @Override
                    public void run() {
                        billingManager.consume(test2);
                    }
                };
                if (BillingManager.SKU_FIRSTPACK.equals(purchase.getSku())) {
                    beer_coins = getArguments().getInt("BEER_COINS") + ApiUrl.POINTS_PACKONE;
                    PointsManager.addPoints(user_id, ApiUrl.POINTS_PACKONE, ApiUrl.REASON_BUYBEERCOINS_1, getContext(),test);
                } else if (BillingManager.SKU_SECONDPACK.equals(purchase.getSku())){
                    beer_coins = getArguments().getInt("BEER_COINS") + ApiUrl.POINTS_PACKTWO;
                    PointsManager.addPoints(user_id, ApiUrl.POINTS_PACKTWO, ApiUrl.REASON_BUYBEERCOINS_2, getContext(),test);
                } else if (BillingManager.SKU_THIRDPACK.equals(purchase.getSku())) {
                    beer_coins = getArguments().getInt("BEER_COINS") + ApiUrl.POINTS_PACKTWO;
                    PointsManager.addPoints(user_id, ApiUrl.POINTS_PACKTHREE, ApiUrl.REASON_BUYBEERCOINS_3, getContext(),test);
                }
            }

            @Override
            public void onPurchase(Purchase purchase) {
                Log.i(logTag, "PURCHASED " + purchase.getSku());
                /*
                String orderId = purchase.getOrderId();
                if (orderId != null && orderId.length() > 1) {
                    orderId = orderId.substring(0, orderId.length() - 2);
                } else {
                    // Test purchase
                    orderId = purchase.getToken();
                }
                */
                final Purchase test2 = purchase;
                Runnable test = new Runnable() {
                    @Override
                    public void run() {
                        billingManager.consume(test2);
                    }
                };
                if (BillingManager.SKU_FIRSTPACK.equals(purchase.getSku())) {
                    beer_coins = getArguments().getInt("BEER_COINS") + ApiUrl.POINTS_PACKONE;
                    PointsManager.addPoints(user_id, ApiUrl.POINTS_PACKONE, ApiUrl.REASON_BUYBEERCOINS_1, getContext(),test);
                } else if (BillingManager.SKU_SECONDPACK.equals(purchase.getSku())){
                    beer_coins = getArguments().getInt("BEER_COINS") + ApiUrl.POINTS_PACKTWO;
                    PointsManager.addPoints(user_id, ApiUrl.POINTS_PACKTWO, ApiUrl.REASON_BUYBEERCOINS_2, getContext(),test);
                } else if (BillingManager.SKU_THIRDPACK.equals(purchase.getSku())) {
                    beer_coins = getArguments().getInt("BEER_COINS") + ApiUrl.POINTS_PACKTWO;
                    PointsManager.addPoints(user_id, ApiUrl.POINTS_PACKTHREE, ApiUrl.REASON_BUYBEERCOINS_3, getContext(),test);
                }

               //TODO refresh
            }
        };

        return rootView;
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    void buyBeercoinsPopup() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_buy_beercoins, null);

        TextView buyBeercoinsDescriptionFirstTextView = pop.findViewById(R.id.buyBeercoinsDescriptionFirstTextView);
        TextView buyBeercoinscostFirstTextView = pop.findViewById(R.id.buyBeercoinscostFirstTextView);
        Button shopFirstButton = pop.findViewById(R.id.shopFirstButton);

        TextView buyBeercoinsDescriptionSecondTextView = pop.findViewById(R.id.buyBeercoinsDescriptionSecondTextView);
        TextView buyBeercoinscostSecondTextView = pop.findViewById(R.id.buyBeercoinscostSecondTextView);
        Button shopSecondButton = pop.findViewById(R.id.shopSecondButton);

        TextView buyBeercoinsDescriptionThirdTextView = pop.findViewById(R.id.buyBeercoinsDescriptionThirdTextView);
        TextView buyBeercoinscostThirdTextView = pop.findViewById(R.id.buyBeercoinscostThirdTextView);
        Button shopThirdButton = pop.findViewById(R.id.shopThirdButton);

        SkuDetails skufirstPrice = billingManager.getProductBySku(BillingManager.SKU_FIRSTPACK);
        SkuDetails skusecondPrice = billingManager.getProductBySku(BillingManager.SKU_SECONDPACK);
        SkuDetails skuthirdPrice = billingManager.getProductBySku(BillingManager.SKU_THIRDPACK);

        if (skufirstPrice == null) {
            buyBeercoinsDescriptionFirstTextView.setText(R.string.unavailable_product);
            buyBeercoinscostFirstTextView.setText("");
        } else {
            buyBeercoinsDescriptionFirstTextView.setText(skufirstPrice.getTitle());
            buyBeercoinscostFirstTextView.setText(String.format(getString(R.string.btn_buy), skufirstPrice.getPrice()));
        }
        if (skusecondPrice == null) {
            buyBeercoinsDescriptionSecondTextView.setText(R.string.unavailable_product);
            buyBeercoinscostSecondTextView.setText("");
        } else {
            buyBeercoinsDescriptionSecondTextView.setText(skusecondPrice.getTitle());
            buyBeercoinscostSecondTextView.setText(String.format(getString(R.string.btn_buy), skusecondPrice.getPrice()));
        }
        if (skuthirdPrice == null) {
            buyBeercoinsDescriptionThirdTextView.setText(R.string.unavailable_product);
            buyBeercoinscostThirdTextView.setText("");
        } else {
            buyBeercoinsDescriptionThirdTextView.setText(skuthirdPrice.getTitle());
            buyBeercoinscostThirdTextView.setText(String.format(getString(R.string.btn_buy), skuthirdPrice.getPrice()));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(PromotionsDetailsFragment.this.getActivity())
                //.setTitle(getString(R.string.maltappNotice))
                .setView(pop)
                .setPositiveButton(R.string.cancel, null);
        final AlertDialog alert = builder.create();

        shopFirstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                billingManager.purchase(BillingManager.SKU_FIRSTPACK);
                //alert.dismiss();
            }
        });
        shopSecondButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                billingManager.purchase(BillingManager.SKU_SECONDPACK);
                //alert.dismiss();
            }
        });
        shopThirdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                billingManager.purchase(BillingManager.SKU_THIRDPACK);
                //alert.dismiss();
            }
        });

        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        alert.show();
    }

    private void getUser(int userId) {
        String url = ApiUrl.GET_USER + userId + ".json" + ApiUrl.ACCESS_TOKEN;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONObject jsonUser = jsonResponse.getJSONObject("user");
                            user = new User(jsonUser);
                            //getPoints();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getPoints() {

        String url = ApiUrl.POINTS + "&user_id=" + user_id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONObject jsonPoint = jsonResponse.getJSONObject("points");
                            totalPoint = new TotalPoint(jsonPoint);
                            if ((totalPoint.points_earned - totalPoint.points_used) > promo.points) {
                                confirmDialog();
                                /*
                            } else {
                               // notEnoughtPoints();
                                showAlertMsj(getString(R.string.insufficientPoints));
                                promo.points = totalPoint.points_earned - totalPoint.points_used;
                                costTextView.setText(String.valueOf(promo.points).concat(" BeerCoins"));*/
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void showAlertMsj (String msj) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_msj, null);
        TextView popMsjTextView = pop.findViewById(R.id.popMsjTextView);
        popMsjTextView.setText(msj);
        AlertDialog.Builder builder = new AlertDialog.Builder(PromotionsDetailsFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });
        alert.show();
    }

    void confirmDialog() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_promo, null);
        TextView popBuyPromoTextView = pop.findViewById(R.id.popBuyPromoTextView);
        Button popBuyPromoButton = pop.findViewById(R.id.popBuyPromoButton);

        popBuyPromoTextView.setText(String.format(getString(R.string.buy_promo_message), promo.name, String.valueOf(promo.points)));

        AlertDialog.Builder builder = new AlertDialog.Builder(PromotionsDetailsFragment.this.getActivity())
                .setView(pop)
                //.setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.close, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                alert.dismiss();
            }
        });
        popBuyPromoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buyPromo();
                alert.dismiss();
            }
        });
        alert.show();
    }

    void notEnoughtPoints() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_promo, null);
        TextView popBuyPromoTextView = pop.findViewById(R.id.popBuyPromoTextView);
        TextView custom_title_login_textView = pop.findViewById(R.id.custom_title_login_textView);
        Button popBuyPromoButton = pop.findViewById(R.id.popBuyPromoButton);

        custom_title_login_textView.setText(R.string.maltappNotice);
        popBuyPromoTextView.setText(getString(R.string.insufficientPoints));
        popBuyPromoButton.setText(getString(R.string.shopCervexxa2));

        AlertDialog.Builder builder = new AlertDialog.Builder(PromotionsDetailsFragment.this.getActivity())
                .setView(pop)
                //.setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.close, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                alert.dismiss();
            }
        });
        popBuyPromoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //buyPromo();
                buyBeercoinsPopup();
                alert.dismiss();
            }
        });
        alert.show();
    }

    void buyPromo() {
        String url = ApiUrl.PROMO_CREATE + "&user_id=" + user_id + "&promo_id=" + promo.id;
        new AsyncRequestString("POST", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            quitPoints();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(PromotionsDetailsFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    void quitPoints() {
        String url = ApiUrl.POINTS_CREATE + "&user_id=" + user_id + "&points=" + promo.points + "&reason=" + ApiUrl.REASON_BUYPROMOTION + "&movement=false";

        new AsyncRequestString("POST", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            showAlertMsj(getString(R.string.successfulBuy));
                            Fragment premiumFragment = new PremiumFragment();
                            Bundle arguments = new Bundle();
                            arguments.putInt("USER_ID", user_id);
                            arguments.putInt("BEER_COINS", totalPoint.points_earned - totalPoint.points_used - promo.points);
                            premiumFragment.setArguments(arguments);
                            parent.pushFragment(premiumFragment);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    /*
    void getProducts() {
        ArrayList<String> additionalSkuList = new ArrayList<>();
        additionalSkuList.addAll(productIdentifiers);
        if (parent.getIabHelper() != null) {
            parent.getIabHelper().flagEndAsync();
            parent.getIabHelper().queryInventoryAsync(true, additionalSkuList, mQueryFinishedListener);
        }
    }

    void checkPurchaseInfoAndAddPoints(Purchase info) {
        if (info.getSku().equals("elixeer.maltapp.premium")) {
            makePremium(info);
        }
    }

    void makePremium(final Purchase purchase) {
        String url = ApiUrl.GET_USER + user_id + ".json" + ApiUrl.ACCESS_TOKEN + "&premium=true";
        new AsyncRequestString("PUT", url) {
            @Override
            public void onResult(int responseCode, String response) {
                if (response == null) {
                    Log.w(getString(R.string.app_name), "WARNING: Response is null (" + responseCode + ")");
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {

                        } else {
                            JSONObject jsonObject = jsonResponse.optJSONObject("user");
                            User user = new User(jsonObject);
                            if (user.id != 0) {
                                parent.getIabHelper().consumeAsync(purchase, mConsumeFinishedListener);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }

    IabHelper.QueryInventoryFinishedListener mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            if (result.isFailure()) {
                // handle error
                return;
            }
            products = new ArrayList<>();
            for (String s : productIdentifiers) {
                products.add(inventory.getSkuDetails(s));
                if (inventory.hasPurchase(s)) {
                    checkPurchaseInfoAndAddPoints(inventory.getPurchase(s));
                }
            }
            for (int i = 0; i < products.size(); i++) {
                if (products.get(i).getSku().equals(productIdentifiers.get(0))) {
                    skuPremium = products.get(i);
                }
            }

            //if (skuPremium != null) {
            //    oneCredit.setText(skuOne.getPrice());
            //    oneCreditText.setText(skuOne.getTitle());
            //}

        }
    };

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase info) {
            if (result.isFailure()) {
                Log.d(getString(R.string.app_name), "Error purchasing: " + result);
                return;
            }
            checkPurchaseInfoAndAddPoints(info);
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            if (result.isSuccess()) {

            } else {
                // handle error
            }
        }
    };
    */
}
