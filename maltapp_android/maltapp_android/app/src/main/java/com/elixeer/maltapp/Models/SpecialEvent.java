package com.elixeer.maltapp.Models;

public class SpecialEvent {

    public int id;
    public String name;
    public String address;
    public int stand;
    public String city;
    public String state;
    public String country;
    public String thumb;
    public int type;

    public SpecialEvent(String name) {
        this.name = name;
    }

    public SpecialEvent(int id, String name, int stand, String thumb, int type) {
        this.id = id;
        this.name = name;
        this.stand = stand;
        this.thumb = thumb;
        this.type = type;
    }
}
