package com.elixeer.maltapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Models.BeerSmall;

import java.util.ArrayList;

/**
 * Created by alebrije2 on 1/26/16.
 *
 */
public class BeerImageCarousel extends HorizontalScrollView {

    private ArrayList<ImageView> ImagesViews;
    private ArrayList<String> ImagesPaths;
    private LinearLayout imagesContainer;

    public BeerImageCarousel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BeerImageCarousel(Context context) {
        super(context);
    }

    public void setBeers(final MainActivity parent, ArrayList<BeerSmall> beers) {
        setBeers(parent, beers, null, null, null);
    }

    public void setBeers(final MainActivity parent, ArrayList<BeerSmall> beers, final String analyticsEvent, final String analyticsAction, final String analyticsLabel) {
        if (imagesContainer == null) {
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            imagesContainer = new LinearLayout(getContext());
            imagesContainer.setLayoutParams(lp);
            this.addView(imagesContainer);
        }

        ImagesViews = new ArrayList<>();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(50, 100);
        layoutParams.setMargins(10, 10, 10, 10);
        for (final BeerSmall beer : beers) {
            final ImageView v = new ImageView(this.getContext());
            v.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Analytics event
                    ((AnalyticsApplication) parent.getApplication()).sendEvent(analyticsEvent, analyticsAction, analyticsLabel);

                    openBeerDetails(parent, beer.id);
                }
            });
            v.setImageBitmap(null);
            ImageCache.getImage(beer.thumb, parent, new ImageCache.OnResponseListener() {
                @Override
                public void onResponse(Bitmap bitmap, String error) {
                    v.setImageBitmap(bitmap);
                }
            });
            v.setLayoutParams(layoutParams);
            ImagesViews.add(v);
            imagesContainer.addView(v);
        }
    }

    private void openBeerDetails(MainActivity parent, int id) {
        Fragment beerFragment = new BeerFragment();
        Bundle arguments = new Bundle();
        arguments.putInt("BEER_ID", id);
        beerFragment.setArguments(arguments);
        parent.pushFragment(beerFragment);
    }

}
