package com.elixeer.maltapp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;

import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Adapters.SpecialCustomAdapter;
import com.elixeer.maltapp.Dynamics.InteractiveMapView;
import com.elixeer.maltapp.Models.BrewerySmall;
import com.elixeer.maltapp.Models.Event;
import com.elixeer.maltapp.Models.PlaceSmall;
import com.elixeer.maltapp.Models.SpecialEvent;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class SpecialMapFragment extends Fragment {
    static MainActivity parent;

    private double totalDist;
    private double angle;
    private SpecialCustomAdapter mAdapter;
    LinearLayout speacialEventMapImage;
    RelativeLayout navEventListLayout;
    RelativeLayout specialEventMapLinearLayout;
    RelativeLayout specialEventListLinearLayout;
    Button mapViewspecialeventButton;
    Button listViewspecialeventButton;
    InteractiveMapView interactiveMapView;
    ListView specialEventBreweries;
    SearchView specialEventMapSearch;
    Event event;
    String searchParams;
    ArrayList<BrewerySmall> breweries;
    ArrayList<PlaceSmall> places;
    boolean scrolling = false;

    public SpecialMapFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        event = (Event) getArguments().get("SPECIAL_EVENT");

        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(event.title);
                actionBar.show();
            }
        }

        View rootView = inflater.inflate(R.layout.fragment_special_map, container, false);

        breweries = new ArrayList<>();
        places = new ArrayList<>();
        JSONArray jsonArrayBreweries;
        JSONArray jsonArrayPlaces;
        try {
            jsonArrayBreweries = new JSONArray(getArguments().getString("SPECIAL_BREWERIES"));
            jsonArrayPlaces = new JSONArray(getArguments().getString("SPECIAL_PLACES"));
        } catch (JSONException e) {
            jsonArrayBreweries = new JSONArray();
            jsonArrayPlaces = new JSONArray();
            e.printStackTrace();
        }
        breweries.addAll(BrewerySmall.breweriesFromJSONArray(jsonArrayBreweries));
        places.addAll(PlaceSmall.placesFromJSONArray(jsonArrayPlaces));

        speacialEventMapImage = rootView.findViewById(R.id.speacialEventMapImage);
        specialEventBreweries = rootView.findViewById(R.id.specialEventBreweries);

        navEventListLayout = rootView.findViewById(R.id.navEventListLayout);
        specialEventMapLinearLayout = rootView.findViewById(R.id.specialEventMapLinearLayout);
        specialEventListLinearLayout = rootView.findViewById(R.id.specialEventListLinearLayout);
        mapViewspecialeventButton = rootView.findViewById(R.id.mapViewStoreListButton);
        listViewspecialeventButton = rootView.findViewById(R.id.listViewStoreListButton);

        specialEventMapSearch = rootView.findViewById(R.id.specialEventMapSearch);

        specialEventBreweries.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;
            int btn_initPosY;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                btn_initPosY = specialEventMapSearch.getScrollY();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(mLastFirstVisibleItem < firstVisibleItem) {
                    if(specialEventMapSearch.getVisibility() == View.VISIBLE) {
                        if (!scrolling) {
                            slideToTop(specialEventMapSearch);
                        }
                    }
                }
                if(mLastFirstVisibleItem > firstVisibleItem) {
                    if(specialEventMapSearch.getVisibility() == View.GONE) {
                        if (scrolling) {
                            slideToBottom(specialEventMapSearch);
                        }
                    }
                }
                mLastFirstVisibleItem = firstVisibleItem;
            }
        });

        searchParams = "";
        specialEventMapSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchParams = query;//.trim();
                if (searchParams.isEmpty()) {
                    loadData(breweries, places);
                } else {
                    ArrayList<BrewerySmall> brws = new ArrayList<>();
                    ArrayList<PlaceSmall> plcs = new ArrayList<>();
                    for (int i = 0; i < breweries.size(); i++) {
                        if (breweries.get(i).name.toLowerCase().contains(query.toLowerCase())){
                            brws.add(breweries.get(i));
                        }
                    }
                    for (int i = 0; i < places.size(); i++) {
                        if (places.get(i).name.toLowerCase().contains(query.toLowerCase())){
                            plcs.add(places.get(i));
                        }
                    }
                    loadData(brws, plcs);
                }
                specialEventMapSearch.clearFocus();
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                ArrayList<BrewerySmall> brws = new ArrayList<>();
                ArrayList<PlaceSmall> plcs = new ArrayList<>();
                for (int i = 0; i < breweries.size(); i++) {
                    if (breweries.get(i).name.toLowerCase().contains(newText.toLowerCase())){
                        brws.add(breweries.get(i));
                    }
                }
                for (int i = 0; i < places.size(); i++) {
                    if (places.get(i).name.toLowerCase().contains(newText.toLowerCase())){
                        plcs.add(places.get(i));
                    }
                }
                loadData(brws, plcs);
                return true;
            }
        });

        ImageCache.getImage(event.imageUrlMap, getContext(), new ImageCache.OnResponseListener() {
            @Override
            public void onResponse(Bitmap bitmap, String error) {
                interactiveMapView = new InteractiveMapView(getActivity(), bitmap);
                //if(interactiveMapView != null) {
                    speacialEventMapImage.addView(interactiveMapView);
                    setMathInfo(event.latitude2, event.longitude2, event.latitude3, event.longitude3);

                    if (breweries.size() > 0) {
                        interactiveMapView.setBreweries(breweries);
                    }
                    if (places.size() > 0) {
                        interactiveMapView.setPlaces(places);
                    }
                //}
            }
        });

        loadList();

        mapViewspecialeventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMap();
            }
        });

        listViewspecialeventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showList();
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == ApiUrl.LOCATION_REQUEST_CODE && (android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            getUserLocation();
        }
    }

    // To animate view slide out from top to bottom
    private void slideToBottom(final View view) {
        final TranslateAnimation animate = new TranslateAnimation(0, 0, -view.getHeight()*2f, 0);
        animate.setDuration(350);
        animate.setFillBefore(false);
        animate.setFillAfter(false);
        animate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                specialEventMapSearch.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                scrolling = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animate);
    }

    // To animate view slide out from bottom to top
    private void slideToTop(final View view) {
        TranslateAnimation animate =  new TranslateAnimation(0, 0, 0, -view.getHeight()*2f);
        animate.setDuration(350);
        animate.setFillBefore(false);
        animate.setFillAfter(true);
        animate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                scrolling = true;
                specialEventMapSearch.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animate);
    }

    private void loadList(){
        mAdapter = new SpecialCustomAdapter(getContext()) {
            @Override
            public void onMapButtonClick(int id, int type) {
                if(interactiveMapView != null) {
                    if (type == 1) {
                        for (int i = 0; i < interactiveMapView.breweries.size(); i++) {
                            if (interactiveMapView.breweries.get(i).id == id) {
                                interactiveMapView.setupBrewery(i);
                                break;
                            }
                        }
                    } else {
                        for (int i = 0; i < interactiveMapView.places.size(); i++) {
                            if (interactiveMapView.places.get(i).id == id) {
                                interactiveMapView.setupPlace(i);
                                break;
                            }
                        }
                    }
                }
                showMap();
            }
            @Override
            public void onElementButtonClick(int id, int type) {
                if (type == 1) {
                    openBrewery(id);
                } else {
                    openPlace(id);
                }
            }
        };
        loadData(breweries, places);
    }

    private void loadData(ArrayList<BrewerySmall> brws, ArrayList<PlaceSmall> plcs){
        mAdapter.cleanAdapter();
        SpecialEvent h1 = new SpecialEvent(getResources().getString(R.string.seeBreweries));
        mAdapter.addSectionHeaderItem(h1);

        for (int i = 0; i < brws.size(); i++) {
            SpecialEvent b = new SpecialEvent(brws.get(i).id, brws.get(i).name, brws.get(i).stand, brws.get(i).thumb, 1);
            b.city = brws.get(i).city;
            b.state = brws.get(i).state;
            b.country = brws.get(i).country;
            mAdapter.addItem(b);
        }

        SpecialEvent h2 = new SpecialEvent(getResources().getString(R.string.bars));
        mAdapter.addSectionHeaderItem(h2);

        for (int i = 0; i < plcs.size(); i++) {
            SpecialEvent p = new SpecialEvent(plcs.get(i).id, plcs.get(i).name, plcs.get(i).stand, plcs.get(i).thumb, 2);
            p.address = plcs.get(i).address;
            mAdapter.addItem(p);
        }

        specialEventBreweries.setAdapter(mAdapter);
    }

    public static void openBrewery(int brewery_id) {
        if (parent != null) {
            Fragment breweryFragment = new BreweryFragment();
            Bundle arguments = new Bundle();
            arguments.putInt("BREWERY_ID", brewery_id);
            breweryFragment.setArguments(arguments);
            parent.pushFragment(breweryFragment);
        }
    }

    public static void openPlace(int place_id) {
        if (parent != null) {
            Fragment placeFragment = new PlaceFragment();
            Bundle arguments = new Bundle();
            arguments.putSerializable("PLACE_ID", place_id);
            arguments.putInt("NEARBY", 1);
            placeFragment.setArguments(arguments);
            parent.pushFragment(placeFragment);
        }
    }

    private void showMap() {
        specialEventMapLinearLayout.setVisibility(View.VISIBLE);
        specialEventListLinearLayout.setVisibility(View.GONE);
        specialEventMapSearch.setVisibility(View.GONE);

        mapViewspecialeventButton.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        mapViewspecialeventButton.setTextColor(getResources().getColor(R.color.colorPrimary));

        listViewspecialeventButton.setBackgroundColor(getResources().getColor(R.color.transparent));
        listViewspecialeventButton.setTextColor(getResources().getColor(R.color.newsDate));
    }

    private void showList() {
        if(interactiveMapView != null) {
            interactiveMapView.cleanup();
        }
        specialEventListLinearLayout.setVisibility(View.VISIBLE);
        specialEventMapSearch.setVisibility(View.VISIBLE);
        if(scrolling) {
            slideToBottom(specialEventMapSearch);
        }
        //specialEventMapSearch.setVisibility(View.VISIBLE);
        specialEventMapLinearLayout.setVisibility(View.GONE);


        mapViewspecialeventButton.setBackgroundColor(getResources().getColor(R.color.transparent));
        mapViewspecialeventButton.setTextColor(getResources().getColor(R.color.newsDate));

        listViewspecialeventButton.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        listViewspecialeventButton.setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    private void getUserLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, ApiUrl.LOCATION_REQUEST_CODE);
        } else {
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            String locationProvider = null;
            if (locationManager == null) {
                Log.w(getString(R.string.app_name), "Unable to get location provider");
            } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Log.i(getString(R.string.app_name), "Using GPS location provider");
                locationProvider = LocationManager.GPS_PROVIDER;
            } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                Log.i(getString(R.string.app_name), "Using network location provider");
                locationProvider = LocationManager.NETWORK_PROVIDER;
            } else {
                Log.w(getString(R.string.app_name), "Using no location provider");
            }
            if (locationProvider != null) {
                locationManager.requestLocationUpdates(locationProvider, 1000, 5.0f, locationListener);
                Location location = locationManager.getLastKnownLocation(locationProvider);
                setUserLocation(location);
            }
        }
    }

    private void setUserLocation (Location location) {
        if (location == null) {
            Log.w(getString(R.string.app_name), "Unable to retrieve location!");
        } else {
            double tX = (location.getLatitude() - event.latitude) / totalDist;
            double tY = (location.getLongitude() - event.longitude) / totalDist;

            double pX = (tX * Math.sin(angle)) + (tY * Math.cos(angle));
            double pY = (tX * Math.cos(angle)) - (tY * Math.sin(angle));

            if (pX > -1.5 && pX < 1.5 && pY > -1.5 && pY < 1.5) {
                interactiveMapView.setNear(true);
                interactiveMapView.setUser(pX, pY);
            } else {
                interactiveMapView.setNear(false);
            }
        }
    }

    private void setMathInfo(double pOriX, double pOriY, double pOri2X, double pOri2Y) {
        totalDist = distVect(pOriX, pOriY, pOri2X, pOri2Y);
        angle = angle(pOriX, pOriY, pOri2X, pOri2Y);
        getUserLocation();
    }

    private double distVect(double lat1, double lng1, double lat2, double lng2) {
        double lat = lat2 - lat1;
        double lon = lng2 - lng1;

        return Math.sqrt((lat * lat) + (lon * lon));
    }

    private double angle(double lat1, double lng1, double lat2, double lng2) {
        double lat = lat2 - lat1;
        double lon = lng2 - lng1;

        double a = Math.atan2(lon, lat);
        return Math.PI/2 - a;
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (!isAdded()) {
                return;
            }
            setUserLocation(location);
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
        @Override
        public void onProviderEnabled(String provider) {

        }
        @Override
        public void onProviderDisabled(String provider) {

        }
    };

}