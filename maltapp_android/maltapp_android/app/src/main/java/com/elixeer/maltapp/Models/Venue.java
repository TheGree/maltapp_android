package com.elixeer.maltapp.Models;

import com.elixeer.maltapp.ApiUrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class Venue implements Serializable {

    public int id;
    public String name;
    public String location;
    public double latitude;
    public double longitude;
    public String icon;

    public Venue(JSONObject jsonVenues) throws JSONException {
        this.id = jsonVenues.optInt("id", 0);
        this.name = jsonVenues.optString("name", null);
        if (jsonVenues.optJSONObject("location").optString("address", "").equals("")) {
            this.location = jsonVenues.optJSONObject("location").getString("formattedAddress").replace("\"", "").replace("[", "").replace("]", "").replace(",", " ");
        } else {
            this.location = jsonVenues.optJSONObject("location").optString("address", "");
        }
        this.latitude = jsonVenues.optJSONObject("location").getDouble("lat");
        this.longitude = jsonVenues.optJSONObject("location").getDouble("lng");
        if (jsonVenues.getJSONArray("categories").length() > 0) {
            String uno = jsonVenues.getJSONArray("categories").getJSONObject(0).optJSONObject("icon").optString("prefix", null);
            String dos = jsonVenues.getJSONArray("categories").getJSONObject(0).optJSONObject("icon").optString("suffix", null);
            this.icon = uno + ApiUrl.ICON_SIZE + dos;
        } else {
            this.icon = null;
        }
    }

    public static ArrayList<Venue> venuesFromJSONArray(JSONArray jsonVenues) throws JSONException {
        ArrayList<Venue> venues = new ArrayList<>();
        for (int i = 0; i < jsonVenues.length(); i++) {
            JSONObject jsonVenue = jsonVenues.optJSONObject(i);
            if (jsonVenue != null) {
                venues.add(new Venue(jsonVenue));
            }
        }
        return venues;
    }

}