package com.elixeer.maltapp.Comparators;

import com.elixeer.maltapp.Models.Event;

import java.util.Comparator;


public class EventComparator implements Comparator<Event> {
    public int compare(Event left, Event right) {
        return left.startDate.compareTo(right.startDate);
    }

}
