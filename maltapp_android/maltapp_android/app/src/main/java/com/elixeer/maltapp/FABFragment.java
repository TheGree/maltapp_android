package com.elixeer.maltapp;

import android.support.v4.app.Fragment;
import android.view.View;

public abstract class FABFragment extends Fragment {
    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).fab.setImageResource(FABImageDrawable());
            ((MainActivity) getActivity()).fab.show();
        }
    }

    @Override
    public void onPause() {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).fab.hide();
        }
        super.onPause();
    }

    public abstract int FABImageDrawable();

    public abstract void onFABClick(View view);
}
