package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.alebrije.core.SimpleListAdapter;
import com.elixeer.maltapp.Handlers.PointsManager;
import com.elixeer.maltapp.Models.BeerSmall;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by alebrije2 on 1/25/16.
 *
 */
public class BeerSearchFragment extends Fragment {

    AppCompatActivity parent;
    LinearLayout backgroundItemNotFound;
    Button letsUsKnowLibraryButton;
    ArrayList<BeerSmall> beers;
    SearchView beerSearchView;
    //Para el carrusel
    //ImageCarousel images;
    String searchParams;
    private int allBeers = 0;
    private int currentPage = 1;
    private int paginate = 0;
    int user_id;

    public BeerSearchFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        if (getActivity() instanceof AppCompatActivity) {
            parent = (AppCompatActivity) getActivity();
            //parent.setTab(0);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.title_beer_search);
                actionBar.show();
            }
        }

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_beer_search_view, container, false);

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        if (sharedPreferences.contains("userId")) {
            user_id = sharedPreferences.getInt("userId", -1);
        }

        backgroundItemNotFound = (LinearLayout) inflater.inflate(R.layout.list_item_not_found, null, false);
        letsUsKnowLibraryButton = backgroundItemNotFound.findViewById(R.id.letsUsKnowLibraryButton);

        letsUsKnowLibraryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user_id > 0) {
                    PointsManager.addPoints(user_id, ApiUrl.POINTS_BREWERYLINK, ApiUrl.REASON_BREWERYLINK, getActivity());
                }
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", getResources().getString(R.string.contact_mail), null));
                startActivity(Intent.createChooser(intent, getString(R.string.send)));
            }
        });

        beerSearchView = rootView.findViewById(R.id.beerSearchView);

        //AutoCompleteTextView search_text = (AutoCompleteTextView) beerSearchView.findViewById(beerSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null));
        //AutoCompleteTextView search_hint = (AutoCompleteTextView) beerSearchView.findViewById(beerSearchView.getContext().getResources().getIdentifier("android:id/search_hint", null, null));
        //search_text.setTextColor(Color.WHITE);
        //search_hint.setTextColor(Color.WHITE);
        //int searchSrcTextId = getResources().getIdentifier("android:id/search_src_text", null, null);
        //EditText searchEditText = (EditText) beerSearchView.findViewById(searchSrcTextId);
        //searchEditText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getDimensionPixelSize(R.dimen.home_text_description_size));
        //searchEditText.setTextColor(Color.BLUE); // set the text color
        //searchEditText.setHintTextColor(Color.BLUE); // set the hint color
        //search_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getDimensionPixelSize(R.dimen.home_text_description_size));
        //search_hint.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getDimensionPixelSize(R.dimen.home_text_description_size));

        //beerSearchView.setFocusable(true);

        searchParams = getArguments().getString("QUERY_SEARCH", null);

        if(searchParams != null) {
            getArguments().remove("QUERY_SEARCH");
            beerSearchView.clearFocus();
            beerSearchView.setQuery(searchParams, true);
            getTotalBeers(searchParams);
        }

        beerSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchParams = query;//.trim();
                if (!searchParams.isEmpty()) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("home", "beer_search", searchParams);

                    getTotalBeers(searchParams);
                    beerSearchView.clearFocus();
                    return true;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        ListView beerListView = rootView.findViewById(R.id.beerSearchListView);
        beerListView.addFooterView(backgroundItemNotFound);
        beerListView.setAdapter(adapter);
        beerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (beers != null) {
                    if (position < beers.size()) {
                        // Analytics event
                        ((AnalyticsApplication) getActivity().getApplication()).sendEvent("BeerSearch", "beer_pressed", beers.get(position).name);
                        openBeerDetails(position);
                    }
                }
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("BeerSearch");
    }

    private void openBeerDetails(int position) {
        if (parent != null) {
            Fragment beerFragment = new BeerFragment();
            Bundle arguments = new Bundle();
            arguments.putInt("BEER_ID", beers.get(position).id);
            beerFragment.setArguments(arguments);
            if(parent instanceof MainActivity) {
                ((MainActivity)parent).pushFragment(beerFragment);
            }else{
                ((ZXingScanner)parent).pushFragment(beerFragment);
            }
        }
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void getTotalBeers(String searchParams) {
        currentPage = 1;
        beers = new ArrayList<>();
        getBeers(searchParams, currentPage);
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(BeerSearchFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void getBeers(String searchParams, int page) {
        String url = ApiUrl.BEER_SEARCH
                + "&name=" + searchParams.replace(" ","+")
                + "&page=" + page;

        //Log.d(getString(R.string.app_name), "Requested url " + url);
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            allBeers = jsonResponse.optInt("total", 0);
                            paginate = jsonResponse.optInt("paginate", 0);
                            JSONArray jsonBeers = jsonResponse.getJSONArray("beers");
                            beers.addAll(BeerSmall.beersFromJSONArray(jsonBeers));
                            if (beers.size() < 1) {
                                Log.i(getString(R.string.app_name), "No beers found");
                            }// else {
                            adapter.setList(beers);
                            adapter.setSize(allBeers);
                            adapter.notifyDataSetChanged();
                            //}
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    /*private void getTempo() {
        if (isOnline()) {
            String url = "http://174.37.69.10/beers/image_list.json?name=o";

            //Log.d(getString(R.string.app_name), "Requested url " + url);
            new AsyncRequestString("GET", url) {
                @Override
                public void onResult(int responseCode, String response) {
                    if (response == null) {
                        Log.w(getString(R.string.app_name), "WARNING: Response is null (" + responseCode + ")");
                    } else {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                            if (jsonResponse.has("error")) {
                                reportError(jsonResponse.getString("error"));
                            } else {
                                JSONArray jsonBeers = jsonResponse.getJSONArray("beers");
                                beers = BeerSmall.beersFromJSONArray(jsonBeers);
                                if (beers.size() < 1) {
                                    Log.i(getString(R.string.app_name), "No beers found");
                                } else {
                                    //TODO:Para el carrusel
                                    ArrayList<String> s = new ArrayList<>();
                                    for(BeerSmall b:beers){
                                        s.add(b.medium);
                                    }
                                    images.setImages(s);
                                    //
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.execute();
        }
    }*/

    SimpleListAdapter adapter = new SimpleListAdapter(R.layout.list_item_beer_search, new ArrayList<>()) {
        @Override
        public void onListItem(Object listItem, View view, int position) {
            final ImageView itemImageView = view.findViewById(R.id.searchBeerImageView);
            TextView titleTextView = view.findViewById(R.id.searchBeerNameTextView);
            TextView descriptionTextView = view.findViewById(R.id.searchBreweryNameTextView);

            if (listItem == null) {
                int nextPage = beers.size() / paginate + 1;
                Log.d(getString(R.string.app_name), beers.size() + ": " + nextPage + " == " + currentPage);
                if (nextPage > currentPage) {
                    currentPage = nextPage;
                    getBeers(searchParams, currentPage);
                }

                titleTextView.setText("");
                descriptionTextView.setText("");
                itemImageView.setImageResource(R.drawable.img_default_beer);
            } else {
                BeerSmall beer = (BeerSmall) listItem;

                titleTextView.setText(beer.name);
                descriptionTextView.setText(beer.brewery_name);
                if (beer.thumb.contains("/missing.png")) {
                    itemImageView.setImageResource(R.drawable.img_default_beer);
                } else {
                    itemImageView.setImageBitmap(null);
                    ImageCache.getImage(beer.thumb, getContext(), new ImageCache.OnResponseListener() {
                        @Override
                        public void onResponse(Bitmap bitmap, String error) {
                            itemImageView.setImageBitmap(bitmap);
                        }
                    });
                }
            }
        }
    };
}
