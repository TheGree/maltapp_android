package com.elixeer.maltapp.Models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class Score implements Serializable {
    public int id;
    public int user_id;
    public int badgetype_id;
    public int points;

    public Score(JSONObject jsonScore) {
        this.id = jsonScore.optInt("id", 0);
        this.user_id = jsonScore.optInt("user_id", 0);
        this.badgetype_id = jsonScore.optInt("badgetype_id", 0);
        this.points = jsonScore.optInt("points", 0);
    }

    public static ArrayList<Score> tastesFromJSONArray(JSONArray jsonScores) {
        ArrayList<Score> scores = new ArrayList<>();
        for (int i = 0; i < jsonScores.length(); i++) {
            JSONObject jsonScore = jsonScores.optJSONObject(i);
            if (jsonScore != null) {
                scores.add(new Score(jsonScore));
            }
        }
        return scores;
    }
}
