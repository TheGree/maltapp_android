package com.elixeer.maltapp;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";

    public static String RECIVEACTION = "RECIVE_ACTION";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        final String message = remoteMessage.getNotification().getBody();
        final String messageTitle = remoteMessage.getNotification().getTitle();
        Log.d(TAG, remoteMessage.getData().toString());
        Intent intent = new Intent();
        intent.setAction(RECIVEACTION);
        sendNotification(message, messageTitle);
        intent.putExtra("value", remoteMessage.getData().get("clave"));

        sendBroadcast(intent);
        /*if(MainActivity.instance != null) {
            MainActivity.instance.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Toast.makeText(MainActivity.instance, message, Toast.LENGTH_SHORT).show();
                }
            });
        }*/
    }

    private void sendNotification(String messageBody, String messageTitle) {

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_notify)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
