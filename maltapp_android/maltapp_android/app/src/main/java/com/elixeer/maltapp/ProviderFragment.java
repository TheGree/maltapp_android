package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Models.Provider;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class ProviderFragment extends Fragment {

    MainActivity parent;
    ImageView providerImageView;
    TextView providerNameText;
    TextView providerAddressText;
    TextView providerDescriptionText;
    ImageButton providerPhoneButton;
    ImageButton providerMailButton;
    ImageButton providerFacebookButton;
    ImageButton providerWebButton;
    Provider provider;
    int provider_id;
    String tag;
    int user_id;

    public ProviderFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        if (sharedPreferences.contains("userId")) {
            user_id = sharedPreferences.getInt("userId", -1);
        }

        provider_id = getArguments().getInt("PROVIDER_ID");
        tag = getArguments().getString("TAG", getString(R.string.provider));

        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(tag);
                actionBar.show();
            }
        }
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_provider, container, false);
        getProvider();

        providerImageView = rootView.findViewById(R.id.providerImageView);
        providerNameText = rootView.findViewById(R.id.providerNameText);
        providerAddressText = rootView.findViewById(R.id.providerAddressText);
        providerDescriptionText = rootView.findViewById(R.id.providerDescriptionText);
        providerPhoneButton = rootView.findViewById(R.id.providerPhoneButton);
        providerMailButton = rootView.findViewById(R.id.providerMailButton);
        providerFacebookButton = rootView.findViewById(R.id.providerFacebookButton);
        providerWebButton = rootView.findViewById(R.id.providerWebButton);

        providerPhoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (provider != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Providers", "contact_pressed", "Phone");

                    if (getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
                        if (!provider.phone.equals("") && TextUtils.isDigitsOnly(provider.phone)) {
                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                            callIntent.setData(Uri.parse("tel:" + provider.phone));
                            startActivity(callIntent);
                        }
                    }
                }
            }
        });

        providerMailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (provider != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Providers", "contact_pressed", "Email");

                    if (!provider.email.equals("")) {
                        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", provider.email, null));
                        startActivity(Intent.createChooser(intent, getString(R.string.send)));
                    }
                }
            }
        });

        providerFacebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (provider != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Providers", "contact_pressed", "Facebook");

                    String facebookPath;
                    if (!provider.facebook_id.equals("")) {
                        facebookPath = provider.facebook_id;
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + facebookPath));
                            startActivity(intent);
                        } catch (Exception e) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/" + facebookPath)));
                        }
                    } else if (!provider.facebook.equals("")) {
                        String facebookUrl = getFacebookPageURL(provider.facebook);
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl));
                        startActivity(intent);
                    }
                }
            }
        });

        providerWebButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (provider != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Providers", "contact_pressed", "WebPage");
                    if (!provider.webpage.equals("")) {
                        String url = provider.webpage;
                        if (!url.startsWith("http://") && !url.startsWith("https://")) {
                            url = "http://" + url;
                        }
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        });

        /*
        twitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brewery != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("brewery_details", "twitter_pressed", brewery.name);

                    if (!brewery.twitter.equals("")) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + brewery.twitter));
                            startActivity(intent);
                        } catch (Exception e) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/#!/" + brewery.twitter)));
                        }
                    }  else {
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View pop = inflater.inflate(R.layout.pop_item_alert, null);

                        AlertDialog.Builder builder = new AlertDialog.Builder(BreweryFragment.this.getActivity())
                                .setTitle(brewery.name)
                                .setView(pop)
                                .setPositiveButton(R.string.ok, null);

                        AlertDialog alert = builder.create();
                        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                            }
                        });

                        alert.show();
                    }
                }
            }
        });
        */

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        /*
        if (provider != null) {
            ((AnalyticsApplication) getActivity().getApplication()).setScreenName("Brewery-" + brewery.name);
        }
        */
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
    }

    private void fillProviderData() {
        // Analytics screen
        //((AnalyticsApplication) getActivity().getApplication()).setScreenName("Brewery-" + brewery.name);

        if (provider.phone.equals("")) {
            providerPhoneButton.setAlpha(0.50f);
            providerPhoneButton.setClickable(false);
        }

        if (provider.email.equals("")) {
            providerMailButton.setAlpha(0.50f);
            providerMailButton.setClickable(false);
        }

        if (provider.facebook_id.equals("") && provider.facebook.equals("")) {
            providerFacebookButton.setAlpha(0.50f);
            providerFacebookButton.setClickable(false);
        }

        if (provider.webpage.equals("")) {
            providerWebButton.setAlpha(0.50f);
            providerWebButton.setClickable(false);
        }

        ImageCache.getImage(provider.imageUrlMedium, getContext(), new ImageCache.OnResponseListener() {
            @Override
            public void onResponse(Bitmap bitmap, String error) {
                providerImageView.setImageBitmap(bitmap);
            }
        });
        providerNameText.setText(provider.name);

        providerAddressText.setText(provider.city_name);
        providerDescriptionText.setText(provider.description);
        //getBeerImages(brewery.id);

    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(ProviderFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void getProvider() {
        String url = ApiUrl.PROVIDER + provider_id + ".json" + ApiUrl.ACCESS_TOKEN;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONObject jsonProvider = jsonResponse.getJSONObject("provider");
                            provider = new Provider(jsonProvider, getContext());
                            fillProviderData();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    //method to get the right URL to use in the intent
    public String getFacebookPageURL(String fbPath) {
        fbPath = fbPath.toLowerCase()
                .replace("https://", "")
                .replace("http://", "")
                .replace("www.facebook.com/", "")
                .replace("facebook.com/", "");

        String FACEBOOK_URL = "https://www.facebook.com/" + fbPath;
        String FACEBOOK_PAGE_ID = fbPath;

        PackageManager packageManager = getContext().getPackageManager();
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
                if (versionCode >= 3002850) { //newer versions of fb app
                    return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
                } else { //older versions of fb app
                    return "fb://page/" + FACEBOOK_PAGE_ID;
                }
            } else {
                return FACEBOOK_URL; //normal web url
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL; //normal web url
        }
    }

}
