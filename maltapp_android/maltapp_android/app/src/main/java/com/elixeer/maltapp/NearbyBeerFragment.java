package com.elixeer.maltapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.core.SimpleRecyclerAdapter;
import com.elixeer.maltapp.Models.BeerSmall;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by alebrije2 on 1/25/16.
 *
 */
public class NearbyBeerFragment extends Fragment {

    MainActivity parent;
    RecyclerView beerNearbyListView;
    ArrayList<BeerSmall> beers;
    private static final int LOCATION_REQUEST_CODE = 101;

    public NearbyBeerFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {

            parent = (MainActivity) getActivity();
            //parent.setTab(0);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.beers);
                actionBar.show();
            }
        }
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_nearby_beer, container, false);

        beerNearbyListView = rootView.findViewById(R.id.beerNearbyListView);
        beerNearbyListView.setLayoutManager(new LinearLayoutManager(getContext()));
        beerNearbyListView.setAdapter(adapter);

        getUserLocation();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("NearbyBeer");
    }

    private void openNearbyMapBeers(int position) {
        if (parent != null) {
            Fragment nearbyMapFragment = new NearbyMapFragment();
            Bundle arguments = new Bundle();
            arguments.putInt("NEARBY", 2);
            arguments.putInt("BEER_ID", beers.get(position).id);
            nearbyMapFragment.setArguments(arguments);
            parent.pushFragment(nearbyMapFragment);
        }
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_REQUEST_CODE && (android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            getUserLocation();
        }
    }

    private void getUserLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_REQUEST_CODE);
        } else {
            /* LOCATION */
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            String locationProvider = null;

            if (locationManager == null) {
                Log.w(getString(R.string.app_name), "Unable to get location provider");
            } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Log.i(getString(R.string.app_name), "Using GPS location provider");
                locationProvider = LocationManager.GPS_PROVIDER;
            } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                Log.i(getString(R.string.app_name), "Using network location provider");
                locationProvider = LocationManager.NETWORK_PROVIDER;
            } else {
                Log.w(getString(R.string.app_name), "Using no location provider");
            }

            if (locationProvider != null) {
                Location location = locationManager.getLastKnownLocation(locationProvider);
                if (location == null) {
                    Log.w(getString(R.string.app_name), "Unable to retrieve location!");
                    getBeers(ApiUrl.MEX_CTY_LAT, ApiUrl.MEX_CTY_LONG, ApiUrl.RADIUS_MAX);
                } else {
                    getBeers(location.getLatitude(), location.getLongitude(), ApiUrl.RADIUS_MIN);
                }
            } else {
                getBeers(ApiUrl.MEX_CTY_LAT, ApiUrl.MEX_CTY_LONG, ApiUrl.RADIUS_MAX);
            }
        }
    }

    private void getBeers(double latitude, double longitude, double rad) {

        double kmInLongitudeDegree = ApiUrl.DEGREE_LON * Math.cos(latitude / 180.0 * Math.PI);
        double deltaLat = rad / ApiUrl.DEGREE_LAT;
        double deltaLong = rad / kmInLongitudeDegree;
        double minLat = latitude - deltaLat;
        double maxLat = latitude + deltaLat;
        double minLong = longitude - deltaLong;
        double maxLong = longitude + deltaLong;

        String url = ApiUrl.BEERS + "&coords=" + minLong + "," + minLat + "," + maxLong + "," + maxLat;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonBeers = jsonResponse.getJSONArray("beers");
                            beers = BeerSmall.beersFromJSONArray(jsonBeers);
                            if (beers.size() < 1) {
                                Log.i(getString(R.string.app_name), "No beers found");
                            } else {
                                adapter.setList(beers);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(NearbyBeerFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private double distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371; //Km
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return earthRadius * c;
    }

    SimpleRecyclerAdapter adapter = new SimpleRecyclerAdapter(R.layout.list_item_nearby_beer) {
        @Override
        public void onListItem(Object listItem, View view, final int position) {
            BeerSmall beer = (BeerSmall) listItem;

            TextView titleTextView = view.findViewById(R.id.nearbyBeerNameTextView);
            TextView descriptionTextView = view.findViewById(R.id.nearbyBreweryNameTextView);

            titleTextView.setText(beer.name);
            descriptionTextView.setText(beer.brewery_name);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openNearbyMapBeers(position);
                }
            });
        }
    };
}
