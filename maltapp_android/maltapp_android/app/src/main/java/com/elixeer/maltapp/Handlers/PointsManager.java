package com.elixeer.maltapp.Handlers;

import android.app.AlertDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;

import com.alebrije.async.AsyncRequestString;
import com.elixeer.maltapp.ApiUrl;
import com.elixeer.maltapp.MainActivity;
import com.elixeer.maltapp.R;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class PointsManager {

    private static void reportError(String error, Context context) {
        Log.e(context.getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    public static void addPoints(int id, final int pts, int reason, final Context context) {
        addPoints(id,pts,reason, context, null);
    }

    public static void addPoints(int id, final int pts, int reason, final Context context, final Runnable purchase) {

        String url = ApiUrl.POINTS_CREATE + "&user_id=" + id + "&points=" + pts + "&reason=" + reason + "&movement=true";
        new AsyncRequestString("POST", url, context, new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(context.getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"), context);
                        } else {
                            Log.i(context.getString(R.string.app_name), pts + " points added");

                            Snackbar snackbar = Snackbar.make(((MainActivity) context).coordinatorLayout, String.format(context.getString(R.string.got_points), pts), Snackbar.LENGTH_LONG);
                            snackbar.show();

                            if (purchase != null){
                                purchase.run();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    reportError(error, context);
                }
            }
        }).execute();
    }
}
