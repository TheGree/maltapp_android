package com.elixeer.maltapp.Models;

import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class User implements Serializable {

    public int id;
    public String email;
    public String firstName;
    public String lastName;
    public int gender;
    public Date birthday;
    public String levelName;
    public int levelId;
    public int levelNext;
    public int points_earned;
    public int points_used;
    public String imageUrlThumb;
    public String imageUrlMedium;
    public String cityName;
    public String stateName;
    public String countryName;
    public int cityId;
    public int stateId;
    public int countryId;
    //public double pointsEarned;
    //public double pointsUsed;
    public boolean isPremium;
    public long timeLeft;

    public User(JSONObject jsonUser) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

        this.id = jsonUser.optInt("id", 0);
        this.email = jsonUser.optString("email", "");
        this.firstName = jsonUser.optString("name", "");
        this.lastName = jsonUser.optString("lastname", "");
        this.gender = jsonUser.optInt("sex", 0);
        String birthday = jsonUser.optString("birthday", null);
        this.birthday = simpleDateFormat.parse(birthday, new ParsePosition(0));
        this.levelName = jsonUser.optString("level_name", "");
        this.levelId = jsonUser.optInt("level_id", 0);
        this.levelNext = jsonUser.optInt("level_next", 0);
        this.points_earned = jsonUser.optInt("points_earned", 0);
        this.points_used = jsonUser.optInt("points_used", 0);
        this.imageUrlThumb = jsonUser.optString("thumb", null);
        this.imageUrlMedium = jsonUser.optString("medium", null);
        this.cityName = jsonUser.optString("city_name", "");
        this.stateName = jsonUser.optString("state_name", "");
        this.countryName = jsonUser.optString("country_name", "");
        this.cityId = jsonUser.optInt("city_id", 0);
        this.stateId = jsonUser.optInt("state_id", 0);
        this.countryId = jsonUser.optInt("country_id", 0);
        //this.pointsEarned = jsonUser.optDouble("points_earned", 0);
        //this.pointsUsed = jsonUser.optDouble("points_used", 0);
        this.isPremium = jsonUser.optBoolean("premium", false);
        this.timeLeft = jsonUser.optLong("timeleft", 0);
    }
}
