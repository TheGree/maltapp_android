package com.elixeer.maltapp;

public class ApiUrl {
    static final int LOCATION_REQUEST_CODE = 101;
    static final double EARTH_RADIUS = 6371;
    static final double RADIUS_MIN = 20; // KM
    static final double RADIUS_MED = 200; // KM
    static final double RADIUS_MAX = 2000; // KM
    static final double DEGREE_LAT = 111.1; // KM/DEGREE_LAT
    static final double DEGREE_LON = 111.320; // KM/DEGREE_LON ***APROX***
    static final double MEX_CTY_LAT = 19.419444;
    static final double MEX_CTY_LONG = -99.145556;

    static final int RANK_SCORE_ID = 13;
    static final int FACE_SCORE_ID = 14;

    static final int REASON_NOTREASON = 0;
    static final int REASON_REGISTER = 1;
    static final int REASON_CHECKIN = 2;
    static final int REASON_RATE = 3;
    static final int REASON_ADDFAV = 4;
    static final int REASON_ADDWISH = 5;
    static final int REASON_SHARE = 6;
    static final int REASON_INVITE = 7;
    static final int REASON_COMPLETEPROFILE = 8;
    static final int REASON_BREWERYLINK = 9;

    static final int REASON_BUYPROMOTION = 10;

    static final int REASON_BUYBEERCOINS = 11;

    static final int REASON_BUYBEERCOINS_1 = 12;
    static final int REASON_BUYBEERCOINS_2 = 13;
    static final int REASON_BUYBEERCOINS_3 = 14;

    static final int POINTS_SHARE = 5;
    static final int POINTS_REGISTER = 5;
    static final int POINTS_RATE = 2;
    static final int POINTS_MAIL = 2;
    static final int POINTS_ADDFAV = 1;
    static final int POINTS_ADDWISH = 1;
    static final int POINTS_BREWERYLINK = 1;
    static final int POINTS_PACKONE = 50;
    static final int POINTS_PACKTWO = 400;
    static final int POINTS_PACKTHREE = 2000;


    static final String SERVER = "http://174.37.69.10";
    static final String ACCESS_TOKEN = "?access_token=10";
    static final String MALTAPP_MX = "http://maltapp.mx";

    static final String FSQR_API = "https://api.foursquare.com/v2/";
    static final String FSQR_CLIENT_ID = "EDJR0SEGYH0J5FWIH33AOWL3ITLBNY2CPT1QCR4E50IX3BJ1";
    static final String FSQR_CLIENT_SECRET = "HYOGUYRY2LTC4MA1UVJVCCSHDCT1MWMVSSIK4MZJSB4JN15N";
    static final String FSQR_CLIENT_V = "20130815";
    static final String FSQR_SEARCH = FSQR_API + "venues/search?client_id=" + FSQR_CLIENT_ID + "&client_secret=" + FSQR_CLIENT_SECRET + "&v=" + FSQR_CLIENT_V;

    static final String CERVEXXA = "http://www.cervezaartesanalmexicana.mx/";

    public static final String ICON_SIZE = "100";

    static final String EVENTS = SERVER + "/events.json" + ACCESS_TOKEN;
    static final String SPECIAL_EVENTS = SERVER + "/events/special_event.json" + ACCESS_TOKEN;
    static final String EVENTS_VOTE = SERVER + "/events/vote_create.json" + ACCESS_TOKEN;
    static final String MY_VOTE = SERVER + "/events/my_vote.json" + ACCESS_TOKEN;
    static final String EVENTS_TOP = SERVER + "/beers/event_rank.json" + ACCESS_TOKEN;
    static final String PLACES = SERVER + "/places.json" + ACCESS_TOKEN;
    static final String GET_PLACE = SERVER + "/places/";
    static final String GET_USER = SERVER + "/users/";
    static final String USERS = SERVER + "/users.json" + ACCESS_TOKEN;
    static final String EXTEND_PREMIUM = SERVER + "/users/extend_premium.json" + ACCESS_TOKEN;
    static final String ADD_PREMIUM = SERVER + "/users/add_premium.json" + ACCESS_TOKEN;
    static final String VALIDATE_COUPON = SERVER + "/coupons.json" + ACCESS_TOKEN;
    static final String GET_BEER = SERVER + "/beers/";
    static final String BEERS = SERVER + "/beers.json" + ACCESS_TOKEN;
    static final String USER_LOGIN = SERVER + "/users/login.json" + ACCESS_TOKEN;
    static final String EMAIL_RECOVERY = SERVER + "/users/reset_password.json" + ACCESS_TOKEN;
    static final String COUNTRIES = SERVER + "/countries.json" + ACCESS_TOKEN;
    static final String STATES = SERVER + "/states.json" + ACCESS_TOKEN;
    static final String CITIES = SERVER + "/cities.json" + ACCESS_TOKEN;
    static final String BEER_SEARCH = SERVER + "/beers/image_list.json" + ACCESS_TOKEN;
    static final String BEER_FAV_CREATED = SERVER + "/beers/fav_create.json" + ACCESS_TOKEN;
    static final String BEER_FAV_DELETE = SERVER + "/beers/fav_delete.json" + ACCESS_TOKEN;
    static final String BEER_FAV_LIST = SERVER + "/beers/fav_list.json" + ACCESS_TOKEN;
    //static final String BEER_WISH_CREATED = SERVER + "/beers/wish_create.json" + ACCESS_TOKEN;
    //static final String BEER_WISH_DELETE = SERVER + "/beers/wish_delete.json" + ACCESS_TOKEN;
    //static final String BEER_WISH_LIST = SERVER + "/beers/wish_list.json" + ACCESS_TOKEN;
    static final String BREWERY = SERVER + "/breweries/";
    static final String PLACE_BREWERIES = SERVER + "/places/brewery_list.json" + ACCESS_TOKEN;
    static final String TASTES = SERVER + "/tastes.json" + ACCESS_TOKEN;
    static final String TASTES_LIST = SERVER + "/users/taste_list.json" + ACCESS_TOKEN;
    static final String TASTES_CREATE = SERVER + "/users/taste_create.json" + ACCESS_TOKEN;
    static final String TASTES_DELETE = SERVER + "/users/taste_delete.json" + ACCESS_TOKEN;
    static final String TOP_RANK = SERVER + "/beers/top_rank.json" + ACCESS_TOKEN;
    static final String BEER_RANKING = SERVER + "/beers/beer_ranking.json" + ACCESS_TOKEN;
    static final String RANK_LIST = SERVER + "/beers/beer_ranking_list.json" + ACCESS_TOKEN;
    static final String RANK_CREATE = SERVER + "/beers/rank_create.json" + ACCESS_TOKEN;
    static final String RANK_UPDATE = SERVER + "/beers/rank_update.json" + ACCESS_TOKEN;
    static final String DRANK_CREATE = SERVER + "/beers/drank_create.json" + ACCESS_TOKEN;
    static final String DRANK_LIST = SERVER + "/beers/drank_history.json" + ACCESS_TOKEN;
    static final String BADGES = SERVER + "/badges.json" + ACCESS_TOKEN;
    static final String BADGES_TYPES = SERVER + "/badgetypes.json" + ACCESS_TOKEN;
    static final String BADGES_DYNAMICS = SERVER + "/badge_dynamics.json" + ACCESS_TOKEN;
    static final String DYNAMIC_CREATE = SERVER + "/badge_dynamics/dynamic_create.json" + ACCESS_TOKEN;
    static final String DATA_DYNAMIC_BADGES = SERVER + "/badge_dynamics/dynamic_badges.json" + ACCESS_TOKEN;
    static final String SCORES = SERVER + "/scores.json" + ACCESS_TOKEN;
    static final String SCORE_UPDATE = SERVER + "/scores/score_update.json" + ACCESS_TOKEN;
    static final String POINTS = SERVER + "/users/points.json" + ACCESS_TOKEN;
    static final String POINTS_LIST = SERVER + "/users/points_list.json" + ACCESS_TOKEN;
    static final String PROMOS = SERVER + "/promos.json" + ACCESS_TOKEN;
    static final String PROMO_CREATE = SERVER + "/promos/promo_create.json" + ACCESS_TOKEN;
    static final String NEW_PROMOS = SERVER + "/promos/promo_list.json" + ACCESS_TOKEN;
    static final String MY_PROMOS = SERVER + "/promos/my_promo_list.json" + ACCESS_TOKEN;
    public static final String POINTS_CREATE = SERVER + "/users/points_create.json" + ACCESS_TOKEN;
    static final String TYPES = SERVER + "/beerstyles.json" + ACCESS_TOKEN;
    static final String STYLES = SERVER + "/beertypes.json" + ACCESS_TOKEN;
    static final String GLASSES = SERVER + "/glasses.json" + ACCESS_TOKEN;
    static final String PROVIDER = SERVER + "/providers/";
    static final String PROVIDERS = SERVER + "/providers.json" + ACCESS_TOKEN;
    static final String MALTAPP_NEWS = MALTAPP_MX + "/news/?id=";
    static final String SQUARE_IMAGE = MALTAPP_MX + "/square.png.php?src=";
}