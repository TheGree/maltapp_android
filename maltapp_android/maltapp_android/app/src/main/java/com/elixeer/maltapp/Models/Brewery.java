package com.elixeer.maltapp.Models;

import android.content.Context;
import android.content.SharedPreferences;

import com.elixeer.maltapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class Brewery {

    public int id;
    public String name;
    public String description;
    public int city_id;
    public String city_name;
    public String state_name;
    public String country_name;
    public String address;
    public double latitude;
    public double longitude;
    public String phone;
    public String mail;
    public String facebook;
    public String twitter;
    public String imageUrlThumb;
    public String imageUrlMedium;
    public Boolean approved;
    public Boolean subscribed;
    public String facebook_id;

    public Brewery(JSONObject jsonBrewery, Context context) {
        String language = Locale.getDefault().getLanguage();
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
            if (sharedPreferences.contains("language")) {
                language = sharedPreferences.getString("language", Locale.getDefault().getLanguage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String language2 = "en";
        if (language.equals("en")) {
            language2 = "es";
        }

        this.id = jsonBrewery.optInt("id", 0);
        this.approved = jsonBrewery.optBoolean("approved", false);
        this.name = jsonBrewery.optString("name", "");
        this.description = jsonBrewery.optString("description_" + language, "");
        if (this.description.equals("")) {
            this.description = jsonBrewery.optString("description_" + language2, "");
        }
        this.city_id = jsonBrewery.optInt("city_id", 0);
        this.city_name = jsonBrewery.optString("city_name", "");
        this.state_name = jsonBrewery.optString("state_name", "");
        this.country_name = jsonBrewery.optString("country_name", "");
        this.address = jsonBrewery.optString("address", "");
        this.latitude = jsonBrewery.optDouble("latitude", 0);
        this.longitude = jsonBrewery.optDouble("longitude", 0);
        this.phone = jsonBrewery.optString("phone", "");
        this.mail = jsonBrewery.optString("mail", "");
        this.facebook = jsonBrewery.optString("facebook", "");
        this.twitter = jsonBrewery.optString("twitter", "");
        this.imageUrlThumb = jsonBrewery.optString("thumb", "");
        this.imageUrlMedium = jsonBrewery.optString("medium", "");
        this.subscribed = jsonBrewery.optBoolean("subscribed", false);
        this.facebook_id = jsonBrewery.optString("face_code", "");
    }

    public static ArrayList<Brewery> breweriesFromJSONArray(JSONArray jsonBreweries, Context context) {
        ArrayList<Brewery> breweries = new ArrayList<>();
        for (int i = 0; i < jsonBreweries.length(); i++) {
            JSONObject jsonBrewery = jsonBreweries.optJSONObject(i);
            if (jsonBrewery != null) {
                breweries.add(new Brewery(jsonBrewery, context));
            }
        }
        return breweries;
    }
}