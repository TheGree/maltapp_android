package com.elixeer.maltapp;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.elixeer.maltapp.Adapters.LibraryCustomAdapter;
import com.elixeer.maltapp.Handlers.PointsManager;

import java.util.ArrayList;

public class LibraryMainFragment extends Fragment {
    MainActivity parent;
    ListView libraryListView;
    LinearLayout backgroundItemNotFound;
    ArrayList<String> elements;
    int user_id;

    public LibraryMainFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {

            parent = (MainActivity) getActivity();
            //parent.setTab(0);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.library);
                actionBar.show();
            }
        }

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        if (sharedPreferences.contains("userId")) {
            user_id = sharedPreferences.getInt("userId", -1);
        }

        backgroundItemNotFound = (LinearLayout) inflater.inflate(R.layout.list_item_not_found, null, false);

        View rootView = inflater.inflate(R.layout.fragment_library, container, false);

        libraryListView = rootView.findViewById(R.id.libraryListView);

        TextView letsUsKnowLibraryTextView = backgroundItemNotFound.findViewById(R.id.letsUsKnowLibraryTextView);
        Button letsUsKnowLibraryButton = backgroundItemNotFound.findViewById(R.id.letsUsKnowLibraryButton);

        letsUsKnowLibraryTextView.setText(getString(R.string.cantFindInfo));

        letsUsKnowLibraryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user_id > 0) {
                    PointsManager.addPoints(user_id, ApiUrl.POINTS_BREWERYLINK, ApiUrl.REASON_BREWERYLINK, getActivity());
                }
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", getResources().getString(R.string.contact_mail), null));
                startActivity(Intent.createChooser(intent, getString(R.string.send)));
            }
        });

        libraryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Analytics event
                ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Library", "button_pressed", elements.get(position));
                if (position == 0) {
                    openProvidersView();
                } else if (position < 8) {
                    openLibrarySubView(position);
                } else {
                    openLibraryDetail(position);
                }
            }
        });

        fillElements();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("library");
    }

    private void fillElements() {
        elements = new ArrayList<>();
        elements.add("");
        elements.add(getResources().getString(R.string.styles));
        elements.add(getResources().getString(R.string.glasses));
        elements.add(getResources().getString(R.string.fact_sheet));
        elements.add(getResources().getString(R.string.beer_ingredients));
        elements.add(getResources().getString(R.string.brewing_process));
        elements.add(getResources().getString(R.string.how_to_review_a_beer));
        elements.add(getResources().getString(R.string.tutorial));
        elements.add(getResources().getString(R.string.brief_history));
        elements.add(getResources().getString(R.string.terms_and_conditions_title));

        libraryListView.addFooterView(backgroundItemNotFound);

        LibraryCustomAdapter mAdapter = new LibraryCustomAdapter(getContext());

        for (int i = 0; i < elements.size(); i++) {
            if (i == 0) {
                mAdapter.addSectionHeaderItem(elements.get(i));
            } else {
                mAdapter.addItem(elements.get(i));
            }
        }
        libraryListView.setAdapter(mAdapter);
        /*
        libraryListView.setAdapter(new SimpleListAdapter(R.layout.list_item_library, elements) {
            @Override
            public void onListItem(Object listItem, View view, int position) {
                String title = (String) listItem;
                TextView libraryNameTextView = (TextView) view.findViewById(R.id.libraryNameTextView);
                libraryNameTextView.setText(title);
            }
        });
        */
    }

    private void openProvidersView() {
        if (parent != null) {
            Fragment librarySubSectionFragment = new LibrarySubSectionFragment();
            Bundle arguments = new Bundle();
            arguments.putBoolean("IS_PROVIDER", true);
            arguments.putString("TITLE", getString(R.string.providers));
            librarySubSectionFragment.setArguments(arguments);
            parent.pushFragment(librarySubSectionFragment);
        }
    }

    private void openLibrarySubView(int position) {
        if (parent != null) {
            Fragment librarySubSectionFragment = new LibrarySubSectionFragment();
            Bundle arguments = new Bundle();
            arguments.putInt("POSITION", position);
            arguments.putString("TITLE", elements.get(position));
            librarySubSectionFragment.setArguments(arguments);
            parent.pushFragment(librarySubSectionFragment);
        }
    }

    private void openLibraryDetail(int position) {
        if (parent != null) {
            Fragment libraryDetailFragment = new LibraryDetailFragment();
            Bundle arguments = new Bundle();
            arguments.putBoolean("DIRECT", true);
            arguments.putInt("POSITION", position);
            arguments.putString("TITLE", elements.get(position));
            libraryDetailFragment.setArguments(arguments);
            parent.pushFragment(libraryDetailFragment);
        }
    }
}
