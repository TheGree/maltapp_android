package com.elixeer.maltapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.alebrije.core.SimpleRecyclerAdapter;
import com.elixeer.maltapp.Models.Venue;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FoursquareFragment extends Fragment {

    MainActivity parent;
    RecyclerView foursquareListView;
    SearchView foursquareSearchView;
    ArrayList<Venue> venues;
    int beer_id;
    int user_id;
    String title;
    Long rank;
    boolean created;

    public FoursquareFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {

            parent = (MainActivity) getActivity();
            //parent.setTab(0);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.beers);
                actionBar.show();
            }
        }

        title = getArguments().getString("TITLE");
        beer_id = getArguments().getInt("BEER_ID");
        created = getArguments().getBoolean("CREATED", false);
        rank = getArguments().getLong("RANK", 0);

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        if (sharedPreferences.contains("userId")) {
            if (sharedPreferences.getInt("userId", -1) > 0) {
                user_id = sharedPreferences.getInt("userId", -1);
            }
        }

        View rootView = inflater.inflate(R.layout.fragment_foursquare, container, false);

        foursquareListView = rootView.findViewById(R.id.foursquareListView);
        foursquareListView.setLayoutManager(new LinearLayoutManager(getContext()));
        foursquareListView.setAdapter(adapter);

        foursquareSearchView = rootView.findViewById(R.id.foursquareSearchView);

        getUserLocation("");

        foursquareSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String searchParams = query.trim();
                if (!searchParams.isEmpty()) {
                    getUserLocation(searchParams);
                    foursquareSearchView.clearFocus();
                    return true;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("Foursquare");
    }

    private void sendPlace(int position) {
        if (parent != null) {
            Fragment rankFragment = new RankFragment();
            Bundle arguments = new Bundle();

            arguments.putInt("BEER_ID", beer_id);
            arguments.putString("TITLE", title);
            arguments.putLong("RANK", rank);
            arguments.putBoolean("CREATED", created);
            arguments.putString("FSQR_NAME", venues.get(position).name);
            arguments.putString("FSQR_ADDRESS", venues.get(position).location);
            arguments.putString("FSQR_LAT", venues.get(position).latitude + "");
            arguments.putString("FSQR_LONG", venues.get(position).longitude + "");
            rankFragment.setArguments(arguments);
            parent.pushFragment(rankFragment);
        }
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == ApiUrl.LOCATION_REQUEST_CODE && (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            getUserLocation("");
        }
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(FoursquareFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void getFoursquares(double latitude, double longitude, String query) {
        String url = ApiUrl.FSQR_SEARCH + "&ll=" + latitude + "," + longitude + "&query=" + query;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonVenues = jsonResponse.getJSONObject("response").getJSONArray("venues");
                            venues = Venue.venuesFromJSONArray(jsonVenues);
                            if (venues.size() < 1) {
                                Log.i(getString(R.string.app_name), "No venues found");
                            } else {
                                adapter.setList(venues);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getUserLocation(String query) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, ApiUrl.LOCATION_REQUEST_CODE);
        } else {
            /* LOCATION */
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            String locationProvider = null;

            if (locationManager == null) {
                Log.w(getString(R.string.app_name), "Unable to get location provider");
            }else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Log.i(getString(R.string.app_name), "Using GPS location provider");
                locationProvider = LocationManager.GPS_PROVIDER;
            } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                Log.i(getString(R.string.app_name), "Using network location provider");
                locationProvider = LocationManager.NETWORK_PROVIDER;
            } else {
                Log.w(getString(R.string.app_name), "Using no location provider");
            }

            LatLng userLatLng;
            if (locationProvider != null) {
                Location location = locationManager.getLastKnownLocation(locationProvider);
                if (location == null) {
                    Log.w(getString(R.string.app_name), "Unable to retrieve location!");
                    userLatLng = new LatLng(ApiUrl.MEX_CTY_LAT, ApiUrl.MEX_CTY_LONG);
                    getFoursquares(userLatLng.latitude, userLatLng.longitude, query);
                } else {
                    userLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                    getFoursquares(userLatLng.latitude, userLatLng.longitude, query);
                }
            } else {
                userLatLng = new LatLng(ApiUrl.MEX_CTY_LAT, ApiUrl.MEX_CTY_LONG);
                getFoursquares(userLatLng.latitude, userLatLng.longitude, query);
            }
        }
    }

    SimpleRecyclerAdapter adapter = new SimpleRecyclerAdapter(R.layout.list_item_forsquare) {
        @Override
        public void onListItem(Object listItem, View view, final int position) {
            Venue venue = (Venue) listItem;

            final ImageView foursquareImageView = view.findViewById(R.id.foursquareImageView);
            TextView foursquareNameTextView = view.findViewById(R.id.foursquareNameTextView);
            TextView foursquareAddressNameTextView = view.findViewById(R.id.foursquareAddressNameTextView);

            foursquareNameTextView.setText(venue.name);
            foursquareAddressNameTextView.setText(venue.location);
            foursquareImageView.setImageBitmap(null);
            ImageCache.getImage(venue.icon, getContext(), new ImageCache.OnResponseListener() {
                @Override
                public void onResponse(Bitmap bitmap, String error) {
                    foursquareImageView.setImageBitmap(bitmap);
                }
            });
            foursquareImageView.setColorFilter(getResources().getColor(R.color.list_text_color));

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(venues != null) {
                        // Analytics event
                        ((AnalyticsApplication) getActivity().getApplication()).sendEvent("drankIt", "venue_select", venues.get(position).name);
                        sendPlace(position);
                    }
                }
            });
        }
    };
}

