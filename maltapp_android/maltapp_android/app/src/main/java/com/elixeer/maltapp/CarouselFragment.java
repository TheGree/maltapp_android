package com.elixeer.maltapp;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.alebrije.core.SimpleRecyclerAdapter;
import com.elixeer.maltapp.Models.BeerSmall;
import com.elixeer.maltapp.Models.BrewerySmall;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class CarouselFragment extends Fragment {
    ArrayList<BeerSmall> beers;
    ArrayList<BrewerySmall> breweries;
    RecyclerView beerStylesListView;
    MainActivity parent;
    int item_id;
    int item_type;
    String item_title;

    public CarouselFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        item_title = getArguments().getString("CAROUSEL_NAME");
        item_id = getArguments().getInt("CAROUSEL_ID");
        item_type = getArguments().getInt("CAROUSEL_TYPE");

        if (getActivity() instanceof MainActivity) {

            parent = (MainActivity) getActivity();
            //parent.setTab(0);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(item_title);
                actionBar.show();
            }
        }

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_carousel, container, false);

        beerStylesListView = rootView.findViewById(R.id.beerStylesListView);
        beerStylesListView.setLayoutManager(new LinearLayoutManager(getContext()));
        beerStylesListView.setAdapter(adapter);

        switch (item_type){
            case 1: {
                getBeerImages();
                break;
            }
            case 2: {
                getBreweries();
                break;
            }
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("Carousel");
    }

    private void openBeerDetails(int position) {
        if (parent != null) {
            Fragment beerFragment = new BeerFragment();
            Bundle arguments = new Bundle();
            arguments.putInt("BEER_ID", beers.get(position).id);
            arguments.putString("TITLE", beers.get(position).brewery_name);
            beerFragment.setArguments(arguments);
            parent.pushFragment(beerFragment);
        }
    }

    private void openBreweryDetails(int position) {
        if (parent != null) {
            Fragment breweryFragment = new BreweryFragment();
            Bundle arguments = new Bundle();
            arguments.putInt("BREWERY_ID", breweries.get(position).id);
            breweryFragment.setArguments(arguments);
            parent.pushFragment(breweryFragment);
        }
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void getBeerImages() {
        String url = ApiUrl.BEER_SEARCH + "&brewery_id=" + item_id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonBeers = jsonResponse.getJSONArray("beers");
                            beers = BeerSmall.beersFromJSONArray(jsonBeers);
                            if (beers.size() < 1) {
                                Log.i(getString(R.string.app_name), "No beers found");
                            } else {
                                adapter.setList(beers);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(CarouselFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void getBreweries() {
        String url = ApiUrl.PLACE_BREWERIES + "&place_id=" + item_id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonBreweries = jsonResponse.getJSONArray("breweries");
                            breweries = BrewerySmall.breweriesFromJSONArray(jsonBreweries);
                            if (breweries.size() < 1) {
                                Log.i(getString(R.string.app_name), "No beers found");
                            } else {
                                adapter.setList(breweries);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    SimpleRecyclerAdapter adapter = new SimpleRecyclerAdapter(R.layout.list_item_carousel) {
        @Override
        public void onListItem(Object listItem, View view, final int position) {
            final ImageView carouselBeerImageView = view.findViewById(R.id.carouselBeerImageView);
            TextView carouselBeerNameTextView = view.findViewById(R.id.carouselBeerNameTextView);

            switch (item_type) {
                case 1: {
                    BeerSmall beer = (BeerSmall) listItem;

                    carouselBeerNameTextView.setText(beer.name);
                    if (beer.thumb.contains("/missing.png")) {
                        carouselBeerImageView.setImageResource(R.drawable.img_default_beer);
                    } else {
                        carouselBeerImageView.setImageBitmap(null);
                        ImageCache.getImage(beer.thumb, getContext(), new ImageCache.OnResponseListener() {
                            @Override
                            public void onResponse(Bitmap bitmap, String error) {
                                carouselBeerImageView.setImageBitmap(bitmap);
                            }
                        });
                    }
                    break;
                }
                case 2: {
                    BrewerySmall brewery = (BrewerySmall) listItem;

                    carouselBeerNameTextView.setText(brewery.name);
                    if (brewery.thumb.contains("/missing.png")) {
                        carouselBeerImageView.setImageResource(R.drawable.img_default_beer);
                    } else {
                        carouselBeerImageView.setImageBitmap(null);
                        ImageCache.getImage(brewery.thumb, getContext(), new ImageCache.OnResponseListener() {
                            @Override
                            public void onResponse(Bitmap bitmap, String error) {
                                carouselBeerImageView.setImageBitmap(bitmap);
                            }
                        });
                    }
                    break;
                }
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (item_type){
                        case 1: {
                            // Analytics event
                            ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Carousel", "beer_pressed", beers.get(position).name);
                            openBeerDetails(position);
                            break;
                        }
                        case 2: {
                            // Analytics event
                            ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Carousel", "brewery_pressed", breweries.get(position).name);
                            openBreweryDetails(position);
                            break;
                        }

                    }
                }
            });
        }
    };
}