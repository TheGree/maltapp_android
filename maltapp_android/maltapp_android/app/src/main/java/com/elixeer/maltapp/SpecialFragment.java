package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Models.Event;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

public class SpecialFragment extends Fragment {
    MainActivity parent;
    ImageView specialEventImage;
    //ImageButton specialEventMapButton;
    //ImageButton specialEventVoteButton;
    //ImageButton specialEventTreasureHunterButton;
    //LinearLayout specialEventTreasureHunterLinearLayout;
    ImageButton specialEventFindButton;
    ImageButton specialEventBuyButton;
    TextView specialEventDescriptionText, specialEventTitleTextView, specialEventSubtitleTextView, specialEventTreasureHunterTextView;
    ImageButton specialEventInfoButton;

    int user_id;
    int event_id;
    int beer_id;
    Event event;
    String event_name;
    String breweries;
    String places;

    public SpecialFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        if (sharedPreferences.contains("userId")) {
            user_id = sharedPreferences.getInt("userId", -1);
        }

        event_id = getArguments().getInt("SPECIAL_EVENT_ID", -1);
        event_name = getArguments().getString("SPECIAL_EVENT_NAME", "");

        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(event_name);
                actionBar.show();
            }
        }

        View rootView = inflater.inflate(R.layout.fragment_special, container, false);

        specialEventImage = rootView.findViewById(R.id.specialEventImage);
        /*specialEventMapButton = rootView.findViewById(R.id.specialEventMapButton);
        specialEventVoteButton = rootView.findViewById(R.id.specialEventVoteButton);
        specialEventTreasureHunterButton = rootView.findViewById(R.id.specialEventTreasureHunterButton);
        specialEventTreasureHunterLinearLayout = rootView.findViewById(R.id.specialEventTreasureHunterLinearLayout);
        specialEventTreasureHunterTextView = rootView.findViewById(R.id.specialEventTreasureHunterTextView);*/
        specialEventFindButton = rootView.findViewById(R.id.specialEventFindButton);
        specialEventBuyButton = rootView.findViewById(R.id.specialEventBuyButton);
        specialEventDescriptionText = rootView.findViewById(R.id.specialEventDescriptionText);
        specialEventTitleTextView = rootView.findViewById(R.id.specialEventTitleTextView);
        specialEventSubtitleTextView = rootView.findViewById(R.id.specialEventSubtitleTextView);
        specialEventInfoButton = rootView.findViewById(R.id.specialEventInfoButton);

        /*specialEventMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(event != null) {
                    if (parent != null) {
                        Fragment specialMapFragment = new SpecialMapFragment();
                        Bundle arguments = new Bundle();
                        arguments.putSerializable("SPECIAL_EVENT", event);
                        arguments.putString("SPECIAL_BREWERIES", breweries);
                        arguments.putString("SPECIAL_PLACES", places);
                        specialMapFragment.setArguments(arguments);
                        parent.pushFragment(specialMapFragment);
                    }
                }
            }
        });*/
        /*specialEventVoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(event != null) {
                    if (parent != null) {
                        if (beer_id > 0) {
                            Fragment specialTopFragment = new SpecialTopFragment();
                            Bundle arguments = new Bundle();
                            arguments.putInt("SPECIAL_EVENT_ID", event_id);
                            arguments.putString("SPECIAL_EVENT_NAME", event_name);
                            specialTopFragment.setArguments(arguments);
                            parent.pushFragment(specialTopFragment);
                        } else {
                            Fragment specialListFragment = new SpecialListFragment();
                            Bundle arguments = new Bundle();
                            arguments.putInt("SPECIAL_EVENT_ID", event_id);
                            arguments.putString("SPECIAL_EVENT_NAME", event_name);
                            specialListFragment.setArguments(arguments);
                            parent.pushFragment(specialListFragment);
                        }
                    }
                }
            }
        });*/
        /*specialEventTreasureHunterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(event != null) {
                    if (parent != null) {
                        Fragment treasureHunterFragment = new TreasureHunterFragment();
                        Bundle arguments = new Bundle();
                        arguments.putInt("SPECIAL_EVENT_ID", event_id);
                        treasureHunterFragment.setArguments(arguments);
                        parent.pushFragment(treasureHunterFragment);
                    }
                }
            }
        });*/
       /* specialEventTreasureHunterLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(event != null) {
                    if (parent != null) {
                        Fragment treasureHunterFragment = new TreasureHunterFragment();
                        Bundle arguments = new Bundle();
                        arguments.putInt("SPECIAL_EVENT_ID", event_id);
                        treasureHunterFragment.setArguments(arguments);
                        parent.pushFragment(treasureHunterFragment);
                    }
                }
            }
        });*/
        /*specialEventTreasureHunterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(event != null) {
                    if (parent != null) {
                        Fragment treasureHunterFragment = new TreasureHunterFragment();
                        Bundle arguments = new Bundle();
                        arguments.putInt("SPECIAL_EVENT_ID", event_id);
                        treasureHunterFragment.setArguments(arguments);
                        parent.pushFragment(treasureHunterFragment);
                    }
                }
            }
        });*/
        specialEventFindButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(event != null) {
                    try {
                        String uri = "geo: " + event.latitude + "," + event.longitude + "?q=" + event.latitude + "," + event.longitude;
                        startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri)));
                    } catch (Exception e) {
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + event.latitude + "," + event.longitude));
                        startActivity(intent);
                    }
                }
            }
        });
        specialEventBuyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(event != null) {
                    Fragment webViewFragment = new WebViewFragment();
                    Bundle arguments = new Bundle();
                    arguments.putString("NETWORK", event.ticket);
                    webViewFragment.setArguments(arguments);
                    SpecialFragment.this.parent.pushFragment(webViewFragment);
                }
            }
        });
        specialEventInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(event != null) {
                    Fragment webViewFragment = new WebViewFragment();
                    Bundle arguments = new Bundle();
                    arguments.putString("NETWORK", event.website);
                    webViewFragment.setArguments(arguments);
                    SpecialFragment.this.parent.pushFragment(webViewFragment);
                }
            }
        });
        getSpecial();
        if (user_id != -1) {
            getVote();
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
    }

    private void fillEventData() {
        if(event.ticket.equals("")){
            specialEventBuyButton.setAlpha(0.50f);
            specialEventBuyButton.setClickable(false);
        }
        if(event.website.equals("")){
            specialEventInfoButton.setAlpha(0.50f);
            specialEventInfoButton.setClickable(false);
        }
        ImageCache.getImage(event.imageUrlMedium, getContext(), new ImageCache.OnResponseListener() {
            @Override
            public void onResponse(Bitmap bitmap, String error) {
                specialEventImage.setImageBitmap(bitmap);
            }
        });
        specialEventDescriptionText.setText(event.largeDescription);
        specialEventTitleTextView.setText(event.title);
        DateFormat dateFormat = new SimpleDateFormat("MMM d");
        String dateString = dateFormat.format(event.startDate) + " al " + dateFormat.format(event.endDate);
        specialEventSubtitleTextView.setText(dateString);
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(SpecialFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (parent != null) {
                            parent.pushFragment(new HomeFragment());
                        }
                    }
                });

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (parent != null) {
                    parent.pushFragment(new HomeFragment());
                }
            }
        });
        alert.show();
    }

    private void getSpecial() {
        String url = ApiUrl.SPECIAL_EVENTS;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONObject jsonEvent = jsonResponse.getJSONObject("event");
                            if (jsonEvent.getInt("id") > 0) {
                                event = new Event(jsonEvent, getContext());
                                breweries = jsonEvent.getJSONArray("breweries").toString();
                                places = jsonEvent.getJSONArray("places").toString();
                                fillEventData();
                            } else {
                                if (parent != null) {
                                    parent.pushFragment(new HomeFragment());
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getVote() {
        String url = ApiUrl.MY_VOTE + "&event_id=" + event_id + "&user_id=" + user_id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            beer_id = jsonResponse.getInt("beer_id");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }
}