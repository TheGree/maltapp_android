package com.elixeer.maltapp.Models;

import android.content.Context;
import android.content.SharedPreferences;

import com.elixeer.maltapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

public class Glass implements Serializable {

    public int id;
    public String name;
    public String description;
    public String imageUrlThumb;
    public String imageUrlMedium;

    public Glass(JSONObject jsonGlass, Context context) {
        String language = Locale.getDefault().getLanguage();
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
            if (sharedPreferences.contains("language")) {
                language = sharedPreferences.getString("language", Locale.getDefault().getLanguage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String language2 = "en";
        if (language.equals("en")) {
            language2 = "es";
        }

        this.id = jsonGlass.optInt("id", 0);
        this.name = jsonGlass.optString("name", "");
        this.description = jsonGlass.optString("description_" + language, "");
        if (this.description.equals("")) {
            this.description = jsonGlass.optString("description_" + language2, "");
        }
        this.imageUrlThumb = jsonGlass.optString("thumb", null);
        this.imageUrlMedium = jsonGlass.optString("medium", null);
    }

    public static ArrayList<Glass> glassesFromJSONArray(JSONArray jsonGlasses, Context context) {
        ArrayList<Glass> glasses = new ArrayList<>();
        for (int i = 0; i < jsonGlasses.length(); i++) {
            JSONObject jsonGlass = jsonGlasses.optJSONObject(i);
            if (jsonGlass != null) {
                glasses.add(new Glass(jsonGlass, context));
            }
        }
        return glasses;
    }

}