package com.elixeer.maltapp;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.core.SimpleListAdapter;
import com.elixeer.maltapp.Adapters.ProvidersCustomAdapter;
import com.elixeer.maltapp.Models.Glass;
import com.elixeer.maltapp.Models.Provider;
import com.elixeer.maltapp.Models.Style;
import com.elixeer.maltapp.Models.Tag;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LibrarySubSectionFragment extends Fragment {
    MainActivity parent;
    private ProvidersCustomAdapter mAdapter;
    ListView libraryListView;
    ArrayList<String> element_subtitle;
    ArrayList<String> element_description;
    ArrayList<Integer> element_image;
    ArrayList<Glass> glasses;
    ArrayList<Style> styles;
    ArrayList<Provider> providers;
    ArrayList<Tag> tags;
    int subSection;
    boolean isProvider;
    String header;
    //private static final int NOIMAGE = -1;

    public LibrarySubSectionFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        subSection = getArguments().getInt("POSITION", -1);
        isProvider = getArguments().getBoolean("IS_PROVIDER", false);
        header = getArguments().getString("TITLE", getString(R.string.library));

        if (getActivity() instanceof MainActivity) {

            parent = (MainActivity) getActivity();
            //parent.setTab(0);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(header);
                actionBar.show();
            }
        }

        View rootView = inflater.inflate(R.layout.fragment_library, container, false);

        libraryListView = rootView.findViewById(R.id.libraryListView);

        if (isProvider) {
            getProviders();
        } else {
            fillTypesElements();
        }

        libraryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (isProvider) {
                    Tag item = mAdapter.getItem(position);
                    if (item.type) {
                        for (int i = 0; i < providers.size(); i++) {
                            if (providers.get(i).id == item.id) {
                                // Analytics event
                                ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Library", "optionSelected", providers.get(i).name);
                                String tag = "";
                                for (int j = 0; j <= mAdapter.getCount(); j++) {
                                    if (!mAdapter.getItem(j).type) {
                                        tag = mAdapter.getItem(j).name;
                                    }
                                    if (j == position) {
                                        break;
                                    }
                                }
                                openProvider(providers.get(i).id, tag);
                                break;
                            }
                        }

                    }
                } else {
                    openLibraryDetail(position);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("Library");
    }

    private void fillTypesElements() {
        element_subtitle = new ArrayList<>();
        element_description = new ArrayList<>();
        element_image = new ArrayList<>();
        switch (subSection) {
            case 1: {
                getStyles();
                break;
            }
            case 2: {
                getGlasses();
                break;
            }
            case 3: {
                getFacts();
                break;
            }
            case 4: {
                getIngredients();
                break;
            }
            case 5: {
                getProcess();
                break;
            }
            case 6: {
                getReviews();
                break;
            }
            case 7: {
                getTutorials();
                break;
            }
        }

    }

    private void fillElements() {
        libraryListView.setAdapter(new SimpleListAdapter(R.layout.list_item_library, element_subtitle) {
            @Override
            public void onListItem(Object listItem, View view, int position) {
                String title = (String) listItem;
                TextView libraryNameTextView = view.findViewById(R.id.libraryNameTextView);
                libraryNameTextView.setText(title);
            }
        });
    }

    private void fillProviders() {
        tags = new ArrayList<>();
        ArrayList<Tag> Rtags = new ArrayList<>();
        for (int i = 0; i < providers.size(); i++){
            if(providers.get(i).tags.size() > 0) {
                Rtags.addAll(providers.get(i).tags);
            }
        }
        for (int i = 0; i < Rtags.size(); i++){
            boolean isFound = false;
            for (int j = 0; j < tags.size(); j++) {
                if (tags.get(j).id == Rtags.get(i).id) {
                    isFound = true;
                }
            }
            if (!isFound) {
                tags.add(Rtags.get(i));
            }
        }
        //Collections.sort(events, new EventComparator());
        mAdapter = new ProvidersCustomAdapter(getContext());
        for (int i = 0; i < tags.size(); i++) {
            mAdapter.addSectionHeaderItem(tags.get(i));
            for (int j = 0; j < providers.size(); j++) {
                for (int k = 0; k < providers.get(j).tags.size(); k++) {
                    if (providers.get(j).tags.get(k).id == tags.get(i).id) {
                        Tag tag = new Tag (providers.get(j).id, providers.get(j).name, providers.get(j).city_name, true);
                        mAdapter.addItem(tag);
                    }
                }
            }

        }
        libraryListView.setAdapter(mAdapter);
        libraryListView.setDivider(null);
    }

    private void openLibraryDetail(int position) {
        if (parent != null) {
            Fragment libraryDetailFragment = new LibraryDetailFragment();
            Bundle arguments = new Bundle();
            if (subSection == 1) {
                // Analytics event
                ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Library", "optionSelected", styles.get(position).name);

                arguments.putSerializable("STYLE", styles.get(position));
            } else if (subSection == 2) {
                // Analytics event
                ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Library", "optionSelected", glasses.get(position).name);

                arguments.putSerializable("GLASS", glasses.get(position));
            } else {
                // Analytics event
                ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Library", "optionSelected", element_subtitle.get(position));

                arguments.putString("SUBTITLE", element_subtitle.get(position));
                arguments.putString("DESCRIPTION", element_description.get(position));
                if(position < element_image.size()){
                    arguments.putInt("IMAGE", element_image.get(position));
                }
            }
            libraryDetailFragment.setArguments(arguments);
            parent.pushFragment(libraryDetailFragment);
        }
    }

    private void openProvider(int provider_id, String tag) {
        if (parent != null) {
            Fragment providerFragment = new ProviderFragment();
            Bundle arguments = new Bundle();
            arguments.putInt("PROVIDER_ID", provider_id);
            arguments.putString("TAG", tag);
            providerFragment.setArguments(arguments);
            parent.pushFragment(providerFragment);
        }
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(LibrarySubSectionFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void getStyles() {
        String url = ApiUrl.STYLES;
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonStyles = jsonResponse.getJSONArray("beertypes");
                            styles = Style.stylesFromJSONArray(jsonStyles, getContext());

                            libraryListView.setAdapter(new SimpleListAdapter(R.layout.list_item_library, styles) {
                                @Override
                                public void onListItem(Object listItem, View view, int position) {
                                    Style style = (Style) listItem;
                                    TextView libraryNameTextView = view.findViewById(R.id.libraryNameTextView);
                                    libraryNameTextView.setText(style.name);
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getGlasses() {
        String url = ApiUrl.GLASSES;
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonGlasses = jsonResponse.getJSONArray("glasses");
                            glasses = Glass.glassesFromJSONArray(jsonGlasses, getContext());

                            libraryListView.setAdapter(new SimpleListAdapter(R.layout.list_item_library, glasses) {
                                @Override
                                public void onListItem(Object listItem, View view, int position) {
                                    Glass glass = (Glass) listItem;
                                    TextView libraryNameTextView = view.findViewById(R.id.libraryNameTextView);
                                    libraryNameTextView.setText(glass.name);
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getFacts() {
        element_subtitle.add(getResources().getString(R.string.whatIsABV));
        element_description.add(getResources().getString(R.string.abv_info));
        element_image.add(R.drawable.img_abv);
        element_subtitle.add(getResources().getString(R.string.whatIsIBU));
        element_description.add(getResources().getString(R.string.ibu_info));
        element_image.add(R.drawable.img_ibu);
        element_subtitle.add(getResources().getString(R.string.whatIsSRM));
        element_description.add(getResources().getString(R.string.srm_info));
        element_image.add(R.drawable.img_srm);
        element_subtitle.add(getResources().getString(R.string.whatIsOE));
        element_description.add(getResources().getString(R.string.oe_info));
        element_image.add(R.drawable.img_oe);

        fillElements();
    }

    private void getIngredients() {
        element_subtitle.add(getResources().getString(R.string.water));
        element_description.add(getResources().getString(R.string.water_info));
        element_image.add(R.drawable.img_water);
        element_subtitle.add(getResources().getString(R.string.grains));
        element_description.add(getResources().getString(R.string.grains_info));
        element_image.add(R.drawable.img_malt);
        element_subtitle.add(getResources().getString(R.string.yeast));
        element_description.add(getResources().getString(R.string.yeast_info));
        element_image.add(R.drawable.img_yeast);
        element_subtitle.add(getResources().getString(R.string.hops));
        element_description.add(getResources().getString(R.string.hops_info));
        element_image.add(R.drawable.img_hops);
        element_subtitle.add(getResources().getString(R.string.clarifyingAgent));
        element_description.add(getResources().getString(R.string.clarifyingAgent_info));

        fillElements();
    }

    private void getProcess() {
        element_subtitle.add(getResources().getString(R.string.malting));
        element_description.add(getResources().getString(R.string.malting_info));
        element_image.add(R.drawable.malt_silo);
        element_subtitle.add(getResources().getString(R.string.mashing));
        element_description.add(getResources().getString(R.string.mashing_info));
        element_image.add(R.drawable.mash_tun);
        element_subtitle.add(getResources().getString(R.string.boiling));
        element_description.add(getResources().getString(R.string.boiling_info));
        element_image.add(R.drawable.wort_copper);
        element_subtitle.add(getResources().getString(R.string.cooling));
        element_description.add(getResources().getString(R.string.cooling_info));
        element_image.add(R.drawable.wort_cooler);
        element_subtitle.add(getResources().getString(R.string.fermentation));
        element_description.add(getResources().getString(R.string.fermentation_info));
        element_image.add(R.drawable.fermentation_tank);
        element_subtitle.add(getResources().getString(R.string.maturation));
        element_description.add(getResources().getString(R.string.maturation_info));
        element_image.add(R.drawable.lauter_tun);
        element_subtitle.add(getResources().getString(R.string.bottling));
        element_description.add(getResources().getString(R.string.bottling_info));
        element_image.add(R.drawable.filling_plant);

        fillElements();
    }

    private void getReviews() {
        element_subtitle.add(getResources().getString(R.string.how_to_review_a_beer));
        element_description.add(getResources().getString(R.string.how_to_review_a_beer_info));
        element_image.add(R.drawable.img_how);
        element_subtitle.add(getResources().getString(R.string.detailed_information_on_each_rateable_attribute));
        element_description.add(getResources().getString(R.string.detailed_information_on_each_rateable_attribute_info));
        element_image.add(R.drawable.img_information);
        element_subtitle.add(getResources().getString(R.string.other_considerations_when_rating_a_beer));
        element_description.add(getResources().getString(R.string.other_considerations_when_rating_a_beer_info));
        element_image.add(R.drawable.img_others);

        fillElements();
    }

    private void getTutorials() {
        element_subtitle.add(getResources().getString(R.string.welcome_to_maltapp));
        element_description.add(getResources().getString(R.string.welcome_to_maltapp_info));
        element_image.add(R.drawable.maltapp_slogan);
        element_subtitle.add(getResources().getString(R.string.the_screens));
        element_description.add(getResources().getString(R.string.the_screens_info));
        element_image.add(R.drawable.home_new_android);
        element_subtitle.add(getResources().getString(R.string.search_beer));
        element_description.add(getResources().getString(R.string.search_beer_info));
        element_image.add(R.drawable.home_new_android);
        element_subtitle.add(getResources().getString(R.string.scan_beer));
        element_description.add(getResources().getString(R.string.scan_beer_info));
        element_image.add(R.drawable.escaner_android);
        element_subtitle.add(getResources().getString(R.string.library));
        element_description.add(getResources().getString(R.string.library_info));
        element_image.add(R.drawable.library_andorid);
        element_subtitle.add(getResources().getString(R.string.title_news));
        element_description.add(getResources().getString(R.string.news_info));
        element_image.add(R.drawable.lista_noticias_android);
        element_subtitle.add(getResources().getString(R.string.title_nearby));
        element_description.add(getResources().getString(R.string.nearby_info));
        element_image.add(R.drawable.cerca_de_ti_android);
        element_subtitle.add(getResources().getString(R.string.title_profile));
        element_description.add(getResources().getString(R.string.profile_info));
        element_image.add(R.drawable.perfil_android);
        element_subtitle.add(getResources().getString(R.string.experience_points));
        element_description.add(getResources().getString(R.string.experience_points_info));
        element_image.add(R.drawable.premium_android);

        fillElements();
    }

    private void getProviders() {
        String url = ApiUrl.PROVIDERS + "&activate=true";

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonProviders = jsonResponse.getJSONArray("providers");
                            providers = Provider.providersFromJSONArray(jsonProviders, getContext());
                            if (providers.size() < 1) {
                                Log.i(getString(R.string.app_name), "No providers found");
                            }
                            fillProviders();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }
}