package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.core.SimpleRecyclerAdapter;
import com.elixeer.maltapp.Models.Beer;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by alebrije2 on 1/25/16.
 *
 */
public class FavFragment extends Fragment {

    MainActivity parent;
    RecyclerView favListView;
    ArrayList<Beer> beers;
    int user_id;

    public FavFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {

            parent = (MainActivity) getActivity();
            //parent.setTab(0);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.title_my_favorites);
                actionBar.show();
            }
        }

        user_id = getArguments().getInt("USER_ID");

        View rootView = inflater.inflate(R.layout.fragment_fav, container, false);

        favListView = rootView.findViewById(R.id.favListView);
        favListView.setLayoutManager(new LinearLayoutManager(getContext()));
        favListView.setAdapter(adapter);

        getBeers();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("MyFavorites");
    }

    private void openBeerDetails(int position) {
        if (parent != null) {
            Fragment beerFragment = new BeerFragment();
            Bundle arguments = new Bundle();
            arguments.putSerializable("BEER", beers.get(position));
            arguments.putBoolean("PRELOAD", true);
            beerFragment.setArguments(arguments);
            parent.pushFragment(beerFragment);
        }
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(FavFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void getBeers() {
        String url = ApiUrl.BEER_FAV_LIST + "&user_id=" + user_id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonBeers = jsonResponse.getJSONArray("beers");
                            beers = Beer.beersFromJSONArray(jsonBeers, getContext());
                            if (beers.size() < 1) {
                                Log.i(getString(R.string.app_name), "No beers found");
                            } else {
                                adapter.setList(beers);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    SimpleRecyclerAdapter adapter = new SimpleRecyclerAdapter(R.layout.list_item_fav) {
        @Override
        public void onListItem(Object listItem, View view, final int position) {
            Beer beer = (Beer) listItem;

            //ImageView favImageView = view.findViewById(R.id.favImageView);
            TextView favBeerNameTextView = view.findViewById(R.id.favBeerNameTextView);
            TextView favBreweryNameTextView = view.findViewById(R.id.favBreweryNameTextView);

            favBeerNameTextView.setText(beer.name);
            favBreweryNameTextView.setText(beer.brewery_name);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("favorites", "beer_pressed", beers.get(position).name);

                    openBeerDetails(position);
                }
            });
        }
    };
}


