package com.elixeer.maltapp;

import android.content.Intent;

/**
 * Created by Alebrije05 on 07/06/2017.
 */

public abstract class iabHelperDelegate {
    public abstract void didCompleteTransaction(int requestCode, int resultCode, Intent data);
}
