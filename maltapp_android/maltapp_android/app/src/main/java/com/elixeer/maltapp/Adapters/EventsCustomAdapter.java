package com.elixeer.maltapp.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.elixeer.maltapp.Models.Event;
import com.elixeer.maltapp.R;

import org.apache.commons.lang3.text.WordUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TreeSet;

public class EventsCustomAdapter extends BaseAdapter {

    private Context context;

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;

    private ArrayList<Event> mData = new ArrayList<Event>();
    private TreeSet<Integer> sectionHeader = new TreeSet<Integer>();

    private LayoutInflater mInflater;

    public EventsCustomAdapter(Context context) {
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItem(final Event item) {
        mData.add(item);
        notifyDataSetChanged();
    }

    public void addSectionHeaderItem(final Event item) {
        mData.add(item);
        sectionHeader.add(mData.size() - 1);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return sectionHeader.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Event getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        int rowType = getItemViewType(position);

        holder = new ViewHolder();

        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
        SimpleDateFormat simpleDateFormatDay = new SimpleDateFormat("dd", Locale.getDefault());
        SimpleDateFormat simpleDateFormatMonth = new SimpleDateFormat("MMMM", Locale.getDefault());

        switch (rowType) {
            case TYPE_ITEM:
                convertView = mInflater.inflate(R.layout.list_item_nearby, null);

                holder.nearbyNameTextView = (TextView) convertView.findViewById(R.id.nearbyNameTextView);
                holder.addressTextView = (TextView) convertView.findViewById(R.id.addressTextView);
                holder.distanceTextView = (TextView) convertView.findViewById(R.id.distanceTextView);

                holder.nearbyNameTextView.setText(mData.get(position).title);
                holder.addressTextView.setText(mData.get(position).address);
                String eventDate = String.format(context.getString(R.string.event_date),
                        WordUtils.capitalize(simpleDateFormatMonth.format(mData.get(position).startDate)),
                        simpleDateFormatDay.format(mData.get(position).startDate));
                holder.distanceTextView.setText(eventDate);
                break;

            case TYPE_SEPARATOR:
                convertView = mInflater.inflate(R.layout.list_item_nearby_separator, null);
                holder.nearbySeparatorTextView = (TextView) convertView.findViewById(R.id.nearbySeparatorTextView);
                holder.nearbySeparatorTextView.setText(simpleDateFormatMonth.format(mData.get(position).startDate));
                break;
        }
        convertView.setTag(holder);

        return convertView;
    }

    public static class ViewHolder {
        public TextView nearbyNameTextView;
        public TextView addressTextView;
        public TextView distanceTextView;
        public TextView nearbySeparatorTextView;
    }

}