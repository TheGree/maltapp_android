package com.elixeer.maltapp.Models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class City implements Serializable {
    public int id;
    public String name;
    public int stateId;

    public City(JSONObject jsonCity) {
        this.id = jsonCity.optInt("id", 0);
        this.name = jsonCity.optString("name", "");
        this.stateId = jsonCity.optInt("state_id", 0);
    }

    public static ArrayList<City> citiesFromJSONArray(JSONArray jsonCities) {
        ArrayList<City> cities = new ArrayList<>();
        for (int i = 0; i < jsonCities.length(); i++) {
            JSONObject jsonCity = jsonCities.optJSONObject(i);
            if (jsonCity != null) {
                cities.add(new City(jsonCity));
            }
        }
        return cities;
    }
}
