package com.elixeer.maltapp.Models;

import android.content.Context;
import android.content.SharedPreferences;

import com.elixeer.maltapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

public class Badge implements Serializable {

    public int id;
    public String name;
    public int order_index;
    public String description;
    public ArrayList<Integer> points;
    public boolean only_beer;
    public boolean is_param;
    public int badgetype_id;
    public int index;
    public String medium;

    public Badge(JSONObject jsonBadge, Context context) {
        String language = Locale.getDefault().getLanguage();
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
            if (sharedPreferences.contains("language")) {
                language = sharedPreferences.getString("language", Locale.getDefault().getLanguage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String language2 = "en";
        if (language.equals("en")) {
            language2 = "es";
        }

        this.id = jsonBadge.optInt("id", 0);
        this.name = jsonBadge.optString("name_" + language, "");
        if (this.name.equals("")) {
            this.name = jsonBadge.optString("name_" + language2, "");
        }
        this.order_index = jsonBadge.optInt("order_index", 0);
        this.description = jsonBadge.optString("description_" + language, "");
        if (this.description.equals("")) {
            this.description = jsonBadge.optString("description_" + language2, "");
        }
        String[] items = jsonBadge.optString("points", "").split("-");
        this.points = new ArrayList<>();
        for (String item : items) {
            this.points.add(Integer.parseInt(item));
        }
        this.only_beer = jsonBadge.optBoolean("only_beer", false);
        this.is_param = jsonBadge.optBoolean("is_param", false);
        this.badgetype_id = jsonBadge.optInt("badgetype_id", 0);
        this.index = jsonBadge.optInt("index", 0);
        this.medium = jsonBadge.optString("medium", null);
    }

    public static ArrayList<Badge> badgesFromJSONArray(JSONArray jsonBadges, Context context) {
        ArrayList<Badge> badges = new ArrayList<>();
        for (int i = 0; i < jsonBadges.length(); i++) {
            JSONObject jsonBadge = jsonBadges.optJSONObject(i);
            if (jsonBadge != null) {
                badges.add(new Badge(jsonBadge, context));
            }
        }
        return badges;
    }

}
