package com.elixeer.maltapp;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.alebrije.images.ImagePicker;
import com.elixeer.maltapp.Handlers.MaltappDataHandler;
import com.elixeer.maltapp.Handlers.PointsManager;
import com.elixeer.maltapp.Models.City;
import com.elixeer.maltapp.Models.Country;
import com.elixeer.maltapp.Models.State;
import com.elixeer.maltapp.Models.User;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class RegisterFragment extends Fragment {
    MainActivity parent;
    ImageView registerImageView;
    EditText registerNameText, registerLastNameText, registerEmailText, registerPasswordText, registerCouponText;
    TextView registerBirthDateText;
    Spinner registerCountrySpinner, registerStateSpinner, registerCitySpinner, registerGenderSpinner;
    Button registerLoginButton, registerGenderButton, registerCountryButton, registerstateButton, registerCityButton, registerButton;//,registerCouponButton;
    Dialog registerBirthDatePicker;
    User user;
    ArrayList<Country> countries;
    ArrayList<State> states;
    ArrayList<City> cities;
    ArrayList<String> genders;
    Bitmap bitmap;
    String nameTextField, emailTextField, LastNameTextField, passwordTextField, couponTextField, cityNameTemp, params, facebookId;
    Typeface type;
    Boolean flagfisrtTimeGender = true, flagfisrtTimeState = true, flagfisrtTimeCountry = true;
    private CallbackManager callbackManager;
    private String appName;

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 112;
    private static final int SELECT_PICTURE = 1;
    int user_id, cityIdTemp, lastCountry, lastCity, lastState;
    public static final int PICK_IMAGE = 1;
    private enum GenderNames {
        Undefined(0), Male(1), Female(2);

        private final int value;

        GenderNames(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    GenderNames gender;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        user_id = sharedPreferences.getInt("userId", -1);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        appName = getString(R.string.app_name);
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            //parent.setTab(3);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                if (user_id > 0) {
                    actionBar.setTitle(R.string.title_edit_profile);
                } else {
                    actionBar.setTitle(R.string.title_create_profile);
                }
                actionBar.show();
            }
        }

        genders = new ArrayList<>();
        genders.add(getString(R.string.notdefined));
        genders.add(getString(R.string.man));
        genders.add(getString(R.string.woman));
        gender = GenderNames.Undefined;
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_register, container, false);

        registerLoginButton = rootView.findViewById(R.id.registerLoginButton);
        registerBirthDateText = rootView.findViewById(R.id.registerBirthDateText);
        registerImageView = rootView.findViewById(R.id.registerImageView);
        registerNameText = rootView.findViewById(R.id.registerNameText);
        registerLastNameText = rootView.findViewById(R.id.registerLastNameText);
        registerEmailText = rootView.findViewById(R.id.registerEmailText);
        registerPasswordText = rootView.findViewById(R.id.registerPasswordText);
        registerCouponText = rootView.findViewById(R.id.registerCouponText);
        registerCountrySpinner = rootView.findViewById(R.id.registerCountrySpinner);
        registerStateSpinner = rootView.findViewById(R.id.registerStateSpinner);
        registerCitySpinner = rootView.findViewById(R.id.registerCitySpinner);
        registerGenderSpinner = rootView.findViewById(R.id.registerGenderSpinner);
        //registerCouponButton = rootView.findViewById(R.id.registerCouponButton);
        registerButton = rootView.findViewById(R.id.registerButton);
        registerGenderButton = rootView.findViewById(R.id.registerGenderButton);
        registerCountryButton = rootView.findViewById(R.id.registerCountryButton);
        registerstateButton = rootView.findViewById(R.id.registerstateButton);
        registerCityButton = rootView.findViewById(R.id.registerCityButton);

        type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helveticaneueltcom_roman.ttf");

        registerLoginButton.setTypeface(type);
        registerBirthDateText.setTypeface(type);
        registerNameText.setTypeface(type);
        registerLastNameText.setTypeface(type);
        registerEmailText.setTypeface(type);
        registerPasswordText.setTypeface(type);
        registerCouponText.setTypeface(type);
        //registerCouponButton.setTypeface(type);
        registerButton.setTypeface(type);
        registerGenderButton.setTypeface(type);
        registerCountryButton.setTypeface(type);
        registerstateButton.setTypeface(type);
        registerCityButton.setTypeface(type);

        LinearLayout loginSectionLinearLayout = rootView.findViewById(R.id.loginSectionLinearLayout);
        LinearLayout promotionCodeLinearLayout = rootView.findViewById(R.id.promotionCodeLinearLayout);

        ArrayAdapter<String> adapter_gender = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, genders) {
            @NonNull
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTypeface(type);
                return v;
            }

            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                ((TextView) v).setTypeface(type);
                return v;
            }
        };

        adapter_gender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        registerGenderSpinner.setAdapter(adapter_gender);

        loginSectionLinearLayout.setVisibility(user_id == -1 ? View.VISIBLE : View.GONE);
        promotionCodeLinearLayout.setVisibility(user_id == -1 ? View.VISIBLE : View.GONE);

        registerImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        && android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                } else {
                    choosePicture();
                }
            }
        });
        registerLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parent != null) {
                    parent.pushFragment(new LoginFragment());
                }
            }
        });


        registerGenderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerGenderSpinner.setVisibility(View.VISIBLE);
                registerGenderButton.setVisibility(View.GONE);
                registerGenderSpinner.performClick();
            }
        });

        registerGenderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               if(flagfisrtTimeGender){
                   flagfisrtTimeGender = false;
               }else {
                   if (position < genders.size()) {
                       gender = GenderNames.values()[position];
                       //registerGenderButton.setText(gender.name());
                       //registerGenderButton.setTextColor(getResources().getColor(R.color.newsDate));

                   } else {
                       Log.e(appName, "Gender selected beyond bounds");
                   }
                   //registerGenderSpinner.setVisibility(View.GONE);
                   //registerGenderButton.setVisibility(View.VISIBLE);
               }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.i(appName, "No gender selected");
                //registerGenderSpinner.setVisibility(View.GONE);
                //registerGenderButton.setVisibility(View.VISIBLE);
            }
        });

        registerCountryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerCountrySpinner.setVisibility(View.VISIBLE);
                registerCountryButton.setVisibility(View.GONE);
                registerCountrySpinner.performClick();
            }
        });

        registerCountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(flagfisrtTimeCountry){
                    flagfisrtTimeCountry = false;
                }else {
                    if (position < countries.size()) {
                        int countryId = countries.get(position).id;
                        if (countryId != lastCountry) {
                            getStates(countryId);
                            lastCountry = countryId;
                            //registerCountryButton.setText(registerCountrySpinner.getSelectedItem().toString());
                            //registerCountryButton.setTextColor(getResources().getColor(R.color.newsDate));
                            registerstateButton.setVisibility(View.VISIBLE);
                            registerStateSpinner.setVisibility(View.GONE);
                            registerstateButton.setText(R.string.select_state_spinner);
                            registerstateButton.setTextColor(getResources().getColor(R.color.hint_color));
                            registerCityButton.setVisibility(View.VISIBLE);
                            registerCitySpinner.setVisibility(View.GONE);
                            registerCityButton.setText(R.string.select_city_spinner);
                            registerCityButton.setTextColor(getResources().getColor(R.color.hint_color));
                        }
                    } else {
                        Log.e(appName, "Country selected beyond bounds");
                    }
                    //registerCountryButton.setVisibility(View.VISIBLE);
                    //registerCountrySpinner.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.i(appName, "No country selected");
                //registerCountrySpinner.setVisibility(View.GONE);
                //registerCountryButton.setVisibility(View.VISIBLE);
            }
        });

        registerstateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerStateSpinner.setVisibility(View.VISIBLE);
                registerstateButton.setVisibility(View.GONE);
                registerStateSpinner.performClick();
            }
        });

        registerStateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(flagfisrtTimeState){
                    flagfisrtTimeState = false;
                }else {
                    if (position < states.size()) {
                        int stateId = states.get(position).id;
                        if (stateId != lastState) {
                            getCities(stateId);
                            //getStates(stateId);
                            lastState = stateId;
                            //registerstateButton.setText(registerStateSpinner.getSelectedItem().toString());
                            //registerstateButton.setTextColor(getResources().getColor(R.color.newsDate));
                            registerCitySpinner.setVisibility(View.GONE);
                            registerCityButton.setVisibility(View.VISIBLE);
                            registerCityButton.setText(R.string.select_city_spinner);
                            registerCityButton.setTextColor(getResources().getColor(R.color.hint_color));
                        }
                    } else {
                        Log.e(appName, "State selected beyond bounds");
                    }
                    //registerStateSpinner.setVisibility(View.GONE);
                    //registerstateButton.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.i(appName, "No state selected");
                //registerStateSpinner.setVisibility(View.GONE);
                //registerstateButton.setVisibility(View.VISIBLE);
            }
        });

        registerCityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: OPEN SPINER
                registerCitySpinner.setVisibility(View.VISIBLE);
                registerCityButton.setVisibility(View.GONE);
                registerCitySpinner.performClick();
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline()) {
                    OnRegisterButtonClicked();
                } else {
                    notInternetMSG();
                }
            }
        });

        /*
        registerCouponButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline()) {
                    if (!registerCouponText.getText().equals("")) {
                        validateCoupon();
                    }
                } else {
                    notInternetMSG();
                }
            }
        });
        */

        registerBirthDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (registerBirthDatePicker != null) {
                    registerBirthDatePicker.show();
                }
            }
        });

        lastCountry = lastState = lastCity = 0;

        if (user_id > 0) {
            getUser(user_id);
        } else {
            countries = new ArrayList<>();
            getCountries();
        }

        registerBirthDatePicker = new DatePickerDialog(getContext(), birthDateListener, 1999, 1, 1);
        /*
        if (registerBirthDatePicker == null) {
            registerBirthDatePicker = onCreateDialog(999);
        }
        */


        callbackManager = CallbackManager.Factory.create();

        LoginButton loginButton = rootView.findViewById(R.id.facebook_login_button);
        loginButton.setReadPermissions("email");
        loginButton.setFragment(this);

        //LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                facebookId = loginResult.getAccessToken().getUserId();
                loginWithFacebook(facebookId, loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.e(appName, "Facebook cancel");
            }

            @Override
            public void onError(FacebookException error) {
                reportError(error.toString());
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        if (bitmap != null) {
            registerImageView.setImageBitmap(bitmap);
        }
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("EditAccount");
    }

    @Override
    public void onPause() {
        registerImageView.setImageBitmap(null);
        super.onPause();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, menuInflater);
        if (user_id > 0) {
            menuInflater.inflate(R.menu.menu_logout, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_log) {
            if (parent != null) {
                logoutDialog();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE && (android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                || android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)) {
            choosePicture();
        }
    }

    private void choosePicture(){
        //Intent chooseImageIntent = ImagePicker.getPickImageIntent(getContext());
        //startActivityForResult(chooseImageIntent, SELECT_PICTURE);
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    private void fillUserData() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MMM/yyyy", Locale.getDefault());
        registerNameText.setText(user.firstName);
        registerLastNameText.setText(user.lastName);
        registerEmailText.setText(user.email);
        if (user.imageUrlThumb != null) {
            if (!user.imageUrlThumb.contains("/missing.png")) {
                registerImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                ImageCache.getImage(user.imageUrlThumb, getContext(), new ImageCache.OnResponseListener() {
                    @Override
                    public void onResponse(Bitmap bitmap, String error) {
                        registerImageView.setImageBitmap(bitmap);
                    }
                });
            }
        }
        gender = GenderNames.values()[user.gender];
        registerBirthDateText.setText(simpleDateFormat.format(user.birthday));
        registerGenderSpinner.setSelection(gender.getValue());
        registerGenderButton.setText(registerGenderSpinner.getSelectedItem().toString());
        registerGenderButton.setTextColor(getResources().getColor(R.color.newsDate));
        lastCity = user.cityId;
        lastCountry = user.countryId;
        lastState = user.stateId;
        getCountries(true);
    }

    private void reportError(String error) {
        Log.e(appName, "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                
            }
        });
        alert.show();
    }

    private void getUser(int userId) {
        String url = ApiUrl.GET_USER + userId + ".json" + ApiUrl.ACCESS_TOKEN;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONObject jsonUser = jsonResponse.getJSONObject("user");
                            user = new User(jsonUser);
                            fillUserData();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void validateCoupon() {
        String url = ApiUrl.VALIDATE_COUPON + "&coupon=" + registerCouponText.getText();

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonCoupons = jsonResponse.getJSONArray("coupons");

                            if (jsonCoupons.length() > 0) {
                                createAccountServer();
                                //registerCouponText.setBackground(getResources().getDrawable(R.drawable.rounded_bg_stroke_green));
                            } else {
                                couponMsj();
                                //registerCouponText.setBackground(getResources().getDrawable(R.drawable.rounded_bg_stroke_red));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getCountries() {
        getCountries(false);
    }

    private void getCountries(final boolean setCountry) {
        countries = new ArrayList<>();
        String url = ApiUrl.COUNTRIES;
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonCountries = jsonResponse.getJSONArray("countries");
                            countries = Country.countriesFromJSONArray(jsonCountries);

                            ArrayList<String> countryNames = new ArrayList<>();
                            for (Country country : countries) {
                                countryNames.add(country.name);
                            }

                            ArrayAdapter<String> adapter_countries = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, countryNames) {
                                @NonNull
                                public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                                    View v = super.getView(position, convertView, parent);
                                    ((TextView) v).setTypeface(type);
                                    return v;
                                }

                                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                                    View v = super.getDropDownView(position, convertView, parent);
                                    ((TextView) v).setTypeface(type);
                                    return v;
                                }
                            };

                            adapter_countries.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            registerCountrySpinner.setAdapter(adapter_countries);

                            if (setCountry) {
                                int selected = getSelectedCountry(user.countryId);
                                if (selected != -1)
                                    registerCountrySpinner.setSelection(selected);
                                    registerCountryButton.setText(registerCountrySpinner.getSelectedItem().toString());
                                    registerCountryButton.setTextColor(getResources().getColor(R.color.newsDate));
                                getStates(user.countryId, true);
                            } else {
                                getStates(countries.get(0).id);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getStates(int countryId, final boolean setState) {
        states = new ArrayList<>();

        String url = ApiUrl.STATES + "&country_id=" + countryId + "&all=1";
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonStates = jsonResponse.getJSONArray("states");
                            states = State.statesFromJSONArray(jsonStates);

                            ArrayList<String> stateNames = new ArrayList<>();
                            for (State state : states) {
                                stateNames.add(state.name);
                            }

                            ArrayAdapter<String> adapter_states = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, stateNames) {
                                @NonNull
                                public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                                    View v = super.getView(position, convertView, parent);
                                    ((TextView) v).setTypeface(type);
                                    return v;
                                }

                                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                                    View v = super.getDropDownView(position, convertView, parent);
                                    ((TextView) v).setTypeface(type);
                                    return v;
                                }
                            };

                            adapter_states.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            registerStateSpinner.setAdapter(adapter_states);

                            if (setState) {
                                int selected = getSelectedState(user.stateId);
                                if (selected != -1)
                                    registerStateSpinner.setSelection(selected);
                                registerstateButton.setText(registerStateSpinner.getSelectedItem().toString());
                                registerstateButton.setTextColor(getResources().getColor(R.color.newsDate));
                                getCities(user.stateId, true);
                            } else {
                                getCities(states.get(0).id, false);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getStates(int countryId) {
        getStates(countryId, false);
    }

    private void getCities(int stateId) {
        getCities(stateId, false);
    }

    private int getSelectedCountry(int countryId) {
        int ret = -1;
        if (countries.size() > 0) {
            for (int i = 0; i < countries.size(); i++) {
                if (countries.get(i).id == countryId) {
                    ret = i;
                    break;
                }
            }
        }
        return ret;
    }

    private int getSelectedState(int stateId) {
        int ret = -1;
        if (states.size() > 0) {
            for (int i = 0; i < states.size(); i++) {
                if (states.get(i).id == stateId) {
                    ret = i;
                    break;
                }
            }
        }
        return ret;
    }

    private int getSelectedCity(int cityId) {
        int ret = -1;
        if (cities.size() > 0) {
            for (int i = 0; i < cities.size(); i++) {
                if (cities.get(i).id == cityId) {
                    ret = i;
                    break;
                }
            }
        }
        return ret;
    }

    private boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager != null && connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    private void getCities(int stateId, final boolean setCity) {
        cities = new ArrayList<>();
        String url = ApiUrl.CITIES + "&state_id=" + stateId + "&all=1";
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonCities = jsonResponse.getJSONArray("cities");
                            cities = City.citiesFromJSONArray(jsonCities);

                            ArrayList<String> cityNames = new ArrayList<>();
                            for (City city : cities) {
                                cityNames.add(city.name);
                            }

                            ArrayAdapter<String> adapter_cities = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, cityNames) {
                                @NonNull
                                public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                                    View v = super.getView(position, convertView, parent);
                                    ((TextView) v).setTypeface(type);
                                    return v;
                                }

                                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                                    View v = super.getDropDownView(position, convertView, parent);
                                    ((TextView) v).setTypeface(type);
                                    return v;
                                }
                            };

                            adapter_cities.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            registerCitySpinner.setAdapter(adapter_cities);

                            if (setCity) {
                                int selected = getSelectedCity(user.cityId);
                                if (selected != -1) {
                                    registerCitySpinner.setSelection(selected);
                                    registerCityButton.setText(registerCitySpinner.getSelectedItem().toString());
                                    registerCityButton.setTextColor(getResources().getColor(R.color.newsDate));
                                }
                                registerCitySpinner.setVisibility(View.GONE);
                                registerCityButton.setVisibility(View.VISIBLE);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void OnRegisterButtonClicked() {
        setValues();

        if (verifyFields()) {
            List<String> paramList = new ArrayList<>();
            if (!nameTextField.isEmpty()) {
                paramList.add("\"name\":\"" + nameTextField + "\"");
            }
            if (!emailTextField.isEmpty()) {
                paramList.add("\"email\":\"" + emailTextField + "\"");
            }
            if (!LastNameTextField.isEmpty()) {
                paramList.add("\"lastname\":\"" + LastNameTextField + "\"");
            }
            if (!passwordTextField.isEmpty()) {
                paramList.add("\"password\":\"" + passwordTextField + "\"");
            }
            if (!couponTextField.isEmpty()) {
                paramList.add("\"coupon\":\"" + couponTextField + "\"");
            }
            if (facebookId != null) {
                paramList.add("\"facebook_id\":\"" + facebookId + "\"");
            }
            paramList.add("\"sex\":\"" + gender.getValue() + "\"");

            paramList.add("\"not_cty\":\"false\"");
            paramList.add("\"city_name\":\"" + cityNameTemp + "\", \"city_id\":\"" + cityIdTemp + "\"");
            SimpleDateFormat format = new SimpleDateFormat("dd/MMM/yyyy", Locale.getDefault());
            try {
                if (registerBirthDateText.getText().equals("")) {
                    Date date = format.parse("01/Jan/1901");
                    paramList.add("\"birthday\":\"" + date.toString() + "\"");
                } else {
                    Date date = format.parse(registerBirthDateText.getText().toString());
                    paramList.add("\"birthday\":\"" + date.toString() + "\"");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (bitmap != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                String encodedImage = "data:image/jpg;base64," + Base64.encodeToString(b, Base64.NO_WRAP);
                paramList.add("\"image\":\"" + encodedImage + "\"");
            }
            params = "{" + TextUtils.join(",", paramList) + "}";

            if (user != null) {
                editAccountServer();
            } else {
                if (couponTextField.isEmpty()) {
                    createAccountServer();
                } else {
                    validateCoupon();
                }
            }
        }
    }

    private void editAccountServer() {
        String url = ApiUrl.GET_USER + user.id + ".json" + ApiUrl.ACCESS_TOKEN;
        String[][] headers = {{"Content-Type", "application/json"}};
        new AsyncRequestString("PUT", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            getFragmentManager().popBackStack();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .setData(params)
                .setHeaders(headers)
                .execute();
    }

    private void createAccountServer() {
        String url = ApiUrl.USERS;
        String[][] headers = {{"Content-Type", "application/json"}};
        new AsyncRequestString("POST", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONObject jsonUser = jsonResponse.getJSONObject("user");
                            user = new User(jsonUser);
                            PointsManager.addPoints(user.id, ApiUrl.POINTS_REGISTER, ApiUrl.REASON_REGISTER, getActivity());
                            SharedPreferences.Editor editor = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE).edit();
                            editor.putInt("userId", user.id);
                            editor.putBoolean("premium",false);
                            editor.apply();
                            getFragmentManager().popBackStack();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else if(responseCode == 422) {
                    alertMsj(getString(R.string.message_email_already));
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .setData(params)
                .setHeaders(headers)
                .execute();
    }

    private void registerWithFacebook(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(final JSONObject jsonObject, GraphResponse response) {
                if (response.getError() != null) {
                    Log.e(appName, response.getError().toString());
                } else {
                    try {
                        Log.d("app jsonResult", jsonObject.toString(1));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String email = jsonObject.optString("email");
                    //String profilePicture = "http://graph.facebook.com/" + facebookId + "/picture?type=large";

                    String url = ApiUrl.USERS + "&email=" + email;
                    new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
                        @Override
                        public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                            if (error == null) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                if (jsonResponse.has("error")) {
                                    reportError(jsonResponse.getString("error"));
                                } else {
                                    JSONArray users = jsonResponse.getJSONArray("users");
                                    if (users.length() > 0)  {
                                        // Update user
                                        int id = users.getJSONObject(0).getInt("id");
                                        String url = ApiUrl.GET_USER + id + ".json" + ApiUrl.ACCESS_TOKEN;
                                        String[][] headers = {{"Content-Type", "application/json"}};
                                        new AsyncRequestString("PUT", url, getActivity(), new AsyncRequestString.OnResponseListener() {
                                            @Override
                                            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                                                if (error == null) {
                                                    try {
                                                        JSONObject jsonResponse = new JSONObject(response);
                                                        //Log.d(appName, "Response " + jsonResponse.toString(1));
                                                        if (jsonResponse.has("error")) {
                                                            reportError(jsonResponse.getString("error"));
                                                        } else {
                                                            JSONObject jsonUser = jsonResponse.getJSONObject("user");
                                                            user = new User(jsonUser);
                                                            SharedPreferences.Editor editor = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE).edit();
                                                            editor.putInt("userId", user.id);
                                                            editor.putBoolean("premium",false);
                                                            editor.apply();
                                                            getFragmentManager().popBackStack();
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                } else if(error.contains("No connection")) {
                                                    notInternetMSG();
                                                } else {
                                                    reportError(error);
                                                }
                                            }
                                        })
                                                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                                                .setData("{\"facebook_id\": \"" + facebookId + "\"}")
                                                .setHeaders(headers)
                                                .execute();
                                    } else {
                                        // Fill user data
                                        String email = jsonObject.optString("email");
                                        String firstName = jsonObject.optString("first_name");
                                        String lastName = jsonObject.optString("last_name");
                                        String gender = jsonObject.optString("gender");
                                        registerNameText.setText(firstName);
                                        registerLastNameText.setText(lastName);
                                        registerEmailText.setText(email);
                                        registerGenderSpinner.setSelection(gender.charAt(0) == 'm' ? 1 : 2);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            } else if(error.contains("No connection")) {
                                notInternetMSG();
                            } else {
                                reportError(error);
                            }
                        }
                    })
                            .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                            .execute();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email, gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void loginWithFacebook(String facebookId, final AccessToken accessToken) {
        Log.i(appName, facebookId);

        String url = ApiUrl.USER_LOGIN + "&facebook_id=" + facebookId;
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    if (jsonResponse.has("error")) {
                        reportError(jsonResponse.getString("error"));
                    } else {
                        int id = jsonResponse.optInt("id", -1);
                        boolean premium = jsonResponse.optBoolean("premium", false);
                        if (id > 0) {
                            SharedPreferences.Editor editor = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE).edit();
                            editor.putInt("userId", id);
                            editor.putBoolean("premium", premium);
                            editor.apply();
                            if (parent != null) {
                                int count = getFragmentManager().getBackStackEntryCount() - 2;
                                int count_id = getFragmentManager().getBackStackEntryAt(count).getId();
                                getFragmentManager().popBackStack(count_id, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                parent.pushFragment(new ProfileFragment());
                            }
                        } else {
                            registerWithFacebook(accessToken);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        callbackManager.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                bitmap = ImagePicker.getImageFromResult(getContext(), resultCode, intent);
                registerImageView.setImageBitmap(bitmap);
                registerImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        }
        if (requestCode == PICK_IMAGE) {
            //TODO: action
        }
    }

    private DatePickerDialog.OnDateSetListener birthDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker dialog, int year, int monthOfYear, int dayOfMonth) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy/M/d", Locale.getDefault());
            SimpleDateFormat format2 = new SimpleDateFormat("dd/MMM/yyyy", Locale.getDefault());
            try {
                Date date = format.parse(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);
                registerBirthDateText.setText(format2.format(date));
            } catch (Exception e) {
                e.printStackTrace();
            }
            registerBirthDatePicker.dismiss();
        }
    };

    private void setValues() {
        params = "";
        nameTextField = registerNameText.getText().toString();
        emailTextField = registerEmailText.getText().toString();
        LastNameTextField = registerLastNameText.getText().toString();
        passwordTextField = registerPasswordText.getText().toString();
        couponTextField = registerCouponText.getText().toString();
        if (registerCitySpinner.getCount() == 0) {
            cityNameTemp = "";
            cityIdTemp = 0;
        } else {
            cityNameTemp = registerCitySpinner.getSelectedItem().toString();
            cityIdTemp = cities.get(registerCitySpinner.getSelectedItemPosition()).id;
        }
    }

    boolean verifyFields() {
        if (user == null) {
            if (!emailTextField.isEmpty() && !passwordTextField.isEmpty()) {
                if(registerCountryButton.getVisibility() == View.VISIBLE || registerstateButton.getVisibility() == View.VISIBLE || registerCityButton.getVisibility() == View.VISIBLE ){
                    alertMsj(getString(R.string.message_incomplete_spinner));
                    return false;
                }
                if (!MaltappDataHandler.isEmailValid(emailTextField)) {
                    alertMsj(getString(R.string.message_invalid_email));
                    return false;
                }
                if (passwordTextField.length() < 8) {
                    alertMsj(getString(R.string.message_invalid_password));
                    return false;
                }
            } else {
                alertMsj(getString(R.string.message_email_password_missing));
                return false;
            }
        } else {
            if (!emailTextField.isEmpty()) {
                if (!MaltappDataHandler.isEmailValid(emailTextField)) {
                    alertMsj(getString(R.string.message_invalid_email));
                    return false;
                }
            }
            if (!passwordTextField.isEmpty()) {
                if (passwordTextField.length() < 8) {
                    alertMsj(getString(R.string.message_invalid_password));
                    return false;
                }
            }
        }

        return true;
    }

    void couponMsj() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_coupon, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterFragment.this.getActivity())
                //.setTitle(getString(R.string.maltappNotice))
                .setView(pop)
                .setPositiveButton(R.string.cont, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        createAccountServer();
                    }
                })
                .setNegativeButton(R.string.retry, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    void alertMsj(String errorCode) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_msj, null);
        TextView popMsjTextView = pop.findViewById(R.id.popMsjTextView);

        popMsjTextView.setText(errorCode);

        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterFragment.this.getActivity())
                //.setTitle(getString(R.string.maltappNotice))
                .setView(pop)
                .setPositiveButton(R.string.ok, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    void logoutDialog() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_msj, null);
        TextView popMsjTextView = pop.findViewById(R.id.popMsjTextView);
        TextView titleMsj = pop.findViewById(R.id.custom_title_msj_textView);
        LinearLayout bodyTextLinear = pop.findViewById(R.id.bodyTextLinear);

        titleMsj.setText(getString(R.string.logout));
        popMsjTextView.setText(getString(R.string.logoutButtonMessage));
        bodyTextLinear.setPadding(16, 72, 16, 72);

        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterFragment.this.getActivity())
                //.setTitle(getString(R.string.maltappNotice))
                .setView(pop)
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.ok,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        facebookId = null;
                        LoginManager.getInstance().logOut();
                        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt("userId", -1);
                        editor.putBoolean("premium",false);
                        editor.apply();
                        parent.tabLayout.getTabAt(4).setIcon(getResources().getDrawable(R.drawable.ic_tab_profile));
                        Fragment profileFragment = new ProfileFragment();
                        parent.getFragmentManager().popBackStack(null, 0);
                        parent.pushFragment(profileFragment);
                        //TODO: chance icon tablayout
                    }
                } );

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }
}
