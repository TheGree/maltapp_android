package com.elixeer.maltapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class BeerInfoPageFragment extends Fragment {


    public BeerInfoPageFragment() {
        // Required empty public constructor
    }

    public static BeerInfoPageFragment newInstance(String title, String description) {
        Bundle bundle = new Bundle();
        bundle.putString("TITLE", title);
        bundle.putString("DESCRIPTION", description);

        BeerInfoPageFragment fragment = new BeerInfoPageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle arguments = getArguments();

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_beer_info_page, container, false);

        if (arguments != null) {
            TextView beerInfoTitleTextView = rootView.findViewById(R.id.beerInfoTitleTextView);
            beerInfoTitleTextView.setText(arguments.getString("TITLE", ""));

            TextView beerInfoContentTextView = rootView.findViewById(R.id.beerInfoContentTextView);
            beerInfoContentTextView.setText(arguments.getString("DESCRIPTION", ""));
        }

        return rootView;
    }

}
