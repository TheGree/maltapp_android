package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.alebrije.core.SimpleRecyclerAdapter;
import com.elixeer.maltapp.Models.TreasureHunterBU;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class TreasureHunterFragment extends Fragment {
    MainActivity parent;
    RecyclerView BreweryTreasureHunter;
    int user_id;
    ArrayList<TreasureHunterBU> breweries = new ArrayList<>();
    ArrayList<String> statusArr = new ArrayList<String>();
    private static String PREFERENCE_KEY = "arrayTreasureHunterStatus";
    private static String PREFERENCE_RESET_KEY = "codesReseted";
    public TreasureHunterFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        if (sharedPreferences.contains("userId")) {
            user_id = sharedPreferences.getInt("userId", -1);
        }
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.ensenadaTreasureHunterBtn);
                actionBar.show();
            }
        }

        View rootView = inflater.inflate(R.layout.fragment_treasure_hunter, container, false);
        BreweryTreasureHunter = rootView.findViewById(R.id.treasureHunterListView);
        BreweryTreasureHunter.setLayoutManager(new LinearLayoutManager(getContext()));
        BreweryTreasureHunter.setAdapter(adapter);
        BreweryTreasureHunter.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        getBeers();

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu_prizes, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_prizes) {
            if (parent != null) {
                showInfoPopup();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(TreasureHunterFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void getBeers() {
        if (breweries.size() == 0) {
            try {
                JSONObject obj = new JSONObject(loadJSONFromAsset());
                JSONArray jsonTreasuresHunter = obj.getJSONArray("treasureHunter");

                breweries.addAll(TreasureHunterBU.treasureHunterFromJSONArray(jsonTreasuresHunter, getContext()));

                adapter.setList(breweries);
                adapter.setSize(breweries.size());
                adapter.notifyDataSetChanged();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        for(TreasureHunterBU beer : breweries){
                            beer.setActive(true);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, 1000);

                SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
                if (sharedPreferences.contains(PREFERENCE_RESET_KEY)) {
                    //ya se reinicio la lista de cervecerias anteriores
                    if (sharedPreferences.contains(PREFERENCE_KEY)) {
                        String jsonObject = sharedPreferences.getString(PREFERENCE_KEY, null);
                        if (jsonObject != null) {
                            statusArr = new Gson().fromJson(jsonObject, new TypeToken<List<String>>(){}.getType());
                        }
                    }  else {
                        for (TreasureHunterBU brewery : breweries)
                            statusArr.add(String.valueOf(brewery.isTaken()));
                    }
                } else {
                    //borrar datos anteriores anteriores, en caso de compartir mismo codigo
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean(PREFERENCE_RESET_KEY, true);
                    editor.remove(PREFERENCE_KEY);
                    editor.apply();
                    for (TreasureHunterBU brewery : breweries){
                        statusArr.add(String.valueOf(brewery.isTaken()));
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = getActivity().getAssets().open("Local/treasureHunter.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void showInfoPopup () {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_data, null);
        TextView prizesMsjTextView = pop.findViewById(R.id.popMsjTextView);
        TextView prizesTitleTextView = pop.findViewById(R.id.dataTitleTextView);
        prizesTitleTextView.setText(R.string.treasure_hunter_prizes_title);
        prizesMsjTextView.setText(R.string.treasure_hunter_prizes_msj);

        AlertDialog.Builder builder = new AlertDialog.Builder(TreasureHunterFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }


    private void showMsjPopup (final int position, final String code) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_code, null);
        TextView popMsjTextView = pop.findViewById(R.id.popCodeMsjTextView);
        TextView poptitleTextView = pop.findViewById(R.id.popCode_title_msj_textView);
        final TextView popErrotTextView = pop.findViewById(R.id.popCodeErrorTextView);
        final EditText popTextEditText = pop.findViewById(R.id.popCodeEditText);
        popErrotTextView.setVisibility(TextView.INVISIBLE);

        final AlertDialog builder = new AlertDialog.Builder(TreasureHunterFragment.this.getActivity())
                .setView(pop)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, null)
                .show();

        Button b = builder.getButton(AlertDialog.BUTTON_POSITIVE);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Do Your thing
                String enterCode = popTextEditText.getText().toString();
                if (enterCode.equals(code)){
                    statusArr.set(position, String.valueOf(true));
                    saveArrayList(statusArr);
                    adapter.notifyDataSetChanged();
                    builder.dismiss();
                }
                else {
                    popErrotTextView.setVisibility(TextView.VISIBLE);
                }
            }
        });
    }

    public void saveArrayList(ArrayList<String> list){
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(PREFERENCE_KEY, json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    SimpleRecyclerAdapter adapter = new SimpleRecyclerAdapter(R.layout.list_item_treasure_hunter, new ArrayList<>()) {
        @Override
        public void onListItem(Object listItem, View view, final int position) {
            final TextView BreweryTextView    = view.findViewById(R.id.TreasureHunterBreweryNameTextView);
            //final TextView DetailTextView     = view.findViewById(R.id.TreasureHunterDescriptionTextView);
            final CheckBox BreweryCheckBox    = view.findViewById(R.id.TreasureHunterCheckBox);
            final Button treasureHuntGetButton  = view.findViewById(R.id.treasureHuntGetButton);
            final ImageView treasureHunterViewImage = view.findViewById(R.id.TreasureHunterSpecialEventViewImage);
            final ImageView treasureHunteGetImage = view.findViewById(R.id.treasureHunteGetImage);
            TreasureHunterBU beer = (TreasureHunterBU) listItem;

            BreweryTextView.setText(beer.getBrewery());
            //DetailTextView.setText(beer.getCode());
            BreweryCheckBox.setChecked(Boolean.valueOf(statusArr.get(position)));
            String nameImage = beer.getImage();
            treasureHunterViewImage.setImageResource(getContext().getResources().getIdentifier(nameImage, "drawable", getContext().getPackageName()));
            final String code = beer.getCode();

            if (Boolean.valueOf(statusArr.get(position))) {
                treasureHunteGetImage.setVisibility(View.VISIBLE);
                treasureHuntGetButton.setVisibility(View.GONE);
            } else {
                treasureHunteGetImage.setVisibility(View.GONE);
                treasureHuntGetButton.setVisibility(View.VISIBLE);
            }

            /*view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (breweries != null && position < breweries.size()) {
                        //String code = DetailTextView.getText().toString();
                        showMsjPopup(position, code);
                    }
                }
            });*/

            treasureHuntGetButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (breweries != null && position < breweries.size()) {
                        //String code = DetailTextView.getText().toString();
                        showMsjPopup(position, code);
                    }
                }
            });
        }
    };
}
