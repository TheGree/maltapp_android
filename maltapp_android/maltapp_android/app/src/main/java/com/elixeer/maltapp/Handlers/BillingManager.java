package com.elixeer.maltapp.Handlers;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.elixeer.maltapp.BuildConfig;
import com.elixeer.maltapp.MainActivity;
import com.elixeer.maltapp.R;
import com.elixeer.maltapp.iabHelperDelegate;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.googlesamples.billing.IabBroadcastReceiver;
import com.googlesamples.billing.IabHelper;
import com.googlesamples.billing.IabResult;
import com.googlesamples.billing.Inventory;
import com.googlesamples.billing.Purchase;
import com.googlesamples.billing.SkuDetails;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by alebrije4 on 10/27/16.
 *
 */

public abstract class BillingManager implements IabBroadcastReceiver.IabBroadcastListener {
    public static final String SKU_SUBSCRIPTION = "elixeer.maltapp.premium.subscription";
    public static final String SKU_PREMIUM = "elixeer.maltapp.premium";
    public static final String SKU_FIRSTPACK = "elixeer.maltapp.firstpack";
    public static final String SKU_SECONDPACK = "elixeer.maltapp.secondpack";
    public static final String SKU_THIRDPACK = "elixeer.maltapp.thirdpack";
    private static final String RSA_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuPh5O2DWUO9y5mWkVFpAyKw5x0JYrclORsCz8tlJ4rrNGeZeUDuwaDkm/ad5LVM8iO0i5YSxhZo4tuQjuzU6gYGrwSkitmBEhFWH4HftDumxcadVwDujmMUE+i3p6mnxLT2FmpJf8mooBEkoQWSyiMxQVPziYZKh1HDyi7BbBw1qSq7yvrzAckPJO4NmKh0bCDVlGONIOBezQNnoo0MkASKy+/x/fLpGC/98PeV1cr0BGSIn1RgLJlQwfIhAneg3/iI6Np0FFn6/RTb0m5evyrULs5G4C1Zg50wgblaMdeR1Oov9a12ubtAH7nNCDjmqWymyA6ktsv7hus5s56loPwIDAQAB";
    private Activity activity;
    private ArrayList<SkuDetails> products;
    private IabBroadcastReceiver broadcastReceiver;
    private IabHelper iabHelper;
    private String logTag;
    private String[] items = {
           // SKU_PREMIUM
            SKU_FIRSTPACK,
            SKU_SECONDPACK,
            SKU_THIRDPACK
    };
    private String[] subscriptions = {
            SKU_SUBSCRIPTION
    };

    protected BillingManager(Activity activity) {
        this.activity = activity;
        logTag = activity.getString(R.string.app_name);

        iabHelper = new IabHelper(activity, RSA_KEY);
        iabHelper.enableDebugLogging(BuildConfig.DEBUG);
        iabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (result.isSuccess()) {
                    Log.i(logTag, "In-app Billing setup started");

                    if (iabHelper == null) {
                        Log.w(logTag, "In-app Billing doesn't exists anymore");
                        return;
                    }

                    broadcastReceiver = new IabBroadcastReceiver(BillingManager.this);
                    IntentFilter broadcastFilter = new IntentFilter(IabBroadcastReceiver.ACTION);
                    BillingManager.this.activity.registerReceiver(broadcastReceiver, broadcastFilter);

                    queryAllProducts();
                } else {
                    Log.e(logTag, "Problem setting up In-app Billing: " + result);
                }
            }
        });
        if (this.activity instanceof MainActivity) {
            MainActivity parent = (MainActivity) this.activity;
            parent.delegate = new iabHelperDelegate() {
                @Override
                public void didCompleteTransaction(int requestCode, int resultCode, Intent data) {
                    if( !BillingManager.this.iabHelper.handleActivityResult(requestCode, resultCode, data)) {
                        // not handled, so handle it ourselves (here's where you'd
                        // perform any handling of activity results not related to in-app
                        // billing...
                    }
                }
            };
        }
    }

    @Override
    public void receivedBroadcast() {
        Log.d(logTag, "Received broadcast notification. Querying inventory.");
        queryInventory();
    }

    public void onDestroy() {
        if (broadcastReceiver != null) {
            activity.unregisterReceiver(broadcastReceiver);
        }
        if (iabHelper != null) {
            iabHelper.disposeWhenFinished();
            iabHelper = null;
        }
    }

    public void purchase(String sku) {
        if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(activity) == ConnectionResult.SUCCESS) {
            try {
                iabHelper.launchPurchaseFlow(activity, sku, 10001, new IabHelper.OnIabPurchaseFinishedListener() {
                    @Override
                    public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                        if (result.isFailure()) {
                            Log.e(logTag, "Error purchasing: " + result);
                        } else {
                            Log.i(logTag, "Purchased: " + purchase.getSku());
                            onPurchase(purchase);
                        }
                    }
                }, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.e(logTag, "Google Play Services are not available");
        }
    }

    public void consume(Purchase purchase) {
        try {
            iabHelper.consumeAsync(purchase, new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(Purchase purchase, IabResult result) {
                    if (result.isSuccess()) {
                        Log.i(logTag, "Purchase consumed: " + purchase.getSku());
                        onConsume(purchase);
                    } else {
                        Log.e(logTag, "Error consuming purchase " + purchase.getSku() + ": " + result);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void queryAllProducts() {
        if (iabHelper == null) {
            Log.w(logTag, "In-app Billing doesn't exists anymore");
            return;
        }
        try {
            iabHelper.queryInventoryAsync(true, Arrays.asList(items), Arrays.asList(subscriptions), new IabHelper.QueryInventoryFinishedListener() {
                public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                    if (result.isFailure()) {
                        Log.e(logTag, "Error retrieving inventory: " + result);
                    } else {
                        products = new ArrayList<>();
                        if (items != null) {
                            for (String sku : items) {
                                products.add(inventory.getSkuDetails(sku));
                            }
                        }
                        if (subscriptions != null) {
                            for (String sku : subscriptions) {
                                products.add(inventory.getSkuDetails(sku));
                            }
                        }
                    }
                    queryInventory();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void queryInventory() {
        if (iabHelper == null) {
            Log.w(logTag, "In-app Billing doesn't exists anymore");
            return;
        }
        try {
            iabHelper.queryInventoryAsync(new IabHelper.QueryInventoryFinishedListener() {
                public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                    if (result.isFailure()) {
                        Log.e(logTag, "Error retrieving inventory: " + result);
                    } else {
                        if (items != null) {
                            for (String sku : items) {
                                if (inventory.hasPurchase(sku)) {
                                    Purchase store = inventory.getPurchase(sku);
                                    onInventory (store);

                                }
                            }
                        }
                        if (subscriptions != null) {
                            for (String sku : subscriptions) {
                                if (inventory.hasPurchase(sku)) {
                                    onInventory(inventory.getPurchase(sku));
                                }
                            }
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public SkuDetails getProductBySku(String sku) {
        if (products != null) {
            for (SkuDetails product : products) {
                if (product != null && product.getSku().equals(sku)) {
                    return product;
                }
            }
        }
        return null;
    }

    public abstract void onConsume(Purchase purchase);
    public abstract void onInventory(Purchase purchase);
    public abstract void onPurchase(Purchase purchase);
}
