package com.elixeer.maltapp.Models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Rankings implements Serializable {

    public int id;
    public int rank;
    public String comment;
    public int userId;
    public String userName;
    public String userLastname;
    public String userImg;
    public int beerId;
    public String beerName;
    public String beerImg;
    public boolean beerApproved;
    public String breweryName;
    public boolean breweryApproved;
    public Date created;
    public Date updated;

    public Rankings(JSONObject jsonRanking) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

        this.id = jsonRanking.optInt("id", 0);
        this.rank = jsonRanking.optInt("rank", 0);
        this.comment = jsonRanking.optString("comment", "");
        this.userId = jsonRanking.optInt("user_id", 0);
        this.userName = jsonRanking.optString("user_name", "");
        this.userLastname = jsonRanking.optString("user_lastname", "");
        this.userImg = jsonRanking.optString("user_img", null);
        this.beerId = jsonRanking.optInt("beer_id", 0);
        this.beerName = jsonRanking.optString("beer_name", "");
        this.beerImg = jsonRanking.optString("beer_img", null);
        this.beerApproved = jsonRanking.optBoolean("beer_approved", false);
        this.breweryName = jsonRanking.optString("brewery_name", "");
        this.breweryApproved = jsonRanking.optBoolean("brewery_approved", false);
        String created = jsonRanking.optString("created_at", null);
        this.created = simpleDateFormat.parse(created, new ParsePosition(0));
        String updated = jsonRanking.optString("updated_at", null);
        this.updated = simpleDateFormat.parse(updated, new ParsePosition(0));
    }

    public static ArrayList<Rankings> rankingsFromJSONArray(JSONArray jsonRankings) {
        ArrayList<Rankings> rankings = new ArrayList<>();
        for (int i = 0; i < jsonRankings.length(); i++) {
            JSONObject jsonRanking = jsonRankings.optJSONObject(i);
            if (jsonRanking != null) {
                rankings.add(new Rankings(jsonRanking));
            }
        }
        return rankings;
    }

}