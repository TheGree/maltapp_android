package com.elixeer.maltapp.Models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class BrewerySmall implements Serializable {

    public int id;
    public String name;
    public int stand;
    public int posX;
    public int posY;
    public String city;
    public String state;
    public String country;
    public boolean approved;
    public String thumb;

    public BrewerySmall(JSONObject jsonBrewery) {
        this.id = jsonBrewery.optInt("id", 0);
        this.name = jsonBrewery.optString("name", "");
        this.stand = jsonBrewery.optInt("stand", -1);
        this.posX = jsonBrewery.optInt("posX", -1);
        this.posY = jsonBrewery.optInt("posY", -1);
        this.city = jsonBrewery.optString("city_name", "");
        this.state = jsonBrewery.optString("state_name", "");
        this.country = jsonBrewery.optString("country_name", "");
        this.approved = jsonBrewery.optBoolean("approved", false);
        this.thumb = jsonBrewery.optString("thumb", null);
    }

    public static ArrayList<BrewerySmall> breweriesFromJSONArray(JSONArray jsonBreweries) {
        ArrayList<BrewerySmall> breweries = new ArrayList<>();
        for (int i = 0; i < jsonBreweries.length(); i++) {
            JSONObject jsonBrewery = jsonBreweries.optJSONObject(i);
            if (jsonBrewery != null) {
                breweries.add(new BrewerySmall(jsonBrewery));
            }
        }
        return breweries;
    }
}
