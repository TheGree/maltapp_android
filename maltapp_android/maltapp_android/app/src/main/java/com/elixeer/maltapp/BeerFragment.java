package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Handlers.PointsManager;
import com.elixeer.maltapp.Models.Beer;
import com.elixeer.maltapp.Models.BeerRanking;
import com.elixeer.maltapp.Models.Score;
/*import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;*/

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BeerFragment extends Fragment {
    MainActivity parent;
    RelativeLayout circle_percent;
    ImageView beerImageView;
    Button rankingButton;
    Button favButton;
    Button infoButton;
    Button breweryButton;
    Button findButton;
    ImageButton beerGlassImageButton;
    TextView similarButton;
    TextView beerNameText;
    TextView beerStyleText;
    TextView beerABVText1;
    TextView beerABVText2;
    TextView beerIBUText1;
    TextView beerIBUText2;
    TextView beerSRMText1;
    TextView beerSRMText2;
    TextView beerOEText1;
    TextView beerOEText2;
    TextView beerRankingText;
    TextView beerPercent;
    TextView beerPercentFalse;
    TextView beerRankingNumText;
    TextView beerCommentNumText;
    Beer beer;
    BeerRanking beer_ranking;
    ArrayList<Score> scores;
    int beer_id;
    int user_id;
    boolean preload;

    public BeerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            //parent.setTab(1);
            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                //TODO: change name
                actionBar.setTitle(R.string.beers);
                actionBar.show();
            }
        }
        preload = getArguments().getBoolean("PRELOAD", false);
        if (preload) {
            beer = (Beer) getArguments().get("BEER");
            if (beer != null) {
                beer_id = beer.id;
            }
        } else {
            beer_id = getArguments().getInt("BEER_ID");
        }

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        if (sharedPreferences.contains("userId")) {
            if (sharedPreferences.getInt("userId", -1) > 0) {
                user_id = sharedPreferences.getInt("userId", -1);
            }
        }

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_beer, container, false);

        circle_percent = rootView.findViewById(R.id.circle_percent);
        beerImageView = rootView.findViewById(R.id.beerImageView);
        rankingButton = rootView.findViewById(R.id.rankingButton);
        favButton = rootView.findViewById(R.id.favButton);
        infoButton = rootView.findViewById(R.id.infoButton);
        breweryButton = rootView.findViewById(R.id.breweryButton);
        findButton = rootView.findViewById(R.id.findButton);
        beerGlassImageButton = rootView.findViewById(R.id.beerGlassImageButton);
        similarButton = rootView.findViewById(R.id.similarButton);
        beerNameText = rootView.findViewById(R.id.beerNameText);
        beerStyleText = rootView.findViewById(R.id.beerStyleText);
        beerABVText1 = rootView.findViewById(R.id.beerABVText1);
        beerABVText2 = rootView.findViewById(R.id.beerABVText2);
        beerIBUText1 = rootView.findViewById(R.id.beerIBUText1);
        beerIBUText2 = rootView.findViewById(R.id.beerIBUText2);
        beerSRMText1 = rootView.findViewById(R.id.beerSRMText1);
        beerSRMText2 = rootView.findViewById(R.id.beerSRMText2);
        beerOEText1 = rootView.findViewById(R.id.beerOEText1);
        beerOEText2 = rootView.findViewById(R.id.beerOEText2);
        beerRankingText = rootView.findViewById(R.id.beerRankingText);
        beerPercent = rootView.findViewById(R.id.beerPercent);
        beerPercentFalse = rootView.findViewById(R.id.beerPercentFalse);
        beerRankingNumText = rootView.findViewById(R.id.beerRankingNumText);
        beerCommentNumText = rootView.findViewById(R.id.beerCommentNumText);

        rankingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (beer != null) {
                    if (user_id > 0) {
                        if (beer_ranking != null) {
                            sendRating();
                        }
                    } else {
                        loginPopUp();
                    }
                }
            }
        });
        circle_percent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendComments();
            }
        });
        favButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (beer != null) {
                    if (beer_ranking != null) {
                        if (user_id > 0) {
                            if (beer_ranking.notFav) {
                                PointsManager.addPoints(user_id, ApiUrl.POINTS_ADDFAV, ApiUrl.REASON_ADDFAV, getContext());
                            }
                            addFav(user_id, beer_id);
                        } else {
                            loginPopUp();
                        }
                    }
                }
            }
        });
        breweryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (beer != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("beer_details", "brewery_pressed", beer.name);
                    if (parent != null) {
                        Fragment breweryFragment = new BreweryFragment();
                        Bundle arguments = new Bundle();
                        arguments.putInt("BREWERY_ID", beer.brewery_id);
                        breweryFragment.setArguments(arguments);
                        parent.pushFragment(breweryFragment);
                    }
                }
            }
        });
        findButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (beer != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("beer_details", "whereToBuy_pressed", beer.name);
                    if (parent != null) {
                        Fragment nearbyMapFragment = new NearbyMapFragment();
                        Bundle arguments = new Bundle();
                        arguments.putInt("NEARBY", 2);
                        arguments.putInt("BREWERY_ID", beer.brewery_id);
                        arguments.putString("ITEM_NAME", beer.name);
                        nearbyMapFragment.setArguments(arguments);
                        parent.pushFragment(nearbyMapFragment);
                    }
                }
            }
        });
        similarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (beer != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("beer_details", "similar_pressed", beer.name);
                    if (parent != null) {
                        Fragment similarFragment = new SimilarFragment();
                        Bundle arguments = new Bundle();
                        arguments.putString("STYLE_NAME", beer.style_name);
                        arguments.putInt("STYLE_ID", beer.style_id);
                        arguments.putInt("BREWERY_ID", beer.brewery_id);
                        arguments.putString("BREWERY_NAME", beer.brewery_name);
                        similarFragment.setArguments(arguments);
                        parent.pushFragment(similarFragment);
                    }
                }
            }
        });
        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInfoDialogAtPage(0);
            }
        });
        beerNameText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInfoDialogAtPage(0);
            }
        });
        beerStyleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInfoDialogAtPage(1);
            }
        });
        beerGlassImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { openInfoDialogAtPage(2);
            }
        });
        beerABVText1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInfoDialogAtPage(3);
            }
        });
        beerABVText2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInfoDialogAtPage(3);
            }
        });
        beerIBUText1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInfoDialogAtPage(4);
            }
        });
        beerIBUText2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInfoDialogAtPage(4);
            }
        });
        beerSRMText1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInfoDialogAtPage(5);
            }
        });
        beerSRMText2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInfoDialogAtPage(5);
            }
        });
        beerOEText1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInfoDialogAtPage(6);
            }
        });
        beerOEText2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInfoDialogAtPage(6);
            }
        });
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (preload) {
            fillBeerData();
        } else {
            getBeer(beer_id);
        }
        if (user_id > 0) {
            getData(user_id, beer_id);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("DetailsBeers");
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu_share, menu);
    }*/

  /*  @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_share) {
            if (beer != null) {
                // Analytics event
                ((AnalyticsApplication) getActivity().getApplication()).sendEvent("beer_details", "shared_pressed", beer.name);
                if (user_id > 0) {
                    getScores(ApiUrl.FACE_SCORE_ID);
                }
                /*
                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse(ApiUrl.MALTAPP_MX))
                        .setContentTitle(beer.name)
                        .setContentDescription(String.format(getString(R.string.shareBeer), beer.name))
                        .setImageUrl(Uri.parse(ApiUrl.SQUARE_IMAGE + beer.medium))
                        .build();
                * /

                SharePhoto photo = new SharePhoto.Builder()
                        .setCaption(String.format(getString(R.string.shareBeer), beer.name))
                        .setImageUrl(Uri.parse(ApiUrl.SQUARE_IMAGE + beer.medium))
                        .setUserGenerated(false)
                        .build();

                SharePhotoContent content = new SharePhotoContent.Builder()
                        .addPhoto(photo)
                        .build();

                ShareDialog.show(this, content);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }*/

    /*
    @Override
    public int FABImageDrawable() {
        return R.drawable.ic_share_white_24px;
    }

    @Override
    public void onFABClick(View view) {
        // Analytics event
        ((AnalyticsApplication) getActivity().getApplication()).sendEvent("beer_details", "shared_pressed", "Facebook");
        if (user_id > 0) {
            getScores(ApiUrl.FACE_SCORE_ID);
        }
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(ApiUrl.MALTAPP_MX))
                .setContentTitle(beer.name)
                .setContentDescription(String.format(getString(R.string.shareBeer), beer.name))
                .setImageUrl(Uri.parse(ApiUrl.SQUARE_IMAGE + beer.medium))
                .build();
        ShareDialog.show(this, content);
    }
    */
    private void fillBeerData() {
        // Analytics event
        ((AnalyticsApplication) getActivity().getApplication()).sendEvent("beer_details", "view_details", beer.name);
        beerNameText.setText(beer.name);
        //beerBreweryText.setText(beer.brewery_name);
        beerStyleText.setText(beer.style_name + " • " + beer.weight + "ml.");
        //beerWeighText.setText(beer.weight + " ml");
        if (beer.abv == 0) {
            beerABVText2.setText(getString(R.string.n_a));
        } else {
            beerABVText2.setText(beer.abv + "%");
        }
        if (beer.ibu == 0) {
            beerIBUText2.setText(getString(R.string.n_a));
        } else {
            beerIBUText2.setText(String.valueOf(beer.ibu));
        }
        if (beer.srm == 0) {
            beerSRMText2.setText(getString(R.string.n_a));
        } else {
            beerSRMText2.setText(beer.srm + "°L");
        }
        if (beer.srm == 0) {
            beerOEText2.setText(getString(R.string.n_a));
        } else {
            beerOEText2.setText(beer.original_extract + "°P");
        }
        if (beer.has_min_ranks) {
            beerRankingText.setText(String.valueOf(beer.ranking));
        } else {
            beerRankingText.setText(getString(R.string.n_a));
            beerRankingText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 60);
            beerPercent.setText("");
            beerPercentFalse.setText("");
        }
        beerRankingNumText.setText(beer.number_ranks + " " + getString(R.string.ratings));
        beerCommentNumText.setText(beer.number_comments + " " + getString(R.string.comments));

        switch (beer.glass_id) {
            case 3:
                beerGlassImageButton.setBackground(getResources().getDrawable(R.drawable.ic_flute_glass_yellow));
                break;
            case 4:
                beerGlassImageButton.setBackground(getResources().getDrawable(R.drawable.ic_goblet_glass_yellow));
                break;
            case 5:
                beerGlassImageButton.setBackground(getResources().getDrawable(R.drawable.ic_mug_glass_yellow));
                break;
            case 6:
                beerGlassImageButton.setBackground(getResources().getDrawable(R.drawable.ic_pilsner_glass_yellow));
                break;
            case 7:
                beerGlassImageButton.setBackground(getResources().getDrawable(R.drawable.ic_pint_glass_yellow));
                break;
            case 8:
                beerGlassImageButton.setBackground(getResources().getDrawable(R.drawable.ic_snifter_glass_yellow));
                break;
            case 9:
                beerGlassImageButton.setBackground(getResources().getDrawable(R.drawable.ic_strange_glass_yellow));
                break;
            case 10:
                beerGlassImageButton.setBackground(getResources().getDrawable(R.drawable.ic_tulip_glass_yellow));
                break;
            case 11:
                beerGlassImageButton.setBackground(getResources().getDrawable(R.drawable.ic_weizen_glass_yellow));
                break;
            case 12:
                beerGlassImageButton.setBackground(getResources().getDrawable(R.drawable.ic_wine_glass_yellow));
                break;
            default:
                beerGlassImageButton.setBackground(getResources().getDrawable(R.drawable.ic_weizen_glass_yellow));
                break;
        }
        ImageCache.getImage(beer.medium, getContext(), new ImageCache.OnResponseListener() {
            @Override
            public void onResponse(Bitmap bitmap, String error) {
                beerImageView.setImageBitmap(bitmap);
            }
        });
    }

    private void fillRankData() {
        if (beer_ranking.fav) {
            setDrawableTop(favButton, R.drawable.btn_fav_on);
        } else {
            setDrawableTop(favButton, R.drawable.btn_fav);
        }
    }

    private void setDrawableTop(TextView view, int resource) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(resource, null), null, null);
        } else {
            view.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(resource), null, null);
        }
    }

    private void openInfoDialogAtPage(int page) {
        if (beer != null) {
            if (parent != null) {
                // Analytics event
                ((AnalyticsApplication) getActivity().getApplication()).sendEvent("beer_details", "info_pressed", beer.name);
                Bundle bundle = new Bundle();
                bundle.putSerializable("BEER", beer);
                bundle.putSerializable("PAGE", page);
                BeerInfoDialogFragment dialogFragment = new BeerInfoDialogFragment();
                dialogFragment.setArguments(bundle);
                dialogFragment.show(getChildFragmentManager(), "BeerInfo");
            }
        }
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(BeerFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (parent != null) {
                            parent.pushFragment(new HomeFragment());
                        }
                    }
                });

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (parent != null) {
                    parent.pushFragment(new HomeFragment());
                }
            }
        });
        alert.show();
    }

    private void addFav(int userId, int beerId) {
        if (beer_ranking.notFav) {
            // Analytics event
            ((AnalyticsApplication) getActivity().getApplication()).sendEvent("beer_details", "favorite_pressed", beer.name, 1);
            String url = ApiUrl.BEER_FAV_CREATED;
            String params = "{\"user_id\":\"" + userId + "\",\"beer_id\":\"" + beerId + "\"}";
            String[][] headers = {{"Content-Type", "application/json"}};
            new AsyncRequestString("POST", url, getActivity(), new AsyncRequestString.OnResponseListener() {
                @Override
                public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                    if (error == null) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            if (jsonResponse.has("error")) {
                                reportError(jsonResponse.getString("error"));
                            } else {
                                getData(user_id, beer_id);
                                favoriteMessage();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if(error.contains("No connection")) {
                        notInternetMSG();
                    } else {
                        reportError(error);
                    }
                }
            })
                    .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                    .setData(params)
                    .setHeaders(headers)
                    .execute();
        } else {
            // Analytics event
            ((AnalyticsApplication) getActivity().getApplication()).sendEvent("beer_details", "favorite_pressed", beer.name, 0);
            String url = ApiUrl.BEER_FAV_DELETE
                    + "&user_id=" + userId
                    + "&beer_id=" + beerId;
            new AsyncRequestString("PUT", url, getActivity(), new AsyncRequestString.OnResponseListener() {
                @Override
                public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                    if (error == null) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            if (jsonResponse.has("error")) {
                                reportError(jsonResponse.getString("error"));
                            } else {
                                getData(user_id, beer_id);
                                favoriteMessage();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if(error.contains("No connection")) {
                        notInternetMSG();
                    } else {
                        reportError(error);
                    }
                }
            })
                    .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                    .execute();
        }
    }

    void favoriteMessage(){
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_msj, null);
        TextView popMsjTextView = (TextView) pop.findViewById(R.id.popMsjTextView);
        if (beer_ranking.fav) {
            popMsjTextView.setText(getString(R.string.removed) + " " + getString(R.string.favorites));
        } else {
            popMsjTextView.setText(getString(R.string.added) + " " + getString(R.string.favorites));
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(BeerFragment.this.getActivity())
                //.setIcon(R.drawable.ic_trophy)
                //.setTitle(getString(R.string.maltappNotice))
                //.setMessage(R.string.msg_goal_achieved_message)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });
        alert.show();
    }

    /*
    private void addWish(int userId, int beerId) {
        if (isOnline()) {
            if (beer_ranking.notWish) {
                // Analytics event
                ((AnalyticsApplication) getActivity().getApplication()).sendEvent("beer_details", "wishlist_pressed", beer.name, 1);
                String url = ApiUrl.BEER_WISH_CREATED;
                String params = "user_id=" + userId + "&beer_id=" + beerId;
                new AsyncRequestString("POST", url, getActivity()) {
                    @Override
                    public void onResult(int responseCode, String response) {
                        if (response == null) {
                            Log.w(getString(R.string.app_name), "WARNING: Response is null (" + responseCode + ")");
                        } else {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                if (jsonResponse.has("error")) {
                                    reportError(jsonResponse.getString("error"));
                                } else {
                                    getData(user_id, beer_id);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }.execute(params);
            } else {
                // Analytics event
                ((AnalyticsApplication) getActivity().getApplication()).sendEvent("beer_details", "wishlist_pressed", beer.name, 0);
                String url = ApiUrl.BEER_WISH_DELETE
                        + "&user_id=" + userId
                        + "&beer_id=" + beerId;
                new AsyncRequestString("PUT", url, getActivity()) {
                    @Override
                    public void onResult(int responseCode, String response) {
                        if (response == null) {
                            Log.w(getString(R.string.app_name), "WARNING: Response is null (" + responseCode + ")");
                        } else {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                if (jsonResponse.has("error")) {
                                    reportError(jsonResponse.getString("error"));
                                } else {
                                    getData(user_id, beer_id);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }.execute();
            }
        }
    }
    */
    private void getBeer(int beerId) {
        String url = ApiUrl.GET_BEER + beerId + ".json" + ApiUrl.ACCESS_TOKEN;
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONObject jsonBeer = jsonResponse.getJSONObject("beer");
                            beer = new Beer(jsonBeer, getContext());
                            fillBeerData();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getData(int userId, int beerId) {
        String url = ApiUrl.BEER_RANKING + "&user_id=" + userId + "&beer_id=" + beerId;
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONObject jsonBeerRanking = jsonResponse.getJSONObject("beer");
                            beer_ranking = new BeerRanking(jsonBeerRanking);
                            fillRankData();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getScores(final int badgetype_id) {
        String url = ApiUrl.SCORES + "&user_id=" + user_id + "&badgetype_id=" + badgetype_id;
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonScores = jsonResponse.getJSONArray("scores");
                            scores = Score.tastesFromJSONArray(jsonScores);
                            if (scores.size() < 1) {
                                createScore(badgetype_id);
                            } else {
                                editScores(badgetype_id);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void createScore(int badgetype_id) {
        String scoresFaceParams = "{";
        scoresFaceParams += "\"user_id\":\"" + user_id + "\"";
        scoresFaceParams += ",\"badgetype_id\":\"" + badgetype_id + "\"";
        scoresFaceParams += ",\"points\":\"" + 1 + "\"";
        scoresFaceParams += "}";
        String url = ApiUrl.SCORES;
        String[][] headers = {{"Content-Type", "application/json"}};
        new AsyncRequestString("POST", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .setData(scoresFaceParams)
                .setHeaders(headers)
                .execute();
    }

    private void editScores(int badgetype_id) {
        String url = ApiUrl.SCORE_UPDATE + "&user_id=" + user_id + "&badgetype_id=" + badgetype_id + "&points=" + 1;
        new AsyncRequestString("PUT", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    void sendComments() {
        // Analytics event
        ((AnalyticsApplication) getActivity().getApplication()).sendEvent("beer_details", "similar_pressed", beer.name);
        Fragment beerRankListFragment = new BeerRankListFragment();
        Bundle arguments = new Bundle();
        arguments.putInt("BEER_ID", beer_id);
        arguments.putString("BEER_NAME", beer.name);
        beerRankListFragment.setArguments(arguments);
        parent.pushFragment(beerRankListFragment);
    }

    void sendRating() {
        // Analytics event
        ((AnalyticsApplication) getActivity().getApplication()).sendEvent("beer_details", "drankIt_pressed", beer.name);
        Fragment rankFragment = new RankFragment();
        Bundle arguments = new Bundle();
        arguments.putInt("BEER_ID", beer_id);
        arguments.putString("TITLE", beer.name);
        arguments.putString("BEER_IMAGE", beer.medium);
        if (user_id > 0) {
            arguments.putBoolean("LOGGED", true);
            arguments.putInt("USER_ID", user_id);
            arguments.putLong("RANK", beer_ranking.ranking);
            arguments.putString("COMMENT", beer_ranking.comment);
            arguments.putBoolean("CREATED", beer_ranking.created);
        } else {
            arguments.putBoolean("LOGGED", false);
            arguments.putInt("USER_ID", 0);
            arguments.putLong("RANK", 0);
            arguments.putBoolean("CREATED", false);
        }
        rankFragment.setArguments(arguments);
        parent.pushFragment(rankFragment);
    }

    void loginPopUp() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_login, null);
        Button popLoginButton = pop.findViewById(R.id.popLoginButton);

        AlertDialog.Builder builder = new AlertDialog.Builder(BeerFragment.this.getActivity())
                //.setTitle(getString(R.string.maltappNotice))
                .setView(pop)
                .setPositiveButton(R.string.ok, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        popLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment registerFragment = new RegisterFragment();
                parent.pushFragment(registerFragment);
                alert.dismiss();
            }
        });
        alert.show();
    }
}