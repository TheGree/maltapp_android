package com.elixeer.maltapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.core.SimpleListAdapter;
import com.elixeer.maltapp.Adapters.EventsCustomAdapter;
import com.elixeer.maltapp.Comparators.EventComparator;
import com.elixeer.maltapp.Comparators.PlaceComparator;
import com.elixeer.maltapp.Handlers.PointsManager;
import com.elixeer.maltapp.Models.Event;
import com.elixeer.maltapp.Models.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by alebrije2 on 1/25/16.
 *
 */
public class NearbyMapFragment extends Fragment implements OnMapReadyCallback {

    MainActivity parent;
    LinearLayout backgroundItemNotFound;
    Button letsUsKnowLibraryButton;
    TextView letsUsKnowLibraryTextView;
    private GoogleMap mMap;
    private LatLng userLatLng;
    private LocationManager locationManager;
    ListView nearbyMapListView;
    ArrayList<Event> events;
    ArrayList<Event> events_order;
    ArrayList<Place> places;
    int nearby_type;
    private static View rootView;
    int user_id;
    public String labelParam;

    public NearbyMapFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        nearby_type = getArguments().getInt("NEARBY");

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        if (sharedPreferences.contains("userId")) {
            user_id = sharedPreferences.getInt("userId", -1);
        }

        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            //parent.setTab(0);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.title_nearby);
                switch (nearby_type) {
                    case 1: {
                        actionBar.setTitle(R.string.bars);
                        break;
                    }
                    case 2: {
                        actionBar.setTitle(R.string.beers);
                        break;
                    }
                    case 3: {
                        actionBar.setTitle(R.string.stores);
                        break;
                    }
                    case 4: {
                        actionBar.setTitle(R.string.events);
                        break;
                    }
                    case 5: {
                        actionBar.setTitle(R.string.brewery);
                        break;
                    }
                }
                actionBar.show();
            }
        }

        if (rootView != null) {
            ViewGroup p = (ViewGroup) rootView.getParent();
            if (p != null) {
                p.removeView(rootView);
            }
        }
        try {
            rootView = inflater.inflate(R.layout.fragment_nearby_map, container, false);
        } catch (InflateException e) {
            // map is already there, just return view as it is
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(NearbyMapFragment.this);

        nearbyMapListView = rootView.findViewById(R.id.nearbyMapListView);
        nearbyMapListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (nearby_type) {
                    case 1: {
                        if (parent != null) {
                            Fragment placeFragment = new PlaceFragment();
                            Bundle arguments = new Bundle();
                            arguments.putSerializable("PLACE", places.get(i));
                            placeFragment.setArguments(arguments);
                            parent.pushFragment(placeFragment);
                        }
                        break;
                    }
                    case 2: {
                        if (parent != null) {
                            Fragment placeFragment = new PlaceFragment();
                            Bundle arguments = new Bundle();
                            arguments.putSerializable("PLACE", places.get(i));
                            placeFragment.setArguments(arguments);
                            parent.pushFragment(placeFragment);
                        }
                        break;
                    }
                    case 3: {
                        if (parent != null) {
                            Fragment placeFragment = new PlaceFragment();
                            Bundle arguments = new Bundle();
                            arguments.putSerializable("PLACE", places.get(i));
                            placeFragment.setArguments(arguments);
                            parent.pushFragment(placeFragment);
                        }
                        break;
                    }
                    case 4: {
                        if ((i + 1) < events_order.size()) {
                            if (events_order.get(i).id != events_order.get(i + 1).id) {
                                if (parent != null) {
                                    Fragment eventDetailsFragment = new EventDetailsFragment();
                                    Bundle arguments = new Bundle();
                                    arguments.putSerializable("EVENT", events_order.get(i));
                                    eventDetailsFragment.setArguments(arguments);
                                    parent.pushFragment(eventDetailsFragment);
                                }
                            }
                        } else {
                            if (parent != null) {
                                Fragment eventDetailsFragment = new EventDetailsFragment();
                                Bundle arguments = new Bundle();
                                arguments.putSerializable("EVENT", events_order.get(i));
                                eventDetailsFragment.setArguments(arguments);
                                parent.pushFragment(eventDetailsFragment);
                            }
                        }
                        break;
                    }
                    case 5: {
                        if (parent != null) {
                            Fragment placeFragment = new PlaceFragment();
                            Bundle arguments = new Bundle();
                            arguments.putSerializable("PLACE", places.get(i));
                            placeFragment.setArguments(arguments);
                            parent.pushFragment(placeFragment);
                        }
                        break;
                    }
                }
            }
        });

        backgroundItemNotFound = (LinearLayout) inflater.inflate(R.layout.list_item_not_found, null, false);
        letsUsKnowLibraryButton = backgroundItemNotFound.findViewById(R.id.letsUsKnowLibraryButton);
        letsUsKnowLibraryTextView = backgroundItemNotFound.findViewById(R.id.letsUsKnowLibraryTextView);

        letsUsKnowLibraryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Nearby", "letusknow_pressed", labelParam);

                if (user_id > 0) {
                    PointsManager.addPoints(user_id, ApiUrl.POINTS_BREWERYLINK, ApiUrl.REASON_BREWERYLINK, getActivity());
                }
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", getResources().getString(R.string.contact_mail), null));
                startActivity(Intent.createChooser(intent, getString(R.string.send)));
            }
        });

        // Get user location
        getUserLocation();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("MapDisplay");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        updateMapMarkers();

        //LatLng mty = new LatLng(25.6861101, -100.3168831);
        //mMap.addMarker(new MarkerOptions().position(mty).title("Marker in Monterrey"));
        if (userLatLng != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLatLng, 10));
        }
        if (android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == ApiUrl.LOCATION_REQUEST_CODE && (android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            getUserLocation();
        }
    }

    private void getUserLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, ApiUrl.LOCATION_REQUEST_CODE);
        } else {
            /* LOCATION */
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            String locationProvider = null;

            if (locationManager == null) {
                Log.w(getString(R.string.app_name), "Unable to get location provider");
            } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Log.i(getString(R.string.app_name), "Using GPS location provider");
                locationProvider = LocationManager.GPS_PROVIDER;
            } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                Log.i(getString(R.string.app_name), "Using network location provider");
                locationProvider = LocationManager.NETWORK_PROVIDER;
            } else {
                Log.w(getString(R.string.app_name), "Using no location provider");
            }

            if (locationProvider != null) {
                LocationListener locationListener = new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
						if (!isAdded()) {
                            return;
                        }
                        Log.d(getString(R.string.app_name), "Location: " + location.getLatitude() + "," + location.getLongitude() + " @" + location.getAccuracy());
                        setUserLocation(location);
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                            Log.d(getString(R.string.app_name), "Removing location listener");
                            locationManager.removeUpdates(this);
                        }
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {
                    }

                    @Override
                    public void onProviderEnabled(String provider) {
                    }

                    @Override
                    public void onProviderDisabled(String provider) {
                    }
                };
                Log.d(getString(R.string.app_name), "Adding location listener");
                locationManager.requestLocationUpdates(locationProvider, 1000, 1, locationListener);
                Location location = locationManager.getLastKnownLocation(locationProvider);
                setUserLocation(location);
            } else {
                setUserLocation(null);
            }
        }
    }

    private void setUserLocation (Location location) {
        if (location == null) {
            Log.w(getString(R.string.app_name), "Unable to retrieve location!");
            userLatLng = new LatLng(ApiUrl.MEX_CTY_LAT, ApiUrl.MEX_CTY_LONG);
            if (mMap != null) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLatLng, 5));
                mMap.setMyLocationEnabled(false);
            }
            getPlaces(userLatLng.latitude, userLatLng.longitude, ApiUrl.RADIUS_MAX);
        } else {
            userLatLng = new LatLng(location.getLatitude(), location.getLongitude());
            if (mMap != null) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLatLng, 10));
                mMap.setMyLocationEnabled(true);
            }
            getPlaces(userLatLng.latitude, userLatLng.longitude, ApiUrl.RADIUS_MIN);
        }
    }

    private void updatePlacesDistances() {
        if (userLatLng != null) {
            if (places != null) {
                for (int i = 0; i < places.size(); i++) {
                    places.get(i).distance = distFrom(places.get(i).latitude, places.get(i).longitude, userLatLng.latitude, userLatLng.longitude);
                }
                Collections.sort(places, new PlaceComparator());
                nearbyMapListView.setAdapter(new SimpleListAdapter(R.layout.list_item_nearby, places) {
                    @Override
                    public void onListItem(Object listItem, View view, int position) {
                        Place place = (Place) listItem;

                        TextView nearbyNameTextView = view.findViewById(R.id.nearbyNameTextView);
                        TextView addressTextView = view.findViewById(R.id.addressTextView);
                        TextView distanceTextView = view.findViewById(R.id.distanceTextView);

                        nearbyNameTextView.setText(place.name);
                        addressTextView.setText(place.address);
                        distanceTextView.setText(String.format(Locale.getDefault(), "%.2fKm", place.distance));
                    }
                });
            }
            if (events != null) {
                /*
                for (int i = 0; i < events.size(); i++) {
                    events.get(i).distance = distFrom(events.get(i).latitude, events.get(i).longitude, userLatLng.latitude, userLatLng.longitude);
                }
                */
                Collections.sort(events, new EventComparator());

                //SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
                SimpleDateFormat simpleDateFormatMonth = new SimpleDateFormat("MM", Locale.getDefault());

                EventsCustomAdapter mAdapter = new EventsCustomAdapter(getContext());

                events_order = new ArrayList<>();
                String lastMonth = "";

                for (int i = 0; i < events.size(); i++) {
                    String month = simpleDateFormatMonth.format(events.get(i).startDate);
                    if (!month.equals(lastMonth)) {
                        lastMonth = month;
                        events_order.add(events.get(i));
                        mAdapter.addSectionHeaderItem(events.get(i));
                    }
                    events_order.add(events.get(i));
                    mAdapter.addItem(events.get(i));
                }
                nearbyMapListView.setAdapter(mAdapter);
            }
        }
    }

    private void updateMapMarkers() {
        if (mMap != null) {
            if (places != null) {
                for (int i = 0; i < places.size(); i++) {
                    LatLng pos = new LatLng(places.get(i).latitude, places.get(i).longitude);
                    //BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location);
                    mMap.addMarker(new MarkerOptions().position(pos).title(places.get(i).name));//.icon(icon));
                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            for (int j = 0; j < places.size(); j++) {
                                if (places.get(j).name.equals(marker.getTitle())) {
                                    Fragment placeFragment = new PlaceFragment();
                                    Bundle arguments = new Bundle();
                                    arguments.putSerializable("PLACE", places.get(j));
                                    placeFragment.setArguments(arguments);
                                    parent.pushFragment(placeFragment);
                                }
                            }
                        }
                    });
                }
            }
            if (events != null) {
                for (int i = 0; i < events.size(); i++) {
                    LatLng pos = new LatLng(events.get(i).latitude, events.get(i).longitude);
                    //BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location);
                    mMap.addMarker(new MarkerOptions().position(pos).title(events.get(i).title));//.icon(icon));
                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            for (int j = 0; j < events.size(); j++) {
                                if (events.get(j).title.equals(marker.getTitle())) {
                                    Fragment EventDetailsFragment = new EventDetailsFragment();
                                    Bundle arguments = new Bundle();
                                    arguments.putSerializable("EVENT", events.get(j));
                                    EventDetailsFragment.setArguments(arguments);
                                    parent.pushFragment(EventDetailsFragment);
                                }
                            }
                        }
                    });
                }
            }


        }
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void getPlaces(double latitude, double longitude, double radius) {
        switch (nearby_type) {
            case 1: {
                getBarsShops(latitude, longitude, radius, false);
                break;
            }
            case 2: {
                getByBrewery(latitude, longitude, radius, getArguments().getInt("BREWERY_ID"));
                break;
            }
            case 3: {
                getBarsShops(latitude, longitude, radius, true);
                break;
            }
            case 4: {
                getEvents(latitude, longitude, ApiUrl.RADIUS_MAX);
                break;
            }
            case 5: {
                getByBrewery(latitude, longitude, radius, getArguments().getInt("BREWERY_ID"));
                break;
            }
        }
    }

    private void getBarsShops(double latitude, double longitude, double rad, boolean isShop) {
        double kmInLongitudeDegree = ApiUrl.DEGREE_LON * Math.cos(latitude / 180.0 * Math.PI);
        double deltaLat = rad / ApiUrl.DEGREE_LAT;
        double deltaLong = rad / kmInLongitudeDegree;
        double minLat = latitude - deltaLat;
        double maxLat = latitude + deltaLat;
        double minLong = longitude - deltaLong;
        double maxLong = longitude + deltaLong;

        String url = ApiUrl.PLACES + "&coords=" + minLong + "," + minLat + "," + maxLong + "," + maxLat + "&shop=" + isShop;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonPoints = jsonResponse.getJSONArray("places");
                            places = Place.placesFromJSONArray(jsonPoints, getContext());
                            if (places.size() < 1) {
                                Log.i(getString(R.string.app_name), "No places found");
                                labelParam = "Places";
                                letsUsKnowLibraryTextView.setText(getString(R.string.no_places_msg));
                                nearbyMapListView.addFooterView(backgroundItemNotFound);
                                updatePlacesDistances();
                            } else {
                                updateMapMarkers();
                                updatePlacesDistances();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getEvents(double latitude, double longitude, double rad) {
        double kmInLongitudeDegree = ApiUrl.DEGREE_LON * Math.cos(latitude / 180.0 * Math.PI);
        double deltaLat = rad / ApiUrl.DEGREE_LAT;
        double deltaLong = rad / kmInLongitudeDegree;
        double minLat = latitude - deltaLat;
        double maxLat = latitude + deltaLat;
        double minLong = longitude - deltaLong;
        double maxLong = longitude + deltaLong;

        String url = ApiUrl.EVENTS + "&coords=" + minLong + "," + minLat + "," + maxLong + "," + maxLat + "&is_events=true";

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonEvents = jsonResponse.getJSONArray("events");
                            events = Event.eventsFromJSONArray(jsonEvents, getContext());
                            if (events.size() < 1) {
                                Log.i(getString(R.string.app_name), "No events found");
                                labelParam = "Events";
                                letsUsKnowLibraryTextView.setText(getString(R.string.no_events_msg));
                                nearbyMapListView.addFooterView(backgroundItemNotFound);
                                updatePlacesDistances();
                            } else {
                                updateMapMarkers();
                                updatePlacesDistances();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(NearbyMapFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void getByBrewery(double latitude, double longitude, double rad, int brewery_id) {
        double kmInLongitudeDegree = ApiUrl.DEGREE_LON * Math.cos(latitude / 180.0 * Math.PI);
        double deltaLat = rad / ApiUrl.DEGREE_LAT;
        double deltaLong = rad / kmInLongitudeDegree;
        double minLat = latitude - deltaLat;
        double maxLat = latitude + deltaLat;
        double minLong = longitude - deltaLong;
        double maxLong = longitude + deltaLong;

        String url = ApiUrl.PLACES + "&activate=true&coords=" + minLong + "," + minLat + "," + maxLong + "," + maxLat + "&brewery_id=" + brewery_id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonPoints = jsonResponse.getJSONArray("places");
                            places = Place.placesFromJSONArray(jsonPoints, getContext());
                            if (places.size() < 1) {
                                Log.i(getString(R.string.app_name), "No places found");
                                labelParam = getArguments().getString("ITEM_NAME");
                                letsUsKnowLibraryTextView.setText(getString(R.string.no_nearby_msg));
                                nearbyMapListView.addFooterView(backgroundItemNotFound);
                                updatePlacesDistances();
                            } else {
                                updateMapMarkers();
                                updatePlacesDistances();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private double distFrom(double lat1, double lng1, double lat2, double lng2) {
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return ApiUrl.EARTH_RADIUS * c;
    }

}