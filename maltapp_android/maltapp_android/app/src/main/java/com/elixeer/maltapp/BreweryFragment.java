package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Handlers.PointsManager;
import com.elixeer.maltapp.Models.Brewery;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * Created by alebrije2 on 1/27/16.
 *
 */
public class BreweryFragment extends Fragment {

    MainActivity parent;
    ImageView breweryImageView;
    TextView breweryNameText;
    TextView breweryCityText;
    TextView breweryDescriptionText;
    Button findBeersButton;
    ImageButton phoneButton;
    ImageButton mailButton;
    ImageButton facebookButton;
    ImageButton beersButton;
    //BeerImageCarousel beerCarousel;
    Brewery brewery;
    int user_id;

    public BreweryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            //parent.setTab(1);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.brewery);
                actionBar.show();
            }
        }
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_brewery, container, false);

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        if (sharedPreferences.contains("userId")) {
            user_id = sharedPreferences.getInt("userId", -1);
        }

        breweryImageView = rootView.findViewById(R.id.breweryImageView);
        breweryNameText = rootView.findViewById(R.id.breweryNameText);
        breweryCityText = rootView.findViewById(R.id.breweryCityText);
        breweryDescriptionText =  rootView.findViewById(R.id.breweryDescriptionText);
        findBeersButton = rootView.findViewById(R.id.findBeersButton);
        phoneButton = rootView.findViewById(R.id.breweryPhoneButton);
        mailButton = rootView.findViewById(R.id.breweryMailButton);
        facebookButton = rootView.findViewById(R.id.breweryFacebookButton);
        beersButton = rootView.findViewById(R.id.breweryBeersButton);
        //beerCarousel = rootView.findViewById(R.id.breweryBeerCarousel);

        getBrewery(getArguments().getInt("BREWERY_ID"));

        findBeersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brewery != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("brewery_details", "directions_pressed", brewery.name);

                    if (parent != null) {
                        Fragment nearbyMapFragment = new NearbyMapFragment();
                        Bundle arguments = new Bundle();
                        arguments.putInt("NEARBY", 5);
                        arguments.putInt("BREWERY_ID", brewery.id);
                        arguments.putString("ITEM_NAME", brewery.name);
                        nearbyMapFragment.setArguments(arguments);
                        parent.pushFragment(nearbyMapFragment);
                    }
                }
            }
        });

        phoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brewery != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("brewery_details", "call_pressed", brewery.name);

                    if (getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
                        if (brewery.phone != null && TextUtils.isDigitsOnly(brewery.phone)) {
                            if (user_id > 0) {
                                PointsManager.addPoints(user_id, ApiUrl.POINTS_BREWERYLINK, ApiUrl.REASON_BREWERYLINK, getActivity());
                            }
                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                            callIntent.setData(Uri.parse("tel:" + brewery.phone));
                            startActivity(callIntent);
                        } /* else {
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View pop = inflater.inflate(R.layout.pop_item_msj, null);

                        AlertDialog.Builder builder = new AlertDialog.Builder(BreweryFragment.this.getActivity())
                                //.setTitle(brewery.name)
                                .setView(pop)
                                .setPositiveButton(R.string.ok, null);

                        AlertDialog alert = builder.create();
                        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                            }
                        });

                        alert.show();
                    } */

                    } /*else {
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View pop = inflater.inflate(R.layout.pop_item_msj, null);

                        TextView popPhoneTextView = (TextView) pop.findViewById(R.id.popAlertTextView);

                        if (!brewery.phone.equals("")) {
                            popPhoneTextView.setText(brewery.phone);
                        }

                        AlertDialog.Builder builder = new AlertDialog.Builder(BreweryFragment.this.getActivity())
                                //.setTitle(brewery.name)
                                .setView(pop)
                                .setPositiveButton(R.string.ok, null);

                        AlertDialog alert = builder.create();
                        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                            }
                        });

                        alert.show();
                    } */
                }
            }
        });

        mailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brewery != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("brewery_details", "mail_pressed", brewery.name);

                    if (brewery.mail != null) {
                        if (user_id > 0) {
                            PointsManager.addPoints(user_id, ApiUrl.POINTS_BREWERYLINK, ApiUrl.REASON_BREWERYLINK, getActivity());
                        }
                        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", brewery.mail, null));
                        //intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
                        //intent.putExtra(Intent.EXTRA_TEXT, "message");
                        startActivity(Intent.createChooser(intent, getString(R.string.send)));
                    } /* else {
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View pop = inflater.inflate(R.layout.pop_item_msj, null);

                        AlertDialog.Builder builder = new AlertDialog.Builder(BreweryFragment.this.getActivity())
                                //.setTitle(brewery.name)
                                .setView(pop)
                                .setPositiveButton(R.string.ok, null);

                        AlertDialog alert = builder.create();
                        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                            }
                        });

                        alert.show();
                    } */
                }
            }
        });

        facebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brewery != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("brewery_details", "facebook_pressed", brewery.name);

                    String facebookPath;
                    if (!brewery.facebook_id.equals("")) {
                        facebookPath = brewery.facebook_id;

                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + facebookPath));
                            startActivity(intent);
                        } catch (Exception e) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/" + facebookPath)));
                        }

                    } else if (!brewery.facebook.equals("")) {
                        facebookPath = brewery.facebook
                                .toLowerCase()
                                .replace("https://", "")
                                .replace("http://", "")
                                .replace("www.facebook.com/", "")
                                .replace("facebook.com/", "");

                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + facebookPath));
                            startActivity(intent);
                        } catch (Exception e) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/" + facebookPath)));
                        }

                    } /* else {
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View pop = inflater.inflate(R.layout.pop_item_msj, null);

                        AlertDialog.Builder builder = new AlertDialog.Builder(BreweryFragment.this.getActivity())
                                //.setTitle(brewery.name)
                                .setView(pop)
                                .setPositiveButton(R.string.ok, null);

                        AlertDialog alert = builder.create();
                        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                            }
                        });

                        alert.show();
                    } */
                }
            }
        });

        beersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brewery != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("brewery_details", "carousel_pressed", brewery.name);

                    if (parent != null) {
                        Fragment carouselFragment = new CarouselFragment();
                        Bundle arguments = new Bundle();
                        arguments.putString("CAROUSEL_NAME", brewery.name);
                        arguments.putInt("CAROUSEL_ID", brewery.id);
                        arguments.putInt("CAROUSEL_TYPE", 1);
                        carouselFragment.setArguments(arguments);
                        parent.pushFragment(carouselFragment);
                    }
                }
            }
        });

        /*

        twitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brewery != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("brewery_details", "twitter_pressed", brewery.name);

                    if (!brewery.twitter.equals("")) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + brewery.twitter));
                            startActivity(intent);
                        } catch (Exception e) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/#!/" + brewery.twitter)));
                        }
                    }  else {
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View pop = inflater.inflate(R.layout.pop_item_alert, null);

                        AlertDialog.Builder builder = new AlertDialog.Builder(BreweryFragment.this.getActivity())
                                .setTitle(brewery.name)
                                .setView(pop)
                                .setPositiveButton(R.string.ok, null);

                        AlertDialog alert = builder.create();
                        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                            }
                        });

                        alert.show();
                    }
                }
            }
        });
        */

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        if (brewery != null) {
            ((AnalyticsApplication) getActivity().getApplication()).setScreenName("Brewery-" + brewery.name);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
    }

    private void fillBreweryData() {
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("Brewery-" + brewery.name);

        if(brewery.phone.equals("") || !TextUtils.isDigitsOnly(brewery.phone)){
            phoneButton.setAlpha(0.50f);
            phoneButton.setClickable(false);
        }

        if(brewery.mail.equals("")){
            mailButton.setAlpha(0.50f);
            mailButton.setClickable(false);
        }

        if(brewery.facebook_id.equals("") && brewery.facebook.equals("")){
            facebookButton.setAlpha(0.50f);
            facebookButton.setClickable(false);
        }

        ImageCache.getImage(brewery.imageUrlMedium, getContext(), new ImageCache.OnResponseListener() {
            @Override
            public void onResponse(Bitmap bitmap, String error) {
                breweryImageView.setImageBitmap(bitmap);
            }
        });
        breweryNameText.setText(brewery.name);
        String cityText = brewery.city_name;
        if (!cityText.isEmpty() && !brewery.state_name.isEmpty()) {
            cityText += ", ";
        }
        cityText += brewery.state_name;
        if (!cityText.isEmpty() && !brewery.country_name.isEmpty()) {
            cityText += ", ";
        }
        cityText += brewery.country_name;
        breweryCityText.setText(cityText);
        breweryDescriptionText.setText(brewery.description);
        //getBeerImages(brewery.id);

    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(BreweryFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (parent != null) {
                            parent.pushFragment(new HomeFragment());
                        }
                    }
                });

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (parent != null) {
                    parent.pushFragment(new HomeFragment());
                }
            }
        });
        alert.show();
    }

    private void getBrewery(int breweryId) {
        String url = ApiUrl.BREWERY + breweryId + ".json" + ApiUrl.ACCESS_TOKEN;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONObject jsonBrewery = jsonResponse.getJSONObject("brewery");
                            brewery = new Brewery(jsonBrewery, getContext());
                            fillBreweryData();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    /*
    private void getBeerImages(int breweryId) {
        if (isOnline()) {
            String url = ApiUrl.BREWERY_BEERS + "&brewery_id=" + breweryId;

            new AsyncRequestString("GET", url) {
                @Override
                public void onResult(int responseCode, String response) {
                    if (response == null) {
                        Log.w(getString(R.string.app_name), "WARNING: Response is null (" + responseCode + ")");
                    } else {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            if (jsonResponse.has("error")) {
                                reportError(jsonResponse.getString("error"));
                            } else {
                                JSONArray jsonBeers = jsonResponse.getJSONArray("beers");
                                ArrayList<BeerSmall> beers = BeerSmall.beersFromJSONArray(jsonBeers);
                                if (beers.size() < 1) {
                                    Log.i(getString(R.string.app_name), "No beers found");
                                } else {
                                    beerCarousel.setBeers(parent, beers, "brewery_details", "beer_pressed", brewery.name);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.execute();
        }
    }
    */
}
