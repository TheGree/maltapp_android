package com.elixeer.maltapp.Comparators;

import com.elixeer.maltapp.Models.Badge;

import java.util.Comparator;


public class BadgeComparator implements Comparator<Badge> {
    public int compare(Badge left, Badge right) {
        return left.order_index - right.order_index;
    }

}
