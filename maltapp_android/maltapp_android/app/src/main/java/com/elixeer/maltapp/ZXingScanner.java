package com.elixeer.maltapp;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.CompoundBarcodeView;

public class ZXingScanner extends MainActivity {

    private ActionBar actionBar;
    private CaptureManager capture;
    private CompoundBarcodeView barcodeScannerView;
    private LinearLayout noBeerMessage;
    private ImageButton btnBackScanner;
    private ImageButton btnFlashScannerON;
    private ImageButton btnFlashScannerOFF;
    private FragmentManager fragmentManager;
    private TabLayout tabLayout;

    private int tabIcons[] = {
            R.drawable.ic_tab_home,
            R.drawable.ic_tab_news,
            R.drawable.ic_tab_nearby,
            R.drawable.ic_tab_rank,
            R.drawable.ic_tab_profile
    };

    private int tabIconsPremium[] = {
            R.drawable.ic_tab_home,
            R.drawable.ic_tab_news,
            R.drawable.ic_tab_nearby,
            R.drawable.ic_tab_rank,
            R.drawable.ic_tab_premium
    };

    private Fragment sectionFragments[] = {
            new HomeFragment(),
            new NewsFragment(),
            new NearbyFragment(),
            new TopFragment(),
            new ProfileFragment()
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActivity(savedInstanceState);
    }

    @Override
    protected void initActivity(Bundle savedInstanceState){
        setContentView(R.layout.activity_zxing_scanner);

        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.hide();
        }
        fragmentManager = getSupportFragmentManager();
        this.barcodeScannerView = this.findViewById(R.id.scanner);
        this.capture = new CaptureManager(this, this.barcodeScannerView);
        this.capture.initializeFromIntent(this.getIntent(), savedInstanceState);
        this.capture.decode();
        this.noBeerMessage = this.findViewById(R.id.noBeerMessage);
        this.btnBackScanner = this.findViewById(R.id.btnBackScanner);
        this.btnFlashScannerON = this.findViewById(R.id.btnFlashScannerON);
        this.btnFlashScannerOFF = this.findViewById(R.id.btnFlashScannerOFF);

        btnBackScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment homeFragment = new HomeFragment();
                onBackPressed();
                MainActivity.instance.pushFragment(homeFragment);
            }
        });

        btnFlashScannerON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                barcodeScannerView.setTorchOn();
                btnFlashScannerOFF.setVisibility(View.VISIBLE);
                btnFlashScannerON.setVisibility(View.GONE);
            }
        });

        btnFlashScannerOFF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                barcodeScannerView.setTorchOff();
                btnFlashScannerON.setVisibility(View.VISIBLE);
                btnFlashScannerOFF.setVisibility(View.GONE);
            }
        });

        noBeerMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment beerSearchFragment = new BeerSearchFragment();
                Bundle arguments = new Bundle();
                beerSearchFragment.setArguments(arguments);
                onBackPressed();
                MainActivity.instance.pushFragment(beerSearchFragment);
            }
        });

        hideMessage();
        setTimer(10);

        tabLayout = findViewById(R.id.tabs);
        //SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);

        //boolean user_premium = sharedPreferences.getBoolean("premium", false);
        //if (user_premium){
        //    for (int tabIcon : tabIconsPremium) {
        //        tabLayout.addTab(tabLayout.newTab().setIcon(tabIcon));
        //    }
        //}
        //else{
            for (int tabIcon : tabIcons) {
                tabLayout.addTab(tabLayout.newTab().setIcon(tabIcon));
            }
        //}

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onBackPressed();
                //fragmentManager.popBackStack();
                MainActivity.instance.setTab(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void onResume() {
        super.onResume();
        this.capture.onResume();
    }

    protected void onPause() {
        super.onPause();
        this.capture.onPause();
    }

    public void onDestroy() {
        super.onDestroy();
        this.capture.onDestroy();
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.capture.onSaveInstanceState(outState);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return this.barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    private  void showMessage(){
        int MAXMESSAGESIZE = 80;
        LayoutParams params = noBeerMessage.getLayoutParams();
        // Changes the height and width to the specified *pixels*
        params.height = MAXMESSAGESIZE;
        noBeerMessage.setLayoutParams(params);


    }

    private  void hideMessage(){
        // Gets the layout params that will allow you to resize the layout
        LayoutParams params = noBeerMessage.getLayoutParams();
        // Changes the height and width to the specified *pixels*
        params.height = 0;
        noBeerMessage.setLayoutParams(params);
    }

    private void setTimer(int seconds){
        new CountDownTimer(seconds * 1000, 1000) {
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                //showMessage();
                expand(noBeerMessage);
            }
        }.start();
    }

    public void pushFragment(Fragment fragment) {
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.container);
        if (currentFragment == null || !(currentFragment.getClass().isAssignableFrom(fragment.getClass()))) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            //transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
            transaction.replace(R.id.container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();

            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    public static void expand(final LinearLayout v) {
        v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1 ? LayoutParams.WRAP_CONTENT : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final LinearLayout v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    @Override
    public boolean setTab(int position) {
        if (position < tabLayout.getTabCount()) {
            TabLayout.Tab tab = tabLayout.getTabAt(position);
            if (tab != null) {
                tab.select();
                return true;
            }
        }
        return false;
    }
}
