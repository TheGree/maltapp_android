package com.elixeer.maltapp;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.core.SimpleRecyclerAdapter;
import com.elixeer.maltapp.Models.BeerSmall;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class TopFragment extends Fragment {
    ArrayList<BeerSmall> beers;
    MainActivity parent;
    private int allBeers = 0;
    private int currentPage = 1;
    private int paginate = 0;

    public TopFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            parent.setTab(3);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.title_top);
                actionBar.show();
            }
        }

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_top, container, false);

        RecyclerView topListView = rootView.findViewById(R.id.topListView);
        topListView.setLayoutManager(new LinearLayoutManager(getContext()));
        topListView.setAdapter(adapter);

        getTotalTops();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("TopBeer");
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(TopFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void getTotalTops() {
        currentPage = 1;
        beers = new ArrayList<>();
        getTops(currentPage);
    }

    private void getTops(int page) {
        String url = ApiUrl.TOP_RANK + "&page=" + page;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            allBeers = jsonResponse.optInt("total", 0);
                            paginate = jsonResponse.optInt("paginate", 0);
                            JSONArray jsonBeers = jsonResponse.getJSONArray("beers");
                            beers.addAll(BeerSmall.beersFromJSONArray(jsonBeers));
                            if (beers.size() < 1) {
                                Log.i(getString(R.string.app_name), "No beers found");
                            } else {
                                adapter.setList(beers);
                                adapter.setSize(allBeers);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    SimpleRecyclerAdapter adapter = new SimpleRecyclerAdapter(R.layout.list_item_top, new ArrayList<>()) {
        @Override
        public void onListItem(Object listItem, View view, final int position) {
            ImageView topRankImageView = view.findViewById(R.id.topRankViewImage);
            TextView topRankTextView = view.findViewById(R.id.topRankTextView);
            TextView topBeerPercentTextView = view.findViewById(R.id.topBeerPercentTextView);
            TextView topBeerNameTextView = view.findViewById(R.id.topBeerNameTextView);
            TextView topBreweryNameTextView = view.findViewById(R.id.topBreweryNameTextView);
            topRankImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);

            if (listItem == null) {
                int nextPage = beers.size() / paginate + 1;
                //Log.d(getString(R.string.app_name), beers.size() + ": " + nextPage + " == " + currentPage);
                if (nextPage > currentPage) {
                    currentPage = nextPage;
                    getTops(currentPage);
                }

                topBeerPercentTextView.setText("");
                topBeerNameTextView.setText("");
                topBreweryNameTextView.setText("");
            } else {
                BeerSmall beer = (BeerSmall) listItem;

                topBeerPercentTextView.setText(String.valueOf(beer.ranking));
                topBeerNameTextView.setText(beer.name);
                topBreweryNameTextView.setText(beer.brewery_name);
            }

            topRankTextView.setText(String.valueOf(position + 1));

            if (position < 3) {
                topRankTextView.setText("");
                switch (position) {
                    case 0:
                        topRankImageView.setImageResource(R.drawable.ic_king_gold);
                        break;
                    case 1:
                        topRankImageView.setImageResource(R.drawable.ic_king_silver);
                        break;
                    case 2:
                        topRankImageView.setImageResource(R.drawable.ic_king_bronze);
                        break;
                }
            } else {
                topRankTextView.setTextColor(getResources().getColor(R.color.newsDate));
                topRankImageView.setImageResource(R.drawable.ic_circunference);
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (position < beers.size()) {
                        // Analytics event
                        ((AnalyticsApplication) getActivity().getApplication()).sendEvent("TopBeer", "beer_pressed", beers.get(position).name);

                        if (parent != null) {
                            Fragment beerFragment = new BeerFragment();
                            Bundle arguments = new Bundle();
                            arguments.putInt("BEER_ID", beers.get(position).id);
                            beerFragment.setArguments(arguments);
                            parent.pushFragment(beerFragment);
                        }
                    }
                }
            });
        }
    };
}
