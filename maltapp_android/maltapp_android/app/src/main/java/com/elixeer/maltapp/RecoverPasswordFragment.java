package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class RecoverPasswordFragment extends Fragment {

    MainActivity parent;
    Button recoverButton;

    public RecoverPasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            //parent.setTab(3);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.title_login);
                actionBar.show();
            }
        }
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_recover_password, container, false);

        final EditText emailEditText = rootView.findViewById(R.id.emailRecoverEditText);

        recoverButton = rootView.findViewById(R.id.recoverPasswordButton);

        recoverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(emailEditText.getText().toString().equals("")) {
                    showAlertMsj(getString(R.string.loginAllParams));
                } else {
                    if(isEmailValid(emailEditText.getText().toString())) {
                        recoverButton.setEnabled(false);
                        recover(emailEditText.getText().toString());
                    } else {
                        showAlertMsj(getString(R.string.loginValidEmail));
                    }
                }
            }
        });
        return rootView;
    }

    private void showAlertMsj (String msj) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_msj, null);
        TextView popMsjTextView = pop.findViewById(R.id.popMsjTextView);
        popMsjTextView.setText(msj);
        AlertDialog.Builder builder = new AlertDialog.Builder(RecoverPasswordFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });
        alert.show();
    }

    public boolean isEmailValid(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        return Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE).matcher(email).matches();
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(RecoverPasswordFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void recover(String email){
        String url = ApiUrl.EMAIL_RECOVERY + "&email=" + email;
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    showAlertMsj(getString(R.string.mailSentMessage));
                    recoverButton.setEnabled(true);
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }
}
