package com.elixeer.maltapp.Models;

import android.content.Context;
import android.content.SharedPreferences;

import com.elixeer.maltapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

public class Beer implements Serializable {

    public int id;
    public String name;
    public String description;
    public int brewery_id;
    public String brewery_name;
    public boolean brewery_approved;
    public int style_id;
    public String style_name;
    public String style_description;
    public int type_id;
    public int presentation_id;
    public String presentation_name;
    public int glass_id;
    public String glass_name;
    public String glass_description;
    public int abv;
    public int ibu;
    public int srm;
    public int weight;
    public int original_extract;
    public long ranking;
    public int number_ranks;
    public int number_comments;
    public boolean has_min_ranks;
    public boolean approved;
    public double barcode;
    public String thumb;
    public String medium;

    public Beer(JSONObject jsonBeer, Context context) {
        String language = Locale.getDefault().getLanguage();
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
            if (sharedPreferences.contains("language")) {
                language = sharedPreferences.getString("language", Locale.getDefault().getLanguage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String language2 = "en";
        if (language.equals("en")) {
            language2 = "es";
        }

        this.id = jsonBeer.optInt("id", 0);
        this.name = jsonBeer.optString("name", null);
        this.description = jsonBeer.optString("description_" + language, "");
        if (this.description.equals("")) {
            this.description = jsonBeer.optString("description_" + language2, "");
        }
        this.brewery_id = jsonBeer.optInt("brewery_id", 0);
        this.brewery_name = jsonBeer.optString("brewery_name", "");
        this.brewery_approved = jsonBeer.optBoolean("brewery_approved", false);
        this.style_id = jsonBeer.optInt("type_id", 0);
        this.style_name = jsonBeer.optString("type_name", "");
        this.style_description = jsonBeer.optString("type_description_" + language, "");
        if (this.style_description.equals("")) {
            this.style_description = jsonBeer.optString("type_description_" + language2, "");
        }
        this.type_id = jsonBeer.optInt("style_id", 0);
        this.presentation_id = jsonBeer.optInt("presentation_id", 0);
        this.presentation_name = jsonBeer.optString("presentation_name", "");
        this.glass_id = jsonBeer.optInt("glass_id", 0);
        this.glass_name = jsonBeer.optString("glass_name", "");
        this.glass_description = jsonBeer.optString("glass_description_" + language, "");
        if (this.glass_description.equals("")) {
            this.glass_description = jsonBeer.optString("glass_description_" + language2, "");
        }
        this.glass_id = jsonBeer.optInt("glass_id", 0);
        this.abv = jsonBeer.optInt("abv", 0);
        this.ibu = jsonBeer.optInt("ibu", 0);
        this.srm = jsonBeer.optInt("color", 0);
        this.weight = jsonBeer.optInt("weight", 0);
        this.original_extract = jsonBeer.optInt("original_extract", 0);
        double bruteRank = jsonBeer.optDouble("ranking", 0);
        this.ranking = Math.round(bruteRank);
        this.number_ranks = jsonBeer.optInt("number_ranks", 0);
        this.number_comments = jsonBeer.optInt("number_comments", 0);
        this.approved = jsonBeer.optBoolean("approved", false);
        this.has_min_ranks = jsonBeer.optBoolean("has_min_ranks", false);
        this.barcode = jsonBeer.optDouble("barcode", 0);
        this.thumb = jsonBeer.optString("thumb", null);
        this.medium = jsonBeer.optString("medium", null);
    }

    public static ArrayList<Beer> beersFromJSONArray(JSONArray jsonBeers, Context context) {
        ArrayList<Beer> beers = new ArrayList<>();
        for (int i = 0; i < jsonBeers.length(); i++) {
            JSONObject jsonBeer = jsonBeers.optJSONObject(i);
            if (jsonBeer != null) {
                beers.add(new Beer(jsonBeer, context));
            }
        }
        return beers;
    }

}
