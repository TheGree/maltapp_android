package com.elixeer.maltapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import static com.elixeer.maltapp.SpecialMapFragment.parent;


public class WebViewFragment extends Fragment {

    public WebViewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle("");
                actionBar.show();
            }
        }
        Bundle arguments = getArguments();

        String url = arguments.getString("NETWORK", "");

        View view = inflater.inflate(R.layout.fragment_webview, container, false);

        // Adds Progrss bar Support
        //getActivity().getWindow().requestFeature(Window.FEATURE_PROGRESS);
        //getActivity().setContentView(R.layout.activity_main);

        // Makes Progress bar Visible
        //getActivity().getWindow().setFeatureInt( Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);

        WebView socialNetworkWebView = (WebView) view.findViewById(R.id.socialNetworkWebView);
        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Loading...", true);

        socialNetworkWebView.setWebViewClient(new WebViewClient());

        // Enable Javascript
        socialNetworkWebView.getSettings().setJavaScriptEnabled(true);
        socialNetworkWebView.getSettings().setDomStorageEnabled(true);
        socialNetworkWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress)
            {
                //Make the bar disappear after URL is loaded, and changes string to Loading...
                WebViewFragment.this.getActivity().setTitle("Loading...");
                WebViewFragment.this.getActivity().setProgress(progress * 100); //Make the bar disappear after URL is loaded

                // Return the app name after finish loading
                if(progress == 100)
                    WebViewFragment.this.getActivity().setTitle(R.string.app_name);
            }
        });
        socialNetworkWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                    url = "http://" + url;
                }
                // Otherwise allow the OS to handle things like tel, mailto, etc.
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon){
                dialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url){
                dialog.dismiss();
            }
        });
        if(!url.equals("")) {
            socialNetworkWebView.loadUrl(url);
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
    }
}