package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.alebrije.core.SimpleRecyclerAdapter;
import com.elixeer.maltapp.Models.BeerSmall;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SpecialListFragment extends Fragment {
    MainActivity parent;

    RecyclerView specialEventBeers;
    SearchView specialEventSearch;

    ArrayList<BeerSmall> beers;
    private int allBeers = 0;
    private int currentPage = 1;
    private int paginate = 0;
    String searchParams;
    String event_name;
    int event_id;
    int user_id;
    Boolean updateVote;
    public SpecialListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        if (sharedPreferences.contains("userId")) {
            user_id = sharedPreferences.getInt("userId", -1);
        }

        event_id = getArguments().getInt("SPECIAL_EVENT_ID", -1);
        event_name = getArguments().getString("SPECIAL_EVENT_NAME", "");

        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(event_name);
                actionBar.show();
            }
        }

        View rootView = inflater.inflate(R.layout.fragment_special_list, container, false);

        specialEventSearch = rootView.findViewById(R.id.specialEventSearch);
        specialEventBeers = rootView.findViewById(R.id.specialEventBeers);

        searchParams = "";
        specialEventSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchParams = query;//.trim();
                if (!searchParams.isEmpty()) {
                    /*
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("home", "beer_search", searchParams);
                    */

                    getTotalBeers();
                    specialEventSearch.clearFocus();
                    return true;
                }
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        specialEventBeers.setLayoutManager(new LinearLayoutManager(getContext()));
        specialEventBeers.setAdapter(adapter);

        if(event_id != -1) {
            getTotalBeers();
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void openBeer(int beer_id) {
        if (parent != null) {
            Fragment breweryFragment = new BeerFragment();
            Bundle arguments = new Bundle();
            arguments.putInt("BEER_ID", beer_id);
            breweryFragment.setArguments(arguments);
            parent.pushFragment(breweryFragment);
        }
    }

    private void getTotalBeers() {
        currentPage = 1;
        beers = new ArrayList<>();
        getBeers(currentPage);
    }

    private void getBeers(int page) {
        String url = ApiUrl.BEER_SEARCH
                + "&event_id=" + event_id
                + "&name=" + searchParams.replace(" ","+")
                + "&page=" + page;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            allBeers = jsonResponse.optInt("total", 0);
                            paginate = jsonResponse.optInt("paginate", 0);
                            JSONArray jsonBeers = jsonResponse.getJSONArray("beers");
                            beers.addAll(BeerSmall.beersFromJSONArray(jsonBeers));
                            if (beers.size() < 1) {
                                Log.i(getString(R.string.app_name), "No beers found");
                            }// else {
                            adapter.setList(beers);
                            adapter.setSize(allBeers);
                            adapter.notifyDataSetChanged();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(SpecialListFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void createVote(int beer_id) {
        String Params = "{";
        Params += "\"event_id\":\"" + event_id + "\"";
        Params += ",\"user_id\":\"" + user_id + "\"";
        Params += ",\"beer_id\":\"" + beer_id + "\"";
        Params += "}";

        String url = ApiUrl.EVENTS_VOTE;
        String[][] headers = {{"Content-Type", "application/json"}};
        new AsyncRequestString("POST", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View pop = inflater.inflate(R.layout.pop_item_special_event_vote, null);
                            Button popVoteButton = pop.findViewById(R.id.popVoteButton);

                            AlertDialog.Builder builder = new AlertDialog.Builder(SpecialListFragment.this.getActivity())
                                    //.setTitle(getString(R.string.maltappNotice))
                                    .setView(pop);
                            final AlertDialog alert = builder.create();
                            alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                }
                            });

                            String responseContain = jsonResponse.toString();
                            //if (jsonResponse.has("updated")) {
                            if (responseContain.contains("updated")) {
                                updateVote=true;
                            }
                            //else if (jsonResponse.has("added")){
                            else if (responseContain.contains("added")){
                                updateVote=false;
                            }

                            popVoteButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if(updateVote){
                                        int count = getFragmentManager().getBackStackEntryCount() - 2;
                                        int count_id = getFragmentManager().getBackStackEntryAt(count).getId();
                                        getFragmentManager().popBackStack(count_id, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                        //getFragmentManager().popBackStack();
                                        Fragment specialTopFragment = new SpecialTopFragment();
                                        Bundle arguments = new Bundle();
                                        arguments.putInt("SPECIAL_EVENT_ID", event_id);
                                        arguments.putString("SPECIAL_EVENT_NAME", event_name);
                                        specialTopFragment.setArguments(arguments);
                                        parent.pushFragment(specialTopFragment);
                                        alert.dismiss();
                                    }
                                    else {
                                        //getFragmentManager().popBackStack();
                                        Fragment specialTopFragment = new SpecialTopFragment();
                                        Bundle arguments = new Bundle();
                                        arguments.putInt("SPECIAL_EVENT_ID", event_id);
                                        arguments.putString("SPECIAL_EVENT_NAME", event_name);
                                        specialTopFragment.setArguments(arguments);
                                        parent.pushFragment(specialTopFragment);
                                        alert.dismiss();
                                    }

                                }
                            });
                            alert.show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .setData(Params)
                .setHeaders(headers)
                .execute();
    }

    void loginPopUp() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_login, null);
        Button popLoginButton = pop.findViewById(R.id.popLoginButton);

        AlertDialog.Builder builder = new AlertDialog.Builder(SpecialListFragment.this.getActivity())
                //.setTitle(getString(R.string.maltappNotice))
                .setView(pop)
                .setPositiveButton(R.string.ok, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });

        popLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment registerFragment = new RegisterFragment();
                parent.pushFragment(registerFragment);
                alert.dismiss();
            }
        });
        alert.show();
    }

    SimpleRecyclerAdapter adapter = new SimpleRecyclerAdapter(R.layout.list_item_special_event_beers, new ArrayList<>()) {
        @Override
        public void onListItem(Object listItem, View view, final int position) {
            final ImageView specialEventBeerImage = view.findViewById(R.id.specialEventBeerImage);
            TextView specialEventBeerName = view.findViewById(R.id.specialEventBeerName);
            TextView specialEventBeerBrewery = view.findViewById(R.id.specialEventBeerBrewery);
            ImageButton specialEventBeerVoteImage = view.findViewById(R.id.specialEventBeerVoteImage);
            LinearLayout specialBeerDetail = view.findViewById(R.id.specialBeerDetail);

            if (listItem == null) {
                int nextPage = beers.size() / paginate + 1;
                Log.d(getString(R.string.app_name), beers.size() + ": " + nextPage + " == " + currentPage);
                if (nextPage > currentPage) {
                    currentPage = nextPage;
                    getBeers(currentPage);
                }

                specialEventBeerName.setText("");
                specialEventBeerBrewery.setText("");
                specialEventBeerImage.setImageResource(R.drawable.img_default_beer);
            } else {
                final BeerSmall beer = (BeerSmall) listItem;

                if (beer.thumb.contains("/missing.png")) {
                    specialEventBeerImage.setImageResource(R.drawable.img_default_beer);
                } else {
                    specialEventBeerImage.setImageBitmap(null);
                    ImageCache.getImage(beer.thumb, getContext(), new ImageCache.OnResponseListener() {
                        @Override
                        public void onResponse(Bitmap bitmap, String error) {
                            specialEventBeerImage.setImageBitmap(bitmap);
                        }
                    });
                }
                specialEventBeerName.setText(beer.name);
                specialEventBeerBrewery.setText(beer.brewery_name);

                specialEventBeerVoteImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (user_id > 0) {
                            createVote(beer.id);
                        } else {
                            loginPopUp();
                        }
                    }
                });
                specialBeerDetail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (beers != null /*&& beer.id < beers.size()*/) {
                            openBeer(beer.id);
                        }
                    }
                });
            }

            /*
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (beers != null && position < beers.size()) {
                        openBeer(beers.get(position).id);
                    }
                }
            });
            */
        }
    };

}