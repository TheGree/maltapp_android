package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.alebrije.core.SimpleRecyclerAdapter;
import com.elixeer.maltapp.Models.Rankings;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class BeerRankListFragment extends Fragment {
    ArrayList<Rankings> rankings;
    MainActivity parent;
    private int allRankings = 0;
    private int currentPage = 1;
    private int paginate = 0;
    int beer_id;
    String beer_name;

    public BeerRankListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        beer_id = getArguments().getInt("BEER_ID");
        beer_name = getArguments().getString("BEER_NAME");

        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(beer_name);
                actionBar.show();
            }
        }

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_beer_ranking_list, container, false);

        RecyclerView topListView = rootView.findViewById(R.id.rankingsListView);
        topListView.setLayoutManager(new LinearLayoutManager(getContext()));
        topListView.setAdapter(adapter);

        if (isOnline()) {
            getTotalRankList();
        } else {
            notInternetMSG();
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        /*
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("TopBeer");
        */
    }

    private boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager != null && connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(BeerRankListFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void getTotalRankList() {
        currentPage = 1;
        rankings = new ArrayList<>();
        getRankList(currentPage);
    }

    private void getRankList(int page) {
        String url = ApiUrl.RANK_LIST + "&page=" + page + "&beer_id=" + beer_id + "&order=updated_at-DESC";

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            allRankings = jsonResponse.optInt("total", 0);
                            paginate = jsonResponse.optInt("paginate", 0);
                            JSONArray jsonRankings = jsonResponse.getJSONArray("comments");
                            rankings.addAll(Rankings.rankingsFromJSONArray(jsonRankings));
                            if (rankings.size() < 1) {
                                Log.i(getString(R.string.app_name), "No comments found");
                            } else {
                                adapter.setList(rankings);
                                adapter.setSize(allRankings);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    SimpleRecyclerAdapter adapter = new SimpleRecyclerAdapter(R.layout.list_item_beer_rankings, new ArrayList<>()) {
        @Override
        public void onListItem(Object listItem, View view, final int position) {
            RelativeLayout infoRelativeLayout = view.findViewById(R.id.infoRelativeLayout);
            final ImageView beerRankingListImageView = view.findViewById(R.id.beerRankingListImageView);
            ImageView beerRankingListNullImageView = view.findViewById(R.id.beerRankingListNullImageView);
            final TextView beerRankingListCommentTextView = view.findViewById(R.id.beerRankingListCommentTextView);
            TextView beerRankingListNameTextView = view.findViewById(R.id.beerRankingListNameTextView);
            TextView beerRankingListDateTextView = view.findViewById(R.id.beerRankingListDateTextView);
            TextView beerRankingListRateTextView = view.findViewById(R.id.beerRankingListRateTextView);
            final ImageView arrowRankingList = view.findViewById(R.id.arrowRankingList);
            arrowRankingList.setVisibility(View.GONE);
            if (listItem == null) {
                int nextPage = rankings.size() / paginate + 1;
                if (nextPage > currentPage) {
                    currentPage = nextPage;
                    getRankList(currentPage);
                }

                beerRankingListCommentTextView.setText("");
                beerRankingListCommentTextView.setMaxLines(25);
                beerRankingListNameTextView.setText("");
                beerRankingListDateTextView.setText("");
                beerRankingListRateTextView.setText("");

                //beerRankingListImageView.setImageResource(R.drawable.ic_no_photo);
                beerRankingListImageView.setVisibility(View.GONE);
                beerRankingListNullImageView.setVisibility(View.VISIBLE);
            } else {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MMM/yyyy", Locale.getDefault());
                Rankings rank = (Rankings) listItem;

                beerRankingListCommentTextView.setMaxLines(25);
                beerRankingListCommentTextView.setText(String.valueOf(rank.comment));
                beerRankingListNameTextView.setText(String.valueOf(rank.userName + " " + rank.userLastname));
                beerRankingListDateTextView.setText(simpleDateFormat.format(rank.updated));
                beerRankingListRateTextView.setText(String.valueOf(rank.rank));

                if (rank.userImg.contains("/missing.png")) {
                    beerRankingListImageView.setVisibility(View.GONE);
                    beerRankingListNullImageView.setVisibility(View.VISIBLE);
                } else {
                    beerRankingListImageView.setVisibility(View.VISIBLE);
                    beerRankingListNullImageView.setVisibility(View.GONE);
                    beerRankingListImageView.setImageBitmap(null);
                    ImageCache.getImage(rank.userImg, getContext(), new ImageCache.OnResponseListener() {
                        @Override
                        public void onResponse(Bitmap bitmap, String error) {
                            beerRankingListImageView.setImageBitmap(bitmap);
                        }
                    });
                }

                beerRankingListCommentTextView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (beerRankingListCommentTextView.getLineCount() > 1) {
                            arrowRankingList.setVisibility(View.VISIBLE);
                            beerRankingListCommentTextView.setMaxLines(1);
                        }
                    }
                });

                infoRelativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (beerRankingListCommentTextView.getMaxLines() > 1) {
                            if (beerRankingListCommentTextView.getLineCount() > 1) {
                                arrowRankingList.setVisibility(View.VISIBLE);
                            }
                            beerRankingListCommentTextView.setMaxLines(1);
                        } else {
                            beerRankingListCommentTextView.setMaxLines(25);
                            arrowRankingList.setVisibility(View.GONE);
                        }
                    }
                });
            }
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (position < rankings.size()) {
                    /*
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("TopBeer", "beer_pressed", rankings.get(position).name);
                    */
                        if (parent != null) {
                        /*
                        Fragment beerFragment = new BeerFragment();
                        Bundle arguments = new Bundle();
                        arguments.putInt("BEER_ID", rankings.get(position).id);
                        beerFragment.setArguments(arguments);
                        parent.pushFragment(beerFragment);
                        */
                        }
                    }
                }
            });
        }
    };
}
