package com.elixeer.maltapp.Models;

import android.content.Context;
import android.content.SharedPreferences;

import com.elixeer.maltapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

public class Promo implements Serializable {

    public int id;
    public String name;
    public String description;
    public int points;
    public String imageUrlThumb;
    public String imageUrlMedium;
    public int type;

    public Promo() {

    }

    public Promo(JSONObject jsonPromo, Context context) {
        String language = Locale.getDefault().getLanguage();
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
            if (sharedPreferences.contains("language")) {
                language = sharedPreferences.getString("language", Locale.getDefault().getLanguage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String language2 = "en";
        if (language.equals("en")) {
            language2 = "es";
        }

        this.id = jsonPromo.optInt("id", 0);
        this.name = jsonPromo.optString("name_" + language, "");
        if (this.name.equals("")) {
            this.name = jsonPromo.optString("name_" + language2, "");
        }
        this.description = jsonPromo.optString("description_" + language, "");
        if (this.description.equals("")) {
            this.description = jsonPromo.optString("description_" + language2, "");
        }
        this.points = jsonPromo.optInt("points", 0);
        this.imageUrlThumb = jsonPromo.optString("thumb", "");
        this.imageUrlMedium = jsonPromo.optString("medium", "");
    }

    public static ArrayList<Promo> promosFromJSONArray(JSONArray jsonPromos, Context context) {
        ArrayList<Promo> promos = new ArrayList<>();
        for (int i = 0; i < jsonPromos.length(); i++) {
            JSONObject jsonPromo = jsonPromos.optJSONObject(i);
            if (jsonPromo != null) {
                promos.add(new Promo(jsonPromo, context));
            }
        }
        return promos;
    }

}
