package com.elixeer.maltapp.Models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class History implements Serializable {

    public int id;
    public int beer_id;
    public String beer_name;
    public Date created;
    public String foursquare_name;
    public String foursquare_address;
    public int latitude;
    public int longitude;
    public boolean approved;
    public String brewery_name;
    public boolean brewery_approved;
    public String thumb;

    public History(JSONObject jsonHistory) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

        this.id = jsonHistory.optInt("id", 0);
        this.beer_id = jsonHistory.optInt("beer_id", 0);
        this.beer_name = jsonHistory.optString("beer_name", "");

        String created = jsonHistory.optString("created", null);
        this.created = simpleDateFormat.parse(created, new ParsePosition(0));

        this.foursquare_name = jsonHistory.optString("foursquare_name", "");
        this.foursquare_address = jsonHistory.optString("foursquare_address", "");
        this.latitude = jsonHistory.optInt("latitude", 0);
        this.longitude = jsonHistory.optInt("longitude", 0);
        this.approved = jsonHistory.optBoolean("approved", false);
        this.brewery_name = jsonHistory.optString("brewery_name", "");
        this.brewery_approved = jsonHistory.optBoolean("brewery_approved", false);
        this.thumb = jsonHistory.optString("thumb", null);
    }

    public static ArrayList<History> historiesFromJSONArray(JSONArray jsonHistories) {
        ArrayList<History> Histories = new ArrayList<>();
        for (int i = 0; i < jsonHistories.length(); i++) {
            JSONObject jsonHistory = jsonHistories.optJSONObject(i);
            if (jsonHistory != null) {
                Histories.add(new History(jsonHistory));
            }
        }
        return Histories;
    }

}
