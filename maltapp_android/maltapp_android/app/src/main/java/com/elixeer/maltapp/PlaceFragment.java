package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Models.Place;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * Created by alebrije2 on 1/27/16.
 *
 */
public class PlaceFragment extends Fragment {

    MainActivity parent;
    LinearLayout placeSubscription;
    ImageView placesImageView;
    Button findPlaceButton;
    Button seeBreweriesButton;
    TextView placesNameText;
    TextView placesAddressText;
    TextView placesDescriptionText;
    ImageButton placePhoneButton;
    ImageButton placeMailButton;
    ImageButton placeFacebookButton;
    ImageButton placeWebButton;
    //BeerImageCarousel placesBeerCarousel;
    Place place;
    int place_id;
    int nearby_type;

    public PlaceFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        place_id = getArguments().getInt("PLACE_ID", -1);
        nearby_type = getArguments().getInt("NEARBY");

        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            //parent.setTab(1);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                switch(nearby_type){
                    case 1:
                        actionBar.setTitle(R.string.bars);
                        break;
                    case 3:
                        actionBar.setTitle(R.string.stores);
                        break;
                }
                actionBar.show();
            }
        }
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_place, container, false);
        placeSubscription = rootView.findViewById(R.id.placeSubscription);
        placesImageView = rootView.findViewById(R.id.placesImageView);
        findPlaceButton = rootView.findViewById(R.id.findPlaceButton);
        seeBreweriesButton = rootView.findViewById(R.id.seeBreweriesButton);
        placesNameText = rootView.findViewById(R.id.placesNameText);
        placesAddressText = rootView.findViewById(R.id.placesAddressText);
        placesDescriptionText = rootView.findViewById(R.id.placesDescriptionText);
        //placesBeerCarousel = rootView.findViewById(R.id.placesBeerCarousel);
        placePhoneButton = rootView.findViewById(R.id.placePhoneButton);
        placeMailButton = rootView.findViewById(R.id.placeMailButton);
        placeFacebookButton = rootView.findViewById(R.id.placeFacebookButton);
        placeWebButton = rootView.findViewById(R.id.placeWebButton);

        findPlaceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (place != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("bar_details", "directions_pressed", place.name);

                    try {
                        String uri = "geo: " + place.latitude + "," + place.longitude + "?q=" + place.latitude + "," + place.longitude;
                        startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri)));
                    } catch (Exception e) {
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + place.latitude + "," + place.longitude));
                        startActivity(intent);
                    }
                }
            }
        });

        seeBreweriesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (place != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("brewery_details", "carousel_pressed", place.name);

                    if (parent != null) {
                        Fragment carouselFragment = new CarouselFragment();
                        Bundle arguments = new Bundle();
                        arguments.putString("CAROUSEL_NAME", place.name);
                        arguments.putSerializable("CAROUSEL_ID", place.id);
                        arguments.putInt("CAROUSEL_TYPE", 2);
                        carouselFragment.setArguments(arguments);
                        parent.pushFragment(carouselFragment);
                    }
                }
            }
        });

        placePhoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (place != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Places", "contact_pressed", "Phone");

                    if (getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
                        if (!place.phone.equals("") && TextUtils.isDigitsOnly(place.phone)) {
                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                            callIntent.setData(Uri.parse("tel:" + place.phone));
                            startActivity(callIntent);
                        }
                    }
                }
            }
        });

        placeMailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (place != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Places", "contact_pressed", "Email");

                    if (!place.mail.equals("")) {
                        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", place.mail, null));
                        startActivity(Intent.createChooser(intent, getString(R.string.send)));
                    }
                }
            }
        });

        placeFacebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (place != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Places", "contact_pressed", "Facebook");

                    String facebookPath;
                    if (!place.facebook_id.equals("")) {
                        facebookPath = place.facebook_id;
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + facebookPath));
                            startActivity(intent);
                        } catch (Exception e) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/" + facebookPath)));
                        }
                    } else if (!place.facebook.equals("")) {
                        String facebookUrl = getFacebookPageURL(place.facebook);
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl));
                        startActivity(intent);
                    }
                }
            }
        });

        placeWebButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (place != null) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Places", "contact_pressed", "WebPage");
                    if (!place.website.equals("")) {
                        String url = place.website;
                        if (!url.startsWith("http://") && !url.startsWith("https://")) {
                            url = "http://" + url;
                        }
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        });

        if (place_id == -1) {
            place = (Place) getArguments().getSerializable("PLACE");
            fillPlaceData();
        } else {
            getPlace();
        }
        if (place != null) {
            fillPlaceData();
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (place != null) {
            // Analytics screen
            ((AnalyticsApplication) getActivity().getApplication()).setScreenName((place.shop ? "Shop-" : "Bar-") + place.name);
        }
    }

    private void fillPlaceData() {
        if(place.subscription){
            placeSubscription.setVisibility(View.VISIBLE);
        } else {
            placeSubscription.setVisibility(View.GONE);
        }

        if (place.phone.equals("")) {
            placePhoneButton.setAlpha(0.50f);
            placePhoneButton.setClickable(false);
        }

        if (place.mail.equals("")) {
            placeMailButton.setAlpha(0.50f);
            placeMailButton.setClickable(false);
        }

        if (place.facebook_id.equals("") && place.facebook.equals("")) {
            placeFacebookButton.setAlpha(0.50f);
            placeFacebookButton.setClickable(false);
        }

        if (place.website.equals("")) {
            placeWebButton.setAlpha(0.50f);
            placeWebButton.setClickable(false);
        }

        ImageCache.getImage(place.imageUrlThumb, getContext(), new ImageCache.OnResponseListener() {
            @Override
            public void onResponse(Bitmap bitmap, String error) {
                placesImageView.setImageBitmap(bitmap);
            }
        });
        placesNameText.setText(place.name);
        placesAddressText.setText(place.address);
        if (place.description.equals("")) {
            placesDescriptionText.setVisibility(View.GONE);
        } else {
            placesDescriptionText.setText(place.description);
        }
        //getBeerImages(place.id);
    }

    //method to get the right URL to use in the intent
    public String getFacebookPageURL(String fbPath) {
        fbPath = fbPath.toLowerCase()
                .replace("https://", "")
                .replace("http://", "")
                .replace("www.facebook.com/", "")
                .replace("facebook.com/", "");

        String FACEBOOK_URL = "https://www.facebook.com/" + fbPath;
        String FACEBOOK_PAGE_ID = fbPath;

        PackageManager packageManager = getContext().getPackageManager();
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
                if (versionCode >= 3002850) { //newer versions of fb app
                    return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
                } else { //older versions of fb app
                    return "fb://page/" + FACEBOOK_PAGE_ID;
                }
            } else {
                return FACEBOOK_URL; //normal web url
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL; //normal web url
        }
    }

    private void getPlace() {
        String url = ApiUrl.GET_PLACE + place_id + ".json" + ApiUrl.ACCESS_TOKEN;

        new AsyncRequestString("GET", url, getContext(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONObject jsonPlace = jsonResponse.getJSONObject("place");
                            place = new Place(jsonPlace, getContext());
                            fillPlaceData();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(PlaceFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (parent != null) {
                            parent.pushFragment(new HomeFragment());
                        }
                    }
                });

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (parent != null) {
                    parent.pushFragment(new HomeFragment());
                }
            }
        });
        alert.show();
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    /*
    private void getBeerImages(int placeId) {
        if (isOnline()) {
            String url = ApiUrl.BREWERY_BEERS + "&place_id=" + placeId;

            new AsyncRequestString("GET", url) {
                @Override
                public void onResult(int responseCode, String response) {
                    if (response == null) {
                        Log.w(getString(R.string.app_name), "WARNING: Response is null (" + responseCode + ")");
                    } else {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            if (jsonResponse.has("error")) {
                                reportError(jsonResponse.getString("error"));
                            } else {
                                JSONArray jsonBeers = jsonResponse.getJSONArray("beers");
                                ArrayList<BeerSmall> beers = BeerSmall.beersFromJSONArray(jsonBeers);
                                if (beers.size() < 1) {
                                    Log.i(getString(R.string.app_name), "No beers found");
                                } else {
                                    placesBeerCarousel.setBeers(parent, beers, "bar_details", "beer_pressed", place.name);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.execute();
        }
    }
    */
}
