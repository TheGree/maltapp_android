package com.elixeer.maltapp.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Models.Promo;
import com.elixeer.maltapp.R;

import java.util.ArrayList;

public class PromoCustomAdapter extends BaseAdapter {

    private static final int TYPE_HEADER_NEW = 0;
    private static final int TYPE_HEADER_MY = 1;
    private static final int TYPE_NEW = 2;
    private static final int TYPE_MY = 3;
    private static final int TYPE_BEER_COINS = 4;

    private ArrayList<Promo> mData = new ArrayList<>();
    private LayoutInflater mInflater;

    public PromoCustomAdapter(Context context) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItem(String title, int type) {
        Promo promo = new Promo();
        promo.name = title;
        promo.type = type;
        addItem(promo);
    }

    public void addItem(final Promo item) {
        mData.add(item);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 5;
    }

    @Override
    public int getItemViewType(int position) {
        return mData.get(position).type;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        int rowType = getItemViewType(position);

        holder = new ViewHolder();

        switch (rowType) {
            case TYPE_HEADER_NEW: {
                convertView = mInflater.inflate(R.layout.list_item_promo_title, null);

                holder.promoTitleNameTextView = convertView.findViewById(R.id.promoTitleNameTextView);

                holder.promoTitleNameTextView.setText(mData.get(position).name);

                break;
            }
            case TYPE_HEADER_MY: {
                convertView = mInflater.inflate(R.layout.list_item_promo_shops, null);

                holder.shopsHistoryTitleNameTextView = convertView.findViewById(R.id.shopsHistoryTitleNameTextView);

                holder.shopsHistoryTitleNameTextView.setText(mData.get(position).name);

                break;
            }
            case TYPE_NEW: {
                convertView = mInflater.inflate(R.layout.list_item_promo_new, null);

                holder.promoNewPointsTextView = convertView.findViewById(R.id.promoNewPointsTextView);
                holder.promoNewTitleTextView = convertView.findViewById(R.id.promoNewTitleTextView);
                holder.promoNewListImageView = convertView.findViewById(R.id.promoNewListImageView);

                holder.promoNewPointsTextView.setText(String.valueOf(mData.get(position).points + " " + " BeerCoins"));
                holder.promoNewTitleTextView.setText(mData.get(position).name);
                if (mData.get(position).imageUrlThumb.contains("/missing.png")) {
                    holder.promoNewListImageView.setImageResource(R.drawable.ic_no_photo);
                } else {
                    holder.promoNewListImageView.setImageBitmap(null);
                    ImageCache.getImage(mData.get(position).imageUrlThumb, parent.getContext(), new ImageCache.OnResponseListener() {
                        @Override
                        public void onResponse(Bitmap bitmap, String error) {
                            holder.promoNewListImageView.setImageBitmap(bitmap);
                        }
                    });
                }

                break;
            }
            case TYPE_MY: {
                convertView = mInflater.inflate(R.layout.list_item_promo_my, null);

                //holder.promoMyPointsTextView = convertView.findViewById(R.id.promoMyPointsTextView);
                holder.promoOwnListImageView = convertView.findViewById(R.id.promoOwnListImageView);
                holder.promoMyTitleTextView = convertView.findViewById(R.id.promoMyTitleTextView);

                //holder.promoMyPointsTextView.setText(String.valueOf(mData.get(position).points));
                if (mData.get(position).imageUrlThumb.contains("/missing.png")) {
                    holder.promoOwnListImageView.setImageResource(R.drawable.ic_no_photo);
                } else {
                    holder.promoOwnListImageView.setImageBitmap(null);
                    ImageCache.getImage(mData.get(position).imageUrlThumb, parent.getContext(), new ImageCache.OnResponseListener() {
                        @Override
                        public void onResponse(Bitmap bitmap, String error) {
                            holder.promoOwnListImageView.setImageBitmap(bitmap);
                        }
                    });
                }
                holder.promoMyTitleTextView.setText(mData.get(position).name);

                break;
            }
            case TYPE_BEER_COINS: {
                convertView = mInflater.inflate(R.layout.list_item_promo_beer_coins, null);
                convertView.setClickable(false);
                holder.promotionsPointsTextView = convertView.findViewById(R.id.promotionsPointsTextView);
                holder.promotionsPointsTextView.setText(mData.get(position).name);
                holder.shopButton = convertView.findViewById(R.id.shopButton);
                holder.historyButton = convertView.findViewById(R.id.historyButton);
                break;
            }
        }
        convertView.setTag(holder);

        return convertView;
    }

    public static class ViewHolder {
        public TextView shopsHistoryTitleNameTextView;
        public TextView promoTitleNameTextView;
        public ImageView promoNewListImageView;
        public TextView promoNewPointsTextView;
        public TextView promoNewTitleTextView;
        //public TextView promoMyPointsTextView;
        public ImageView promoOwnListImageView;
        public TextView promoMyTitleTextView;
        public TextView promotionsPointsTextView;
        public Button shopButton;
        public Button historyButton;
    }
}
