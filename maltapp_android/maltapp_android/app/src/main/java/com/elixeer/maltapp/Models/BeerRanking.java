package com.elixeer.maltapp.Models;

import org.json.JSONObject;

import java.io.Serializable;

public class BeerRanking implements Serializable {

    public long ranking;
    public String comment;
    public boolean fav;
    //public boolean wish;
    public boolean created;
    public boolean notFav;
    //public boolean notWish;

    public BeerRanking(JSONObject jsonBeer) {
        if (jsonBeer.optLong("ranking", -1) < 0) {
            created = false;
        } else {
            created = true;
        }
        this.ranking = jsonBeer.optLong("ranking", 0);
        this.comment = jsonBeer.optString("comment", "");
        if (this.comment.equals("null")) {
            this.comment = "";
        }

        if (jsonBeer.optString("fav").equals("f")) {
            this.fav = true;
            this.notFav = false;
        } else if (jsonBeer.optString("fav").equals("t")) {
            this.fav = false;
            this.notFav = false;
        } else {
            this.fav = false;
            this.notFav = true;
        }

/*
        if(jsonBeer.optString("wish").equals("f")) {
            this.wish = true;
            this.notWish = false;
        } else if(jsonBeer.optString("wish").equals("t")) {
            this.wish = false;
            this.notWish = false;
        } else {
            this.wish = false;
            this.notWish = true;
        }
*/
    }

}
