package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Models.User;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class ProfileFragment extends Fragment {
    LinearLayout profileBlockLayout;
    LinearLayout ptsRankLinearLayout;
    RelativeLayout profileLayout;
    ImageView profileImageView;
    ImageView profileNullImageView;
    ImageView rankImageView;
    MainActivity parent;
    TextView nameTextView;
    TextView emailTextView;
    TextView experienceTextView;
    TextView levelTextView;
    ProgressBar experienceProgressBar;
    Button badgeButton;
    Button premiumButton;
    //TODO: PREMIUM
    Button shoplistButton;
    Button wishlistButton;
    Button favoritesButton;
    Button historyButton;
    //BillingManager billingManager;
    User user;
    String logTag;
    int user_id;
    boolean once;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        once =  true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        logTag = getString(R.string.app_name);

        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            parent.setTab(4);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.title_profile);
                actionBar.show();
            }
        }

        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        if (sharedPreferences.contains("userId")) {
            user_id = sharedPreferences.getInt("userId", -1);
        }

        experienceProgressBar = rootView.findViewById(R.id.experienceProgressBar);
        profileLayout = rootView.findViewById(R.id.profileLayout);
        profileBlockLayout = rootView.findViewById(R.id.profileBlockLayout);
        ptsRankLinearLayout = rootView.findViewById(R.id.ptsRankLinearLayout);
        profileImageView = rootView.findViewById(R.id.profileImageView);
        profileNullImageView = rootView.findViewById(R.id.profileNullImageView);
        rankImageView = rootView.findViewById(R.id.rankImageView);
        nameTextView = rootView.findViewById(R.id.nameTextView);
        emailTextView = rootView.findViewById(R.id.emailTextView);
        experienceTextView = rootView.findViewById(R.id.experienceTextView);
        levelTextView = rootView.findViewById(R.id.levelTextView);
        badgeButton = rootView.findViewById(R.id.badgeButton);
        premiumButton = rootView.findViewById(R.id.premiumButton);
        //TODO: PREMIUM
        shoplistButton = rootView.findViewById(R.id.shoplistButton);
        wishlistButton = rootView.findViewById(R.id.wishlistButton);
        favoritesButton = rootView.findViewById(R.id.favoritesButton);
        historyButton = rootView.findViewById(R.id.historyButton);

        profileNullImageView.setColorFilter(getResources().getColor(R.color.backgroundLight));

        if (isOnline()) {
            profileLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (parent != null) {
                        Fragment registerFragment = new RegisterFragment();
                        parent.pushFragment(registerFragment);
                    }
                }
            });

            ptsRankLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (user_id > 0) {
                        Fragment pointFragment = new PointFragment();
                        Bundle arguments = new Bundle();
                        arguments.putInt("USER_ID", user_id);
                        arguments.putInt("PTS", user.points_earned);
                        arguments.putString("LEVEL", user.levelName);
                        arguments.putBoolean("PURCHASED", false);
                        pointFragment.setArguments(arguments);
                        parent.pushFragment(pointFragment);
                    } else {
                        loginPopUp();
                    }
                }
            });

            badgeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment badgesFragment = new BadgesFragment();
                    Bundle arguments = new Bundle();
                    arguments.putInt("USER_ID", user_id);
                    badgesFragment.setArguments(arguments);
                    parent.pushFragment(badgesFragment);
                }
            });

            premiumButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (user_id > 0) {
                        //TODO: PREMIEM
                        /*
                        if (totalPoint != null) {
                            if (user.isPremium && user.timeLeft >= 0) {
                                goToPremium();
                            } else {
                                buyPremiumPop();
                            }
                        }
                        */
                        //buyPremiumPop();
                        goToPremium();
                    } else {
                        loginPopUp();
                    }
                    //TODO: agregar mensaje de comprar
                }
            });

            wishlistButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (user_id > 0) {
                        Fragment wishFragment = new WishFragment();
                        Bundle arguments = new Bundle();
                        arguments.putInt("USER_ID", user_id);
                        wishFragment.setArguments(arguments);
                        parent.pushFragment(wishFragment);
                    } else {
                        loginPopUp();
                    }
                }
            });
            //TODO: PREMIUM

            shoplistButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment webViewFragment = new WebViewFragment();
                    Bundle arguments = new Bundle();
                    arguments.putString("NETWORK", ApiUrl.CERVEXXA);
                    webViewFragment.setArguments(arguments);
                    parent.pushFragment(webViewFragment);
                }
            });

            favoritesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (user_id > 0) {
                        Fragment favFragment = new FavFragment();
                        Bundle arguments = new Bundle();
                        arguments.putInt("USER_ID", user_id);
                        favFragment.setArguments(arguments);
                        parent.pushFragment(favFragment);
                    } else {
                        loginPopUp();
                    }
                }
            });

            historyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (user_id > 0) {
                        Fragment historyFragment = new HistoryFragment();
                        Bundle arguments = new Bundle();
                        arguments.putInt("USER_ID", user_id);
                        historyFragment.setArguments(arguments);
                        parent.pushFragment(historyFragment);
                    } else {
                        loginPopUp();
                    }
                }
            });
        }

        //TODO: PREMIUM
        /*
        billingManager = new BillingManager(getActivity()) {
            @Override
            public void onConsume(Purchase purchase) {
                Log.i(logTag, "CONSUMED " + purchase.getSku());
            }

            @Override
            public void onInventory(Purchase purchase) {
                Log.i(logTag, "ON INVENTORY " + purchase.getSku());
                if (BillingManager.SKU_SUBSCRIPTION.equals(purchase.getSku())) {
                    if (user != null && (!user.isPremium || user.timeLeft < 0)) {
                        String orderId = purchase.getOrderId();
                        long year = (long)24 * 60 * 60 * 1000;
                        if (orderId != null && orderId.length() > 1) {
                            orderId = orderId.substring(0, orderId.length() - 2);
                            year *= 365;
                        } else {
                            // Test purchase
                            orderId = purchase.getToken();
                        }
                        addPremium(orderId, purchase.getPurchaseTime() + year, false);
                    }
                }

                //if (BillingManager.SKU_PREMIUM.equals(purchase.getSku())) {
                //    if (user.isPremium) {
                //        extendPremium(purchase);
                //    } else {
                //        makePremium(purchase);
                //    }
                //}
            }

            @Override
            public void onPurchase(Purchase purchase) {
                Log.i(logTag, "PURCHASED " + purchase.getSku());
                if (BillingManager.SKU_SUBSCRIPTION.equals(purchase.getSku())) {
                    String orderId = purchase.getOrderId();
                    long year = (long)24 * 60 * 60 * 1000;
                    if (orderId != null && orderId.length() > 1) {
                        orderId = orderId.substring(0, orderId.length() - 2);
                        year *= 365;
                    } else {
                        // Test purchase
                        orderId = purchase.getToken();
                    }
                    addPremium(orderId, purchase.getPurchaseTime() + year, true);
                }

                //if (BillingManager.SKU_PREMIUM.equals(purchase.getSku())) {
                //    if (user.isPremium) {
                //        extendPremium(purchase);
                //    } else {
                //        makePremium(purchase);
                //    }
                //}
            }
        };
        */

        return rootView;
    }

    private boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager != null && connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (isOnline()) {
            if (user_id > 0) {
                getUser(user_id);
            }
        } else {
            profileBlockLayout.setEnabled(false);
            profileBlockLayout.setBackground(getResources().getDrawable(R.color.backgroundLight));
            profileBlockLayout.setAlpha(0.75f);
            notInternetMSG();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("Profile");
    }

    @Override
    public void onDestroy() {
        //TODO: PREMIUM
        //billingManager.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            if (parent != null) {
                parent.pushFragment(new SettingsFragment());
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void getUser(int userId) {
        String url = ApiUrl.GET_USER + userId + ".json" + ApiUrl.ACCESS_TOKEN;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONObject jsonUser = jsonResponse.getJSONObject("user");
                            user = new User(jsonUser);
                            fillUserData();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void buyPremiumPop() {
        //TODO: PREMIUM
        /*
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_buy_premium, null);
        Button buy = (Button) pop.findViewById(R.id.buyPremiumButton);
        SkuDetails skuDetails = billingManager.getProductBySku(BillingManager.SKU_SUBSCRIPTION);
        if (skuDetails == null) {
            buy.setText(R.string.unavailable_product);
        } else {
            buy.setText(String.format(getString(R.string.btn_buy), skuDetails.getPrice()));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //goToPremium();
            }
        });
        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                billingManager.purchase(BillingManager.SKU_SUBSCRIPTION);

            }
        });
        alert.show();
        */
        //TODO: P

        /*LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_msj, null);
        TextView popMsjTextView = (TextView) pop.findViewById(R.id.popMsjTextView);
        LinearLayout bodyTextLinear = (LinearLayout) pop.findViewById(R.id.bodyTextLinear);

        popMsjTextView.setText(getString(R.string.premiumAdvice));
        bodyTextLinear.setPadding(32, 72, 32, 72);

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });
        alert.show();*/
        goToPremium();
    }

    private void fillUserData() {
        String name = "";
        String lname = "";
        if (!user.firstName.equals("null")) {
            name = user.firstName;
        }
        if (!user.lastName.equals("null")) {
            lname = user.lastName;
        }
        nameTextView.setText(String.format("%s %s", name, lname));
        emailTextView.setText(user.email);
        if (!user.levelName.equals("null")) {
            levelTextView.setText(user.levelName);
        }

        experienceTextView.setText(String.valueOf(user.points_earned));
        experienceProgressBar.setMax(user.levelNext);
        experienceProgressBar.setProgress(user.points_earned);

        ImageCache.getImage(user.imageUrlMedium, getContext(), new ImageCache.OnResponseListener() {
            @Override
            public void onResponse(Bitmap bitmap, String error) {
                profileImageView.setImageBitmap(bitmap);
                profileImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                if (profileImageView.getDrawable() == null) {
                    profileNullImageView.setVisibility(View.GONE);
                } else {
                    profileNullImageView.setVisibility(View.VISIBLE);
                }
            }
        });

        int imageResource = getResources().getIdentifier("drawable/img_level_" + user.levelId, null, getContext().getPackageName());
        if (user.levelId > 11){
            imageResource = getResources().getIdentifier("drawable/img_level_11",null, getContext().getPackageName());
        }
        if (imageResource > 0) {
            rankImageView.setImageResource(imageResource);
        }

        //TODO: PREMIUM
        /*
        if (user.isPremium) {
            premiumButton.setText(getString(R.string.premium));
            shoplistButton.setVisibility(View.VISIBLE);
        } else {
            premiumButton.setText(getString(R.string.becomePremium));
            shoplistButton.setVisibility(View.GONE);
        }
        */
        //TODO: P

        /*
        long oneWeek = (long)7 * 24 * 60 * 60 * 1000;
        if (once && user.timeLeft < oneWeek) {
            premiumAlert(TimeUnit.MILLISECONDS.toDays(user.timeLeft));
            once = false;
        }
        */
    }

    void loginPopUp() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_login, null);
        Button popLoginButton = pop.findViewById(R.id.popLoginButton);

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        popLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment registerFragment = new RegisterFragment();
                parent.pushFragment(registerFragment);
                alert.dismiss();
            }
        });
        alert.show();
    }

    /*
    void premiumAlert(long days) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_extend_premium, null);
        TextView popBuyText = (TextView) pop.findViewById(R.id.popBuyText);
        Button popBuyButton = (Button) pop.findViewById(R.id.popBuyButton);

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //goToPremium();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //goToPremium();
            }
        });

        popBuyText.setText(String.format(getString(R.string.extend_premium_message_center), String.valueOf(days)));

        popBuyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                billingManager.purchase(BillingManager.SKU_SUBSCRIPTION);
            }
        });
        alert.show();
    }
    */

    //TODO: PREMIUM

    private void goToPremium() {
        Fragment premiumFragment = new PremiumFragment();
        Bundle arguments = new Bundle();
        arguments.putInt("USER_ID", user_id);
        arguments.putInt("BEER_COINS", user.points_earned - user.points_used);
        premiumFragment.setArguments(arguments);
        parent.pushFragment(premiumFragment);
    }


    /*
    private void offPremium() {
        String url = ApiUrl.GET_USER + user_id + ".json" + ApiUrl.ACCESS_TOKEN + "&premium=false";
        new AsyncRequestString("PUT", url, getActivity()) {
            @Override
            public void onError(int responseCode, String error) {

            }

            @Override
            public void onResult(int responseCode, String response) {
                if (response == null) {
                    Log.w(getString(R.string.app_name), "WARNING: Response is null (" + responseCode + ")");
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            Log.e(getString(R.string.app_name), "ERROR: " + jsonResponse.optString("error"));
                        } else {
                            JSONObject jsonObject = jsonResponse.optJSONObject("user");
                            User user = new User(jsonObject);
                            if (user.id != 0) {
                                SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean("premium", user.isPremium);
                                editor.apply();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.displayLoadingDialog(true).execute();
    }
    */

    //TODO: PREMIUM
    /*
    private void addPremium(String orderId, long expiryTimeMillis, final boolean openPremium) {
        String Params = "{";
        Params += "\"user_id\":\"" + user_id + "\"";
        Params += ",\"premium_store\": 2";
        Params += ",\"ticket\":\"" + orderId + "\"";
        Params += ",\"time_expires\":\"" + expiryTimeMillis + "\"";
        Params += "}";

        String url = ApiUrl.ADD_PREMIUM;
        String[][] headers = {{"Content-Type", "application/json"}};
        new AsyncRequestString("PUT", url, getActivity()) {
            @Override
            public void onError(int responseCode, String error) {

            }

            @Override
            public void onResult(int responseCode, String response) {
                if (response == null) {
                    Log.w(getString(R.string.app_name), "WARNING: Response is null (" + responseCode + ")");
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            Log.e(getString(R.string.app_name), "ERROR: " + jsonResponse.optString("error"));
                        } else {
                            JSONObject jsonObject = jsonResponse.optJSONObject("user");
                            User user = new User(jsonObject);
                            if (user.id != 0) {
                                SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean("premium", user.isPremium);
                                editor.apply();

                                if (openPremium) {
                                    goToPremium();
                                } else {
                                    fillUserData();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.displayLoadingDialog(true).setData(Params).setHeaders(headers).execute();
    }
    */

    /*
    private void makePremium(final Purchase consumible) {
        String url = ApiUrl.GET_USER + user_id + ".json" + ApiUrl.ACCESS_TOKEN + "&premium=true";
        new AsyncRequestString("PUT", url, getActivity()) {
            @Override
            public void onError(int responseCode, String error) {

            }

            @Override
            public void onResult(int responseCode, String response) {
                if (response == null) {
                    Log.w(getString(R.string.app_name), "WARNING: Response is null (" + responseCode + ")");
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            Log.e(getString(R.string.app_name), "ERROR: " + jsonResponse.optString("error"));
                        } else {
                            JSONObject jsonObject = jsonResponse.optJSONObject("user");
                            User user = new User(jsonObject);
                            if (user.id != 0) {
                                SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean("premium", user.isPremium);
                                editor.apply();

                                if (consumible != null) {
                                    billingManager.consume(consumible);
                                }
                                goToPremium();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.displayLoadingDialog(true).execute();
    }
    */

    /*
    private void extendPremium(final Purchase consumible) {
        String url = ApiUrl.EXTEND_PREMIUM + "&user_id=" + user_id;
        new AsyncRequestString("PUT", url, getActivity()) {
            @Override
            public void onError(int responseCode, String error) {

            }

            @Override
            public void onResult(int responseCode, String response) {
                if (response == null) {
                    Log.w(getString(R.string.app_name), "WARNING: Response is null (" + responseCode + ")");
                } else {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            Log.e(getString(R.string.app_name), "ERROR: " + jsonResponse.optString("error"));
                        } else {
                            JSONObject jsonObject = jsonResponse.optJSONObject("user");
                            User user = new User(jsonObject);
                            if (user.id != 0) {
                                if (consumible != null) {
                                    billingManager.consume(consumible);
                                }
                                goToPremium();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.displayLoadingDialog(true).execute();
    }
    */
}