package com.elixeer.maltapp.Models;

import org.json.JSONObject;

import java.io.Serializable;

public class TotalPoint implements Serializable {
    public int points_earned;
    public int points_used;

    public TotalPoint(JSONObject jsonPoint) {
        this.points_earned = jsonPoint.optInt("points_earned", 0);
        this.points_used = jsonPoint.optInt("points_used", 0);
    }
}
