package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.core.SimpleRecyclerAdapter;
import com.elixeer.maltapp.Models.BeerSmall;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SpecialTopFragment  extends Fragment {
    MainActivity parent;
    RecyclerView rankSpecialEventBeers;
    int user_id;
    int event_id;
    String event_name;
    ArrayList<BeerSmall> beers;

    public SpecialTopFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        if (sharedPreferences.contains("userId")) {
            user_id = sharedPreferences.getInt("userId", -1);
        }

        event_id = getArguments().getInt("SPECIAL_EVENT_ID", -1);
        event_name = getArguments().getString("SPECIAL_EVENT_NAME", "");

        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(event_name);
                actionBar.show();
            }
        }

        View rootView = inflater.inflate(R.layout.fragment_special_top, container, false);

        rankSpecialEventBeers = rootView.findViewById(R.id.specialTopListView);
        rankSpecialEventBeers.setLayoutManager(new LinearLayoutManager(getContext()));
        rankSpecialEventBeers.setAdapter(adapter);

        if(event_id != -1) {
            getBeers();
        }
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu_vote, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_vote) {
            if (parent != null) {
                Fragment specialListFragment = new SpecialListFragment();
                Bundle arguments = new Bundle();
                arguments.putInt("SPECIAL_EVENT_ID", event_id);
                arguments.putString("SPECIAL_EVENT_NAME", event_name);
                specialListFragment.setArguments(arguments);
                parent.pushFragment(specialListFragment);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(SpecialTopFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void openBeer(int beer_id) {
        if (parent != null) {
            Fragment breweryFragment = new BeerFragment();
            Bundle arguments = new Bundle();
            arguments.putInt("BEER_ID", beer_id);
            breweryFragment.setArguments(arguments);
            parent.pushFragment(breweryFragment);
        }
    }

    private void getBeers() {
        beers = new ArrayList<>();
        String url = ApiUrl.EVENTS_TOP + "&event_id=" + event_id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonBeers = jsonResponse.getJSONArray("beers");
                            beers.addAll(BeerSmall.beersFromJSONArray(jsonBeers));
                            if (beers.size() < 1) {
                                Log.i(getString(R.string.app_name), "No beers found");
                            }
                            adapter.setList(beers);
                            adapter.setSize(beers.size());
                            adapter.notifyDataSetChanged();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    SimpleRecyclerAdapter adapter = new SimpleRecyclerAdapter(R.layout.list_item_special_event_top, new ArrayList<>()) {
        @Override
        public void onListItem(Object listItem, View view, final int position) {
            ImageView topRankImageView = view.findViewById(R.id.topRankSpecialEventViewImage);
            TextView topRankTextView = view.findViewById(R.id.topRankSpecialEventTextView);
            TextView topBeerPercentTextView = view.findViewById(R.id.topSpecialEventBeerPtsTextView);
            //TextView topBeerPercentSymbolTextView = view.findViewById(R.id.topBeerPercentSymbolTextView);
            TextView topBeerNameTextView = view.findViewById(R.id.topSpecialEventBeerNameTextView);
            TextView topBreweryNameTextView = view.findViewById(R.id.topSpecialEventBreweryNameTextView);
            topRankImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);


            BeerSmall beer = (BeerSmall) listItem;

            //topBeerPercentSymbolTextView.setVisibility(View.GONE);

            topBeerPercentTextView.setText(String.valueOf(beer.points));
            topBeerNameTextView.setText(beer.name);
            topBreweryNameTextView.setText(beer.brewery_name);

            topRankTextView.setText(String.valueOf(position + 1));

            if (position < 3) {
                topRankTextView.setText("");
                switch (position) {
                    case 0:
                        topRankImageView.setImageResource(R.drawable.ic_king_gold);
                        break;
                    case 1:
                        topRankImageView.setImageResource(R.drawable.ic_king_silver);
                        break;
                    case 2:
                        topRankImageView.setImageResource(R.drawable.ic_king_bronze);
                        break;
                }
            } else {
                topRankTextView.setTextColor(getResources().getColor(R.color.newsDate));
                topRankImageView.setImageResource(R.drawable.ic_circunference);
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (beers != null && position < beers.size()) {
                        openBeer(beers.get(position).id);
                    }
                }
            });
        }
    };
    //TODO: al momento de regresar, volver a la pantalla de inicio de Ensenada
    //TODO:Monstrar ranking de cervezas
}
