package com.elixeer.maltapp.Models;

import android.content.Context;
import android.content.SharedPreferences;

import com.elixeer.maltapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

public class Place implements Serializable {

    public int id;
    public String name;
    public String description;
    public String address;
    public double latitude;
    public double longitude;
    public double distance;
    public String phone;
    public String mail;
    public String website;
    public String facebook;
    public String facebook_id;
    public String twitter;
    public boolean shop;
    public Boolean active;
    public int city_id;
    public String city_name;
    public String imageUrlThumb;
    public String imageUrlMedium;
    public boolean subscription;
    //private JSONArray brewery;
    //public ArrayList<BrewerySmall> breweries;

    public Place(JSONObject jsonPlace, Context context) {
        String language = Locale.getDefault().getLanguage();
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
            if (sharedPreferences.contains("language")) {
                language = sharedPreferences.getString("language", Locale.getDefault().getLanguage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String language2 = "en";
        if (language.equals("en")) {
            language2 = "es";
        }

        this.id = jsonPlace.optInt("id", 0);
        this.name = jsonPlace.optString("name", "");
        this.description = jsonPlace.optString("description_" + language, "");
        if (this.description.equals("")) {
            this.description = jsonPlace.optString("description_" + language2, "");
        }
        this.address = jsonPlace.optString("address", "");
        this.latitude = jsonPlace.optDouble("latitude", 0);
        this.longitude = jsonPlace.optDouble("longitude", 0);
        this.phone = jsonPlace.optString("phone", "");
        this.mail = jsonPlace.optString("mail", "");
        this.website = jsonPlace.optString("website", "");
        this.facebook = jsonPlace.optString("facebook", "");
        this.facebook_id = jsonPlace.optString("face_code", "");
        this.twitter = jsonPlace.optString("twitter", "");
        this.shop = jsonPlace.optBoolean("shop", false);
        this.active = jsonPlace.optBoolean("activate", false);
        this.city_id = jsonPlace.optInt("city_id", 0);
        this.city_name = jsonPlace.optString("city_name", "");
        this.imageUrlThumb = jsonPlace.optString("thumb", null);
        this.imageUrlMedium = jsonPlace.optString("medium", null);
        this.subscription = jsonPlace.optBoolean("subscription", false);
        //this.brewery = jsonPlace.optJSONArray("breweries");
        //this.breweries = BrewerySmall.breweriesFromJSONArray(this.brewery);
    }

    public static ArrayList<Place> placesFromJSONArray(JSONArray jsonPlaces, Context context) {
        ArrayList<Place> Places = new ArrayList<>();
        for (int i = 0; i < jsonPlaces.length(); i++) {
            JSONObject jsonPlace = jsonPlaces.optJSONObject(i);
            if (jsonPlace != null) {
                Places.add(new Place(jsonPlace, context));
            }
        }
        return Places;
    }
}
