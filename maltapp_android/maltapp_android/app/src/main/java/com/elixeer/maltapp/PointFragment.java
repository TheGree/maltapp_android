package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.core.SimpleRecyclerAdapter;
import com.elixeer.maltapp.Models.Point;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by alebrije2 on 1/25/16.
 *
 */
public class PointFragment extends Fragment {

    MainActivity parent;
    RecyclerView pointListView;
    TextView experienceTextView;
    TextView levelTextView;
    ImageView pointsCrownImageView;
    ArrayList<Point> points;
    int user_id;
    int pts;
    boolean purchased;
    String level;

    public PointFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.title_points);
                actionBar.show();
            }
        }

        user_id = getArguments().getInt("USER_ID");
        pts = getArguments().getInt("PTS");
        level = getArguments().getString("LEVEL");
        purchased = getArguments().getBoolean("PURCHASED");

        View rootView = inflater.inflate(R.layout.fragment_point, container, false);

        pointListView = rootView.findViewById(R.id.pointListView);
        pointListView.setLayoutManager(new LinearLayoutManager(getContext()));

        experienceTextView = rootView.findViewById(R.id.experienceTextView);
        levelTextView = rootView.findViewById(R.id.levelTextView);
        pointsCrownImageView = rootView.findViewById(R.id.pointsCrownImageView);

        if(purchased) {
            experienceTextView.setText(String.valueOf(pts).concat(" " + getString(R.string.premium_profile_beer_coins_textview)));
        } else {
            experienceTextView.setText(getString(R.string.experienceExample).concat(String.valueOf(pts)));
            pointsCrownImageView.setVisibility(View.GONE);
        }
        if (!level.equals("null")) {
            levelTextView.setText(level);
        }

        getPoints();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("PointHistory");
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(PointFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void getPoints() {
        String url = ApiUrl.POINTS_LIST + "&user_id=" + user_id  + "&order=created_at-DESC";

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonPoints = jsonResponse.getJSONArray("points");
                            points = Point.pointsFromJSONArray(jsonPoints);
                            if (points.size() < 1) {
                                Log.i(getString(R.string.app_name), "No points found");
                            } else {
                                if(!purchased) {
                                    for (int i = 0; i < points.size(); i++) {
                                        if (!points.get(i).movement) {
                                            points.remove(i);
                                        }
                                    }
                                }
                                pointListView.setAdapter(new SimpleRecyclerAdapter(R.layout.list_item_points, points) {
                                    @Override
                                    public void onListItem(Object listItem, View view, int position) {
                                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MMM/yyyy", Locale.getDefault());
                                        Point points = (Point) listItem;

                                        TextView pointsTextView = view.findViewById(R.id.pointsTextView);
                                        TextView reasonPointsTextView = view.findViewById(R.id.reasonPointsTextView);
                                        TextView datePointsTextView = view.findViewById(R.id.datePointsTextView);

                                        pointsTextView.setText(points.movement ? "+ " : "- ");
                                        pointsTextView.append(points.points + " BeerCoins");
                                        switch (points.reason) {
                                            case 0:
                                                reasonPointsTextView.setText(getString(R.string.reason_0));
                                                break;
                                            case 1:
                                                reasonPointsTextView.setText(getString(R.string.reason_1));
                                                break;
                                            case 2:
                                                reasonPointsTextView.setText(getString(R.string.reason_2));
                                                break;
                                            case 3:
                                                reasonPointsTextView.setText(getString(R.string.reason_3));
                                                break;
                                            case 4:
                                                reasonPointsTextView.setText(getString(R.string.reason_4));
                                                break;
                                            case 5:
                                                reasonPointsTextView.setText(getString(R.string.reason_5));
                                                break;
                                            case 6:
                                                reasonPointsTextView.setText(getString(R.string.reason_6));
                                                break;
                                            case 7:
                                                reasonPointsTextView.setText(getString(R.string.reason_7));
                                                break;
                                            case 8:
                                                reasonPointsTextView.setText(getString(R.string.reason_8));
                                                break;
                                            case 9:
                                                reasonPointsTextView.setText(getString(R.string.reason_9));
                                                break;
                                            case 10:
                                                reasonPointsTextView.setText(getString(R.string.reason_10));
                                                break;
                                            case 11:
                                                reasonPointsTextView.setText(getString(R.string.reason_11));
                                                break;
                                            case 12:
                                                reasonPointsTextView.setText(getString(R.string.reason_11));
                                                break;
                                            case 13:
                                                reasonPointsTextView.setText(getString(R.string.reason_11));
                                                break;
                                            case 14:
                                                reasonPointsTextView.setText(getString(R.string.reason_11));
                                                break;
                                            case 15:
                                                reasonPointsTextView.setText(getString(R.string.reason_15));
                                                break;
                                            default:
                                                reasonPointsTextView.setText(getString(R.string.reason_0));
                                                break;
                                        }
                                        datePointsTextView.setText(simpleDateFormat.format(points.date));
                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }
}




