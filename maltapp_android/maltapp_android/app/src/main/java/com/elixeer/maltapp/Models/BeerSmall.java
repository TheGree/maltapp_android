package com.elixeer.maltapp.Models;

/**
 * Created by alebrije2 on 1/26/16.
 */

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class BeerSmall implements Serializable {

    public int id;
    public String name;
    public String brewery_name;
    public boolean brewery_approved;
    public boolean approved;
    public long ranking;
    public int number_ranks;
    public String thumb;
    public String medium;
    public int points;

    public BeerSmall(JSONObject jsonBeer) {
        this.id = jsonBeer.optInt("id", 0);
        this.name = jsonBeer.optString("name", "");
        this.brewery_name = jsonBeer.optString("brewery_name", "");
        this.brewery_approved = jsonBeer.optBoolean("brewery_approved", false);
        this.approved = jsonBeer.optBoolean("approved", false);
        double bruteRank = jsonBeer.optDouble("ranking", 0);
        this.ranking = Math.round(bruteRank);
        this.number_ranks = jsonBeer.optInt("number_ranks", 0);
        this.points = jsonBeer.optInt("points", 0);
        this.thumb = jsonBeer.optString("thumb", null);
        this.medium = jsonBeer.optString("medium", null);
    }

    public static ArrayList<BeerSmall> beersFromJSONArray(JSONArray jsonBeers) {
        ArrayList<BeerSmall> beers = new ArrayList<>();
        for (int i = 0; i < jsonBeers.length(); i++) {
            JSONObject jsonBeer = jsonBeers.optJSONObject(i);
            if (jsonBeer != null) {
                beers.add(new BeerSmall(jsonBeer));
            }
        }
        return beers;
    }
}
