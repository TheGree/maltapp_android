package com.elixeer.maltapp;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.carousel.CarouselDialogFragment;
import com.inthecheesefactory.thecheeselibrary.fragment.bus.ActivityResultBus;
import com.inthecheesefactory.thecheeselibrary.fragment.bus.ActivityResultEvent;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private ActionBar actionBar;
    ContentLoadingProgressBar progressBar;
    FloatingActionButton fab;
    private FragmentManager fragmentManager;
    public TabLayout tabLayout;
    public MainInterface resultHandler;
    public CoordinatorLayout coordinatorLayout;
    public static MainActivity instance;
    final String welcomeScreenShownPref = "carouselShown";
    public iabHelperDelegate delegate = null;

    private int tabIcons[] = {
            R.drawable.ic_tab_home,
            R.drawable.ic_tab_news,
            R.drawable.ic_tab_nearby,
            R.drawable.ic_tab_rank,
            R.drawable.ic_tab_profile
    };
    //TODO: PREMIUM
    /*
    private int tabIconsPremium[] = {
            R.drawable.ic_tab_home,
            R.drawable.ic_tab_news,
            R.drawable.ic_tab_nearby,
            R.drawable.ic_tab_rank,
            R.drawable.ic_tab_premium
    };
    */
    private Fragment sectionFragments[] = {
            new HomeFragment(),
            new NewsFragment(),
            new NearbyFragment(),
            new TopFragment(),
            new ProfileFragment()
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActivity(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MyFirebaseMessagingService.RECIVEACTION);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    protected void initActivity(Bundle savedInstanceState) {
        //callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_main);
        instance = this;

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        if (!sharedPreferences.contains("language")) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("language", Locale.getDefault().getLanguage());
            editor.apply();
        } else {
            Locale locale = new Locale(sharedPreferences.getString("language", Locale.getDefault().getLanguage()));
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }

        /*
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        */

        actionBar = getSupportActionBar();
        fragmentManager = getSupportFragmentManager();

        tabLayout = findViewById(R.id.tabs);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pushFragment(sectionFragments[tab.getPosition()]);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                pushFragment(sectionFragments[tab.getPosition()]);
            }
        });

        coordinatorLayout = findViewById(R.id.coordinatorLayout);

        progressBar = findViewById(R.id.progressBar);
        progressBar.hide();

        fab = findViewById(R.id.fab);
        fab.hide();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Fragment currentFragment = fragmentManager.findFragmentById(R.id.container);
                if (currentFragment != null && currentFragment instanceof FABFragment) {
                    ((FABFragment) currentFragment).onFABClick(view);
                } else {
                    fab.hide();
                }
            }
        });

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean welcomeScreenShown = mPrefs.getBoolean(welcomeScreenShownPref, false);

        // Set home fragment
        if (!welcomeScreenShown) {
            setTutorial();
        } else {
            setApplication();
        }

        // Debug version
        if (BuildConfig.DEBUG) {
            Toast.makeText(this, getString(R.string.app_name) + " v" + BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (fragmentManager != null && fragmentManager.getBackStackEntryCount() > 0) {
            if (actionBar != null && fragmentManager.getBackStackEntryCount() < 2) {
                actionBar.setDisplayHomeAsUpEnabled(false);
            }
            try {
                fragmentManager.popBackStack();
            } catch (IllegalStateException ie) {
                ie.printStackTrace();
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    public boolean setTab(int position) {
        if (position < tabLayout.getTabCount()) {
            TabLayout.Tab tab = tabLayout.getTabAt(position);
            if (tab != null) {
                tab.select();
                return true;
            }
        }
        return false;
    }

    public void pushFragment(Fragment fragment) {
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.container);
        if (currentFragment == null || !(currentFragment.getClass().isAssignableFrom(fragment.getClass()))) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            //transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
            transaction.replace(R.id.container, fragment);
            transaction.addToBackStack(null);
            transaction.commitAllowingStateLoss();

            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    public void setFragment(Fragment fragment) {
        fragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    public void setApplication() {
        //TODO: PREMIUM
        /*
        boolean user_premium = sharedPreferences.getBoolean("premium", false);
        if (user_premium) {
            for (int tabIcon : tabIconsPremium) {
                tabLayout.addTab(tabLayout.newTab().setIcon(tabIcon));
            }
        } else {
            for (int tabIcon : tabIcons) {
                tabLayout.addTab(tabLayout.newTab().setIcon(tabIcon));
            }
        }
        */
        setFragment(sectionFragments[0]);
        for (int tabIcon : tabIcons) {
            tabLayout.addTab(tabLayout.newTab().setIcon(tabIcon));
        }
        tabLayout.setVisibility(View.VISIBLE);
    }

    private void setTutorial(){
        tabLayout.setVisibility(View.GONE);
        setFragment(new CarouselDialogFragment());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultHandler != null) {
            resultHandler.onResult(requestCode, resultCode, intent);
            resultHandler = null;
        }
        ActivityResultBus.getInstance().postQueue(new ActivityResultEvent(requestCode, resultCode, intent));
        if (delegate != null){
            delegate.didCompleteTransaction(requestCode, resultCode, intent);
        }
    }

    interface MainInterface {
        void onResult(int requestCode, int resultCode, Intent intent);
    }

}