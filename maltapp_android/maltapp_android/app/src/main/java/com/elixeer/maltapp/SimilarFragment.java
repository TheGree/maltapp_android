package com.elixeer.maltapp;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.alebrije.core.SimpleListAdapter;
import com.elixeer.maltapp.Models.Beer;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class SimilarFragment extends Fragment {
    ArrayList<Beer> beers;
    ListView beerSimilarListView;
    LinearLayout backgroundItemNotFound;
    Button letsUsKnowLibraryButton;
    TextView letsUsKnowLibraryTextView;
    MainActivity parent;
    int style_id;
    int brewery_id;
    String title;
    String brewery_name;

    public SimilarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        title = getArguments().getString("STYLE_NAME");
        style_id = getArguments().getInt("STYLE_ID");
        brewery_id = getArguments().getInt("BREWERY_ID");
        brewery_name = getArguments().getString("BREWERY_NAME");

        if (getActivity() instanceof MainActivity) {

            parent = (MainActivity) getActivity();
            //parent.setTab(0);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(title);
                actionBar.show();
            }
        }

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_similar, container, false);

        beerSimilarListView = rootView.findViewById(R.id.beerSimilarListView);

        backgroundItemNotFound = (LinearLayout) inflater.inflate(R.layout.list_item_not_found, null, false);
        letsUsKnowLibraryButton = backgroundItemNotFound.findViewById(R.id.letsUsKnowLibraryButton);
        letsUsKnowLibraryTextView = backgroundItemNotFound.findViewById(R.id.letsUsKnowLibraryTextView);

        letsUsKnowLibraryTextView.setText(getString(R.string.at_moment));

        letsUsKnowLibraryButton.setText(getString(R.string.visit_brewery));

        letsUsKnowLibraryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Similar", "brewery_pressed", brewery_name);

                Fragment breweryFragment = new BreweryFragment();
                Bundle arguments = new Bundle();
                arguments.putInt("BREWERY_ID", brewery_id);
                breweryFragment.setArguments(arguments);
                parent.pushFragment(breweryFragment);
            }
        });

        beerSimilarListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Analytics event
                ((AnalyticsApplication) getActivity().getApplication()).sendEvent("Similar", "beer_pressed", beers.get(position).name);

                openBeerDetails(position);
            }
        });

        getBeerImages();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("Similar");
    }

    private void openBeerDetails(int position) {
        if (parent != null) {
            Fragment beerFragment = new BeerFragment();
            Bundle arguments = new Bundle();
            arguments.putSerializable("BEER", beers.get(position));
            arguments.putBoolean("PRELOAD", true);
            beerFragment.setArguments(arguments);
            parent.pushFragment(beerFragment);
        }
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(SimilarFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void getBeerImages() {
        String url = ApiUrl.BEERS + "&beertype_id=" + style_id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonBeers = jsonResponse.getJSONArray("beers");
                            beers = Beer.beersFromJSONArray(jsonBeers, getContext());
                            if (beers.size() < 1) {
                                Log.i(getString(R.string.app_name), "No beers found");
                            } else {
                                if(beers.size() == 1) {
                                    beerSimilarListView.addFooterView(backgroundItemNotFound);
                                } else {
                                    beerSimilarListView.setAdapter(new SimpleListAdapter(R.layout.list_item_similar, beers) {
                                        @Override
                                        public void onListItem(Object listItem, View view, int position) {
                                            Beer beer = (Beer) listItem;

                                            final ImageView similarBeerImageView = view.findViewById(R.id.similarBeerImageView);
                                            TextView similarBeerNameTextView = view.findViewById(R.id.similarBeerNameTextView);

                                            similarBeerNameTextView.setText(beer.name);
                                            if (beer.thumb.contains("/missing.png")) {
                                                similarBeerImageView.setImageResource(R.drawable.img_default_beer);
                                            } else {
                                                similarBeerImageView.setImageBitmap(null);
                                                ImageCache.getImage(beer.thumb, getContext(), new ImageCache.OnResponseListener() {
                                                    @Override
                                                    public void onResponse(Bitmap bitmap, String error) {
                                                        similarBeerImageView.setImageBitmap(bitmap);
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }
}