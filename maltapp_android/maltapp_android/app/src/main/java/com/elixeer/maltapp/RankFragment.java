package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ClipDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.elixeer.maltapp.Handlers.PointsManager;
import com.elixeer.maltapp.Models.Score;
import com.elixeer.maltapp.Models.Taste;
/*import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;*/

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RankFragment extends Fragment {
    //CallbackManager callbackManager;
    MainActivity parent;
    ScrollView scrollMasterRankFragment;
    TextView percentRankText;
    SeekBar rankSeekBar;
    LinearLayout tasteListView;
    ArrayList<Taste> tastes;
    ArrayList<Taste> tasteslist;
    ArrayList<Score> scores;
    TextView foursquareButton;
    TextView saveRankButton;
    TextView whatLikeBeerButton;
    ImageView image_below;
    ImageView image_above;
    EditText popCommentEditText;
    int beer_id;
    int user_id;
    String title;
    String image;
    Long rank;
    String comment;
    boolean created;
    boolean moved;
    String place;
    boolean showTasteListView;

    public RankFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //callbackManager = CallbackManager.Factory.create();
        title = getArguments().getString("TITLE");
        image = getArguments().getString("BEER_IMAGE");
        beer_id = getArguments().getInt("BEER_ID");
        created = getArguments().getBoolean("CREATED", false);
        rank = getArguments().getLong("RANK", 0);
        comment = getArguments().getString("COMMENT", "");
        place = getArguments().getString("FSQR_NAME");

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        if (sharedPreferences.contains("userId")) {
            if (sharedPreferences.getInt("userId", -1) > 0) {
                user_id = sharedPreferences.getInt("userId", -1);
            }
        }

        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(title);
                actionBar.show();
            }
        }

        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_rank, container, false);

        scrollMasterRankFragment = rootView.findViewById(R.id.scrollMasterRankFragment);
        rankSeekBar = rootView.findViewById(R.id.rankSeekBar);
        percentRankText = rootView.findViewById(R.id.percentRankText);
        tasteListView = rootView.findViewById(R.id.tasteListView);
        foursquareButton = rootView.findViewById(R.id.foursquareButton);
        whatLikeBeerButton = rootView.findViewById(R.id.whatLikeBeerButton);
        saveRankButton = rootView.findViewById(R.id.saveRankButton);
        image_below = rootView.findViewById(R.id.image_below);
        image_above = rootView.findViewById(R.id.image_above);
        final ClipDrawable drawable = (ClipDrawable) image_above.getDrawable();

        tasteListView.setVisibility(View.GONE);
        collapse(tasteListView);

        showTasteListView = false;
        //drawable.setLevel(0);
        //drawable.setLevel(drawable.getLevel() + rank.intValue());

        //rankSeekBar.setMax(0);
        rankSeekBar.setMax(100);

        if (user_id > 0) {
            rankSeekBar.setProgress(rank.intValue());
            percentRankText.setText(rank + "%");
            drawable.setLevel(rank.intValue() * 100);
            if (rank.intValue() > 0) {
                moved = true;
            }
        } else {
            rankSeekBar.setProgress(0);
            percentRankText.setText(0 + "%");
        }

        if (place != null) {
            foursquareButton.setText(place);
        }

        getTastes();

       /* Animation slideOut = AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_out_right);

        slideOut.setAnimationListener(new Animation.AnimationListener(){
            @Override
            public void onAnimationEnd(Animation arg0) {
                image_below.bringToFront();

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                image_above.bringToFront();

            }

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub

            }

        });

        image_above.startAnimation(slideOut);*/

        rankSeekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        ((ScrollView) rootView).requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        ((ScrollView) rootView).requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });

        rankSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                percentRankText.setText(i + "%");
                drawable.setLevel(i * 100);
                moved = true;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                scrollMasterRankFragment.setOverScrollMode(ScrollView.OVER_SCROLL_NEVER);
                scrollMasterRankFragment.requestDisallowInterceptTouchEvent(true);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                scrollMasterRankFragment.setOverScrollMode(ScrollView.OVER_SCROLL_IF_CONTENT_SCROLLS);
                scrollMasterRankFragment.requestDisallowInterceptTouchEvent(false);
            }
        });

        foursquareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment foursquareFragment = new FoursquareFragment();
                Bundle arguments = new Bundle();
                arguments.putInt("BEER_ID", beer_id);
                arguments.putString("TITLE", title);
                arguments.putLong("RANK", Long.parseLong(percentRankText.getText().toString().replace("%", "")));
                arguments.putBoolean("CREATED", created);
                foursquareFragment.setArguments(arguments);
                parent.pushFragment(foursquareFragment);
            }
        });

        whatLikeBeerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!showTasteListView) {
                    expand(tasteListView);
                    //tasteListView.setVisibility(View.GONE);
                    showTasteListView = true;
                } else {
                    collapse(tasteListView);
                    //tasteListView.setVisibility(View.VISIBLE);
                    showTasteListView = false;
                    TextView tasteTextView;
                    CheckBox tasteCheckBox;
                    String whatTaste = "";
                    for (int i = 0; i < tasteListView.getChildCount(); i++) {
                        tasteTextView = tasteListView.getChildAt(i).findViewById(R.id.tasteNameTextView);
                        tasteCheckBox = tasteListView.getChildAt(i).findViewById(R.id.tasteCheckBox);
                        String TasteString = tasteTextView.getText().toString();
                        if (tasteCheckBox.isChecked()) {
                            if(whatTaste.equals("")) {
                                whatTaste += TasteString;
                            } else {
                                whatTaste += ", " + TasteString;
                            }
                        }
                    }
                    if(whatTaste.equals("")){
                        whatLikeBeerButton.setText(getString(R.string.whatLikeBeer));
                    } else {
                        whatLikeBeerButton.setText(whatTaste);
                    }
                }
            }
        });

        saveRankButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_id > 0) {
                    if(moved) {
                        showRateDialog();
                        //OnSaveRankButtonClicked();
                    } else {
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View pop = inflater.inflate(R.layout.pop_item_data, null);
                        TextView dataTitleTextView = pop.findViewById(R.id.dataTitleTextView);
                        TextView popMsjTextView = pop.findViewById(R.id.popMsjTextView);

                        dataTitleTextView.setText(getString(R.string.maltappNotice));
                        popMsjTextView.setText(getString(R.string.rating_advise));

                        AlertDialog.Builder builder = new AlertDialog.Builder(RankFragment.this.getActivity())
                                //.setTitle(getString(R.string.maltappNotice))
                                .setView(pop)
                                .setPositiveButton(R.string.ok, null);
                        final AlertDialog alert = builder.create();
                        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {

                            }
                        });
                        alert.show();
                    }
                } else {
                    loginPopUp();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("DrankIt");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu_info, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_info) {
            if (parent != null) {
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View pop = inflater.inflate(R.layout.pop_item_data, null);
                TextView dataTitleTextView = pop.findViewById(R.id.dataTitleTextView);
                TextView popMsjTextView = pop.findViewById(R.id.popMsjTextView);

                dataTitleTextView.setText(getString(R.string.how_to_review_a_beer));
                popMsjTextView.setText(getString(R.string.how_to_review_a_beer_info));

                AlertDialog.Builder builder = new AlertDialog.Builder(RankFragment.this.getActivity())
                        //.setTitle(getString(R.string.maltappNotice))
                        .setView(pop)
                        .setPositiveButton(R.string.ok, null);
                final AlertDialog alert = builder.create();
                alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {

                    }
                });
                alert.show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void getTastes() {
        String url = ApiUrl.TASTES;
        //Log.d(getString(R.string.app_name), "Requested url " + url);
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonTastes = jsonResponse.getJSONArray("tastes");
                            tastes = Taste.tastesFromJSONArray(jsonTastes, getContext());
                            if (tastes.size() < 1) {
                                Log.i(getString(R.string.app_name), "No tastes found");
                            } else {
                                for (int i = 0; i < tastes.size(); i++) {
                                    fillLayouts(tastes.get(i).id, tastes.get(i).name);
                                }
                                if (user_id > 0) {
                                    getTastesList();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void fillLayouts(int id, String name) {
        LinearLayout child = (LinearLayout) View.inflate(getContext(), R.layout.list_item_taste, null);
        TextView tasteNameTextView = child.findViewById(R.id.tasteNameTextView);
        TextView tasteIDTextView = child.findViewById(R.id.tasteIDTextView);
        tasteIDTextView.setText(String.valueOf(id));
        tasteNameTextView.setText(name);
        tasteListView.addView(child);

    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(RankFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void getTastesList() {
        String url = ApiUrl.TASTES_LIST + "&user_id=" + user_id + "&beer_id=" + beer_id;

        //Log.d(getString(R.string.app_name), "Requested url " + url);
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonTastes = jsonResponse.getJSONArray("tastes");
                            tasteslist = Taste.tastesFromJSONArray(jsonTastes, getContext());
                            if (tasteslist.size() < 1) {
                                Log.i(getString(R.string.app_name), "No tastes found");
                            } else {
                                String whatTaste = "";
                                for (int i = 0; i < tasteslist.size(); i++) {
                                    checkTastes(tasteslist.get(i).id);
                                    if(whatTaste.equals("")) {
                                        whatTaste += tasteslist.get(i).name;
                                    } else {
                                        whatTaste += ", " + tasteslist.get(i).name;
                                    }
                                }
                                whatLikeBeerButton.setText(whatTaste);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void checkTastes(int id) {
        TextView tasteIDTextView;
        CheckBox tasteCheckBox;
        for (int i = 0; i < tasteListView.getChildCount(); i++) {
            tasteIDTextView = tasteListView.getChildAt(i).findViewById(R.id.tasteIDTextView);
            if (tasteIDTextView.getText().equals(id + "")) {
                tasteCheckBox = tasteListView.getChildAt(i).findViewById(R.id.tasteCheckBox);
                tasteCheckBox.setChecked(true);
            }
        }
    }

    void showRateDialog() {
        /*
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.pop_item_comment);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialog.dismiss();
            }
        });
        dialog.show();
        */

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_comment, null);
        popCommentEditText = pop.findViewById(R.id.popCommentEditText);
        popCommentEditText.setText(comment);
        popCommentEditText.setSelection(popCommentEditText.getText().length());

        AlertDialog.Builder builder = new AlertDialog.Builder(RankFragment.this.getActivity())
                //.setIcon(R.drawable.ic_trophy)
                //.setTitle(getString(R.string.maltappNotice))
                //.setMessage(R.string.msg_goal_achieved_message)
                .setView(pop)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        OnSaveRankButtonClicked();
                    }
                });

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        alert.show();
    }

    private void OnSaveRankButtonClicked() {
        final String percentRankTextField = percentRankText.getText().toString().replace("%", "");

        // Analytics event
        ((AnalyticsApplication) getActivity().getApplication()).sendEvent("drankIt", "save_pressed");

        TextView tasteIDTextView;
        CheckBox tasteCheckBox;
        ArrayList<Integer> adds = new ArrayList<>();
        ArrayList<Integer> downs = new ArrayList<>();
        for (int i = 0; i < tasteListView.getChildCount(); i++) {
            tasteIDTextView = tasteListView.getChildAt(i).findViewById(R.id.tasteIDTextView);
            tasteCheckBox = tasteListView.getChildAt(i).findViewById(R.id.tasteCheckBox);
            String idTasteString = tasteIDTextView.getText().toString();
            int idTaste = Integer.parseInt(idTasteString);
            if (tasteCheckBox.isChecked()) {
                adds.add(idTaste);
            } else {
                downs.add(idTaste);
            }
        }

        String dynamicCreateParams = "{";
        dynamicCreateParams += "\"user_id\":\"" + user_id + "\"";
        dynamicCreateParams += ",\"beer_id\":\"" + beer_id + "\"";
        dynamicCreateParams += "}";

        dynamicCreate(dynamicCreateParams);

        if (downs.size() > 0) {
            for (int i = 0; i < downs.size(); i++) {
                deleteTastes(downs.get(i));
            }
        }
        if (adds.size() > 0) {
            for (int i = 0; i < adds.size(); i++) {
                String tastesParams = "{";
                tastesParams += "\"user_id\":\"" + user_id + "\"";
                tastesParams += ",\"beer_id\":\"" + beer_id + "\"";
                tastesParams += ",\"taste_id\":\"" + adds.get(i) + "\"";
                tastesParams += "}";
                createTastes(tastesParams);
            }
        }

        PointsManager.addPoints(user_id, ApiUrl.POINTS_RATE, ApiUrl.REASON_RATE, getActivity());

        String rankParams = "{";
        rankParams += "\"user_id\":\"" + user_id + "\"";
        rankParams += ",\"beer_id\":\"" + beer_id + "\"";
        rankParams += ",\"rank\":\"" + percentRankTextField + "\"";
        if(popCommentEditText.getText() != null) {
            rankParams += ",\"comment\":\"" + popCommentEditText.getText() + "\"";
        } else {
            rankParams += ",\"comment\":\"\"";
        }
        rankParams += "}";

        if (created) {
            editRank(rankParams);
        } else {
            createRank(rankParams);
            getScores(ApiUrl.RANK_SCORE_ID);
        }

        String drankItParams = "{";
        drankItParams += "\"user_id\":\"" + user_id + "\"";
        drankItParams += ",\"beer_id\":\"" + beer_id + "\"";
        drankItParams += ",\"foursquare_name\":\"" + getArguments().getString("FSQR_NAME", null) + "\"";
        drankItParams += ",\"foursquare_address\":\"" + getArguments().getString("FSQR_ADDRESS", null) + "\"";
        drankItParams += ",\"latitude\":\"" + getArguments().getString("FSQR_LAT", null) + "\"";
        drankItParams += ",\"longitude\":\"" + getArguments().getString("FSQR_LONG", null) + "\"";
        drankItParams += "}";

        createDrankIt(drankItParams);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_rank, null);
        //Button popRankButton = pop.findViewById(R.id.popRankButton);

       /* popRankButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Analytics event
                ((AnalyticsApplication) getActivity().getApplication()).sendEvent("drankIt", "shared_pressed", title);

                //ShareDialog shareDialog = new ShareDialog(getActivity());

                PointsManager.addPoints(user_id, ApiUrl.POINTS_SHARE, ApiUrl.REASON_SHARE, getActivity());

                getScores(ApiUrl.FACE_SCORE_ID);

                /*
                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse(ApiUrl.MALTAPP_MX))
                        .setContentTitle(title)
                        .setContentDescription(getString(R.string.rated) + title + getString(R.string.with) + percentRankTextField + "%")
                        .setImageUrl(Uri.parse(ApiUrl.SQUARE_IMAGE + image))
                        .build();
                * /

                SharePhoto photo = new SharePhoto.Builder()
                        .setCaption(getString(R.string.rated) + title + getString(R.string.with) + percentRankTextField + "%")
                        .setImageUrl(Uri.parse(ApiUrl.SQUARE_IMAGE + image))
                        .setUserGenerated(false)
                        .build();

                SharePhotoContent content = new SharePhotoContent.Builder()
                        .addPhoto(photo)
                        .build();

                ShareDialog.show(getActivity(), content);

/ *
                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        getScores(ApiUrl.FACE_SCORE_ID);
                        Snackbar snackbar = Snackbar.make( ((MainActivity) getActivity()).coordinatorLayout, "onSuccess", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }

                    @Override
                    public void onCancel() {
                        Snackbar snackbar = Snackbar.make( ((MainActivity) getActivity()).coordinatorLayout, "onCancel", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Snackbar snackbar = Snackbar.make( ((MainActivity) getActivity()).coordinatorLayout, "onError", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                });
* /
            }
        });*/

        AlertDialog.Builder builder = new AlertDialog.Builder(RankFragment.this.getActivity())
                //.setTitle(R.string.save)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Fragment beerFragment = new BeerFragment();
                Bundle arguments = new Bundle();
                arguments.putInt("BEER_ID", beer_id);
                beerFragment.setArguments(arguments);
                parent.pushFragment(beerFragment);
            }
        });

        alert.show();

    }

    private void getScores(final int badgetype_id) {
        String url = ApiUrl.SCORES + "&user_id=" + user_id + "&badgetype_id=" + badgetype_id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonScores = jsonResponse.getJSONArray("scores");
                            scores = Score.tastesFromJSONArray(jsonScores);
                            if (scores.size() < 1) {
                                createScore(badgetype_id);
                            } else {
                                editScores(badgetype_id);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void createScore(int badgetype_id) {
        String scoresFaceParams = "{";
        scoresFaceParams += "\"user_id\":\"" + user_id + "\"";
        scoresFaceParams += ",\"badgetype_id\":\"" + badgetype_id + "\"";
        scoresFaceParams += ",\"points\":\"" + 1 + "\"";
        scoresFaceParams += "}";

        String url = ApiUrl.SCORES;
        String[][] headers = {{"Content-Type", "application/json"}};
        new AsyncRequestString("POST", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .setData(scoresFaceParams)
                .setHeaders(headers)
                .execute();
    }

    private void editScores(int badgetype_id) {
        String url = ApiUrl.SCORE_UPDATE + "&user_id=" + user_id + "&badgetype_id=" + badgetype_id + "&points=" + 1;
        new AsyncRequestString("PUT", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void createTastes(String params) {
        String url = ApiUrl.TASTES_CREATE;
        String[][] headers = {{"Content-Type", "application/json"}};
        new AsyncRequestString("POST", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            //reportError(jsonResponse.getString("error"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .setData(params)
                .setHeaders(headers)
                .execute();
    }

    private void deleteTastes(int id) {
        String url = ApiUrl.TASTES_DELETE + "&user_id=" + user_id + "&beer_id=" + beer_id + "&taste_id=" + id;
        new AsyncRequestString("DELETE", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void editRank(String params) {
        String url = ApiUrl.RANK_UPDATE;
        String[][] headers = {{"Content-Type", "application/json"}};
        new AsyncRequestString("PUT", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .setData(params)
                .setHeaders(headers)
                .execute();
    }

    private void createRank(String params) {
        String url = ApiUrl.RANK_CREATE;
        String[][] headers = {{"Content-Type", "application/json"}};
        new AsyncRequestString("POST", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .setData(params)
                .setHeaders(headers)
                .execute();
    }

    private void createDrankIt(String params) {
        String url = ApiUrl.DRANK_CREATE;
        String[][] headers = {{"Content-Type", "application/json"}};
        new AsyncRequestString("POST", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .setData(params)
                .setHeaders(headers)
                .execute();
    }

    private void dynamicCreate(String params) {
        String url = ApiUrl.DYNAMIC_CREATE;
        String[][] headers = {{"Content-Type", "application/json"}};
        new AsyncRequestString("POST", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .setData(params)
                .setHeaders(headers)
                .execute();
    }

    public static void expand(final LinearLayout v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final LinearLayout v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    void loginPopUp() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_login, null);
        Button popLoginButton = pop.findViewById(R.id.popLoginButton);

        AlertDialog.Builder builder = new AlertDialog.Builder(RankFragment.this.getActivity())
                //.setIcon(R.drawable.ic_trophy)
                //.setTitle(getString(R.string.maltappNotice))
                //.setMessage(R.string.msg_goal_achieved_message)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        popLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment registerFragment = new RegisterFragment();
                parent.pushFragment(registerFragment);
                alert.dismiss();
            }
        });

        alert.show();
    }

}
