package com.elixeer.maltapp.Dynamics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Models.BrewerySmall;
import com.elixeer.maltapp.Models.PlaceSmall;
import com.elixeer.maltapp.R;
import com.elixeer.maltapp.SpecialMapFragment;

import java.util.ArrayList;

public class InteractiveMapView extends View {
    private Context context;
    private ScaleGestureDetector mScaleDetector;
    private int mLastTouchX;
    private int mLastTouchY;
    private int mPosX;
    private int mPosY;
    private int cursorX;
    private int cursorY;
    private int userX;
    private int userY;
    private float minScale = 1.0f;
    private float maxScale = 4.0f;
    private float mScaleFactor = 1.0f;
    private float scalePointX;
    private float scalePointY;

    public ArrayList<BrewerySmall> breweries;
    public ArrayList<PlaceSmall> places;
    private boolean isLoaded = false;
    private boolean isScaling = false;
    private boolean near = false;
    private boolean isTouch = false;
    private ArrayList<Rect> breweriesPopUp;
    private ArrayList<Rect> placesPopUp;
    private Rect rect;
    private Bitmap main_bitmap;
    private Bitmap thumb;
    private Bitmap brPopUpC;
    private Paint selected, brws, plcs, user, numbers, name, info;
    private int br_marker_size = 8;
    private int br_image_size = 68;
    private int br_background_x = 260;
    private int br_background_y = 83;
    private int indexB = -1;
    private int indexP = -1;

    public InteractiveMapView(Context context, Bitmap bitmap_background) {
        super(context);
        this.context = context;

        this.mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
        this.main_bitmap = bitmap_background;

        this.selected = new Paint();
        this.selected.setColor(Color.parseColor("#CC0000"));

        this.brws = new Paint();
        this.brws.setColor(Color.parseColor("#1F1F1F"));

        this.plcs = new Paint();
        this.plcs.setColor(Color.parseColor("#000066"));

        this.numbers = new Paint();
        numbers.setColor(Color.parseColor("#FFFCF0"));
        numbers.setTextSize(10);
        numbers.setTextAlign(Paint.Align.CENTER);

        this.name = new Paint();
        name.setColor(Color.parseColor("#1F1F1F"));
        name.setTextSize(14);
        name.setTextAlign(Paint.Align.LEFT);

        this.info = new Paint();
        info.setColor(Color.parseColor("#454545"));
        info.setTextSize(12);
        info.setTextAlign(Paint.Align.LEFT);

        this.user = new Paint();
        user.setColor(Color.parseColor("#3369E8"));
        user.setAlpha(125);

        this.breweriesPopUp = new ArrayList<>();
        this.placesPopUp = new ArrayList<>();

        Bitmap b1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.popup_arrow);
        brPopUpC = Bitmap.createScaledBitmap(b1, br_background_x, br_background_y, false);
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

        if (!isLoaded) {
            initScale();
            isLoaded = true;
        }
        setPositionInImage();

        canvas.save();
        canvas.scale(mScaleFactor, mScaleFactor);
        canvas.translate(mPosX - scalePointX, mPosY - scalePointY);
        //canvas.drawARGB(255, 125, 125, 125);
        canvas.drawBitmap(main_bitmap, 0, 0, null);

        for (int i = 0; i < breweriesPopUp.size(); i++) {
            if (i != indexB) {
                canvas.drawRect(breweriesPopUp.get(i), brws);
                canvas.drawText(String.valueOf(breweries.get(i).stand), breweries.get(i).posX, breweries.get(i).posY + 4, numbers);
            }
        }

        for (int i = 0; i < placesPopUp.size(); i++) {
            if (i != indexP) {
                canvas.drawRect(placesPopUp.get(i), plcs);
                canvas.drawText(String.valueOf(places.get(i).stand), places.get(i).posX, places.get(i).posY + 4, numbers);
            }
        }

        if (indexB != -1) {
            if (thumb != null) {
                canvas.drawBitmap(brPopUpC, rect.left, rect.top, null);
                canvas.drawBitmap(thumb, rect.left + 9, rect.top + 9, null);

                if (breweries.get(indexB).name.length() < 20) {
                    canvas.drawText(breweries.get(indexB).name, rect.left + (thumb.getWidth() + 20), rect.top + (br_background_y * 0.3f), name);
                } else {
                    String names[] = breweries.get(indexB).name.split(" ");
                    String nm = "";
                    String nm2 = "";
                    for(int i = 0; i < names.length; i++){
                        if(nm.length() + names[i].length() < 20){
                            nm += names[i];
                        } else {
                            for(int j = i; j < names.length; j++){
                                nm2 += names[j];
                                if(j + 1 != names.length) {
                                    nm2 += " ";
                                }
                            }
                            break;
                        }
                        nm += " ";
                    }
                    canvas.drawText(nm, rect.left + (thumb.getWidth() + 20), rect.top + (br_background_y * 0.3f), name);
                    canvas.drawText(nm2, rect.left + (thumb.getWidth() + 20), rect.top + (br_background_y * 0.5f), name);
                }
                if (breweries.get(indexB).city.length() + breweries.get(indexB).state.length() + breweries.get(indexB).country.length() < 22) {
                    canvas.drawText(breweries.get(indexB).city + ", " + breweries.get(indexB).state + ", " + breweries.get(indexB).country, rect.left + (thumb.getWidth() + 20), rect.top + (br_background_y * 0.7f), info);
                } else if (breweries.get(indexB).city.length() + breweries.get(indexB).state.length() < 22) {
                    canvas.drawText(breweries.get(indexB).city + ", " + breweries.get(indexB).state + ",", rect.left + (thumb.getWidth() + 20), rect.top + (br_background_y * 0.7f), info);
                    canvas.drawText(breweries.get(indexB).country, rect.left + (thumb.getWidth() + 20), rect.top + (br_background_y * 0.9f), info);
                } else {
                    canvas.drawText(breweries.get(indexB).city + ",", rect.left + (thumb.getWidth() + 20), rect.top + (br_background_y * 0.7f), info);
                    canvas.drawText(breweries.get(indexB).state + ", " + breweries.get(indexB).country, rect.left + (thumb.getWidth() + 20), rect.top + (br_background_y * 0.9f), info);
                }
            }
            canvas.drawRect(breweriesPopUp.get(indexB), selected);
            canvas.drawText(String.valueOf(breweries.get(indexB).stand), breweries.get(indexB).posX, breweries.get(indexB).posY + 4, numbers);
        }

        if (indexP != -1) {
            if(thumb != null) {
                canvas.drawBitmap(brPopUpC, rect.left, rect.top, null);
                canvas.drawBitmap(thumb, rect.left + 9, rect.top + 9, null);
                canvas.drawText(places.get(indexP).name, rect.left + (thumb.getWidth() + 20), rect.top + (br_background_y * 0.5f), name);
            }
            canvas.drawRect(placesPopUp.get(indexP), selected);
            canvas.drawText(String.valueOf(places.get(indexP).stand), places.get(indexP).posX, places.get(indexP).posY + 4, numbers);
        }

        if(near) {
            canvas.drawCircle(userX, userY, 10, user);
        }

        //canvas.drawCircle(cursorX, cursorY, 10, cursor);
        //canvas.rotate(90);
        canvas.restore();
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        // Let the ScaleGestureDetector inspect all events.
        mScaleDetector.onTouchEvent(ev);

        final int action = ev.getActionMasked();
        switch(action) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                if (ev.getPointerCount() > 1) {
                    isScaling = true;
                } else {
                    int x = (int)(ev.getX() / mScaleFactor);
                    int y = (int)(ev.getY() / mScaleFactor);
                    mLastTouchX = x;
                    mLastTouchY = y;
                    cursorX = x - mPosX; // canvas X
                    cursorY = y - mPosY; // canvas Y
                    isTouch = true;
                }
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                if (!isScaling) {
                    // Only move if the ScaleGestureDetector isn't processing a gesture.
                    int x = (int) (ev.getX() / mScaleFactor);
                    int y = (int) (ev.getY() / mScaleFactor);
                    int dx = x - mLastTouchX; // change in X
                    int dy = y - mLastTouchY; // change in Y
                    mPosX += dx;
                    mPosY += dy;
                    mLastTouchX = x;
                    mLastTouchY = y;
                    cursorX = x - mPosX; // canvas X
                    cursorY = y - mPosY; // canvas Y
                    isTouch = false;
                    invalidate();
                }
                break;
            }
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP: {
                mLastTouchX = 0;
                mLastTouchY = 0;
                if (isScaling) {
                    if (ev.getPointerCount() < 2) {
                        isScaling = false;
                    } else {
                        mPosX -= scalePointX;
                        mPosY -= scalePointY;
                        scalePointX = 0;
                        scalePointY = 0;
                    }
                } else {
                    int x = (int)(ev.getX() / mScaleFactor);
                    int y = (int)(ev.getY() / mScaleFactor);
                    cursorX = x - mPosX; // canvas X
                    cursorY = y - mPosY; // canvas Y
                    if (rect != null && rect.contains(cursorX, cursorY)) {
                        if (indexB != -1) {
                            SpecialMapFragment.openBrewery(breweries.get(indexB).id);
                        } else if (indexP != -1) {
                            SpecialMapFragment.openPlace(places.get(indexP).id);
                        }
                    } else {
                        if (isTouch) {
                            isTouch = false;
                            cleanup();
                        }
                        for (int i = 0; i < breweriesPopUp.size(); i++) {
                            if (breweriesPopUp.get(i).contains(cursorX, cursorY)) {
                                setupBrewery(i);
                                break;
                            }
                        }
                        for (int i = 0; i < placesPopUp.size(); i++) {
                            if (placesPopUp.get(i).contains(cursorX, cursorY)) {
                                setupPlace(i);
                                break;
                            }
                        }
                    }
                    invalidate();
                }
                break;
            }
        }
        return true;
    }

    private void getThumbnail(String url) {
        if (url.contains("/missing.png")) {
            Bitmap image = Bitmap.createBitmap(br_image_size, br_image_size, Bitmap.Config.ARGB_8888);
            image.eraseColor(Color.WHITE);
            thumb = image;
            invalidate();
        } else {
            ImageCache.getImage(url, context, new ImageCache.OnResponseListener() {
                @Override
                public void onResponse(Bitmap bitmap, String error) {
                    if (error != null) {
                        thumb = Bitmap.createScaledBitmap(bitmap, br_image_size, br_image_size, false);
                    } else {
                        Bitmap image = Bitmap.createBitmap(br_image_size, br_image_size, Bitmap.Config.ARGB_8888);
                        image.eraseColor(Color.WHITE);
                        thumb = image;
                    }
                    invalidate();
                }
            });
        }
    }

    private void setRect (final Rect selected_rect) {
        if ((selected_rect.centerY() - (br_background_y + 20)) < 0) {
            if ((selected_rect.centerX() - (br_background_x / 2)) < 0) {
                rect = new Rect(selected_rect.centerX(), selected_rect.centerY() + 20, selected_rect.centerX() + br_background_x, selected_rect.centerY() + (br_background_y + 20));
            } else if ((selected_rect.centerX() + (br_background_x / 2)) > main_bitmap.getWidth()) {
                rect = new Rect(selected_rect.centerX() - br_background_x, selected_rect.centerY() + 20, selected_rect.centerX(), selected_rect.centerY() + (br_background_y + 20));
            } else {
                rect = new Rect(selected_rect.centerX() - (br_background_x / 2), selected_rect.centerY() + 20, selected_rect.centerX() + (br_background_x / 2), selected_rect.centerY() + (br_background_y + 20));
            }
        } else {
            if ((selected_rect.centerX() - (br_background_x / 2)) < 0) {
                rect = new Rect(selected_rect.centerX(), selected_rect.centerY() - (br_background_y + 20), selected_rect.centerX() + br_background_x, selected_rect.centerY() - 20);
            } else if ((selected_rect.centerX() + (br_background_x / 2)) > main_bitmap.getWidth()) {
                rect = new Rect(selected_rect.centerX() - br_background_x, selected_rect.centerY() - (br_background_y + 20), selected_rect.centerX(), selected_rect.centerY() - 20);
            } else {
                rect = new Rect(selected_rect.centerX() - (br_background_x / 2), selected_rect.centerY() - (br_background_y + 20), selected_rect.centerX() + (br_background_x / 2), selected_rect.centerY() - 20);
            }
        }
        focus(rect.centerX(), rect.centerY());
        invalidate();
    }

    private void setPositionInImage() {
        int minX = (int)(this.getWidth() / mScaleFactor) - main_bitmap.getWidth();
        int minY = (int)(this.getHeight() / mScaleFactor) - main_bitmap.getHeight();
        if (mPosX < minX - 100) {
            mPosX = minX - 100;
        }
        if (mPosX > 100) {
            mPosX = 100;
        }
        if (mPosY < minY - 100) {
            mPosY = minY - 100;
        }
        if (mPosY > 100) {
            mPosY = 100;
        }
    }

    public void cleanup() {
        indexB = -1;
        indexP = -1;
        thumb = null;
        rect = null;
        invalidate();
    }

    public void setupBrewery(int i) {
        cleanup();
        setRect(breweriesPopUp.get(i));
        getThumbnail(breweries.get(i).thumb);
        indexB = i;
    }

    public void setupPlace(int i) {
        cleanup();
        setRect(placesPopUp.get(i));
        getThumbnail(places.get(i).thumb);
        indexP = i;
    }

    public void setUser(double x, double y) {
        this.userX = (int)(x * main_bitmap.getWidth());
        this.userY = main_bitmap.getHeight() - (int)(y * main_bitmap.getWidth());
        invalidate();
    }

    public void setNear(boolean near){
        this.near = near;
    }

    public void setBreweries(ArrayList<BrewerySmall> breweries) {
        this.breweries = breweries;
        for (int i = 0; i < breweries.size(); i++) {
            breweriesPopUp.add(new Rect(breweries.get(i).posX - br_marker_size, breweries.get(i).posY - br_marker_size, breweries.get(i).posX + br_marker_size, breweries.get(i).posY + br_marker_size));
        }
    }

    public void setPlaces(ArrayList<PlaceSmall> places) {
        this.places = places;
        for (int i = 0; i < places.size(); i++) {
            placesPopUp.add(new Rect(places.get(i).posX - br_marker_size, places.get(i).posY - br_marker_size, places.get(i).posX + br_marker_size, places.get(i).posY + br_marker_size));
        }
    }

    public void focus(int x, int y){
        mPosX = (int)((this.getWidth() / 2) / mScaleFactor) - x;
        mPosY = (int)((this.getHeight() / 2) / mScaleFactor) - y;
    }

    private void initScale() {
        mScaleFactor = Math.max((float)this.getWidth() / main_bitmap.getWidth(), (float)this.getHeight() / main_bitmap.getHeight());
        minScale = mScaleFactor / 1.4f;
        maxScale = mScaleFactor * 2;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor *= detector.getScaleFactor();

            // Don't let the object get too small or too large.
            mScaleFactor = Math.max(minScale, Math.min(mScaleFactor, maxScale));

            invalidate();
            return true;
        }
    }
}
