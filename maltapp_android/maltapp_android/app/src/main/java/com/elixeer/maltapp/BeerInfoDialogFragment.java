package com.elixeer.maltapp;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.elixeer.maltapp.Adapters.FragmentListPagerAdapter;
import com.elixeer.maltapp.Models.Beer;
import com.matthewtamlin.sliding_intro_screen_library.DotIndicator;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class BeerInfoDialogFragment extends DialogFragment {

    public BeerInfoDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        if (window != null) {
            window.requestFeature(Window.FEATURE_NO_TITLE);
        }
        Beer beer = null;
        Bundle arguments = getArguments();
        if (arguments != null) {
            beer = (Beer) arguments.getSerializable("BEER");
        }

        ArrayList<Fragment> fragments = new ArrayList<>();

        if (beer != null) {
            fragments.add(BeerInfoPageFragment.newInstance(getString(R.string.description), beer.description));
            fragments.add(BeerInfoPageFragment.newInstance(beer.style_name, beer.style_description));
            fragments.add(BeerInfoPageFragment.newInstance(beer.glass_name, beer.glass_description));
        }
        fragments.add(BeerInfoPageFragment.newInstance(getString(R.string.abv_title), getString(R.string.abv_info)));
        fragments.add(BeerInfoPageFragment.newInstance(getString(R.string.ibu_title), getString(R.string.ibu_info)));
        fragments.add(BeerInfoPageFragment.newInstance(getString(R.string.srm_title), getString(R.string.srm_info)));
        fragments.add(BeerInfoPageFragment.newInstance(getString(R.string.oe_title), getString(R.string.oe_info)));

        FragmentListPagerAdapter pagerAdapter = new FragmentListPagerAdapter(getChildFragmentManager(), fragments);

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_beer_info_dialog, container, false);

        if (beer != null) {
            TextView beerInfoTitleTextView = rootView.findViewById(R.id.beerInfoTitleTextView);
            beerInfoTitleTextView.setText(beer.name);
        }

        ViewPager pager = rootView.findViewById(R.id.beerInfoViewPager);
        pager.setAdapter(pagerAdapter);
        if (arguments != null) {
            pager.setCurrentItem(arguments.getInt("PAGE", 0));
        }

        final DotIndicator indicator = rootView.findViewById(R.id.beerInfoViewPagerIndicator);
        indicator.setNumberOfItems(fragments.size());
        indicator.setSelectedItem(pager.getCurrentItem(), false);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                indicator.setSelectedItem(position, true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        Button beerInfoDismissButton = rootView.findViewById(R.id.beerInfoDismissButton);
        beerInfoDismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return rootView;
    }

}
