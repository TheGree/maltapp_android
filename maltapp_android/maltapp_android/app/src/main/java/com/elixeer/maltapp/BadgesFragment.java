package com.elixeer.maltapp;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Comparators.BadgeComparator;
import com.elixeer.maltapp.Models.Badge;
import com.elixeer.maltapp.Models.DynamicBadges;
import com.elixeer.maltapp.Models.Score;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class BadgesFragment extends Fragment {
    LinearLayout profileBlockLinearLayout;
    MainActivity parent;
    ArrayList<Score> scores;
    DynamicBadges dynamicBadges;
    GridLayout badgesGridLayout;
    ArrayList<Badge> badges_dynamics;
    ArrayList<Badge> badges_normals;
    ArrayList<Badge> badges_totals;
    int user_id;

    public BadgesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            //parent.setTab(4);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.misions);
                actionBar.show();
            }
        }
        View rootView = inflater.inflate(R.layout.fragment_badges, container, false);

        user_id = getArguments().getInt("USER_ID");

        badgesGridLayout = rootView.findViewById(R.id.badgesGridLayout);
        profileBlockLinearLayout = rootView.findViewById(R.id.profileBlockLinearLayout);

        return rootView;
    }

    private boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager != null && connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (isOnline()) {
            if (user_id > 0) {
                getDataBadgesDynamic();
            } else {
                try {
                    JSONObject empty = new JSONObject();
                    dynamicBadges = new DynamicBadges(empty);
                    JSONArray jsonScores = new JSONArray();
                    scores = Score.tastesFromJSONArray(jsonScores);
                    getBagdesDynamics();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            profileBlockLinearLayout.setEnabled(false);
            profileBlockLinearLayout.setAlpha(0.5f);
            notInternetMSG();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("Profile");
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(BadgesFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void getDataBadgesDynamic() {
        String url = ApiUrl.DATA_DYNAMIC_BADGES + "&user_id=" + user_id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONObject jsonDynamicBadge = jsonResponse.getJSONObject("dynamic_badges");
                            dynamicBadges = new DynamicBadges(jsonDynamicBadge);
                            getDataBadgesNormals();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getDataBadgesNormals() {
        String url = ApiUrl.SCORES + "&user_id=" + user_id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonScores = jsonResponse.getJSONArray("scores");
                            scores = Score.tastesFromJSONArray(jsonScores);
                            if (scores.size() < 1) {
                                Log.i(getString(R.string.app_name), "No scores found");
                            }
                            getBagdesDynamics();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getBagdesDynamics() {
        String url = ApiUrl.BADGES_DYNAMICS;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonBadges = jsonResponse.getJSONArray("badges");
                            badges_dynamics = Badge.badgesFromJSONArray(jsonBadges, getContext());
                            if (badges_dynamics.size() < 1) {
                                Log.i(getString(R.string.app_name), "No badges found");
                            } else {
                                getBagdesNormals();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getBagdesNormals() {
        String url = ApiUrl.BADGES;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonBadges = jsonResponse.getJSONArray("badges");
                            badges_normals = Badge.badgesFromJSONArray(jsonBadges, getContext());
                            if (badges_normals.size() < 1) {
                                Log.i(getString(R.string.app_name), "No badges found");
                            } else {
                                fillBadges();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void fillBadges() {
        badges_totals = new ArrayList<>();
        badges_totals.addAll(badges_dynamics);
        badges_totals.addAll(badges_normals);
        Collections.sort(badges_totals, new BadgeComparator());
        for (int i = 0; i < badges_totals.size(); i++) {
            LinearLayout child = (LinearLayout) View.inflate(getContext(), R.layout.list_item_badge, null);
            final ImageView badgeImageView = child.findViewById(R.id.badgeImageView);
            TextView badgeTitleTextView = child.findViewById(R.id.badgeTitleTextView);
            TextView badgeProgressTextView = child.findViewById(R.id.badgeProgressTextView);
            LinearLayout badgeLinearView = child.findViewById(R.id.badgeLinearView);
            ImageCache.getImage(badges_totals.get(i).medium, getContext(), new ImageCache.OnResponseListener() {
                @Override
                public void onResponse(Bitmap bitmap, String error) {
                    badgeImageView.setImageBitmap(bitmap);
                }
            });
            badgeTitleTextView.setText(badges_totals.get(i).name);

            if (badges_totals.get(i).order_index == badges_totals.get(badges_totals.size() - 1).order_index) {
                //badgeImageView.setImageAlpha(50);
                badgeProgressTextView.setText("");
                final int finalI = i;
                badgeLinearView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (user_id > 0) {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View pop = inflater.inflate(R.layout.pop_item_badge, null);
                            TextView custom_title_badge_textView = pop.findViewById(R.id.custom_title_badge_textView);
                            final ImageView popBadgeImageView = pop.findViewById(R.id.popBadgeImageView);
                            Button popBadgeShareButton = pop.findViewById(R.id.popBadgeShareButton);
                            TextView popBadgeTextView = pop.findViewById(R.id.popBadgeTextView);

                            ImageCache.getImage(badges_totals.get(finalI).medium, getContext(), new ImageCache.OnResponseListener() {
                                @Override
                                public void onResponse(Bitmap bitmap, String error) {
                                    popBadgeImageView.setImageBitmap(bitmap);
                                }
                            });
                            custom_title_badge_textView.setText(badges_totals.get(finalI).name);
                            popBadgeTextView.setText(badges_totals.get(finalI).description);
                            //popBadgeImageView.setImageAlpha(50);
                            popBadgeShareButton.setAlpha(0.0f);

                            AlertDialog.Builder builder = new AlertDialog.Builder(BadgesFragment.this.getActivity())
                                    //.setIcon(R.drawable.ic_trophy)
                                    //.setTitle(badges_totals.get(finalI).name)
                                    //.setMessage(R.string.msg_goal_achieved_message)
                                    .setView(pop)
                                    .setPositiveButton(R.string.ok, null);
                            AlertDialog alert = builder.create();
                            alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {

                                }
                            });
                            alert.show();
                        } else {
                            loginPopUp();
                        }
                    }
                });
            } else {
                if (badges_totals.get(i).index == 0) {
                    if (scores.size() > 1) {
                        for (int j = 0; j < scores.size(); j++) {
                            if (scores.get(j).badgetype_id == badges_totals.get(i).badgetype_id) {
                                badgeProgressTextView.setText(String.valueOf(scores.get(j).points));
                            }
                        }
                    } else {
                        badgeProgressTextView.setText(String.valueOf(0));
                    }
                } else if (badges_totals.get(i).only_beer) {
                    badgeProgressTextView.setText(String.valueOf(dynamicBadges.beers));
                } else if (badges_totals.get(i).is_param) {
                    switch (badges_totals.get(i).badgetype_id) {
                        case 1:
                            badgeProgressTextView.setText(String.valueOf(dynamicBadges.breweries_ids.size()));
                            break;
                        case 2:
                            badgeProgressTextView.setText(String.valueOf(dynamicBadges.cities_ids.size()));
                            break;
                        case 3:
                            badgeProgressTextView.setText(String.valueOf(dynamicBadges.states_ids.size()));
                            break;
                        case 4:
                            badgeProgressTextView.setText(String.valueOf(dynamicBadges.countries_ids.size()));
                            break;
                        case 5:
                            badgeProgressTextView.setText(String.valueOf(dynamicBadges.beerstyles_ids.size()));
                            break;
                        case 6:
                            badgeProgressTextView.setText(String.valueOf(dynamicBadges.beertypes_ids.size()));
                            break;
                    }
                } else {
                    switch (badges_totals.get(i).badgetype_id) {
                        case 1:
                            if (dynamicBadges.breweries_ids.size() > 0) {
                                for (int j = 0; j < dynamicBadges.breweries_ids.size(); j++) {
                                    if (dynamicBadges.breweries_ids.get(j) == badges_totals.get(i).index) {
                                        badgeProgressTextView.setText(String.valueOf(dynamicBadges.beerstyles_pts.get(j)));
                                        break;
                                    } else {
                                        badgeProgressTextView.setText(String.valueOf(0));
                                    }
                                }
                            } else {
                                badgeProgressTextView.setText(String.valueOf(0));
                            }
                            break;
                        case 2:
                            if (dynamicBadges.cities_ids.size() > 0) {
                                for (int j = 0; j < dynamicBadges.cities_ids.size(); j++) {
                                    if (dynamicBadges.cities_ids.get(j) == badges_totals.get(i).index) {
                                        badgeProgressTextView.setText(String.valueOf(dynamicBadges.cities_pts.get(j)));
                                        break;
                                    } else {
                                        badgeProgressTextView.setText(String.valueOf(0));
                                    }
                                }
                            } else {
                                badgeProgressTextView.setText(String.valueOf(0));
                            }
                            break;
                        case 3:
                            if (dynamicBadges.states_ids.size() > 0) {
                                for (int j = 0; j < dynamicBadges.states_ids.size(); j++) {
                                    if (dynamicBadges.states_ids.get(j) == badges_totals.get(i).index) {
                                        badgeProgressTextView.setText(String.valueOf(dynamicBadges.states_pts.get(j)));
                                        break;
                                    } else {
                                        badgeProgressTextView.setText(String.valueOf(0));
                                    }
                                }
                            } else {
                                badgeProgressTextView.setText(String.valueOf(0));
                            }
                            break;
                        case 4:
                            if (dynamicBadges.countries_ids.size() > 0) {
                                for (int j = 0; j < dynamicBadges.countries_ids.size(); j++) {
                                    if (dynamicBadges.countries_ids.get(j) == badges_totals.get(i).index) {
                                        badgeProgressTextView.setText(String.valueOf(dynamicBadges.countries_pts.get(j)));
                                        break;
                                    } else {
                                        badgeProgressTextView.setText(String.valueOf(0));
                                    }
                                }
                            } else {
                                badgeProgressTextView.setText(String.valueOf(0));
                            }
                            break;
                        case 5:
                            if (dynamicBadges.beerstyles_ids.size() > 0) {
                                for (int j = 0; j < dynamicBadges.beerstyles_ids.size(); j++) {
                                    if (dynamicBadges.beerstyles_ids.get(j) == badges_totals.get(i).index) {
                                        badgeProgressTextView.setText(String.valueOf(dynamicBadges.beerstyles_pts.get(j)));
                                        break;
                                    } else {
                                        badgeProgressTextView.setText(String.valueOf(0));
                                    }
                                }
                            } else {
                                badgeProgressTextView.setText(String.valueOf(0));
                            }
                            break;
                        case 6:
                            if (dynamicBadges.beertypes_ids.size() > 0) {
                                for (int j = 0; j < dynamicBadges.beertypes_ids.size(); j++) {
                                    if (dynamicBadges.beertypes_ids.get(j) == badges_totals.get(i).index) {
                                        badgeProgressTextView.setText(String.valueOf(dynamicBadges.beertypes_pts.get(j)));
                                        break;
                                    } else {
                                        badgeProgressTextView.setText(String.valueOf(0));
                                    }
                                }
                            } else {
                                badgeProgressTextView.setText(String.valueOf(0));
                            }
                            break;
                    }
                }

                int level = 0;

                for (int j = 0; j < badges_totals.get(i).points.size(); j++) {
                    if (Integer.parseInt(badgeProgressTextView.getText().toString()) <= badges_totals.get(i).points.get(j)) {
                        badgeProgressTextView.setText(badgeProgressTextView.getText() + "/" + badges_totals.get(i).points.get(j));
                        level = j;
                        break;
                    }
                    if (Integer.parseInt(badgeProgressTextView.getText().toString()) > badges_totals.get(i).points.get(badges_totals.get(i).points.size() - 1)) {
                        badgeProgressTextView.setText(badgeProgressTextView.getText() + "/" + badges_totals.get(i).points.get(badges_totals.get(i).points.size() - 1));
                        level = j;
                        break;
                    }
                }

                if (Integer.parseInt(badgeProgressTextView.getText().toString().split("/")[0]) < badges_totals.get(i).points.get(0)) {
                    badgeImageView.setImageAlpha(50);
                    final int finalI = i;
                    badgeLinearView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (user_id > 0) {
                                LayoutInflater inflater = getActivity().getLayoutInflater();
                                View pop = inflater.inflate(R.layout.pop_item_badge, null);
                                TextView custom_title_badge_textView = pop.findViewById(R.id.custom_title_badge_textView);
                                final ImageView popBadgeImageView = pop.findViewById(R.id.popBadgeImageView);
                                Button popBadgeShareButton = pop.findViewById(R.id.popBadgeShareButton);
                                TextView popBadgeTextView = pop.findViewById(R.id.popBadgeTextView);

                                ImageCache.getImage(badges_totals.get(finalI).medium, getContext(), new ImageCache.OnResponseListener() {
                                    @Override
                                    public void onResponse(Bitmap bitmap, String error) {
                                        popBadgeImageView.setImageBitmap(bitmap);
                                    }
                                });
                                custom_title_badge_textView.setText(badges_totals.get(finalI).name);
                                popBadgeTextView.setText(badges_totals.get(finalI).description);
                                //popBadgeImageView.setImageAlpha(50);
                                popBadgeShareButton.setAlpha(0.5f);

                                AlertDialog.Builder builder = new AlertDialog.Builder(BadgesFragment.this.getActivity())
                                        //.setIcon(R.drawable.ic_trophy)
                                        //.setTitle(badges_totals.get(finalI).name)
                                        //.setMessage(R.string.msg_goal_achieved_message)
                                        .setView(pop)
                                        .setPositiveButton(R.string.ok, null);
                                AlertDialog alert = builder.create();
                                alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                    }
                                });
                                alert.show();
                            } else {
                                loginPopUp();
                            }
                        }
                    });
                } else {
                    final int finalI = i;
                    final int finalLevel = level;
                    badgeLinearView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View pop = inflater.inflate(R.layout.pop_item_badge, null);
                            final ImageView popBadgeImageView = pop.findViewById(R.id.popBadgeImageView);
                            TextView custom_title_badge_textView = pop.findViewById(R.id.custom_title_badge_textView);
                            Button popBadgeShareButton = pop.findViewById(R.id.popBadgeShareButton);
                            TextView popBadgeTextView = pop.findViewById(R.id.popBadgeTextView);

                            popBadgeShareButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    // Analytics event
                                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("profile", "shared_badge_pressed", "Facebook");

                                    getScores(ApiUrl.FACE_SCORE_ID);

                                    /*
                                    ShareLinkContent content = new ShareLinkContent.Builder()
                                            .setContentUrl(Uri.parse(ApiUrl.MALTAPP_MX))
                                            .setContentTitle(badges_totals.get(finalI).name + " " + getString(R.string.level) + " " + (finalLevel + 1))
                                            .setContentDescription(String.format(getString(R.string.shareBadge), badges_totals.get(finalI).name + " " + getString(R.string.level) + " " + (finalLevel + 1)))
                                            .setImageUrl(Uri.parse(ApiUrl.SQUARE_IMAGE + badges_totals.get(finalI).medium))
                                            .build();
                                    */

                                    SharePhoto photo = new SharePhoto.Builder()
                                            .setCaption(String.format(getString(R.string.shareBadge), badges_totals.get(finalI).name + " " + getString(R.string.level) + " " + (finalLevel + 1)))
                                            .setImageUrl(Uri.parse(ApiUrl.SQUARE_IMAGE + badges_totals.get(finalI).medium))
                                            .setUserGenerated(false)
                                            .build();

                                    SharePhotoContent content = new SharePhotoContent.Builder()
                                            .addPhoto(photo)
                                            .build();

                                    ShareDialog.show(parent, content);
                                }
                            });

                            ImageCache.getImage(badges_totals.get(finalI).medium, getContext(), new ImageCache.OnResponseListener() {
                                @Override
                                public void onResponse(Bitmap bitmap, String error) {
                                    popBadgeImageView.setImageBitmap(bitmap);
                                }
                            });
                            custom_title_badge_textView.setText(badges_totals.get(finalI).name + " " + getString(R.string.level) + " " + (finalLevel + 1));
                            popBadgeTextView.setText(badges_totals.get(finalI).description);

                            AlertDialog.Builder builder = new AlertDialog.Builder(BadgesFragment.this.getActivity())
                                    //.setTitle(badges_totals.get(finalI).name + " " + getString(R.string.level) + " " + (finalLevel + 1))
                                    .setView(pop)
                                    .setPositiveButton(R.string.ok, null);
                            AlertDialog alert = builder.create();
                            alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {

                                }
                            });
                            alert.show();
                        }
                    });
                }
            }

            badgesGridLayout.addView(child);
        }
    }

    private void getScores(final int badgetype_id) {
        String url = ApiUrl.SCORES + "&user_id=" + user_id + "&badgetype_id=" + badgetype_id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonScores = jsonResponse.getJSONArray("scores");
                            scores = Score.tastesFromJSONArray(jsonScores);
                            if (scores.size() < 1) {
                                createScore(badgetype_id);
                            } else {
                                editScores(badgetype_id);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void createScore(int badgetype_id) {
        String scoresFaceParams = "{";
        scoresFaceParams += "\"user_id\":\"" + user_id + "\"";
        scoresFaceParams += ",\"badgetype_id\":\"" + badgetype_id + "\"";
        scoresFaceParams += ",\"points\":\"" + 1 + "\"";
        scoresFaceParams += "}";

        String url = ApiUrl.SCORES;
        String[][] headers = {{"Content-Type", "application/json"}};
        new AsyncRequestString("POST", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .setData(scoresFaceParams)
                .setHeaders(headers)
                .execute();
    }

    private void editScores(int badgetype_id) {
        String url = ApiUrl.SCORE_UPDATE + "&user_id=" + user_id + "&badgetype_id=" + badgetype_id + "&points=" + 1;
        new AsyncRequestString("PUT", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    void loginPopUp() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_login, null);
        Button popLoginButton = pop.findViewById(R.id.popLoginButton);

        AlertDialog.Builder builder = new AlertDialog.Builder(BadgesFragment.this.getActivity())
                //.setIcon(R.drawable.ic_trophy)
                //.setTitle(getString(R.string.maltappNotice))
                //.setMessage(R.string.msg_goal_achieved_message)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        popLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment registerFragment = new RegisterFragment();
                parent.pushFragment(registerFragment);
                alert.dismiss();
            }
        });

        alert.show();
    }

}
