package com.elixeer.maltapp.Handlers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ALEBRIJE02 on 15/03/2016.
 */
public class MaltappDataHandler {

    public static Integer decryptData(String value) {

        Integer valT = 0;
        for (int i = value.length() - 1; i > 0; i -= 2) {

            String hex = value.substring(i - 1, i + 1);
            try {
                Integer val = Integer.parseInt(hex, 16);
                valT = (valT * 100) + val;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return valT - 1358;
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

}
