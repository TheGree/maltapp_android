package com.elixeer.maltapp.Models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class Country implements Serializable {
    public int id;
    public String name;

    public Country(JSONObject jsonCountry) {
        this.id = jsonCountry.optInt("id", 0);
        this.name = jsonCountry.optString("name", "");
    }

    public static ArrayList<Country> countriesFromJSONArray(JSONArray jsonCountries) {
        ArrayList<Country> countries = new ArrayList<>();
        for (int i = 0; i < jsonCountries.length(); i++) {
            JSONObject jsonCountry = jsonCountries.optJSONObject(i);
            if (jsonCountry != null) {
                countries.add(new Country(jsonCountry));
            }
        }
        return countries;
    }
}
