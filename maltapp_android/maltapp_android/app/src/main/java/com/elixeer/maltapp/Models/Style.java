package com.elixeer.maltapp.Models;

import android.content.Context;
import android.content.SharedPreferences;

import com.elixeer.maltapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

public class Style implements Serializable {

    public int id;
    public String name;
    public String description;
    public String imageUrlThumb;
    public String imageUrlMedium;

    public Style(JSONObject jsonStyle, Context context) {
        String language = Locale.getDefault().getLanguage();
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
            if (sharedPreferences.contains("language")) {
                language = sharedPreferences.getString("language", Locale.getDefault().getLanguage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String language2 = "en";
        if (language.equals("en")) {
            language2 = "es";
        }

        this.id = jsonStyle.optInt("id", 0);
        this.name = jsonStyle.optString("name", "");
        this.description = jsonStyle.optString("description_" + language, "");
        if (this.description.equals("")) {
            this.description = jsonStyle.optString("description_" + language2, "");
        }
        this.imageUrlThumb = jsonStyle.optString("thumb", null);
        this.imageUrlMedium = jsonStyle.optString("medium", null);
    }

    public static ArrayList<Style> stylesFromJSONArray(JSONArray jsonStyles, Context context) {
        ArrayList<Style> styles = new ArrayList<>();
        for (int i = 0; i < jsonStyles.length(); i++) {
            JSONObject jsonStyle = jsonStyles.optJSONObject(i);
            if (jsonStyle != null) {
                styles.add(new Style(jsonStyle, context));
            }
        }
        return styles;
    }

}