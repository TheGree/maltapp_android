package com.elixeer.maltapp;

import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.alebrije.async.ImageCache;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import io.fabric.sdk.android.Fabric;

public class AnalyticsApplication extends MultiDexApplication {
    private Tracker mTracker;

    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        ImageCache.clear();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (level >= TRIM_MEMORY_UI_HIDDEN) {
            ImageCache.clear();
        }
    }

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker("UA-72082444-1");
        }
        return mTracker;
    }

    public void sendEvent(String category, String action) {
        sendEvent(category, action, null);
    }

    public void sendEvent(String category, String action, String label) {
        if (BuildConfig.DEBUG) {
            Log.d(getString(R.string.app_name), "Debugging event: " + category + ", for action: " + action + ", with label: " + label);
            return;
        }
        Log.i(getString(R.string.app_name), "Sending event: " + category + ", for action: " + action + ", with label: " + label);
        if (mTracker == null) {
            mTracker = getDefaultTracker();
        }
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .build());
    }

    public void sendEvent(String category, String action, String label, long value) {
        if (BuildConfig.DEBUG) {
            Log.d(getString(R.string.app_name), "Debugging event: " + category + ", for action: " + action + ", with label: " + label + ", and value: " + value);
            return;
        }
        Log.i(getString(R.string.app_name), "Sending event: " + category + ", for action: " + action + ", with label: " + label + ", and value: " + value);
        if (mTracker == null) {
            mTracker = getDefaultTracker();
        }
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .setValue(value)
                .build());
    }

    public void setScreenName(String screenName) {
        if (BuildConfig.DEBUG) {
            Log.d(getString(R.string.app_name), "Debugging screen name: " + screenName);
            return;
        }
        Log.i(getString(R.string.app_name), "Setting screen name: " + screenName);
        if (mTracker == null) {
            mTracker = getDefaultTracker();
        }
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}
