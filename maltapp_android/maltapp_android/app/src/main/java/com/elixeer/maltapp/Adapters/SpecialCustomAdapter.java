package com.elixeer.maltapp.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Models.SpecialEvent;
import com.elixeer.maltapp.R;

import java.util.ArrayList;
import java.util.TreeSet;

public abstract class SpecialCustomAdapter extends BaseAdapter {

    private Context context;

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;

    private ArrayList<SpecialEvent> mData = new ArrayList<>();
    private TreeSet<Integer> sectionHeader = new TreeSet<>();

    private LayoutInflater mInflater;

    public SpecialCustomAdapter(Context context) {
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItem(final SpecialEvent item) {
        mData.add(item);
        notifyDataSetChanged();
    }

    public void addSectionHeaderItem(final SpecialEvent item) {
        mData.add(item);
        sectionHeader.add(mData.size() - 1);
        notifyDataSetChanged();
    }

    public void cleanAdapter() {
        mData = new ArrayList<>();
        sectionHeader = new TreeSet<Integer>();
    }

    @Override
    public int getItemViewType(int position) {
        return sectionHeader.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public SpecialEvent getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder = new ViewHolder();
        int rowType = getItemViewType(position);

        switch (rowType) {
            case TYPE_ITEM : {
                convertView = mInflater.inflate(R.layout.list_item_special_event_breweries, null);

                holder.specialEventBreweryLogoImage = convertView.findViewById(R.id.specialEventBreweryLogoImage);
                holder.specialEventBreweryNameText = convertView.findViewById(R.id.specialEventBreweryNameText);
                holder.specialEventBreweryAddressText = convertView.findViewById(R.id.specialEventBreweryAddressText);
                holder.specialEventBreweryStandNumberText = convertView.findViewById(R.id.specialEventBreweryStandNumberText);
                holder.specialEventBreweriesDetail = convertView.findViewById(R.id.specialEventBreweriesDetail);
                holder.specialEventBeerMapImage = convertView.findViewById(R.id.specialEventBeerMapImage);
                holder.specialEventBreweryLogoImage.setImageBitmap(null);

                if (mData.get(position).thumb.contains("/missing.png")) {
                    //specialEventBreweryLogoImage.setImageResource(R.drawable.img_default_beer);
                } else {
                    holder.specialEventBreweryLogoImage.setImageBitmap(null);
                    ImageCache.getImage(mData.get(position).thumb, context, new ImageCache.OnResponseListener() {
                        @Override
                        public void onResponse(Bitmap bitmap, String error) {
                            holder.specialEventBreweryLogoImage.setImageBitmap(bitmap);
                        }
                    });
                }
                holder.specialEventBreweryNameText.setText(mData.get(position).name);
                if(mData.get(position).type == 1) {
                    holder.specialEventBreweryAddressText.setText(mData.get(position).city);
                } else {
                    holder.specialEventBreweryAddressText.setText(mData.get(position).address);
                }
                holder.specialEventBreweryStandNumberText.setText(String.valueOf(mData.get(position).stand));

                holder.specialEventBreweriesDetail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onElementButtonClick(mData.get(position).id, mData.get(position).type);
                    }
                });

                holder.specialEventBeerMapImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onMapButtonClick(mData.get(position).id, mData.get(position).type);
                    }
                });

                break;
            }
            case TYPE_SEPARATOR : {
                convertView = mInflater.inflate(R.layout.list_item_nearby_separator, null);
                holder.nearbySeparatorTextView = convertView.findViewById(R.id.nearbySeparatorTextView);
                holder.nearbySeparatorTextView.setText(mData.get(position).name);
                break;
            }
        }
        convertView.setTag(holder);

        return convertView;
    }

    public static class ViewHolder {
        public TextView nearbySeparatorTextView;
        public ImageView specialEventBreweryLogoImage;
        public TextView specialEventBreweryNameText;
        public TextView specialEventBreweryAddressText;
        public TextView specialEventBreweryStandNumberText;
        public LinearLayout specialEventBreweriesDetail;
        public ImageButton specialEventBeerMapImage;
    }

    public abstract void onMapButtonClick(int id, int type);

    public abstract void onElementButtonClick(int id, int type);
}