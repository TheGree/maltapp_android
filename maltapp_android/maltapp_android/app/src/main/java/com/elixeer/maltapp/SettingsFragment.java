package com.elixeer.maltapp;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;

import com.alebrije.async.AsyncRequestString;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {
    MainActivity parent;
    Spinner languageSpinner;
    Switch cameraSwitch;
    Switch locationSwitch;
    Switch beersSwitch;
    Switch eventSwitch;
    Switch newsSwitch;
    ArrayList<String> languages;
    String language;
    int permissionChangingIndex = 0;

    enum NumLanguages {
        en("en"), es("es");

        private final String value;

        NumLanguages(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    //NumLanguages language;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.action_settings);
                actionBar.show();
            }
        }
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        if (Build.VERSION.SDK_INT < 23) {
            rootView.findViewById(R.id.permissionsLinearLayout).setVisibility(View.GONE);
        }

        cameraSwitch = rootView.findViewById(R.id.cameraSwitch);
        cameraSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT < 23) {
                    // This shouldn't be happening
                    cameraSwitch.setChecked(true);
                } else {
                    permissionChangingIndex = 0;
                    alertExternalSettings();
                }
            }
        });

        locationSwitch = rootView.findViewById(R.id.locationSwitch);
        locationSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT < 23) {
                    // This shouldn't be happening
                    locationSwitch.setChecked(true);
                } else {
                    permissionChangingIndex = 1;
                    alertExternalSettings();
                }
            }
        });

        //
        String[][] headers = {{"Content-Type", "application/json"}, {"Authorization", "key= AAAAzchdLeU:APA91bH_Nsx5u2e4-fPH0pfhjzOkUOAAU108F9qHkH0SWGgx_RQp10Gjkixlo5YuXcuZmjGHtSkbzAe1ecTwPzoPKeseKSKBIR2fnYzxeBW_yd52Ra1Mz9xUo1SG0ce2Z20q6rySBE-i"}};
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        String url = "https://iid.googleapis.com/iid/info/" +  refreshedToken + "?details=true";
        new AsyncRequestString("POST", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                Log.d("RESPONSE", response);
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("rel")){
                            JSONObject rel = jsonResponse.getJSONObject("rel");
                            if (rel.has("topics")){
                                JSONObject topics = rel.getJSONObject("topics");
                                beersSwitch.setChecked(topics.has("Beers"));
                                eventSwitch.setChecked(topics.has("Events"));
                                newsSwitch.setChecked(topics.has("News"));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .setHeaders(headers)
                .execute();

        beersSwitch = rootView.findViewById(R.id.beersSwitch);
        eventSwitch = rootView.findViewById(R.id.eventSwitch);
        newsSwitch = rootView.findViewById(R.id.newsSwitch);
        beersSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    FirebaseMessaging.getInstance().subscribeToTopic("Beers");
                } else {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic("Beers");
                }
            }
        });

        eventSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    FirebaseMessaging.getInstance().subscribeToTopic("Events");
                } else {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic("Events");
                }
            }
        });
        newsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    FirebaseMessaging.getInstance().subscribeToTopic("News");
                } else {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic("News");
                }
            }
        });

        languageSpinner = rootView.findViewById(R.id.languageSpinner);

        languages = new ArrayList<>();
        languages.add(getString(R.string.english));
        languages.add(getString(R.string.spanish));

        language = Locale.getDefault().getLanguage();

        ArrayAdapter<String> adapter_languages = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, languages);
        adapter_languages.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        languageSpinner.setAdapter(adapter_languages);

        if ("en".equals(language)) {
            languageSpinner.setSelection(0);
        } else {
            languageSpinner.setSelection(1);
        }

        languageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    language = NumLanguages.en.toString();
                } else {
                    language = NumLanguages.es.toString();
                }
                setLocale(language);
                // TODO: Alert on app restart for changes to apply?
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.i(getString(R.string.app_name), "No language selected");
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        cameraSwitch.setChecked(android.support.v4.app.ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
        locationSwitch.setChecked(android.support.v4.app.ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("Settings");
    }

    private void alertExternalSettings() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                //.setIcon(R.drawable.ic_trophy)
                //.setTitle("...")
                .setMessage(R.string.alert_external_settings)
                .setPositiveButton(R.string.action_settings, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.setData(Uri.fromParts("package", getActivity().getPackageName(), null));
                        getContext().startActivity(intent);
                    }
                })
                .setNegativeButton(R.string.dismiss, null)
                .create();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                switch (permissionChangingIndex) {
                    case 0:
                        cameraSwitch.setChecked(!cameraSwitch.isChecked());
                        break;
                    case 1:
                        locationSwitch.setChecked(!locationSwitch.isChecked());
                        break;
                }
            }
        });
        alertDialog.show();
    }

    private void setLocale(String localeCode) {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("language", localeCode);
        editor.apply();

        Locale locale = new Locale(localeCode);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        parent.getBaseContext().getResources().updateConfiguration(config, parent.getBaseContext().getResources().getDisplayMetrics());
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }
}
