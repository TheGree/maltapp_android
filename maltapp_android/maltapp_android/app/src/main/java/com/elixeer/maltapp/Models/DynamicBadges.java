package com.elixeer.maltapp.Models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class DynamicBadges implements Serializable {

    public int beers;
    public JSONObject breweries;
    public ArrayList<Integer> breweries_ids;
    public ArrayList<Integer> breweries_pts;
    public JSONObject cities;
    public ArrayList<Integer> cities_ids;
    public ArrayList<Integer> cities_pts;
    public JSONObject states;
    public ArrayList<Integer> states_ids;
    public ArrayList<Integer> states_pts;
    public JSONObject countries;
    public ArrayList<Integer> countries_ids;
    public ArrayList<Integer> countries_pts;
    public JSONObject beerstyles;
    public ArrayList<Integer> beerstyles_ids;
    public ArrayList<Integer> beerstyles_pts;
    public JSONObject beertypes;
    public ArrayList<Integer> beertypes_ids;
    public ArrayList<Integer> beertypes_pts;

    public DynamicBadges(JSONObject jsonDynamicBadges) throws JSONException {
        this.breweries_ids = new ArrayList<>();
        this.breweries_pts = new ArrayList<>();
        this.cities_ids = new ArrayList<>();
        this.cities_pts = new ArrayList<>();
        this.states_ids = new ArrayList<>();
        this.states_pts = new ArrayList<>();
        this.countries_ids = new ArrayList<>();
        this.countries_pts = new ArrayList<>();
        this.beerstyles_ids = new ArrayList<>();
        this.beerstyles_pts = new ArrayList<>();
        this.beertypes_ids = new ArrayList<>();
        this.beertypes_pts = new ArrayList<>();

        this.beers = jsonDynamicBadges.optInt("beers", 0);
        this.breweries = jsonDynamicBadges.optJSONObject("breweries");
        this.cities = jsonDynamicBadges.optJSONObject("cities");
        this.states = jsonDynamicBadges.optJSONObject("states");
        this.countries = jsonDynamicBadges.optJSONObject("countries");
        this.beerstyles = jsonDynamicBadges.optJSONObject("beerstyles");
        this.beertypes = jsonDynamicBadges.optJSONObject("beertypes");

        if (this.breweries != null) {
            for (int i = 0; i < this.breweries.optJSONArray("id").length(); i++) {
                this.breweries_ids.add(this.breweries.optJSONArray("id").getInt(i));
                this.breweries_pts.add(this.breweries.optJSONArray("pts").getInt(i));
            }
        }
        if (this.cities != null) {
            for (int i = 0; i < this.cities.optJSONArray("id").length(); i++) {
                this.cities_ids.add(this.cities.optJSONArray("id").getInt(i));
                this.cities_pts.add(this.cities.optJSONArray("pts").getInt(i));
            }
        }
        if (this.states != null) {
            for (int i = 0; i < this.states.optJSONArray("id").length(); i++) {
                this.states_ids.add(this.states.optJSONArray("id").getInt(i));
                this.states_pts.add(this.states.optJSONArray("pts").getInt(i));
            }
        }
        if (this.countries != null) {
            for (int i = 0; i < this.countries.optJSONArray("id").length(); i++) {
                this.countries_ids.add(this.countries.optJSONArray("id").getInt(i));
                this.countries_pts.add(this.countries.optJSONArray("pts").getInt(i));
            }
        }
        if (this.beerstyles != null) {
            for (int i = 0; i < this.beerstyles.optJSONArray("id").length(); i++) {
                this.beerstyles_ids.add(this.beerstyles.optJSONArray("id").getInt(i));
                this.beerstyles_pts.add(this.beerstyles.optJSONArray("pts").getInt(i));
            }
        }
        if (this.beertypes != null) {
            for (int i = 0; i < this.beertypes.optJSONArray("id").length(); i++) {
                this.beertypes_ids.add(this.beertypes.optJSONArray("id").getInt(i));
                this.beertypes_pts.add(this.beertypes.optJSONArray("pts").getInt(i));
            }
        }

    }

}
