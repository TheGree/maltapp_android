package com.elixeer.maltapp.Models;

import android.content.Context;
import android.content.SharedPreferences;

import com.elixeer.maltapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class Provider {

    public int id;
    public String name;
    public String description;
    public int city_id;
    public String city_name;
    public String address;
    public double latitude;
    public double longitude;
    public String phone;
    public String email;
    public String facebook;
    public String facebook_id;
    public String twitter;
    public String webpage;
    public String imageUrlThumb;
    public String imageUrlMedium;
    public Boolean active;
    private JSONArray tag;
    public ArrayList<Tag> tags;

    public Provider(JSONObject jsonProvider, Context context) throws JSONException {
        String language = Locale.getDefault().getLanguage();
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
            if (sharedPreferences.contains("language")) {
                language = sharedPreferences.getString("language", Locale.getDefault().getLanguage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String language2 = "en";
        if (language.equals("en")) {
            language2 = "es";
        }

        this.id = jsonProvider.optInt("id", 0);
        this.active = jsonProvider.optBoolean("activate", false);
        this.name = jsonProvider.optString("name", "");
        this.description = jsonProvider.optString("description_" + language, "");
        if (this.description.equals("")) {
            this.description = jsonProvider.optString("description_" + language2, "");
        }
        this.city_id = jsonProvider.optInt("city_id", 0);
        this.city_name = jsonProvider.optString("city_name", "");
        this.address = jsonProvider.optString("address", "");
        this.latitude = jsonProvider.optDouble("latitude", 0);
        this.longitude = jsonProvider.optDouble("longitude", 0);
        this.phone = jsonProvider.optString("phone", "");
        this.email = jsonProvider.optString("public_email", "");
        this.facebook = jsonProvider.optString("facebook", "");
        this.facebook_id = jsonProvider.optString("face_code", "");
        this.twitter = jsonProvider.optString("twitter", "");
        this.webpage = jsonProvider.optString("webpage", "");
        this.imageUrlThumb = jsonProvider.optString("thumb", "");
        this.imageUrlMedium = jsonProvider.optString("medium", "");
        this.tag = jsonProvider.optJSONArray("tags");
        this.tags = Tag.tagsFromJSONArray(this.tag, context);

    }

    public static ArrayList<Provider> providersFromJSONArray(JSONArray jsonProviders, Context context) throws JSONException {
        ArrayList<Provider> providers = new ArrayList<>();
        for (int i = 0; i < jsonProviders.length(); i++) {
            JSONObject jsonProvider = jsonProviders.optJSONObject(i);
            if (jsonProvider != null) {
                providers.add(new Provider(jsonProvider, context));
            }
        }
        return providers;
    }
}