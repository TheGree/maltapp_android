package com.elixeer.maltapp.Models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class PlaceSmall implements Serializable {

    public int id;
    public String name;
    public String address;
    public int stand;
    public int posX;
    public int posY;
    public boolean active;
    public boolean shop;
    public String thumb;

    public PlaceSmall(JSONObject jsonPlace) {
        this.id = jsonPlace.optInt("id", 0);
        this.name = jsonPlace.optString("name", "");
        this.address = jsonPlace.optString("address", "");
        this.shop = jsonPlace.optBoolean("shop", false);
        this.active = jsonPlace.optBoolean("activate", false);
        this.thumb = jsonPlace.optString("thumb", null);
        this.stand = jsonPlace.optInt("stand", -1);
        this.posX = jsonPlace.optInt("posX", -1);
        this.posY = jsonPlace.optInt("posY", -1);
    }

    public static ArrayList<PlaceSmall> placesFromJSONArray(JSONArray jsonPlaces) {
        ArrayList<PlaceSmall> Places = new ArrayList<>();
        for (int i = 0; i < jsonPlaces.length(); i++) {
            JSONObject jsonPlace = jsonPlaces.optJSONObject(i);
            if (jsonPlace != null) {
                Places.add(new PlaceSmall(jsonPlace));
            }
        }
        return Places;
    }
}
