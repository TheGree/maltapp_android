package com.elixeer.maltapp;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Models.Beer;
import com.elixeer.maltapp.Models.Event;
import com.google.zxing.integration.android.IntentIntegrator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HomeFragment extends Fragment {
    MainActivity parent;
    ImageButton scanBeerButtonLayout;
    LinearLayout specialEventMainButton;
    SearchView searchBeerView;
    LinearLayout libraryBeerButtonLayout;
    ImageView specialEventPromoLogo;
    TextView topRankButton;
    ArrayList<Beer> beers;
    int event_id;
    String event_name;

    private static final int CAMERA_REQUEST_CODE = 102;

    public HomeFragment() {
        // Required empty public constructor
    }

    private boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager != null && connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            parent.setTab(0);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                //actionBar.setTitle(R.string.app_name);
                actionBar.setShowHideAnimationEnabled(false);
                actionBar.hide();
            }
        }

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        searchBeerView = rootView.findViewById(R.id.searchBeerView);
        scanBeerButtonLayout = rootView.findViewById(R.id.scanBeerButtonLayout);
        specialEventMainButton = rootView.findViewById(R.id.specialEventMainButton);
        specialEventPromoLogo = rootView.findViewById(R.id.specialEventPromoLogo);
        libraryBeerButtonLayout = rootView.findViewById(R.id.libraryBeerButtonLayout);
        topRankButton = rootView.findViewById(R.id.topRankButton);

        //AutoCompleteTextView search_text = (AutoCompleteTextView) searchBeerView.findViewById(searchBeerView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null));
        //AutoCompleteTextView search_hint = (AutoCompleteTextView) searchBeerView.findViewById(searchBeerView.getContext().getResources().getIdentifier("android:id/search_hint", null, null));
        //search_text.setTextColor(getResources().getColor(R.color.search_color));
        //search_hint.setTextColor(Color.WHITE);
        //int searchSrcTextId = getResources().getIdentifier("android:id/search_src_text", null, null);
        //EditText searchEditText = (EditText) searchBeerView.findViewById(searchSrcTextId);
        //searchEditText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getDimensionPixelSize(R.dimen.home_text_description_size));
        //searchEditText.setTextColor(Color.BLUE); // set the text color
        //searchEditText.setHintTextColor(Color.BLUE); // set the hint color
        //search_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getDimensionPixelSize(R.dimen.home_text_description_size));
        //search_hint.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getDimensionPixelSize(R.dimen.home_text_description_size));

        searchBeerView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String searchParams = query;//.trim();
                if (!searchParams.isEmpty()) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("home", "beer_search", searchParams);

                    Fragment beerSearchFragment = new BeerSearchFragment();
                    Bundle arguments = new Bundle();
                    arguments.putString("QUERY_SEARCH", searchParams);
                    beerSearchFragment.setArguments(arguments);
                    parent.pushFragment(beerSearchFragment);

                    return true;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        scanBeerButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCamera();
            }
        });

        libraryBeerButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parent != null) {
                    if (isOnline()) {
                        Fragment libraryMainFragment = new LibraryMainFragment();
                        parent.pushFragment(libraryMainFragment);
                    } else {
                        notInternetMSG();
                    }
                }
            }
        });

        topRankButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parent != null) {
                    if (isOnline()) {
                        Fragment topFragment = new TopFragment();
                        parent.pushFragment(topFragment);
                    } else {
                        notInternetMSG();
                    }
                }
            }
        });

        specialEventMainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parent != null) {
                    Fragment specialFragment = new SpecialFragment();
                    Bundle arguments = new Bundle();
                    arguments.putInt("SPECIAL_EVENT_ID", event_id);
                    arguments.putString("SPECIAL_EVENT_NAME", event_name);
                    specialFragment.setArguments(arguments);
                    parent.pushFragment(specialFragment);
                }
            }
        });
        getSpecial();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("Home");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_REQUEST_CODE && android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            getCamera();
        }
    }

    private void getCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && android.support.v4.app.ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE);
        } else {
            if (parent != null) {
                try {
                    IntentIntegrator integrator = new IntentIntegrator(parent); //IntentIntegrator.forSupportFragment(t);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                    integrator.setCaptureActivity(ZXingScanner.class);
                    parent.resultHandler = new MainActivity.MainInterface() {
                        @Override
                        public void onResult(int requestCode, int resultCode, Intent intent) {
                            //if(requestCode == 0){
                            if (resultCode == Activity.RESULT_OK) {
                                String contents = intent.getStringExtra("SCAN_RESULT");
                                //String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                                //Integer barCodeBeer = MaltappDataHandler.decryptData(contents);
                                if (TextUtils.isDigitsOnly(contents)) {
                                    getBeer(Long.parseLong(contents));
                                }
                                //Log.i("xZing", "contents: " + contents);

                            } else if (resultCode == Activity.RESULT_CANCELED) { // Handle cancel
                                //Log.i("xZing", "Cancelled");
                            }
                            //}
                        }
                    };
                    integrator.setBeepEnabled(false);
                    integrator.initiateScan();
                } catch (OutOfMemoryError OME){
                    OME.printStackTrace();
                }
            }
        }
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(HomeFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void getSpecial() {
        String url = ApiUrl.SPECIAL_EVENTS;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONObject jsonEvent = jsonResponse.getJSONObject("event");
                            if (jsonEvent.getInt("id") > 0) {
                                Event event = new Event(jsonEvent, getContext());
                                event_id = event.id;
                                event_name = event.title;
                                ImageCache.getImage(event.imageUrlSponsor, getContext(), new ImageCache.OnResponseListener() {
                                    @Override
                                    public void onResponse(Bitmap bitmap, String error) {
                                        specialEventPromoLogo.setImageBitmap(bitmap);
                                    }
                                });
                                specialEventMainButton.setVisibility(View.VISIBLE);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getBeer(Long beerBarCode) {
        String url = ApiUrl.BEERS + "&barcode=" + beerBarCode;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonBeers = jsonResponse.getJSONArray("beers");
                            beers = Beer.beersFromJSONArray(jsonBeers, getContext());
                            if (beers.size() < 1) {
                                alertMsj();
                            } else {
                                openBeer();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void openBeer() {
        if (parent != null) {
            Fragment beerFragment = new BeerFragment();
            Bundle arguments = new Bundle();
            arguments.putSerializable("BEER", beers.get(0));
            arguments.putBoolean("PRELOAD", true);
            beerFragment.setArguments(arguments);
            parent.pushFragment(beerFragment);
        }
    }

    private void alertMsj() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_scann, null);
        Button popScanButton = pop.findViewById(R.id.popScanButton);

        AlertDialog.Builder builder = new AlertDialog.Builder(HomeFragment.this.getActivity())
                //.setTitle(getString(R.string.maltappNotice))
                .setView(pop)
                .setPositiveButton(R.string.cancel, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        popScanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCamera();
                alert.dismiss();
            }
        });
        alert.show();
    }
}