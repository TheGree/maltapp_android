package com.elixeer.maltapp.Models;

import android.content.Context;
import android.content.SharedPreferences;

import com.elixeer.maltapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

public class Taste implements Serializable {
    public int id;
    public String name;

    public Taste(JSONObject jsonTaste, Context context) {
        String language = Locale.getDefault().getLanguage();
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
            if (sharedPreferences.contains("language")) {
                language = sharedPreferences.getString("language", Locale.getDefault().getLanguage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String language2 = "en";
        if (language.equals("en")) {
            language2 = "es";
        }

        this.id = jsonTaste.optInt("id", 0);
        this.name = jsonTaste.optString("name_" + language, "");
        if (this.name.equals("")) {
            this.name = jsonTaste.optString("name_" + language2, "");
        }
    }

    public static ArrayList<Taste> tastesFromJSONArray(JSONArray jsonTastes, Context context) {
        ArrayList<Taste> tastes = new ArrayList<>();
        for (int i = 0; i < jsonTastes.length(); i++) {
            JSONObject jsonTaste = jsonTastes.optJSONObject(i);
            if (jsonTaste != null) {
                tastes.add(new Taste(jsonTaste, context));
            }
        }
        return tastes;
    }
}
