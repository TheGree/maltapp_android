package com.elixeer.maltapp;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Models.Event;
import com.elixeer.maltapp.Models.Score;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class EventDetailsFragment extends Fragment {
    MainActivity parent;
    Event event;
    ArrayList<Score> scores;
    int user_id;

    public EventDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
        if (sharedPreferences.contains("userId")) {
            if (sharedPreferences.getInt("userId", -1) > 0) {
                user_id = sharedPreferences.getInt("userId", -1);
            }
        }

        parent = (MainActivity) getActivity();

        ActionBar actionBar = parent.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.event_detail);
            actionBar.show();
        }

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_event_details, container, false);

        Bundle arguments = getArguments();

        if (arguments != null) {
            event = (Event) arguments.getSerializable("EVENT");

            if (event != null) {
                SimpleDateFormat simpleDateFormatDay = new SimpleDateFormat("dd", Locale.getDefault());
                SimpleDateFormat simpleDateFormatMonth = new SimpleDateFormat("MMMM", Locale.getDefault());
                SimpleDateFormat simpleDateFormatHour12 = new SimpleDateFormat("HH:mm a", Locale.getDefault());

                //ImageView newsImageView = rootView.findViewById(R.id.newsImageView);
                TextView titleTextView = rootView.findViewById(R.id.titleNewTextView);
                TextView largeDescriptionNewTextView = rootView.findViewById(R.id.largeDescriptionNewTextView);
                //TextView shortDescriptionNewTextView = rootView.findViewById(R.id.shortDescriptionNewTextView);
                //TextView typeTextView = rootView.findViewById(R.id.typeNewTextView);
                //TextView dateTextView = rootView.findViewById(R.id.dateNewTextView);
                TextView datailDateEventTextView = rootView.findViewById(R.id.datailDateEventTextView);
                TextView datailHourEventTextView = rootView.findViewById(R.id.datailHourEventTextView);
                Button publicityButton = rootView.findViewById(R.id.publicityButton);

                titleTextView.setText(event.title);
                largeDescriptionNewTextView.setText(event.largeDescription);
                //shortDescriptionNewTextView.setText(event.shortDescription);
                //dateTextView.setText(simpleDateFormat.format(event.startDate));
                //typeTextView.setText(getResources().getText(R.string.events));

                publicityButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            final View pop = inflater.inflate(R.layout.pop_item_detail, null);

                            ImageCache.getImage(event.imageUrlMedium, getContext(), new ImageCache.OnResponseListener() {
                                @Override
                                public void onResponse(Bitmap bitmap, String error) {
                                    ImageView popDetailImageView = pop.findViewById(R.id.popDetailImageView);
                                    popDetailImageView.setImageBitmap(bitmap);
                                }
                            });

                            AlertDialog.Builder builder = new AlertDialog.Builder(EventDetailsFragment.this.getActivity())
                                    .setView(pop)
                                    .setPositiveButton(R.string.close, null);
                            AlertDialog alert = builder.create();
                            alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {

                                }
                            });
                            alert.show();
                    }
                });

                String eventDate = String.format(getString(R.string.event_date_range),
                        simpleDateFormatMonth.format(event.startDate),
                        simpleDateFormatDay.format(event.startDate),
                        simpleDateFormatMonth.format(event.endDate),
                        simpleDateFormatDay.format(event.endDate));

                String eventHour = String.format(getString(R.string.event_starting_time), simpleDateFormatHour12.format(event.startDate));

                datailDateEventTextView.setText(eventDate);
                datailHourEventTextView.setText(eventHour);
            }
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("DetailsNews");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu_share, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_share) {
            // Analytics event
            ((AnalyticsApplication) getActivity().getApplication()).sendEvent("event_details", "shared_pressed", event.title);

            if (user_id > 0) {
                getScores(ApiUrl.FACE_SCORE_ID);
            }

            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_SUBJECT, event.title);
            intent.putExtra(Intent.EXTRA_TEXT, event.title + "\n" + ApiUrl.MALTAPP_NEWS + event.id);
            intent.setType("text/plain");
            startActivity(Intent.createChooser(intent, getString(R.string.share)));
            /*
            ShareLinkContent content = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse(ApiUrl.MALTAPP_NEWS + event.id))
                    .setContentTitle(event.title)
                    .setContentDescription(event.shortDescription)
                    .setImageUrl(Uri.parse(event.imageUrlMedium))
                    .build();
            ShareDialog.show(this, content);
            */
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    @Override
    public int FABImageDrawable() {
        return R.drawable.ic_share_white_24px;
    }

    @Override
    public void onFABClick(View view) {
        // Analytics event
        ((AnalyticsApplication) getActivity().getApplication()).sendEvent("news_details", "shared_pressed", "Facebook");

        if (user_id > 0) {
            getScores(ApiUrl.FACE_SCORE_ID);
        }

        //Intent intent = new Intent(Intent.ACTION_SEND);
        //intent.putExtra(Intent.EXTRA_SUBJECT, event.title);
        //intent.putExtra(Intent.EXTRA_TEXT, event.title + "\nhttp://www.maltapp.mx/news/?id=" + event.id);
        //intent.setType("text/plain");
        //startActivity(Intent.createChooser(intent, getString(R.string.share)));

        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(ApiUrl.MALTAPP_NEWS + event.id))
                .setContentTitle(event.title)
                .setContentDescription(event.shortDescription)
                .setImageUrl(Uri.parse(event.imageUrlMedium))
                .build();
        ShareDialog.show(this, content);
    }

    */

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void getScores(final int badgetype_id) {
        String url = ApiUrl.SCORES + "&user_id=" + user_id + "&badgetype_id=" + badgetype_id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonScores = jsonResponse.getJSONArray("scores");
                            scores = Score.tastesFromJSONArray(jsonScores);
                            if (scores.size() < 1) {
                                createScore(badgetype_id);
                            } else {
                                editScores(badgetype_id);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void createScore(int badgetype_id) {
        String scoresFaceParams = "{";
        scoresFaceParams += "\"user_id\":\"" + user_id + "\"";
        scoresFaceParams += ",\"badgetype_id\":\"" + badgetype_id + "\"";
        scoresFaceParams += ",\"points\":\"" + 1 + "\"";
        scoresFaceParams += "}";
        String[][] headers = {{"Content-Type", "application/json"}};

        String url = ApiUrl.SCORES;
        new AsyncRequestString("POST", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .setData(scoresFaceParams)
                .setHeaders(headers)
                .execute();
    }

    private void editScores(int badgetype_id) {
        String url = ApiUrl.SCORE_UPDATE + "&user_id=" + user_id + "&badgetype_id=" + badgetype_id + "&points=" + 1;
        new AsyncRequestString("PUT", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

}
