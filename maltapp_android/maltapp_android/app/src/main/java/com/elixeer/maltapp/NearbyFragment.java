package com.elixeer.maltapp;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


/**
 * A simple {@link Fragment} subclass.
 */
public class NearbyFragment extends Fragment {
    MainActivity parent;

    public NearbyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            parent.setTab(2);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.title_nearby);
                actionBar.show();
            }
        }
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_nearby, container, false);

        LinearLayout barsButtonRelativeLayout = rootView.findViewById(R.id.barsButtonRelativeLayout);
        barsButtonRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parent != null) {
                    if (isOnline()) {
                        Fragment nearbyMapFragment = new NearbyMapFragment();
                        Bundle arguments = new Bundle();
                        arguments.putInt("NEARBY", 1);
                        nearbyMapFragment.setArguments(arguments);
                        parent.pushFragment(nearbyMapFragment);
                    } else {
                        notInternetMSG();
                    }
                }
            }
        });
/*
        Button beersButton = (Button)rootView.findViewById(R.id.beersButton);
        beersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parent != null) {
                    if (isOnline()) {
                        Fragment nearbyBeerFragment = new NearbyBeerFragment();
                        parent.pushFragment(nearbyBeerFragment);
                    } else {
                        notInternetMSG();
                    }
                }
            }
        });
*/
        LinearLayout storesButtonRelativeLayout = rootView.findViewById(R.id.storesButtonRelativeLayout);
        storesButtonRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parent != null) {
                    if (isOnline()) {
                        Fragment nearbyMapFragment = new NearbyMapFragment();
                        Bundle arguments = new Bundle();
                        arguments.putInt("NEARBY", 3);
                        nearbyMapFragment.setArguments(arguments);
                        parent.pushFragment(nearbyMapFragment);
                    } else {
                        notInternetMSG();
                    }
                }
            }
        });

        LinearLayout eventsButtonRelativeLayout = rootView.findViewById(R.id.eventsButtonRelativeLayout);
        eventsButtonRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parent != null) {
                    if (isOnline()) {
                        Fragment nearbyMapFragment = new NearbyMapFragment();
                        Bundle arguments = new Bundle();
                        arguments.putInt("NEARBY", 4);
                        nearbyMapFragment.setArguments(arguments);
                        parent.pushFragment(nearbyMapFragment);
                    } else {
                        notInternetMSG();
                    }
                }
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("Nearby");
    }

    private boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager != null && connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(NearbyFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

}
