package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.alebrije.async.ImageCache;
import com.alebrije.core.SimpleRecyclerAdapter;
import com.elixeer.maltapp.Models.Event;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment {
    ArrayList<Event> news;
    RecyclerView newsListView;
    MainActivity parent;

    public NewsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            parent.setTab(1);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.title_news);
                actionBar.show();
            }
        }
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_news, container, false);

        newsListView = rootView.findViewById(R.id.newsListView);
        newsListView.setLayoutManager(new LinearLayoutManager(getContext()));
        newsListView.setAdapter(adapter);

        if (isOnline()) {
            getNews();
        } else {
            notInternetMSG();
        }

        return rootView;
    }

    private boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager != null && connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("News");
    }

    private void openNewsDetails(int position) {
        if (parent != null) {
            Fragment newsDetailsFragment = new NewsDetailsFragment();
            Bundle arguments = new Bundle();
            arguments.putSerializable("EVENT", news.get(position));
            newsDetailsFragment.setArguments(arguments);
            parent.pushFragment(newsDetailsFragment);
        }
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(NewsFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void getNews() {
        String url = ApiUrl.EVENTS + "&is_events=false&order=created_at-desc";

        //Log.d(getString(R.string.app_name), "Requested url " + url);
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonNews = jsonResponse.getJSONArray("events");
                            news = Event.eventsFromJSONArray(jsonNews, getContext());
                            if (news.size() < 1) {
                                Log.i(getString(R.string.app_name), "No news found");
                            } else {
                                adapter.setList(news);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    SimpleRecyclerAdapter adapter = new SimpleRecyclerAdapter(R.layout.list_item_news) {
        @Override
        public void onListItem(Object listItem, View view, final int position) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MMM/yyyy", Locale.getDefault());
            Event event = (Event) listItem;

            final ImageView itemImageView = view.findViewById(R.id.itemImageView);
            TextView titleTextView = view.findViewById(R.id.titleNewTextView);
            TextView dateTextView = view.findViewById(R.id.dateNewTextView);

            titleTextView.setText(event.title);
            dateTextView.setText(simpleDateFormat.format(event.startDate));
            if (event.imageUrlThumb.isEmpty() || event.imageUrlThumb.contains("/missing.png")) {
                itemImageView.setImageResource(R.drawable.img_default_news);
            } else {
                itemImageView.setImageBitmap(null);
                ImageCache.getImage(event.imageUrlThumb, getContext(), new ImageCache.OnResponseListener() {
                    @Override
                    public void onResponse(Bitmap bitmap, String error) {
                        itemImageView.setImageBitmap(bitmap);
                    }
                });
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Analytics event
                    ((AnalyticsApplication) getActivity().getApplication()).sendEvent("news", "news_detail", news.get(position).title);

                    openNewsDetails(position);
                }
            });
        }
    };
}