package com.elixeer.maltapp.Comparators;

import com.elixeer.maltapp.Models.Place;

import java.util.Comparator;


public class PlaceComparator implements Comparator<Place> {
    public int compare(Place left, Place right) {
        if (left.distance < right.distance) {
            return -1;
        }
        if (left.distance > right.distance) {
            return 1;
        }
        return 0;
    }

}
