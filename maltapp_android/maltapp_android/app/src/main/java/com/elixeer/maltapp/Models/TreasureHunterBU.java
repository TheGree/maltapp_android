package com.elixeer.maltapp.Models;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class TreasureHunterBU {
    private static String PREFERENCE_KEY = "arrayTreasureHunterStatus";
    private int id;
    private String brewery;
    private String code;
    private String image;
    private boolean taken;
    private boolean active;

    public TreasureHunterBU(JSONObject jsonTreasureHunter, Context context) {
        context.getResources().getIdentifier(jsonTreasureHunter.optString("title", ""), "String", context.getPackageName());
        this.id = jsonTreasureHunter.optInt("id", 0);
        this.brewery = jsonTreasureHunter.optString("brewery", "");
        this.code = jsonTreasureHunter.optString("code", "");
        this.image = jsonTreasureHunter.optString("image", "");
        this.taken = jsonTreasureHunter.optBoolean("taken", false);
        this.active = jsonTreasureHunter.optBoolean("active", false);

        //this.active = jsonTreasureHunter.optBoolean("active", false);
        /*this.name = context.getResources().getText(context.getResources().getIdentifier(jsonTreasureHunter.optString("name", ""), "String", context.getPackageName())).toString();
        this.description = context.getResources().getText(context.getResources().getIdentifier(jsonTreasureHunter.optString("description", ""), "String", context.getPackageName())).toString();
        this.image = context.getResources().getIdentifier(jsonTreasureHunter.optString("image", ""), "drawable", context.getPackageName());
        this.active = jsonTreasureHunter.optBoolean("active", false);*/
    }

    public static ArrayList<TreasureHunterBU> treasureHunterFromJSONArray(JSONArray jsonTreasuresHunter, Context context) {
        ArrayList<TreasureHunterBU> treasuresAll = new ArrayList<>();
        ArrayList<TreasureHunterBU> treasures = new ArrayList<>();
        for (int i = 0; i < jsonTreasuresHunter.length(); i++) {
            JSONObject jsonTreasureHunter = jsonTreasuresHunter.optJSONObject(i);
            if (jsonTreasureHunter != null) {
                treasuresAll.add(new TreasureHunterBU(jsonTreasureHunter, context));
            }
        }
        for (TreasureHunterBU treasure : treasuresAll) {
            if(treasure.active){
                treasures.add(treasure);
            }
        }
        return treasures;
    }

    public int getId() {
        return id;
    }

    public String getBrewery() { return brewery; }

    public String getCode() {
        return code;
    }

    public String getImage() {return image; }

    public boolean isTaken() { return taken; }

    public boolean isActive() { return active; }

    public void setActive(boolean active) { this.active = active; }

}
