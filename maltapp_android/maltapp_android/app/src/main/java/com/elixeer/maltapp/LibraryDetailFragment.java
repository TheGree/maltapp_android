package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alebrije.async.ImageCache;
import com.elixeer.maltapp.Models.Glass;
import com.elixeer.maltapp.Models.Style;

public class LibraryDetailFragment extends Fragment {
    MainActivity parent;
    TextView subtitleLibraryTextView;
    TextView descriptionLibraryTextView;
    boolean direct;
    int position;
    Style style;
    Glass glass;
    String title;
    String description;
    int image;
    ImageView libraryImageView;

    public LibraryDetailFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {

            parent = (MainActivity) getActivity();
            //parent.setTab(0);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.library);
                actionBar.show();
            }
        }

        direct = getArguments().getBoolean("DIRECT", false);
        position = getArguments().getInt("POSITION", -1);

        View rootView = inflater.inflate(R.layout.fragment_library_detail, container, false);

        subtitleLibraryTextView = rootView.findViewById(R.id.subtitleLibraryTextView);
        descriptionLibraryTextView = rootView.findViewById(R.id.descriptionLibraryTextView);
        libraryImageView = rootView.findViewById(R.id.libraryImageView);
        if (direct) {
            switch (position) {
                case 8: {
                    subtitleLibraryTextView.setText(getResources().getString(R.string.brief_history));
                    descriptionLibraryTextView.setText(Html.fromHtml(getResources().getString(R.string.brief_history_info)));
                    libraryImageView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.img_history));
                    break;
                }
                case 9: {
                    subtitleLibraryTextView.setText(getResources().getString(R.string.terms_and_conditions_title));
                    descriptionLibraryTextView.setText(getResources().getString(R.string.terms_and_conditions_tex));
                    break;
                }
            }
        } else {
            title = getArguments().getString("SUBTITLE", "");
            description = getArguments().getString("DESCRIPTION", "");
            image = getArguments().getInt("IMAGE", -1);
            style = (Style) getArguments().get("STYLE");
            glass = (Glass) getArguments().get("GLASS");
            if (style != null) {
                subtitleLibraryTextView.setText(style.name);
                descriptionLibraryTextView.setText(style.description);
                ImageCache.getImage(style.imageUrlMedium, getContext(), new ImageCache.OnResponseListener() {
                    @Override
                    public void onResponse(Bitmap bitmap, String error) {
                        libraryImageView.setImageBitmap(bitmap);
                    }
                });
            } else if (glass != null) {
                subtitleLibraryTextView.setText(glass.name);
                descriptionLibraryTextView.setText(glass.description);
                ImageCache.getImage(glass.imageUrlMedium, getContext(), new ImageCache.OnResponseListener() {
                    @Override
                    public void onResponse(Bitmap bitmap, String error) {
                        libraryImageView.setImageBitmap(bitmap);
                    }
                });
            } else {
                subtitleLibraryTextView.setText(title);
                descriptionLibraryTextView.setText(description);
                if(image != -1){
                    libraryImageView.setImageDrawable(ContextCompat.getDrawable(getContext(), image));
                }
            }
        }

        libraryImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View pop = inflater.inflate(R.layout.pop_item_detail, null);

                ImageView popDetailImageView = pop.findViewById(R.id.popDetailImageView);
                popDetailImageView.setImageDrawable(libraryImageView.getDrawable());

                AlertDialog.Builder builder = new AlertDialog.Builder(LibraryDetailFragment.this.getActivity())
                        .setView(pop)
                        .setPositiveButton(R.string.close, null);
                AlertDialog alert = builder.create();
                alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {

                    }
                });
                alert.show();
            }
        });
        /*
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImage();
            }
        });
        */
        return rootView;
    }

    /*
    void openImage() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.pop_open_image);
        final ImageView imagenPop = (ImageView) dialog.findViewById(R.id.dialogDetailImage);
        if (image != null) {
            ImageCache.getImage(getResources().getDrawable(R.drawable.img_pubs).toString(), new ImageCache.OnResponseListener() {
                @Override
                public void onResponse(Bitmap bitmap) {
                    imagenPop.setImageBitmap(bitmap);
                }
            });
        }
        dialog.show();
    }
    */

    /*
    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("Library" + subtitleLibraryTextView.getText().toString());
    }
    */

}
