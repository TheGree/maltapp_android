package com.elixeer.maltapp.Models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Point implements Serializable {

    public int id;
    public int user_id;
    public int points;
    public int reason;
    public boolean movement;
    public Date date;

    public Point(JSONObject jsonPoint) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

        this.id = jsonPoint.optInt("id", 0);
        this.user_id = jsonPoint.optInt("user_id", 0);
        this.points = jsonPoint.optInt("points", 0);
        this.reason = jsonPoint.optInt("reason", 0);
        this.movement = jsonPoint.optBoolean("movement", false);
        String date = jsonPoint.optString("date", null);
        this.date = simpleDateFormat.parse(date, new ParsePosition(0));

    }

    public static ArrayList<Point> pointsFromJSONArray(JSONArray jsonPoints) {
        ArrayList<Point> points = new ArrayList<>();
        for (int i = 0; i < jsonPoints.length(); i++) {
            JSONObject jsonPoint = jsonPoints.optJSONObject(i);
            if (jsonPoint != null) {
                points.add(new Point(jsonPoint));
            }
        }
        return points;
    }

}
