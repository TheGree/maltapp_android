package com.elixeer.maltapp.Models;

import android.content.Context;
import android.content.SharedPreferences;

import com.elixeer.maltapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

public class Tag implements Serializable {

    public int id;
    public String name;
    public boolean active;
    public boolean type;
    public String city;

    public Tag(int id, String name, String city, boolean type) {
        this.id = id;
        this.name = name;
        this.active = true;
        this.city = city;
        this.type = type;
    }

    public Tag(JSONObject jsonProvider, Context context) {
        String language = Locale.getDefault().getLanguage();
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
            if (sharedPreferences.contains("language")) {
                language = sharedPreferences.getString("language", Locale.getDefault().getLanguage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String language2 = "en";
        if (language.equals("en")) {
            language2 = "es";
        }

        this.id = jsonProvider.optInt("id", 0);
        this.name = jsonProvider.optString("name_" + language, "");
        if (this.name.equals("")) {
            this.name = jsonProvider.optString("name_" + language2, "");
        }
        this.active = jsonProvider.optBoolean("activate", false);

        this.city = "";

        this.type = false;
    }

    public static ArrayList<Tag> tagsFromJSONArray(JSONArray jsonTags, Context context) {
        ArrayList<Tag> tags = new ArrayList<>();
        for (int i = 0; i < jsonTags.length(); i++) {
            JSONObject jsonTag = jsonTags.optJSONObject(i);
            if (jsonTag != null) {
                tags.add(new Tag(jsonTag, context));
            }
        }
        return tags;
    }

}