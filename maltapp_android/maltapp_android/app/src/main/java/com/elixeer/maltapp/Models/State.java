package com.elixeer.maltapp.Models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class State implements Serializable {
    public int id;
    public String name;
    public int countryId;

    public State(JSONObject jsonState) {
        this.id = jsonState.optInt("id", 0);
        this.name = jsonState.optString("name", "");
        this.countryId = jsonState.optInt("country_id", 0);
    }

    public static ArrayList<State> statesFromJSONArray(JSONArray jsonStates) {
        ArrayList<State> states = new ArrayList<>();
        for (int i = 0; i < jsonStates.length(); i++) {
            JSONObject jsonState = jsonStates.optJSONObject(i);
            if (jsonState != null) {
                states.add(new State(jsonState));
            }
        }
        return states;
    }
}
