package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.elixeer.maltapp.Adapters.PromoCustomAdapter;
import com.elixeer.maltapp.Handlers.BillingManager;
import com.elixeer.maltapp.Handlers.PointsManager;
import com.elixeer.maltapp.Models.Promo;
import com.elixeer.maltapp.Models.TotalPoint;
import com.elixeer.maltapp.Models.User;
import com.googlesamples.billing.Purchase;
import com.googlesamples.billing.SkuDetails;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PremiumFragment extends Fragment {
    MainActivity parent;
    private PromoCustomAdapter mAdapter;
    ListView promotionsListView;
    //TextView promotionsPointsTextView;
    // TextView promotionsPointsTextView;
    BillingManager billingManager;
    //ArrayList<Promo> totalPromos;
    ArrayList<Promo> newPromos;
    ArrayList<Promo> myPromos;
    User user;
    TotalPoint totalPoint;
    int user_id;
    int beer_coins;
    String logTag;
    Boolean restoredPurchases = false;
    //public iabHelperDelegate delegate = null;

    public PremiumFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.PremiumAccounttitle);
                actionBar.show();
            }
        }
        user_id = getArguments().getInt("USER_ID");
        beer_coins = getArguments().getInt("BEER_COINS");
        View rootView = inflater.inflate(R.layout.fragment_premium, container, false);
        promotionsListView = rootView.findViewById(R.id.promotionsListView);
        //promotionsPointsTextView = rootView.findViewById(R.id.promotionsPointsTextView);

        //promotionsPointsTextView.setText("hola");
        getNewPromos();

        promotionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parentClick, View view, int position, long id) {
                Promo promo = (Promo) mAdapter.getItem(position);
                if (promo.type == 4) {
                    Fragment pointFragment = new PointFragment();
                    Bundle arguments = new Bundle();
                    arguments.putInt("USER_ID", user_id);
                    arguments.putInt("PTS", beer_coins);
                    arguments.putString("LEVEL", "");
                    arguments.putBoolean("PURCHASED", true);
                    pointFragment.setArguments(arguments);
                    parent.pushFragment(pointFragment);
                } else if (promo.id != 0) {
                    openPromotionsDetail(promo);
                }
                //openPromotionsDetail(position);
            }
        });

        if (user_id > 0) {
                getUser(user_id);
        }

        billingManager = new BillingManager(getActivity()) {
            @Override
            public void onConsume(Purchase purchase) {
                Log.i(logTag, "CONSUMED " + purchase.getSku());
            }

            @Override
            public void onInventory(Purchase purchase) {
                Log.i(logTag, "ON INVENTORY " + purchase.getSku());
               // this.consume(purchase);
                final Purchase test2 = purchase;
                Runnable test = new Runnable() {
                    @Override
                    public void run() {
                        billingManager.consume(test2);
                    }
                };
                if (BillingManager.SKU_FIRSTPACK.equals(purchase.getSku())) {
                    beer_coins = getArguments().getInt("BEER_COINS") + ApiUrl.POINTS_PACKONE;
                    PointsManager.addPoints(user_id, ApiUrl.POINTS_PACKONE, ApiUrl.REASON_BUYBEERCOINS_1, getContext(),test);
                } else if (BillingManager.SKU_SECONDPACK.equals(purchase.getSku())){
                    beer_coins = getArguments().getInt("BEER_COINS") + ApiUrl.POINTS_PACKTWO;
                    PointsManager.addPoints(user_id, ApiUrl.POINTS_PACKTWO, ApiUrl.REASON_BUYBEERCOINS_2, getContext(),test);
                } else if (BillingManager.SKU_THIRDPACK.equals(purchase.getSku())) {
                    beer_coins = getArguments().getInt("BEER_COINS") + ApiUrl.POINTS_PACKTWO;
                    PointsManager.addPoints(user_id, ApiUrl.POINTS_PACKTHREE, ApiUrl.REASON_BUYBEERCOINS_3, getContext(),test);
                }
            }

            @Override
            public void onPurchase(Purchase purchase) {
                Log.i(logTag, "PURCHASED " + purchase.getSku());
                /*
                String orderId = purchase.getOrderId();
                if (orderId != null && orderId.length() > 1) {
                    orderId = orderId.substring(0, orderId.length() - 2);
                } else {
                    // Test purchase
                    orderId = purchase.getToken();
                }
                */
                final Purchase test2 = purchase;
                Runnable test = new Runnable() {
                    @Override
                    public void run() {
                        billingManager.consume(test2);
                    }
                };
                if (BillingManager.SKU_FIRSTPACK.equals(purchase.getSku())) {
                    beer_coins = getArguments().getInt("BEER_COINS") + ApiUrl.POINTS_PACKONE;
                    PointsManager.addPoints(user_id, ApiUrl.POINTS_PACKONE, ApiUrl.REASON_BUYBEERCOINS_1, getContext(),test);
                } else if (BillingManager.SKU_SECONDPACK.equals(purchase.getSku())){
                    beer_coins = getArguments().getInt("BEER_COINS") + ApiUrl.POINTS_PACKTWO;
                    PointsManager.addPoints(user_id, ApiUrl.POINTS_PACKTWO, ApiUrl.REASON_BUYBEERCOINS_2, getContext(),test);
                } else if (BillingManager.SKU_THIRDPACK.equals(purchase.getSku())) {
                    beer_coins = getArguments().getInt("BEER_COINS") + ApiUrl.POINTS_PACKTHREE;
                    PointsManager.addPoints(user_id, ApiUrl.POINTS_PACKTHREE, ApiUrl.REASON_BUYBEERCOINS_3, getContext(),test);
                }
                restoredPurchases = true;
                getNewPromos();
            }
        };


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        /*
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("PointHistory");
        */
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu_info, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_info) {
            if (parent != null) {
                premiumInfo();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void premiumInfo() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_store_maltapp, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(PremiumFragment.this.getActivity())
                //.setTitle(getString(R.string.maltappNotice))
                .setView(pop)
                .setPositiveButton(R.string.ok, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        alert.show();
    }

    void buyBeercoinsPopup() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_buy_beercoins, null);

        TextView buyBeercoinsDescriptionFirstTextView = pop.findViewById(R.id.buyBeercoinsDescriptionFirstTextView);
        TextView buyBeercoinscostFirstTextView = pop.findViewById(R.id.buyBeercoinscostFirstTextView);
        Button shopFirstButton = pop.findViewById(R.id.shopFirstButton);

        TextView buyBeercoinsDescriptionSecondTextView = pop.findViewById(R.id.buyBeercoinsDescriptionSecondTextView);
        TextView buyBeercoinscostSecondTextView = pop.findViewById(R.id.buyBeercoinscostSecondTextView);
        Button shopSecondButton = pop.findViewById(R.id.shopSecondButton);

        TextView buyBeercoinsDescriptionThirdTextView = pop.findViewById(R.id.buyBeercoinsDescriptionThirdTextView);
        TextView buyBeercoinscostThirdTextView = pop.findViewById(R.id.buyBeercoinscostThirdTextView);
        Button shopThirdButton = pop.findViewById(R.id.shopThirdButton);

        Button RestorePurchaseButton = pop.findViewById(R.id.RestorePurchaseButton);

        SkuDetails skufirstPrice = billingManager.getProductBySku(BillingManager.SKU_FIRSTPACK);
        SkuDetails skusecondPrice = billingManager.getProductBySku(BillingManager.SKU_SECONDPACK);
        SkuDetails skuthirdPrice = billingManager.getProductBySku(BillingManager.SKU_THIRDPACK);

        if (skufirstPrice == null) {
            buyBeercoinsDescriptionFirstTextView.setText(R.string.unavailable_product);
            buyBeercoinscostFirstTextView.setText("");
        } else {
            buyBeercoinsDescriptionFirstTextView.setText(skufirstPrice.getTitle());
            buyBeercoinscostFirstTextView.setText(String.format(getString(R.string.btn_buy), skufirstPrice.getPrice()));
        }
        if (skusecondPrice == null) {
            buyBeercoinsDescriptionSecondTextView.setText(R.string.unavailable_product);
            buyBeercoinscostSecondTextView.setText("");
        } else {
            buyBeercoinsDescriptionSecondTextView.setText(skusecondPrice.getTitle());
            buyBeercoinscostSecondTextView.setText(String.format(getString(R.string.btn_buy), skusecondPrice.getPrice()));
        }
        if (skuthirdPrice == null) {
            buyBeercoinsDescriptionThirdTextView.setText(R.string.unavailable_product);
            buyBeercoinscostThirdTextView.setText("");
        } else {
            buyBeercoinsDescriptionThirdTextView.setText(skuthirdPrice.getTitle());
            buyBeercoinscostThirdTextView.setText(String.format(getString(R.string.btn_buy), skuthirdPrice.getPrice()));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(PremiumFragment.this.getActivity())
                //.setTitle(getString(R.string.maltappNotice))
                .setView(pop)
                .setPositiveButton(R.string.cancel, null);
        final AlertDialog alert = builder.create();

        shopFirstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                billingManager.purchase(BillingManager.SKU_FIRSTPACK);
                //alert.dismiss();
            }
        });
        shopSecondButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                billingManager.purchase(BillingManager.SKU_SECONDPACK);
                //alert.dismiss();
            }
        });
        shopThirdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                billingManager.purchase(BillingManager.SKU_THIRDPACK);
                //alert.dismiss();
            }
        });
        RestorePurchaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO: restore purchase
                if(restoredPurchases){
                    showAlertMsj(getString(R.string.premium_complete_restore_poup));
                }
                else{
                    showAlertMsj(getString(R.string.premium_imcompossible_restore_poup));
                }
            }
        });

        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        alert.show();
    }

    private void getUser(int userId) {
        String url = ApiUrl.GET_USER + userId + ".json" + ApiUrl.ACCESS_TOKEN;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONObject jsonUser = jsonResponse.getJSONObject("user");
                            user = new User(jsonUser);
                            getPoints();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }


    private void getPoints() {
        String url = ApiUrl.POINTS + "&user_id=" + user.id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONObject jsonPoint = jsonResponse.getJSONObject("points");
                            totalPoint = new TotalPoint(jsonPoint);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void openPromotionsDetail(Promo promo) {
        if (parent != null) {
            Fragment promotionsDetailsFragment = new PromotionsDetailsFragment();
            Bundle arguments = new Bundle();
            arguments.putSerializable("PROMOS", promo);
            arguments.putInt("USER_ID", user_id);
            arguments.putInt("BEER_COINS", beer_coins);
            promotionsDetailsFragment.setArguments(arguments);
            parent.pushFragment(promotionsDetailsFragment);
        }
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(PremiumFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }


    private void showAlertMsj (String msj) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_msj, null);
        TextView popMsjTextView = pop.findViewById(R.id.popMsjTextView);
        popMsjTextView.setText(msj);
        AlertDialog.Builder builder = new AlertDialog.Builder(PremiumFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });
        alert.show();
    }
    private void getNewPromos() {
        String url = ApiUrl.NEW_PROMOS + "&user_id=" + user_id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonPromos = jsonResponse.getJSONArray("promos");
                            newPromos = Promo.promosFromJSONArray(jsonPromos, getContext());
                            getMyPromos();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void getMyPromos() {
        String url = ApiUrl.MY_PROMOS + "&user_id=" + user_id;

        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        //Log.d(getString(R.string.app_name), "Response " + jsonResponse.toString(1));
                        if (jsonResponse.has("error")) {
                            reportError(jsonResponse.getString("error"));
                        } else {
                            JSONArray jsonPromos = jsonResponse.getJSONArray("promos");
                            myPromos = Promo.promosFromJSONArray(jsonPromos, getContext());
                            listPromos();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void listPromos() {
        /*
        totalPromos = new ArrayList<Promo>();
        if (newPromos.size() < 1) {
            Log.i(getString(R.string.app_name), "No new promos found");
        } else {
            totalPromos = newPromos;
        }
        if (myPromos.size() < 1) {
            Log.i(getString(R.string.app_name), "No my promos found");
        } else {
            totalPromos.addAll(myPromos);
        }
        if (totalPromos.size() < 1) {
            Log.i(getString(R.string.app_name), "No promos found");
        } else {
        */

        mAdapter = new PromoCustomAdapter(getContext()){
            @Override
            public View getView(int position, View convertView, final ViewGroup parent){
                View view = super.getView(position, convertView, parent);
                ViewHolder holder = (ViewHolder) view.getTag();
                if(holder != null){
                    if(holder.historyButton != null){
                        holder.historyButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Fragment pointFragment = new PointFragment();
                                Bundle arguments = new Bundle();
                                arguments.putInt("USER_ID", user_id);
                                arguments.putInt("PTS", beer_coins);
                                arguments.putString("LEVEL", "");
                                arguments.putBoolean("PURCHASED", true);
                                pointFragment.setArguments(arguments);
                                PremiumFragment.this.parent.pushFragment(pointFragment);
                            }
                        });
                    }

                    if(holder.shopButton != null){

                        holder.shopButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                buyBeercoinsPopup();
                                /*Fragment webViewFragment = new WebViewFragment();
                                Bundle arguments = new Bundle();
                                arguments.putInt("NETWORK", 1);
                                webViewFragment.setArguments(arguments);
                                PremiumFragment.this.parent.pushFragment(webViewFragment);*/

                            }
                        });
                    }
                }
                return view;
            }
        };
        mAdapter.addItem(String.valueOf(beer_coins), 4);
        mAdapter.addItem(getString(R.string.promotions), 0);
        for (int i = 0; i < newPromos.size(); i++) {
            newPromos.get(i).type = 2;
            mAdapter.addItem(newPromos.get(i));
        }
        mAdapter.addItem(getString(R.string.purchased), 1);
        for (int i = 0; i < myPromos.size(); i++) {
            myPromos.get(i).type = 3;
            mAdapter.addItem(myPromos.get(i));
        }
        promotionsListView.setAdapter(mAdapter);
            /*
            promotionsListView.setAdapter(new SimpleListAdapter(R.layout.list_item_promo_new, totalPromos) {
                @Override
                public void onListItem(Object listItem, View view, int position) {
                    Promo promo = (Promo) listItem;

                    final ImageView promoNewListImageView = (ImageView) view.findViewById(R.id.promoNewListImageView);
                    TextView promoNewPointsTextView = (TextView) view.findViewById(R.id.promoNewPointsTextView);
                    TextView promoNewTitleTextView = (TextView) view.findViewById(R.id.promoNewTitleTextView);

                    promoNewPointsTextView.setText(String.valueOf(promo.points));
                    promoNewTitleTextView.setText(promo.name);

                    if (promo.imageUrlThumb.contains("/missing.png")) {
                        promoNewListImageView.setImageResource(R.drawable.ic_no_photo);
                    } else {
                        promoNewListImageView.setImageBitmap(null);
                        ImageCache.getImage(promo.imageUrlThumb, new ImageCache.OnResponseListener() {
                            @Override
                            public void onResponse(Bitmap bitmap) {
                                promoNewListImageView.setImageBitmap(bitmap);
                            }
                        });
                    }
                }
            });
            */
        //}
    }
}