package com.elixeer.maltapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alebrije.async.AsyncRequestString;
import com.elixeer.maltapp.Models.User;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class LoginFragment extends Fragment {
    MainActivity parent;
    private CallbackManager callbackManager;
    private String appName;
    private String facebookId;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        appName = getString(R.string.app_name);
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();
            //parent.setTab(3);

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.title_login);
                actionBar.show();
            }
        }

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        final EditText emailEditText = rootView.findViewById(R.id.emailEditText);
        final EditText passwordEditText = rootView.findViewById(R.id.passwordEditText);

        Button submitButton = rootView.findViewById(R.id.submitButton);
        Button forgotPasswordButton = rootView.findViewById(R.id.forgotPasswordButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(emailEditText.getText().toString().equals("") || passwordEditText.getText().toString().equals("")) {
                    showAlertMsj(getString(R.string.loginAllParams));
                } else {
                    if(isEmailValid(emailEditText.getText().toString())) {
                        login(emailEditText.getText().toString(), passwordEditText.getText().toString());
                    } else {
                        showAlertMsj(getString(R.string.loginValidEmail));
                    }
                }
            }
        });

        forgotPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parent != null) {
                    parent.pushFragment(new RecoverPasswordFragment());
                }
            }
        });

        callbackManager = CallbackManager.Factory.create();

        LoginButton loginButton = rootView.findViewById(R.id.facebook_login_button);
        loginButton.setReadPermissions("email");
        loginButton.setFragment(this);

        //LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                facebookId = loginResult.getAccessToken().getUserId();
                loginWithFacebook(facebookId, loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.e(appName, "Facebook cancel");
            }

            @Override
            public void onError(FacebookException error) {
                reportError(error.toString());
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Analytics screen
        ((AnalyticsApplication) getActivity().getApplication()).setScreenName("Login");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void reportError(String error) {
        Log.e(getString(R.string.app_name), "ERROR: " + error);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error)
                .setMessage(error)
                .setPositiveButton(R.string.ok, null);
        builder.create().show();
    }

    private void notInternetMSG() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_alert, null);
        //TextView popAlertTextView = (TextView) pop.findViewById(R.id.popAlertTextView);
        //popAlertTextView.setText(getString(R.string.no_internet_message));

        //View custom_alert_textView = inflater.inflate(R.layout.custom_title_alert, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(LoginFragment.this.getActivity())
                //.setTitle(getString(R.string.no_internet_title))
                //.setCustomTitle(custom_alert_textView)
                .setView(pop)
                .setPositiveButton(R.string.ok, null);

        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
        alert.show();
    }

    private void registerWithFacebook(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(final JSONObject jsonObject, GraphResponse response) {
                if (response.getError() != null) {
                    Log.e(appName, response.getError().toString());
                } else {
                    try {
                        Log.d("app jsonResult", jsonObject.toString(1));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String email = jsonObject.optString("email");
                    //String profilePicture = "http://graph.facebook.com/" + facebookId + "/picture?type=large";

                    String url = ApiUrl.USERS + "&email=" + email;
                    new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
                        @Override
                        public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                            if (error == null) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                if (jsonResponse.has("error")) {
                                    reportError(jsonResponse.getString("error"));
                                } else {
                                    JSONArray users = jsonResponse.getJSONArray("users");
                                    if (users.length() > 0)  {
                                        // Update user
                                        int id = users.getJSONObject(0).getInt("id");
                                        String url = ApiUrl.GET_USER + id + ".json" + ApiUrl.ACCESS_TOKEN;
                                        String[][] headers = {{"Content-Type", "application/json"}};
                                        new AsyncRequestString("PUT", url, getActivity(), new AsyncRequestString.OnResponseListener() {
                                            @Override
                                            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                                                if (error == null) {
                                                    try {
                                                        JSONObject jsonResponse = new JSONObject(response);
                                                        //Log.d(appName, "Response " + jsonResponse.toString(1));
                                                        if (jsonResponse.has("error")) {
                                                            reportError(jsonResponse.getString("error"));
                                                        } else {
                                                            JSONObject jsonUser = jsonResponse.getJSONObject("user");
                                                            User user = new User(jsonUser);
                                                            SharedPreferences.Editor editor = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE).edit();
                                                            editor.putInt("userId", user.id);
                                                            editor.putBoolean("premium",false);
                                                            editor.apply();
                                                            getFragmentManager().popBackStack();
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                } else if(error.contains("No connection")) {
                                                    notInternetMSG();
                                                } else {
                                                    reportError(error);
                                                }
                                            }
                                        })
                                                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                                                .setData("{\"facebook_id\": \"" + facebookId + "\"}")
                                                .setHeaders(headers)
                                                .execute();
                                    } else {
                                        reportError(getString(R.string.invalidLogin));
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            } else if(error.contains("No connection")) {
                                notInternetMSG();
                            } else {
                                reportError(error);
                            }
                        }
                    })
                            .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                            .execute();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email, gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void loginWithFacebook(String facebookId, final AccessToken accessToken) {
        Log.i(appName, facebookId);

        String url = ApiUrl.USER_LOGIN + "&facebook_id=" + facebookId;
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    if (jsonResponse.has("error")) {
                        reportError(jsonResponse.getString("error"));
                    } else {
                        int id = jsonResponse.optInt("id", -1);
                        boolean premium = jsonResponse.optBoolean("premium", false);
                        if (id > 0) {
                            SharedPreferences.Editor editor = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE).edit();
                            editor.putInt("userId", id);
                            editor.putBoolean("premium", premium);
                            editor.apply();
                            if (parent != null) {
                                int count = getFragmentManager().getBackStackEntryCount() - 2;
                                int count_id = getFragmentManager().getBackStackEntryAt(count).getId();
                                getFragmentManager().popBackStack(count_id, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                parent.pushFragment(new ProfileFragment());
                            }
                        } else {
                            registerWithFacebook(accessToken);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    private void login(String email, String password) {
        String url = ApiUrl.USER_LOGIN + "&email=" + email + "&password=" + password;
        new AsyncRequestString("GET", url, getActivity(), new AsyncRequestString.OnResponseListener() {
            @Override
            public void onResponse(int responseCode, Map<String, List<String>> headerFields, String response, String error) {
                if (error == null) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    if (jsonResponse.has("error")) {
                        showAlertMsj(getString(R.string.invalidLogin));
                    } else {
                        int id = jsonResponse.optInt("id", -1);
                        boolean premium = jsonResponse.optBoolean("premium", false);
                        if (id > 0) {
                            SharedPreferences.Editor editor = getContext().getSharedPreferences(getContext().getString(R.string.app_name), Context.MODE_PRIVATE).edit();
                            editor.putInt("userId", id);
                            editor.putBoolean("premium", premium);
                            editor.apply();
                            if (parent != null) {
                                int count = getFragmentManager().getBackStackEntryCount() - 2;
                                int count_id = getFragmentManager().getBackStackEntryAt(count).getId();
                                getFragmentManager().popBackStack(count_id, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                parent.pushFragment(new ProfileFragment());
                                //TODO: PREMIUM
                                    /*
                                    if(premium) {
                                        parent.tabLayout.getTabAt(4).setIcon(getResources().getDrawable(R.drawable.ic_tab_premium));
                                    } else{
                                        parent.tabLayout.getTabAt(4).setIcon(getResources().getDrawable(R.drawable.ic_tab_profile));
                                    }
                                    */
                            }
                        } else {
                            showAlertMsj(getString(R.string.invalidLogin));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                } else if(error.contains("No connection")) {
                    notInternetMSG();
                } else {
                    reportError(error);
                }
            }
        })
                .setContentLoadingProgressBar(((MainActivity) getActivity()).progressBar)
                .execute();
    }

    public boolean isEmailValid(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
        return pattern.matcher(email).matches();
    }

    private void showAlertMsj (String msj) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View pop = inflater.inflate(R.layout.pop_item_msj, null);
        TextView popMsjTextView = pop.findViewById(R.id.popMsjTextView);
        popMsjTextView.setText(msj);
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginFragment.this.getActivity())
                .setView(pop)
                .setPositiveButton(R.string.ok, null);
        final AlertDialog alert = builder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });
        alert.show();
    }
}