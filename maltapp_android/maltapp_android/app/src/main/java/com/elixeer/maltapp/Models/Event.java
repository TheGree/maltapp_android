package com.elixeer.maltapp.Models;

import android.content.Context;
import android.content.SharedPreferences;

import com.elixeer.maltapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Event implements Serializable {
    public int id;
    public String title;
    public String shortDescription;
    public String largeDescription;
    public String address;
    public String website;
    public String ticket;
    public Date startDate;
    public Date endDate;
    public double latitude;
    public double longitude;
    public double latitude2;
    public double longitude2;
    public double latitude3;
    public double longitude3;
    public double distance;
    public boolean isEvent;
    public boolean special;
    public boolean active;
    public String imageUrlThumb;
    public String imageUrlMedium;
    public String imageUrlMap;
    public String imageUrlSponsor;

    public Event(JSONObject jsonEvent, Context context) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        String language = Locale.getDefault().getLanguage();
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
            if (sharedPreferences.contains("language")) {
                language = sharedPreferences.getString("language", Locale.getDefault().getLanguage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String language2 = "en";
        if (language.equals("en")) {
            language2 = "es";
        }

        this.id = jsonEvent.optInt("id", 0);
        this.title = jsonEvent.optString("title_" + language, "");
        if (this.title.equals("")) {
            this.title = jsonEvent.optString("title_" + language2, "");
        }
        this.shortDescription = jsonEvent.optString("short_description_" + language, "");
        if (this.shortDescription.equals("")) {
            this.shortDescription = jsonEvent.optString("short_description_" + language2, "");
        }
        this.largeDescription = jsonEvent.optString("large_description_" + language, "");
        if (this.largeDescription.equals("")) {
            this.largeDescription = jsonEvent.optString("large_description_" + language2, "");
        }
        this.address = jsonEvent.optString("address", "");
        this.website = jsonEvent.optString("website", "");
        this.ticket = jsonEvent.optString("ticket", "");
        String startDate = jsonEvent.optString("start", null);
        this.startDate = simpleDateFormat.parse(startDate, new ParsePosition(0));
        String endDate = jsonEvent.optString("finish", null);
        this.endDate = simpleDateFormat.parse(endDate, new ParsePosition(0));
        this.latitude = jsonEvent.optDouble("latitude", 0);
        this.longitude = jsonEvent.optDouble("longitude", 0);
        this.latitude2 = jsonEvent.optDouble("latitude2", 0);
        this.longitude2 = jsonEvent.optDouble("longitude2", 0);
        this.latitude3 = jsonEvent.optDouble("latitude3", 0);
        this.longitude3 = jsonEvent.optDouble("longitude3", 0);
        this.isEvent = jsonEvent.optBoolean("is_events", false);
        this.special = jsonEvent.optBoolean("special", false);
        this.active = jsonEvent.optBoolean("active", false);
        this.imageUrlThumb = jsonEvent.optString("thumb", null);
        this.imageUrlMedium = jsonEvent.optString("medium", null);
        this.imageUrlMap = jsonEvent.optString("map", null);
        this.imageUrlSponsor = jsonEvent.optString("sponsor_" + language, "");
        if (this.imageUrlSponsor.equals("")) {
            this.imageUrlSponsor = jsonEvent.optString("sponsor_" + language2, "");
        }
    }

    public static ArrayList<Event> eventsFromJSONArray(JSONArray jsonEvents, Context context) {
        ArrayList<Event> events = new ArrayList<>();
        for (int i = 0; i < jsonEvents.length(); i++) {
            JSONObject jsonEvent = jsonEvents.optJSONObject(i);
            if (jsonEvent != null) {
                events.add(new Event(jsonEvent, context));
            }
        }
        return events;
    }
}
