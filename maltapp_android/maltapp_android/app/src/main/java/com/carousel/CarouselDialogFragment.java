package com.carousel;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.elixeer.maltapp.MainActivity;
import com.elixeer.maltapp.R;
import com.matthewtamlin.sliding_intro_screen_library.DotIndicator;

import java.util.ArrayList;

public class CarouselDialogFragment extends Fragment {
    static MainActivity parent;
    Typeface type;
    final String welcomeScreenShownPref = "carouselShown";

    public CarouselDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() instanceof MainActivity) {
            parent = (MainActivity) getActivity();

            ActionBar actionBar = parent.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setElevation(0);
                actionBar.hide();
            }
        }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_carousel_dialog, container, false);

        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(CarouselPageFragment.newInstance(R.drawable.descubre));
        fragments.add(CarouselPageFragment.newInstance(R.drawable.califica));
        fragments.add(CarouselPageFragment.newInstance(R.drawable.informate));
        FragmentListPagerAdapter pagerAdapter = new FragmentListPagerAdapter(getChildFragmentManager(), fragments);
        ViewPager pager = view.findViewById(R.id.carouselViewPager);
        pager.setAdapter(pagerAdapter);
        pager.setCurrentItem(0);

        final DotIndicator indicator = view.findViewById(R.id.carouselViewPagerIndicator);
        indicator.setNumberOfItems(fragments.size());
        indicator.setSelectedItem(pager.getCurrentItem(), false);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                indicator.setSelectedItem(position, true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        LinearLayout InfoDismissButton = view.findViewById(R.id.carouselDismissButton);

        InfoDismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = mPrefs.edit();
                editor.putBoolean(welcomeScreenShownPref, true);
                editor.apply();
                if (parent != null) {
                    parent.setApplication();
                }
            }
        });
        return view;
    }
}
