package com.carousel;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.elixeer.maltapp.R;

public class CarouselPageFragment extends Fragment {

    public CarouselPageFragment() {
        // Required empty public constructor
    }

    public static CarouselPageFragment newInstance(int icon) {
        Bundle bundle = new Bundle();
        bundle.putInt("IMAGE", icon);
        CarouselPageFragment fragment = new CarouselPageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle arguments = getArguments();

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_carousel_page, container, false);

        if (arguments != null) {
            ImageView carouselPageImage = rootView.findViewById(R.id.carouselPageImage);
            Drawable image = getResources().getDrawable(arguments.getInt("IMAGE"));
            carouselPageImage.setImageDrawable(image);
        }

        return rootView;
    }

}
